
function onlyNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
    
function ajax(url,data,done,timeout){
    $.ajax({
        crossDomain: true,
        url: base_url+url,
        context : document.body,
        dataType: "json",
        type: "POST",
        data: data,
        timeout: timeout,
        beforeSend: function (){
            $(".preloader").fadeIn();
        }
    }).done(done).fail(function(jqXHR,status,msg){
        $("body").removeClass("loading");
        alert("Error conexion: "+status+" - "+msg);
    });
}


$(document).ajaxStop(function(){
    $(".preloader").fadeOut();
});

//PERMITE LETRAS Y CARACTERES ESPECIALES
//UTILIZAR EN EVENTO ONKEYPRESS
function onlyText(e){
    key = e.keyCode || e.which;
    tecla = String.fromCharCode(key).toLowerCase();
    letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
    especiales = "8-37-39-46";
    tecla_especial = false
    for(var i in especiales){
         if(key == especiales[i]){
             tecla_especial = true;
             break;
         }
     }
     if(letras.indexOf(tecla)==-1 && !tecla_especial){
         return false;
     }
}

//RETORNA EL TRUE SI EL CORREO ES VÁLIDO, FALSE EN CASO CONTRARIO
function validarCorreo(correo){
    var regcorreo = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if(!regcorreo.test(correo))return false;
    return true;
}