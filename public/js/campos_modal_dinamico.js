select_item_add = {
    transportadores: [{
        principal: true,
        principal_select: '#principal [name=id_transportador]',
        save_principal_modal: [{
            url_name: 'transportadores',
            form_add: 'conductores',
            position: 2
        }],
        modal_add_item_selects: false
    }],
    conductores: [{
        principal: true,
        principal_select: '#principal [name=id_conductor]',
        save_principal_modal: [{
            url_name: 'conductores',
            form_add: 'vehiculos',
            position: 1
        }],
        modal_add_item_selects: {
            transportadores: [{
                modal_select: '.modal form [name=id_transportador]',
                forms_add: [{
                    form_add: 'conductores',
                    position: 2
                }],
                principal_select: '#principal [name=id_transportador]'
            }]
        }
    }],
    vehiculos: [{
        principal: true,
        principal_select: '#principal [name=placa_vehiculo]',
        save_principal_modal: false,
        modal_add_item_selects: {
            conductores: [{
                modal_select: '.modal form select.id_conductor',
                forms_add: [{
                    form_add: 'vehiculos',
                    position: 1
                }],
                principal_select: '#principal [name=id_conductor]'
            }]
        }
    }],
    clientes: [{
        principal: true,
        principal_select: '#principal [name=id_cliente]',
        save_principal_modal: false,
        modal_add_item_selects: false
    }],
    navieras: [{
        principal: true,
        principal_select: '#principal [name=codigo_naviera]',
        save_principal_modal: [{
                url_name: 'navieras',
                form_add: 'codigoiso',
                position: 2
            },
            {
                url_name: 'navieras',
                form_add: 'clasificaciones',
                position: 1
            }
        ],
        modal_add_item_selects: false
    }],
    clasificaciones: [{
        principal: true,
        principal_select: '#principal [name=id_clasificacion]',
        save_principal_modal: false,
        modal_add_item_selects: {
            navieras: [{
                modal_select: '.modal form select.id_naviera',
                forms_add: [{
                        form_add: 'clasificaciones',
                        position: 1
                    },
                    {
                        form_add: 'codigoiso',
                        position: 2
                    }
                ],
                principal_select: '#principal [name=id_naviera]'
            }]
        }
    }],
    codigoiso: [{
        principal: true,
        principal_select: '#principal [name=id_tipo]',
        save_principal_modal: [{
            url_name: 'codigoiso',
            form_add: 'contenedores',
            position: 0
        }],
        modal_add_item_selects: {
            navieras: [{
                modal_select: '.modal form select.id_naviera',
                forms_add: [{
                        form_add: 'clasificaciones',
                        position: 1
                    },
                    {
                        form_add: 'codigoiso',
                        position: 2
                    }
                ],
                principal_select: '#principal [name=id_naviera]'
            }]
        }
    }],
    contenedores: [{
        principal: false,
        principal_select: false,
        save_principal_modal: false,
        modal_add_item_selects: {
            navieras: [{
                modal_select: '.modal form [name=id_naviera]',
                forms_add: [{
                        form_add: 'clasificaciones',
                        position: 1
                    },
                    {
                        form_add: 'codigoiso',
                        position: 2
                    }
                ],
                principal_select: '#principal [name=id_naviera]'
            }],
            codigoiso: [{
                modal_select: '.modal form [name=codigo_iso]',
                forms_add: [{
                    form_add: 'contenedores',
                    position: 0
                }],
                principal_select: '#principal [name=id_tipo]'
            }]
        }
    }]
};