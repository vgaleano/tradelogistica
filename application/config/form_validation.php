<?php
$config = [
    'usuarios' => [
        [
            'field' => 'nombre',
            'label' => 'Nombre',
            'rules' => 'required',
            'errors' => [
                'required' => 'El campo {field} es requerido.'
            ]
        ],
        [
            'field' => 'email',
            'label' => 'Correo electrónico',
            'rules' => 'required|valid_email',
            'errors' => [
                'required' => 'El campo {field} es requerido.',
                'valid_email' => 'El formato del {field} es inválido.'
            ]
        ],
        [
            'field' => 'password',
            'label' => 'Contraseña',
            'rules' => 'required',
            'errors' => [
                'required' => 'El campo {field} es requerido.'
            ]
        ],
        [
            'field' => 'id_perfil',
            'label' => 'Perfil',
            'rules' => 'required',
            'errors' => [
                'required' => 'El campo {field} es requerido.'
            ]
        ],
        [
            'field' => 'status_terminos',
            'label' => 'Perfil',
            'rules' => 'required',
            'errors' => [
                'required' => 'El campo {field} es requerido.'
            ]
        ]
    ],
    'clientes' => [
        [
            'field' => 'cliente',
            'label' => 'Nombre',
            'rules' => 'required',
            'errors' => [
                'required' => 'El campo {field} es requerido.'
            ]
        ]
    ],
    'clasificaciones' => [
        [
            'field' => 'clasificacion',
            'label' => 'Nombre',
            'rules' => 'required',
            'errors' => [
                'required' => 'El campo {field} es requerido.'
            ]
        ]
    ],
    'transportadores' => [
        [
            'field' => 'transportador',
            'label' => 'Nombre',
            'rules' => 'required',
            'errors' => [
                'required' => 'El campo {field} es requerido.'
            ]
        ]
    ],
    'conductores' => [
        [
            'field' => 'conductor',
            'label' => 'Nombre',
            'rules' => 'required',
            'errors' => [
                'required' => 'El campo {field} es requerido.'
            ]
        ],
        [
            'field' => 'numero_documento',
            'label' => 'Número de documento',
            'rules' => 'required',
            'errors' => [
                'required' => 'El campo {field} es requerido.'
            ]
        ]
    ],
    'vehiculos' => [
        [
            'field' => 'placa',
            'label' => 'Placa',
            'rules' => 'required',
            'errors' => [
                'required' => 'El campo {field} es requerido.'
            ]
        ]
    ],
    'navieras' => [
        [
            'field' => 'naviera',
            'label' => 'Nombre',
            'rules' => 'required',
            'errors' => [
                'required' => 'El campo {field} es requerido.'
            ]
        ]
    ],
    'codigos_iso' => [
        [
            'field' => 'codigo',
            'label' => 'Código ISO',
            'rules' => 'required',
            'errors' => [
                'required' => 'El campo {field} es requerido.'
            ]
        ],
        [
            'field' => 'dimensiones',
            'label' => 'Dimensione',
            'rules' => 'required',
            'errors' => [
                'required' => 'El campo {field} es requerido.'
            ]
        ]
    ],
    'contenedores' => [
        [
            'field' => 'codigo',
            'label' => 'Número del contenedor',
            'rules' => 'required',
            'errors' => [
                'required' => 'El campo {field} es requerido.'
            ]
        ],
        [
            'field' => 'codigo_iso',
            'label' => 'Código ISO',
            'rules' => 'required',
            'errors' => [
                'required' => 'El campo {field} es requerido.'
            ]
        ]/*,
        [
            'field' => 'id_naviera',
            'label' => 'Naviera',
            'rules' => 'required',
            'errors' => [
                'required' => 'El campo {field} es requerido.'
            ]
        ]*/
    ],
    'codigos_damage' => [
        [
            'field' => 'codigo',
            'label' => 'Código de daño',
            'rules' => 'required',
            'errors' => [
                'required' => 'El campo {field} es requerido.'
            ]
        ],
        [
            'field' => 'descripcion',
            'label' => 'Descripción',
            'rules' => 'required',
            'errors' => [
                'required' => 'El campo {field} es requerido.'
            ]
        ],
        [
            'field' => 'traduccion',
            'label' => 'Traducción',
            'rules' => 'required',
            'errors' => [
                'required' => 'El campo {field} es requerido.'
            ]
        ]
    ],
    'codigos_reparacion' => [
        [
            'field' => 'codigo',
            'label' => 'Código de reparación',
            'rules' => 'required',
            'errors' => [
                'required' => 'El campo {field} es requerido.'
            ]
        ],
        [
            'field' => 'descripcion',
            'label' => 'Descripción',
            'rules' => 'required',
            'errors' => [
                'required' => 'El campo {field} es requerido.'
            ]
        ],
        [
            'field' => 'traduccion',
            'label' => 'Traducción',
            'rules' => 'required',
            'errors' => [
                'required' => 'El campo {field} es requerido.'
            ]
        ]
    ],
    'componentes' => [
        [
            'field' => 'nombre',
            'label' => 'Nombre',
            'rules' => 'required',
            'errors' => [
                'required' => 'El campo {field} es requerido.'
            ]
        ],
        [
            'field' => 'traduccion',
            'label' => 'Traducción',
            'rules' => 'required',
            'errors' => [
                'required' => 'El campo {field} es requerido.'
            ]
        ],
        [
            'field' => 'codigo',
            'label' => 'Código del componente',
            'rules' => 'required',
            'errors' => [
                'required' => 'El campo {field} es requerido.'
            ]
        ],
        [
            'field' => 'codigo_localizacion',
            'label' => 'Código de localización',
            'rules' => 'required',
            'errors' => [
                'required' => 'El campo {field} es requerido.'
            ]
        ],
        [
            'field' => 'id_assembly',
            'label' => 'Assembly',
            'rules' => 'required',
            'errors' => [
                'required' => 'El campo {field} es requerido.'
            ]
        ],
        [
            'field' => 'id_type_designation',
            'label' => 'Tupe designation',
            'rules' => 'required',
            'errors' => [
                'required' => 'El campo {field} es requerido.'
            ]
        ]
    ],
    'medidas' => [
        [
            'field' => 'medida',
            'label' => 'Medida',
            'rules' => 'required',
            'errors' => [
                'required' => 'El campo {field} es requerido.'
            ]
        ],
        [
            'field' => 'codigo_componente',
            'label' => 'Componente',
            'rules' => 'required',
            'errors' => [
                'required' => 'El campo {field} es requerido.'
            ]
        ],
        [
            'field' => 'codigo_reparacion',
            'label' => 'Código de reparación',
            'rules' => 'required',
            'errors' => [
                'required' => 'El campo {field} es requerido.'
            ]
        ],
        [
            'field' => 'numero_reparacion',
            'label' => 'Número de reparación',
            'rules' => 'required',
            'errors' => [
                'required' => 'El campo {field} es requerido.'
            ]
        ],
        [
            'field' => 'tiempo_reparacion',
            'label' => 'Tiempo de reparación',
            'rules' => 'required',
            'errors' => [
                'required' => 'El campo {field} es requerido.'
            ]
        ],
        [
            'field' => 'precio_material',
            'label' => 'Precio del material',
            'rules' => 'required',
            'errors' => [
                'required' => 'El campo {field} es requerido.'
            ]
        ]
    ],
    'codigos_responsabilidad' => [
        [
            'field' => 'codigo',
            'label' => 'Código de responsabilidad',
            'rules' => 'required',
            'errors' => [
                'required' => 'El campo {field} es requerido.'
            ]
        ],
        [
            'field' => 'descripcion',
            'label' => 'Descripción',
            'rules' => 'required',
            'errors' => [
                'required' => 'El campo {field} es requerido.'
            ]
        ],
        [
            'field' => 'traduccion',
            'label' => 'Traducción',
            'rules' => 'required',
            'errors' => [
                'required' => 'El campo {field} es requerido.'
            ]
        ]
    ],
    'monitores' => [
        [
            'field' => 'codigo_contenedor',
            'label' => 'No. Container',
            'rules' => 'required',
            'errors' => [
                'required' => 'El campo {field} es requerido.'
            ]
        ],
        [
            'field' => 'temperatura_suministro',
            'label' => 'Temperatura Suministro',
            'rules' => 'required',
            'errors' => [
                'required' => 'El campo {field} es requerido.'
            ]
        ],
        [
            'field' => 'temperatura_retorno',
            'label' => 'Temperatura Retorno',
            'rules' => 'required',
            'errors' => [
                'required' => 'El campo {field} es requerido.'
            ]
        ],
        [
            'field' => 'evento',
            'label' => 'Evento',
            'rules' => 'required',
            'errors' => [
                'required' => 'El campo {field} es requerido.'
            ]
        ]
    ]
];