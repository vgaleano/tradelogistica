<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/Bogota');
/**
* Estado: 1->activo, 0->desactivado, 2->eliminado
*/
class PerfilVista_Model extends CI_model {
    function __construct() {
        parent::__construct();
    }

    public function getPermisos($id_perfil) {
        $this->db->select('id_vista');
        $this->db->where('id_perfil', $id_perfil);
        $query = $this->db->get('perfil_vista');
        if ($query->num_rows()>0) {
            return $query->result();
        }
        return [];
    }

    public function deletePermisosByIdPerfil($id_perfil) {
        $this->db->where('id_perfil', $id_perfil);
        $response = $this->db->get('perfil_vista');
        return $response;
    }

    public function multiStore($data)
	{
        $response = $this->db->insert_batch('perfil_vista', $data);
		return $response;
    }
}