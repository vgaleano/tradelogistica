<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/Bogota');
/**
* Estado: 1->activo, 0->desactivado, 2->eliminado
*/
class Inspecciones_Model extends CI_model {
    function __construct() {
        parent::__construct();
    }

    public function getCountAll($fecha_inicio='', $fecha_fin='', $id_transportador='', $id_conductor='', $id_clasificacion='', $id_naviera='', $id_cliente='', $tipo_movimiento='') {
        $this->db->where('deleted', '0');
        //Rango de fechas
        $fecha_inicio!='' ? $this->db->where('(created_at between "' . $fecha_inicio . '" and "' . $fecha_fin . '")') : '';
        //Filtro transportador
        $id_transportador!='' ? $this->db->where('id_transportador', $id_transportador) : '';
        //Filtro conductor
        $id_conductor!='' ? $this->db->where('id_conductor', $id_conductor) : '';
        //Filtro Clasificación
        $id_clasificacion!='' ? $this->db->where('id_clasificacion', $id_clasificacion) : '';
        //Filtro Naviera
        $id_naviera!='' ? $this->db->where('id_naviera', $id_naviera) : '';
        //Filtro Cliente
        $id_cliente!='' ? $this->db->where('id_cliente', $id_cliente)  :'';
        //Filtro Movimiento
        $tipo_movimiento!='' ? $this->db->where('estado_in_out', $tipo_movimiento) : '';
        return $this->db->count_all_results('inspecciones');
    }

    public function getAll( $fecha_inicio='', $fecha_fin='', $id_transportador='', $id_conductor='', $id_clasificacion='', $id_naviera='', $id_cliente='', $tipo_movimiento='', $limit=false, $offset=false )
	{
        $this->db->select('inspecciones.id, placa_vehiculo, conductor, transportador,  naviera, codigo_contenedor, cliente'); //, inspecciones.created_at as fecha_creacion
        $this->db->join('conductores', 'conductores.id=inspecciones.id_conductor', 'left');
        $this->db->join('transportadores', 'transportadores.id=inspecciones.id_transportador', 'left');
        $this->db->join('navieras', 'navieras.id=inspecciones.id_naviera', 'left');
        $this->db->join('clientes', 'clientes.id=inspecciones.id_cliente', 'left');
        $this->db->where('inspecciones.deleted', '0');
        //Rango de fechas
        $fecha_inicio!='' ? $this->db->where('(inspecciones.created_at between "' . $fecha_inicio . '" and "' . $fecha_fin . '")') : '';
        //Filtro transportador
        $id_transportador!='' ? $this->db->where('inspecciones.id_transportador', $id_transportador) : '';
        //Filtro conductor
        $id_conductor!=''?$this->db->where('inspecciones.id_conductor', $id_conductor) : '';
        //Filtro Clasificación
        $id_clasificacion!=''?$this->db->where('inspecciones.id_clasificacion', $id_clasificacion) : '';
        //Filtro Naviera
        $id_naviera!=''?$this->db->where('inspecciones.id_naviera', $id_naviera) : '';
        //Filtro Cliente
        $id_cliente!=''?$this->db->where('inspecciones.id_cliente', $id_cliente) : '';
        //Filtro Movimiento
        $tipo_movimiento!=''?$this->db->where('inspecciones.estado_in_out', $tipo_movimiento) : '';
        $limit ? $this->db->limit($limit, $offset) : '';
        $this->db->order_by('inspecciones.created_at', 'asc');
        $query= $this->db->get('inspecciones');
	    if ($query->num_rows()>0) {
            return $query->result();
        }
        return [];
    }

    public function store($data)
	{
		$response = $this->db->insert('inspecciones', $data);
		return $this->db->insert_id();
    }

    public function update($data,$id)
	{	
		$this->db->where('id', $id);
		$response = $this->db->update('inspecciones', $data);
		return $response;
    }

    public function delete($id)
	{	
        $data = ['status'=>'2', 'delete_at'=>date('Y-m-d H:i:s'), 'deleted'=>'1'];
		$this->db->where('id', $id);
		$response = $this->db->update('inspecciones', $data);
		return $response;
    }

    /* public function getAllFormControl()
	{
        $this->db->select('id as value, clasificacion as name');
        $this->db->where('deleted', '0');
		$query= $this->db->get('inspecciones');
	    if ($query->num_rows()>0) {
            return $query->result();
        }
        return [];
    } */

    public function getContenedoresEstanPuerto($fecha_inicio=false, $fecha_fin=false)
    {
        $this->db->select('inspecciones.id, codigo_contenedor, clasificacion, estado_in_out, largo, cliente, inspecciones.created_at');
        $this->db->join('clasificaciones', 'clasificaciones.id=id_clasificacion', 'left');
        $this->db->join('codigos_iso', 'codigos_iso.codigo=codigo_iso', 'left');
        $this->db->join('clientes', 'clientes.id=id_cliente', 'left');
        $fecha_inicio==false?'': $this->db->where('inspecciones.created_at between "' . $fecha_inicio . '" and "' . $fecha_fin . '"');
        $this->db->where('inspecciones.deleted', '0');
        $this->db->where('inspecciones.id in (select max(id) from inspecciones group by codigo_contenedor)');
        $this->db->where('estado_in_out', '1');
		$query= $this->db->get('inspecciones');
	    if ($query->num_rows()>0) {
            return $query->result();
        }
        return [];
    }

    public function getMovimientosDiarios($fecha_inicio=false, $fecha_fin=false)
    {
        $this->db->select('codigo_contenedor, clasificacion, estado_in_out, cliente, inspecciones.created_at');
        $this->db->join('clasificaciones', 'clasificaciones.id=id_clasificacion', 'left');
        $this->db->join('clientes', 'clientes.id=id_cliente', 'left');
        $this->db->where('inspecciones.deleted', '0');
        $fecha_inicio==false?'': $this->db->where('inspecciones.created_at between "' . $fecha_inicio . '" and "' . $fecha_fin . '"');
        $this->db->order_by('created_at', 'asc');
        $this->db->order_by('codigo_contenedor', 'asc');
		$query= $this->db->get('inspecciones');
	    if ($query->num_rows()>0) {
            return $query->result();
        }
        return [];
    }

    public function validateInspeccionContenedor($codigo_contenedor, $movimiento, $id=false)
    {
        $this->db->select('inspecciones.id');
        $this->db->where('id in (select max(id) from inspecciones group by codigo_contenedor)');
        $this->db->where('estado_in_out', $movimiento);
        $this->db->where('codigo_contenedor', $codigo_contenedor);
        if ($id!==false) {
            $this->db->where('id !=', $id);
        }
		$query= $this->db->get('inspecciones');
	    if ($query->num_rows()>0) {
            return true;
        }
        return false;
    }

    public function getAllInspecciones($fecha_inicio, $fecha_fin, $id_transportador, $id_conductor, $id_clasificacion, $id_naviera, $id_cliente, $tipo_movimiento)
    {
        $this->db->select('codigo_contenedor, transportador, codigo_iso, clasificacion, conductor, placa_vehiculo, estado_in_out, cliente, naviera, observaciones, codigo_damage, codigo_localizacion_damage, inspeccion_info_damage.codigo_componente, medida, inspeccion_info_damage.codigo_reparacion, codigos_responsabilidad.descripcion as codigo_responsabilidad, estado_llenado, inspecciones.created_at');
        $this->db->join('clasificaciones', 'clasificaciones.id=id_clasificacion', 'left');
        $this->db->join('transportadores', 'transportadores.id=id_transportador', 'left');
        $this->db->join('conductores', 'conductores.id=id_conductor', 'left');
        $this->db->join('navieras', 'navieras.id=id_naviera', 'left');
        $this->db->join('clientes', 'clientes.id=id_cliente', 'left');
        $this->db->join('inspeccion_info_damage', 'inspeccion_info_damage.id_inspeccion=inspecciones.id', 'left');
        $this->db->join('medidas', 'medidas.id=inspeccion_info_damage.id_medida', 'left');
        $this->db->join('codigos_responsabilidad', 'codigos_responsabilidad.id=codigo_responsabilidad', 'left');
        $this->db->where('inspecciones.deleted', '0');
        $this->db->where('( (inspeccion_info_damage.deleted is not null and inspeccion_info_damage.deleted = "0") || inspeccion_info_damage.deleted is null)');
        $fecha_inicio!='' ? $this->db->where('(inspecciones.created_at between "' . $fecha_inicio . '" and "' . $fecha_fin . '")'):'';
        $id_transportador!='' ? $this->db->where('inspecciones.id_transportador', $id_transportador):'';
        $id_conductor!=''?$this->db->where('inspecciones.id_conductor', $id_conductor):'';
        $id_clasificacion!=''?$this->db->where('inspecciones.id_clasificacion', $id_clasificacion):'';
        $id_naviera!=''?$this->db->where('inspecciones.id_naviera', $id_naviera):'';
        $id_cliente!=''?$this->db->where('inspecciones.id_cliente', $id_cliente):'';
        $tipo_movimiento!=''?$this->db->where('inspecciones.estado_in_out', $tipo_movimiento):'';
        $this->db->order_by('inspecciones.created_at', 'asc');
        $query= $this->db->get('inspecciones');        
	    if ($query->num_rows()>0) {
            return $query->result();
        }
        return [];
    }

    public function getInspeccionById($id)
    {
        $this->db->select('inspecciones.*, largo');
        $this->db->join('codigos_iso', 'codigos_iso.codigo=codigo_iso', 'left');
        $this->db->where('inspecciones.id', $id);
        $query= $this->db->get('inspecciones');        
	    if ($query->num_rows()>0) {
            return $query->result();
        }
        return [];
    }

    public function getById($id)
    {
        $this->db->select('inspecciones.id, codigo_contenedor, estado_in_out, estado_llenado, codigo_iso, clasificacion, inspecciones.created_at, transportador, conductor, numero_documento, largo, placa_vehiculo, cliente, observaciones, naviera, url_firma');
        $this->db->join('codigos_iso', 'codigos_iso.codigo=inspecciones.codigo_iso', 'left');
        $this->db->join('clasificaciones', 'clasificaciones.id=id_clasificacion', 'left');
        $this->db->join('transportadores', 'transportadores.id=id_transportador', 'left');
        $this->db->join('conductores', 'conductores.id=id_conductor', 'left');
        $this->db->join('clientes', 'clientes.id=id_cliente', 'left');
        $this->db->join('navieras', 'navieras.id=id_naviera', 'left');
        $this->db->where('inspecciones.id', $id);
        $query= $this->db->get('inspecciones');        
	    if ($query->num_rows()>0) {
            return $query->result();
        }
        return [];
    }

}