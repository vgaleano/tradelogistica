<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/Bogota');
/**
* Estado: 1->activo, 0->desactivado, 2->eliminado
*/
class Contenedor_Model extends CI_model {
    function __construct() {
        parent::__construct();
    }

    public function getCountAll() {
        $this->db->where('deleted', '0');
        return $this->db->count_all_results('contenedores');
    }

    public function getAll($limit=false, $offset=false )
	{
        $this->db->select('contenedores.id, codigo, codigo_iso, id_naviera, naviera');
        $this->db->join('navieras', 'navieras.id=contenedores.id_naviera', 'left');
        $this->db->where('contenedores.deleted', '0');
        $limit ? $this->db->limit($limit, $offset) : '';
        $this->db->order_by('codigo', 'asc');
		$query= $this->db->get('contenedores');
	    if ($query->num_rows()>0) {
            return $query->result();
        }
        return [];
    }

    public function store($data)
	{
		$response = $this->db->insert('contenedores', $data);
		return $response;
    }

    public function update($data,$id)
	{	
		$this->db->where('id', $id);
		$response = $this->db->update('contenedores', $data);
		return $response;
    }

    public function delete($id)
	{	
        $data = ['status'=>'2', 'delete_at'=>date('Y-m-d H:i:s'), 'deleted'=>'1'];
		$this->db->where('id', $id);
		$response = $this->db->update('contenedores', $data);
		return $response;
    }

    public function getLike( $text, $column ) {
        $this->db->select('contenedores.codigo, naviera, id_naviera, contenedores.codigo_iso, largo');
        $this->db->join('navieras', 'navieras.id=contenedores.id_naviera', 'inner');
        $this->db->join('codigos_iso', 'codigos_iso.codigo=contenedores.codigo_iso', 'inner');
        $this->db->like('contenedores.codigo', $text);
        $this->db->where('contenedores.deleted', '0');
        $query= $this->db->get('contenedores');
	    if ($query->num_rows()>0) {
            return $query->result();
        }
        return [];
    }

}