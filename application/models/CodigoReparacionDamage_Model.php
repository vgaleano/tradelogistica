<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/Bogota');
/**
* Estado: 1->activo, 0->desactivado, 2->eliminado
*/
class Codigoreparaciondamage_Model extends CI_model {
    function __construct() {
        parent::__construct();
    }

    public function store($data)
	{
        $response = $this->db->insert('codigo_reparacion_damages', $data);
		return $response;
    }

    public function multiStore($data)
	{
        $response = $this->db->insert_batch('codigo_reparacion_damages', $data);
		return $response;
    }

    public function update($data, $id)
	{
        $this->db->where('id', $id);
        $response = $this->db->update('codigo_reparacion_damages', $data);
		return $response;
    }

    public function getByCodigoReparacion($codigo_reparacion)
    {
        $this->db->where('codigo_reparacion', $codigo_reparacion);
        $query = $this->db->get('codigo_reparacion_damages');
		if ($query->num_rows()>0) {
            return $query->result();
        }
        return [];
    }

    public function getAllByCodigoReparacion($codigo_reparacion)
    {
        $this->db->select('id');
        $this->db->where('codigo_reparacion', $codigo_reparacion);
        $query = $this->db->get('codigo_reparacion_damages');
		if ($query->num_rows()>0) {
            return $query->result();
        }
        return [];
    }

    public function delete($array_id)
    {
        $this->db->where_in('id', $array_id);
        $response = $this->db->delete('codigo_reparacion_damages');
		return $response;
    }

    public function getRepetidosCodigoDamage($codigo_damage, $codigo_reparacion, $id=false)
    {
        $this->db->select('codigo_damage');
        $this->db->where_in('codigo_damage', $codigo_damage);
        $this->db->where('codigo_reparacion', $codigo_reparacion);
        if ($id!==false && count($id)>0) {
            $this->db->where_not_in('id', $id);
        }
        $query = $this->db->get('codigo_reparacion_damages');
		if ($query->num_rows()>0) {
            return $query->result();
        }
        return false;
    }

    public function getByIdCodigoReparacion($id)
    {
        $this->db->select('codigo_reparacion_damages.id');
        $this->db->join('codigos_reparacion', 'codigos_reparacion.codigo=codigo_reparacion_damages.codigo_reparacion', 'left');
        $this->db->where('codigos_reparacion.id', $id);
        $query = $this->db->get('codigo_reparacion_damages');
		if ($query->num_rows()>0) {
            return $query->result();
        }
        return [];
    }

    public function getAll()
    {
        $this->db->select('codigos_damage.descripcion, codigo_reparacion, codigo_damage');
        $this->db->join('codigos_reparacion', 'codigos_reparacion.codigo=codigo_reparacion_damages.codigo_reparacion', 'left');
        $this->db->join('codigos_damage', 'codigos_damage.codigo=codigo_reparacion_damages.codigo_damage', 'left');
        $this->db->where('codigos_reparacion.deleted', '0');
        $this->db->where('codigos_damage.deleted', '0');
        $query = $this->db->get('codigo_reparacion_damages');
		if ($query->num_rows()>0) {
            return $query->result();
        }
        return [];
    }
}