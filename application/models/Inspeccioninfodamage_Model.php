<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/Bogota');
/**
* Estado: 1->activo, 0->desactivado, 2->eliminado
*/
class Inspeccioninfodamage_Model extends CI_model {
    function __construct() {
        parent::__construct();
    }

    public function store($data)
	{
        $response = $this->db->insert('inspeccion_info_damage', $data);
		return $response;
    }

    public function multiStore($data)
	{
        $response = $this->db->insert_batch('inspeccion_info_damage', $data);
		return $response;
    }

    public function update($data, $id)
	{
        $this->db->where('id', $id);
        $response = $this->db->update('inspeccion_info_damage', $data);
		return $response;
    }

    public function getByIdInspeccion($id_inspeccion) {
        $this->db->select('count(id) as total');
        $this->db->where('id_inspeccion', $id_inspeccion);
        $query = $this->db->get('inspeccion_info_damage');
	    if ($query->num_rows()>0) {
            $query = $query->result();
            return $query[0]->total;
        }
        return 0;
    }

    public function getInfoByIdInspeccion($id_inspeccion) {
        $this->db->select('inspeccion_info_damage.*, medida');
        $this->db->join('medidas', 'medidas.id=id_medida', 'left');
        $this->db->where('id_inspeccion', $id_inspeccion);
        $query = $this->db->get('inspeccion_info_damage');
	    if ($query->num_rows()>0) {
            return $query->result();
        }
        return [];
    }

    public function getAllByIdInspeccion($id_inspeccion)
    {
        $this->db->select('id');
        $this->db->where('id_inspeccion', $id_inspeccion);
        $query = $this->db->get('inspeccion_info_damage');
		if ($query->num_rows()>0) {
            return $query->result();
        }
        return [];
    }

    public function delete($array_id)
    {
        $this->db->where_in('id', $array_id);
        $response = $this->db->delete('inspeccion_info_damage');
		return $response;
    }

}