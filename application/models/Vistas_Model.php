<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/Bogota');
/**
* Estado: 1->activo, 0->desactivado, 2->eliminado
*/
class Vistas_Model extends CI_model {
    function __construct() {
        parent::__construct();
    }

    public function getAll()
	{
		$query= $this->db->get('vistas');
	    if ($query->num_rows()>0) {
            return $query->result();
        }
        return [];
    }
}