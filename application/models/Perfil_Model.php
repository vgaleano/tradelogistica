<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* Estado: 1->activo, 0->desactivado, 2->eliminado
*/
class Perfil_Model extends CI_model {
    function __construct() {
        parent::__construct();
    }

    function getAll()
	{
        $this->db->select('id as valor, perfil as nombre');
		$query= $this->db->get('perfiles');
	    if ($query->num_rows()>0) {
            return $query->result();
        }
        return [];
    }
}