<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/Bogota');
/**
* Estado: 1->activo, 0->desactivado, 2->eliminado
*/
class Monitoreo_Model extends CI_model {
    function __construct() {
        parent::__construct();
    }

    public function getCountAll() {
        $this->db->where('deleted', '0');
        return $this->db->count_all_results('monitores');
    }

    public function getAll( $limit=false, $offset=false )
	{
        $this->db->where('deleted', '0');
        $limit ? $this->db->limit($limit, $offset) : '';
        $this->db->order_by('created_at', 'asc');
		$query= $this->db->get('monitores');
	    if ($query->num_rows()>0) {
            return $query->result();
        }
        return [];
    }

    public function store($data)
	{
		$response = $this->db->insert('monitores', $data);
		return $this->db->insert_id();
    }

    public function update($data,$id)
	{	
		$this->db->where('id', $id);
		$response = $this->db->update('monitores', $data);
		return $response;
    }

    public function delete($id)
	{	
        $data = ['status'=>'2', 'delete_at'=>date('Y-m-d H:i:s'), 'deleted'=>'1'];
		$this->db->where('id', $id);
		$response = $this->db->update('monitores', $data);
		return $response;
    }

}