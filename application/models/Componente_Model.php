<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/Bogota');
/**
* Estado: 1->activo, 0->desactivado, 2->eliminado
*/
class Componente_Model extends CI_model {
    function __construct() {
        parent::__construct();
    }

    public function getCountAll() {
        $this->db->where('deleted', '0');
        return $this->db->count_all_results('componentes');
    }

    public function getAll( $limit=false, $offset=false )
	{
        $this->db->select('componentes.id, nombre, traduccion, codigo, codigo_localizacion, id_assembly, id_type_designation, assembly, type_designation');
        $this->db->join('assembly', 'assembly.id=componentes.id_assembly', 'left');
        $this->db->join('type_designation', 'type_designation.id=componentes.id_type_designation', 'left');
        $this->db->where('componentes.deleted', '0');
        $limit ? $this->db->limit($limit, $offset) : '';
        $this->db->order_by('nombre', 'asc');
		$query= $this->db->get('componentes');
	    if ($query->num_rows()>0) {
            return $query->result();
        }
        return [];
    }

    public function store($data)
	{
		$response = $this->db->insert('componentes', $data);
		return $this->db->insert_id();
    }

    public function update($data, $id)
	{	
		$this->db->where('id', $id);
		$response = $this->db->update('componentes', $data);
		return $response;
    }

    public function delete($id)
	{	
        $data = ['status'=>'2', 'delete_at'=>date('Y-m-d H:i:s'), 'deleted'=>'1'];
		$this->db->where('id', $id);
		$response = $this->db->update('componentes', $data);
		return $response;
    }

    public function validateCodigo($codigo, $id=false)
    {
        $this->db->where('codigo', $codigo);
        if ($id!==false) {
            $this->db->where('id !=', $id);
        }
        $this->db->where('status', '1');
        $query= $this->db->get('componentes');
	    if ($query->num_rows()>0) {
            return true;
        }
        return false;
    }

    public function getAllFormControl()
	{
        $this->db->select('codigo as value, codigo as name');
        $this->db->where('deleted', '0');
		$query = $this->db->get('componentes');
	    if ($query->num_rows()>0) {
            return $query->result();
        }
        return [];
    }

    public function getComponenteByLocation( $codigo_location ) 
    {
        $this->db->select('codigo');
        $this->db->where('deleted', '0');
        $this->db->like('codigo_localizacion', $codigo_location);
		$query = $this->db->get('componentes');
	    if ($query->num_rows()>0) {
            return $query->result();
        }
        return [];
    }
}