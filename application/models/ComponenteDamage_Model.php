<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/Bogota');
/**
* Estado: 1->activo, 0->desactivado, 2->eliminado
*/
class ComponenteDamage_Model extends CI_model {
    function __construct() {
        parent::__construct();
    }

    public function store($data)
	{
        $response = $this->db->insert('componente_damages', $data);
		return $response;
    }

    public function multiStore($data)
	{
        $response = $this->db->insert_batch('componente_damages', $data);
		return $response;
    }

    public function update($data, $id)
	{
        $this->db->where('id', $id);
        $response = $this->db->update('componente_damages', $data);
		return $response;
    }

    public function delete($array_id)
    {
        $this->db->where_in('id', $array_id);
        $response = $this->db->delete('componente_damages');
		return $response;
    }

    public function getByIdCodigoComponente($id)
    {
        $this->db->select('componente_damages.id');
        $this->db->join('componentes', 'componentes.codigo=componente_damages.codigo_componente', 'left');
        $this->db->where('componentes.id', $id);
        $query = $this->db->get('componente_damages');
		if ($query->num_rows()>0) {
            return $query->result();
        }
        return [];
    }

    public function getByCodigoComponente($codigo_componente)
    {
        $this->db->where('codigo_componente', $codigo_componente);
        $query = $this->db->get('componente_damages');
		if ($query->num_rows()>0) {
            return $query->result();
        }
        return [];
    }

}