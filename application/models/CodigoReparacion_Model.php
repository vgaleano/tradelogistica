<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/Bogota');
/**
* Estado: 1->activo, 0->desactivado, 2->eliminado
*/
class Codigoreparacion_Model extends CI_model {
    function __construct() {
        parent::__construct();
    }

    public function getCountAll() {
        $this->db->where('deleted', '0');
        return $this->db->count_all_results('codigos_reparacion');
    }

    public function getAll( $limit=false, $offset=false )
	{
        $this->db->where('deleted', '0');
        $limit ? $this->db->limit($limit, $offset) : '';
        $this->db->order_by('codigo', 'asc');
		$query= $this->db->get('codigos_reparacion');
	    if ($query->num_rows()>0) {
            return $query->result();
        }
        return [];
    }

    public function store($data)
	{
		$response = $this->db->insert('codigos_reparacion', $data);
		return $this->db->insert_id();
    }

    public function update($data,$id)
	{	
		$this->db->where('id', $id);
		$response = $this->db->update('codigos_reparacion', $data);
		return $response;
    }

    public function delete($id)
	{	
        $data = ['status'=>'2', 'delete_at'=>date('Y-m-d H:i:s'), 'deleted'=>'1'];
		$this->db->where('id', $id);
		$response = $this->db->update('codigos_reparacion', $data);
		return $response;
    }

    public function validateCodigo($codigo, $id=false)
    {
        $this->db->where('codigo', $codigo);
        if ($id!==false) {
            $this->db->where('id !=', $id);
        }
        $this->db->where('status', '1');
        $query= $this->db->get('codigos_reparacion');
	    if ($query->num_rows()>0) {
            return true;
        }
        return false;
    }

    public function getAllFormControl()
	{
        $this->db->select('codigo as value, codigo as name');
        $this->db->where('deleted', '0');
		$query= $this->db->get('codigos_reparacion');
	    if ($query->num_rows()>0) {
            return $query->result();
        }
        return [];
    }

}