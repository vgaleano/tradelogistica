<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/Bogota');
/**
* Estado: 1->activo, 0->desactivado, 2->eliminado
*/
class Conductor_Model extends CI_model {
    function __construct() {
        parent::__construct();
    }

    public function getCountAll() {
        $this->db->where('deleted', '0');
        return $this->db->count_all_results('conductores');
    }

    public function getAll( $limit=false, $offset=false )
	{
        $this->db->select('conductores.*');
        $this->db->where('conductores.deleted', '0');
        $limit ? $this->db->limit($limit, $offset) : '';
        $this->db->order_by('conductor', 'asc');
		$query= $this->db->get('conductores');
	    if ($query->num_rows()>0) {
            return $query->result();
        }
        return [];
    }

    public function store($data)
	{
		$response = $this->db->insert('conductores', $data);
		return $this->db->insert_id();
    }

    public function update($data,$id)
	{	
		$this->db->where('id', $id);
		$response = $this->db->update('conductores', $data);
		return $response;
    }

    public function delete($id)
	{	
        $data = ['status'=>'2', 'delete_at'=>date('Y-m-d H:i:s'), 'deleted'=>'1'];
		$this->db->where('id', $id);
		$response = $this->db->update('conductores', $data);
		return $response;
    }

    public function getAllFormControl()
	{
        $this->db->select('id as value, concat(conductor," - ", numero_documento) as name');
        $this->db->where('deleted', '0');
		$query= $this->db->get('conductores');
	    if ($query->num_rows()>0) {
            return $query->result();
        }
        return [];
    }

    public function getAllSelect()
	{
        $this->db->select('conductores.id, conductor, numero_documento, id_transportador');
        $this->db->join('conductor_transportador', 'conductor_transportador.id_conductor=conductores.id', 'inner');
        $this->db->where('conductores.deleted', '0');
        $this->db->order_by('conductor', 'asc');
		$query= $this->db->get('conductores');
	    if ($query->num_rows()>0) {
            return $query->result();
        }
        return [];
    }

    public function getFirmaById($id)
    {
        $this->db->select('url_firma');
        $this->db->where('id', $id);
		$query= $this->db->get('usuarios');
	    if ($query->num_rows()>0) {
            return $query->result();
        }
        return [];
    }

}