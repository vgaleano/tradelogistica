<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/Bogota');
/**
* Estado: 1->activo, 0->desactivado, 2->eliminado
*/
class ConductorTransportador_Model extends CI_model {
    function __construct() {
        parent::__construct();
    }

    public function getByIdClasificacion($id_conductor)
    {
        $this->db->select('transportador, conductor_transportador.id, id_transportador');
        $this->db->join('transportadores', 'transportadores.id=conductor_transportador.id_transportador', 'left');
        $this->db->where('id_conductor', $id_conductor);
        $query= $this->db->get('conductor_transportador');
	    if ($query->num_rows()>0) {
            return $query->result();
        }
        return [];
    }

    public function multiStore($data)
	{
        $response = $this->db->insert_batch('conductor_transportador', $data);
		return $response;
    }

    public function delete($array_id)
    {
        $this->db->where_in('id', $array_id);
        $response = $this->db->delete('conductor_transportador');
		return $response;
    }

    public function getAllByIdClasificacion($id_conductor)
    {
        $this->db->select('id');
        $this->db->where('id_conductor', $id_conductor);
        $query = $this->db->get('conductor_transportador');
		if ($query->num_rows()>0) {
            return $query->result();
        }
        return [];
    }

    public function store($data)
	{
        $response = $this->db->insert('conductor_transportador', $data);
		return $response;
    }

    public function update($data, $id)
	{
        $this->db->where('id', $id);
        $response = $this->db->update('conductor_transportador', $data);
		return $response;
    }
}