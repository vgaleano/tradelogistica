<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/Bogota');
/**
* Estado: 1->activo, 0->desactivado, 2->eliminado
*/
class CargoA_Model extends CI_model {
    function __construct() {
        parent::__construct();
    }

    public function getAllFormControl()
	{
        $this->db->select('id as value, nombre as name');
		$query= $this->db->get('cargo_a');
	    if ($query->num_rows()>0) {
            return $query->result();
        }
        return [];
    }
}