<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/Bogota');
/**
* Estado: 1->activo, 0->desactivado, 2->eliminado
*/
class ClasificacionNaviera_Model extends CI_model {
    function __construct() {
        parent::__construct();
    }

    public function getByIdClasificacion($id_clasificacion)
    {
        $this->db->select('naviera, clasifiacion_navieras.id, id_naviera');
        $this->db->join('navieras', 'navieras.id=clasifiacion_navieras.id_naviera', 'left');
        $this->db->where('id_clasificacion', $id_clasificacion);
        $query= $this->db->get('clasifiacion_navieras');
	    if ($query->num_rows()>0) {
            return $query->result();
        }
        return [];
    }

    public function multiStore($data)
	{
        $response = $this->db->insert_batch('clasifiacion_navieras', $data);
		return $response;
    }

    public function delete($array_id)
    {
        $this->db->where_in('id', $array_id);
        $response = $this->db->delete('clasifiacion_navieras');
		return $response;
    }

    public function getAllByIdClasificacion($id_clasificacion)
    {
        $this->db->select('id');
        $this->db->where('id_clasificacion', $id_clasificacion);
        $query = $this->db->get('clasifiacion_navieras');
		if ($query->num_rows()>0) {
            return $query->result();
        }
        return [];
    }

    public function store($data)
	{
        $response = $this->db->insert('clasifiacion_navieras', $data);
		return $response;
    }

    public function update($data, $id)
	{
        $this->db->where('id', $id);
        $response = $this->db->update('clasifiacion_navieras', $data);
		return $response;
    }
}