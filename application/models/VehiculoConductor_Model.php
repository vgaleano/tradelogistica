<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/Bogota');
/**
* Estado: 1->activo, 0->desactivado, 2->eliminado
*/
class VehiculoConductor_Model extends CI_model {
    function __construct() {
        parent::__construct();
    }

    public function store($data)
	{
        $response = $this->db->insert('vehiculo_conductores', $data);
		return $response;
    }

    public function multiStore($data)
	{
        $response = $this->db->insert_batch('vehiculo_conductores', $data);
		return $response;
    }

    public function update($data, $id)
	{
        $this->db->where('id', $id);
        $response = $this->db->update('vehiculo_conductores', $data);
		return $response;
    }

    public function getByIdVehiculo($id_vehiculo)
    {
        $this->db->select('vehiculo_conductores.id_conductor, conductores.conductor, vehiculo_conductores.id, numero_documento');
        $this->db->join('conductores', 'conductores.id=vehiculo_conductores.id_conductor', 'left');
        $this->db->where('id_vehiculo', $id_vehiculo);
        $this->db->where('deleted', '0');
        $query = $this->db->get('vehiculo_conductores');
		if ($query->num_rows()>0) {
            return $query->result();
        }
        return [];
    }

    public function getAllByIdVehiculo($id_vehiculo)
    {
        $this->db->select('id');
        $this->db->where('id_vehiculo', $id_vehiculo);
        $query = $this->db->get('vehiculo_conductores');
		if ($query->num_rows()>0) {
            return $query->result();
        }
        return [];
    }

    public function delete($array_id)
    {
        $this->db->where_in('id', $array_id);
        $response = $this->db->delete('vehiculo_conductores');
		return $response;
    }

    public function getAllSelect()
    {
        $this->db->select('placa, id_conductor');
        $this->db->join('vehiculos', 'vehiculos.id=vehiculo_conductores.id_vehiculo', 'left');
        $this->db->where('vehiculos.deleted', '0');
        $query = $this->db->get('vehiculo_conductores');
		if ($query->num_rows()>0) {
            return $query->result();
        }
        return [];
    }
}