<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/Bogota');
/**
* Estado: 1->activo, 0->desactivado, 2->eliminado
*/
class NavieraCodigo_Model extends CI_model {
    function __construct() {
        parent::__construct();
    }

    public function store($data)
	{
        $response = $this->db->insert('naviera_codigo', $data);
		return $response;
    }

    public function multiStore($data)
	{
        $response = $this->db->insert_batch('naviera_codigo', $data);
		return $response;
    }

    public function update($data, $id)
	{
        $this->db->where('id', $id);
        $response = $this->db->update('naviera_codigo', $data);
		return $response;
    }

    public function getByCodigoIso($codigo_iso)
    {
        $this->db->select('id_naviera, codigo_naviera, naviera_codigo.id');
        $this->db->join('navieras', 'navieras.id=naviera_codigo.id_naviera', 'left');
        $this->db->where('codigo_iso', $codigo_iso);
        $this->db->where('navieras.status','1');
        $this->db->order_by('naviera, codigo_naviera', 'asc');
        $query = $this->db->get('naviera_codigo');
		if ($query->num_rows()>0) {
            return $query->result();
        }
        return [];
    }

    public function delete($array_id)
    {
        $this->db->where_in('id', $array_id);
        $response = $this->db->delete('naviera_codigo');
		return $response;
    }

    public function getRepetidosCodigoNaviera($codigos_naviera)
    {
        $this->db->select('codigo_naviera');
        $this->db->where_in('codigo_naviera', $codigos_naviera);
        $query = $this->db->get('naviera_codigo');
		if ($query->num_rows()>0) {
            return $query->result();
        }
        return false;
    }

    public function getByIdCodigoIso($id)
    {
        $this->db->select('naviera_codigo.id');
        $this->db->join('codigos_iso', 'codigos_iso.codigo=naviera_codigo.codigo_iso', 'left');
        $this->db->where('codigos_iso.id', $id);
        $query = $this->db->get('naviera_codigo');
		if ($query->num_rows()>0) {
            return $query->result();
        }
        return [];
    }

    public function getAll(){
        $this->db->select('codigo_iso, id_naviera, codigo_naviera, naviera, naviera_codigo.largo');
        $this->db->join('navieras', 'navieras.id=naviera_codigo.id_naviera', 'left');
        $this->db->join('codigos_iso', 'codigos_iso.codigo=naviera_codigo.codigo_iso', 'left');
        $this->db->where('navieras.deleted', '0');
        $this->db->where('codigos_iso.deleted', '0');
        $query = $this->db->get('naviera_codigo');
		if ($query->num_rows()>0) {
            return $query->result();
        }
        return [];
    }
}