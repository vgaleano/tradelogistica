<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/Bogota');
/**
* Estado: 1->activo, 0->desactivado, 2->eliminado
*/
class General_Model extends CI_model {
    function __construct() {
        parent::__construct();
    }

    public function validateRow($data, $table, $id=false)
	{
        $this->db->select('id');
        $this->db->where('deleted', '0');
        if ($id!==false) {
            $this->db->where('id !=', $id);
        }
        foreach ($data as $k => $v) {
            $this->db->where($k, $v);
        }
        $query= $this->db->get($table);
	    if ($query->num_rows()>0) {
            return true;
        }
        return false;
    }

    public function modalOn($controlador)
    {
        $this->session->set_userdata(['modal_on'=> isset($_POST['service']) ? true : false ]);
        $this->session->set_userdata(['count_modal_on' => isset($_POST['service']) && isset($this->session->count_modal_on) && isset($this->session->modal_on_controlador) && $this->session->modal_on_controlador==$controlador ? ($this->session->count_modal_on+1) : 1 ]);
        if ( !isset($this->session->modal_on_controlador) || ( isset($this->session->modal_on_controlador) && $this->session->modal_on_controlador!=$controlador ) ) {
            $this->session->set_userdata(['modal_on_controlador'=> $controlador]);
        }
        $servicio = $this->session->modal_on && $this->session->count_modal_on==1 ? '1' :'';
        return $servicio;
    }
}