<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/Bogota');
/**
* Estado: 1->activo, 0->desactivado, 2->eliminado
*/
class Usuario_Model extends CI_model {
    function __construct() {
        parent::__construct();
    }

    public function getCountAll() {
        $this->db->where('deleted', '0');
        return $this->db->count_all_results('usuarios');
    }

    public function getAll( $limit=false, $offset=false )
	{
        $this->db->select('usuarios.*, perfiles.perfil');
        $this->db->join('perfiles', 'perfiles.id=usuarios.id_perfil', 'left');
        $this->db->where('deleted', '0');
        $limit ? $this->db->limit($limit, $offset) : '';
        $this->db->order_by('nombre', 'asc');
		$query= $this->db->get('usuarios');
	    if ($query->num_rows()>0) {
            return $query->result();
        }
        return [];
    }

    public function store($data)
	{
		$response = $this->db->insert('usuarios', $data);
		return $response;
    }

    public function update($data, $id)
	{	
		$this->db->where('id', $id);
		$response = $this->db->update('usuarios', $data);
		return $response;
    }

    public function delete($id)
	{	
        $data = ['status'=>'2', 'delete_at'=>date('Y-m-d H:i:s'), 'deleted'=>'1'];
		$this->db->where('id', $id);
		$response = $this->db->update('usuarios', $data);
		return $response;
    }

    public function login($email, $password)
    {
        $this->db->select('nombre, id_perfil, id');
        $this->db->where('email', $email);
        $this->db->where('password', $password);
        $this->db->where('status', '1');
        $query= $this->db->get('usuarios');
	    if ($query->num_rows()>0) {
            return $query->result();
        }
        return [];
    }

    public function getFirmaById($id)
    {
        $this->db->select('url_firma');
        $this->db->where('id', $id);
		$query= $this->db->get('usuarios');
	    if ($query->num_rows()>0) {
            return $query->result();
        }
        return [];
    }

    public function getUserByCookie($id, $cookie)
    {
        $this->db->select('usuarios.*, perfiles.perfil');
        $this->db->join('perfiles', 'perfiles.id=usuarios.id_perfil', 'left');
        $this->db->where('deleted', '0');
        $this->db->where('usuarios.id', $id);
        $this->db->where('cookie', $cookie);
        $this->db->where('cookie<>""');
		$query= $this->db->get('usuarios');
	    if ($query->num_rows()>0) {
            return $query->result();
        }
    }

}