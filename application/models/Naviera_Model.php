<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/Bogota');
/**
* Estado: 1->activo, 0->desactivado, 2->eliminado
*/
class Naviera_Model extends CI_model {
    function __construct() {
        parent::__construct();
    }

    public function getCountAll() {
        $this->db->where('deleted', '0');
        return $this->db->count_all_results('navieras');
    }

    public function getAll( $limit=false, $offset=false )
	{
        $this->db->where('deleted', '0');
        $limit ? $this->db->limit($limit, $offset) : '';
        $this->db->order_by('naviera', 'asc');
		$query= $this->db->get('navieras');
	    if ($query->num_rows()>0) {
            return $query->result();
        }
        return [];
    }

    public function store($data)
	{
		$response = $this->db->insert('navieras', $data);
		return $this->db->insert_id();
    }

    public function update($data,$id)
	{	
		$this->db->where('id', $id);
		$response = $this->db->update('navieras', $data);
		return $response;
    }

    public function delete($id)
	{	
        $data = ['status'=>'2', 'delete_at'=>date('Y-m-d H:i:s'), 'deleted'=>'1'];
		$this->db->where('id', $id);
		$response = $this->db->update('navieras', $data);
		return $response;
    }

    public function getAllFormControl()
	{
        $this->db->select('id as value, naviera as name');
        $this->db->where('deleted', '0');
        $this->db->order_by('naviera', 'asc');
		$query= $this->db->get('navieras');
	    if ($query->num_rows()>0) {
            return $query->result();
        }
        return [];
    }

    public function getAllSelect()
	{
        $this->db->select('navieras.id, naviera, id_clasificacion');
        $this->db->join('clasifiacion_navieras', 'clasifiacion_navieras.id_naviera=navieras.id', 'inner');
        $this->db->where('navieras.deleted', '0');
        $this->db->order_by('naviera', 'asc');
		$query= $this->db->get('navieras');
	    if ($query->num_rows()>0) {
            return $query->result();
        }
        return [];
    }

}