<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/Bogota');
/**
* Estado: 1->activo, 0->desactivado, 2->eliminado
*/
class Medidas_Model extends CI_model {
    function __construct() {
        parent::__construct();
    }

    public function getCountAll() {
        $this->db->where('deleted', '0');
        return $this->db->count_all_results('medidas');
    }

    public function getAll( $limit=false, $offset=false )
	{
        $this->db->select('medidas.id, medida, codigo_componente, codigo_reparacion, numero_reparacion, tiempo_reparacion, precio_material');
        $this->db->join('componentes', 'componentes.codigo=medidas.codigo_componente', 'left');
        $this->db->join('codigos_reparacion', 'codigos_reparacion.codigo=medidas.codigo_reparacion', 'left');
        $this->db->where('medidas.deleted', '0');
        $limit ? $this->db->limit($limit, $offset) : '';
        $this->db->order_by('medida', 'asc');
        $query= $this->db->get('medidas');
	    if ($query->num_rows()>0) {
            return $query->result();
        }
        return [];
    }

    public function store($data)
	{
		$response = $this->db->insert('medidas', $data);
		return $response;
    }

    public function update($data, $id)
	{	
		$this->db->where('id', $id);
		$response = $this->db->update('medidas', $data);
		return $response;
    }

    public function delete($id)
	{	
        $data = ['status'=>'2', 'delete_at'=>date('Y-m-d H:i:s'), 'deleted'=>'1'];
		$this->db->where('id', $id);
		$response = $this->db->update('medidas', $data);
		return $response;
    }

    public function getMedidasByComponentReparacion($codigo_componente, $codigo_reparacion)
    {
        $this->db->select('id, medida');
        $this->db->where('codigo_componente', $codigo_componente);
        $this->db->where('codigo_reparacion', $codigo_reparacion);
        $this->db->where('deleted', '0');
        $query = $this->db->get('medidas');
	    if ($query->num_rows()>0) {
            return $query->result();
        }
        return [];
    }

}