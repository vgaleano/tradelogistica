<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Conductores extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Conductor_Model');
        $this->load->model('General_Model');
        $this->load->model('ConductorTransportador_Model');
    }

    public function index()
    {
        redirect(base_url().'conductores/list');
    }

    public function list($offset=0)
    {
        if(!$this->session->logged_in) redirect(base_url()."home");        
        if (!in_array('22', $this->session->permisos)) redirect(base_url().'home/permisos');
        
        $this->load->model('General_Model');
        $servicio = $this->General_Model->modalOn('conductores');
        $this->session->set_userdata(['menu_pos' => $this->input->post('menu_pos')]);
        
        $this->load->model('Transportador_Model');

        $total = $this->Conductor_Model->getCountAll();
        $config = pagination_general($total, 'conductores/list', 10, 3);
        $this->pagination->initialize($config);
        $offset_sql = $offset!=0 ? $offset * $config['per_page'] - $config['per_page'] : $offset;
        $list = $this->Conductor_Model->getAll( $config['per_page'], $offset_sql);
        if (count($list)>0) {
            foreach ($list as $k => $v) {
                $list[$k]->transportadores = $this->ConductorTransportador_Model->getByIdClasificacion($v->id);
            }
        }

        $data = [
            'url' => 'conductores',
            'ver_form' => '23',
            'nuevo_form' => '24',
            'editar_form' => '25',
            'eliminar_form' => '26',
            'columns' => [
                [
                    'key' => 'conductor',
                    'name' => 'Nombre'
                ],[
                    'key' => 'numero_documento',
                    'name' => 'Número de documento'
                ],[
                    'key' => 'transportador',
                    'name' => 'Transportadores'
                ],[
                    'key' => 'btn',
                    'name' => 'Acción'
                ],[
                    'key' => '',
                    'name' => 'Editar'
                ],[
                    'key' => '',
                    'name' => 'Eliminar'
                ]
            ],
            'titulo_view' => 'Conductores',
            'titulo_singular' => 'Conductor',
            'service' => $servicio,
            'lists' => $list,
            'offset' => $offset,
            'list_transportador' => $this->Transportador_Model->getAllFormControl(),
            'forms' => [
                [
                    "form_control" => "input",
                    "label" => "Nombre<span class='text-danger'>*</span>",
                    "column" => "12",
                    "for" => "conductor",
                    "attr"  => [
                        "type" => "text",
                        "name" => "conductor",
                        "id"   => "conductor",
                        "placeholder" => "Nombre",
                        "required" => "true",
                        "class" => "form-control"
                    ],
                    "value" => "",
                    "extra_attr" => ""
                ],
                [
                    "form_control" => "input",
                    "label" => "Número de documento<span class='text-danger'>*</span>",
                    "column" => "6",
                    "for" => "numero_documento",
                    "attr"  => [
                        "type" => "text",
                        "name" => "numero_documento",
                        "id"   => "numero_documento",
                        "placeholder" => "Número de documento",
                        "required" => "true",
                        "class" => "form-control"
                    ],
                    "value" => "",
                    "extra_attr" => "onkeypress='return onlyNumber(event)'"
                ],
                [
                    "form_control" => "select-add",
                    "label" => "Transportador<span class='text-danger'>*</span> <br class='d-none d-sm-block'><button type='button' class='btn btn-success btn-sm addItem btn-popover' data-content='Agregar Campo' data-placement='top'><i class='mdi mdi-plus'></i> <span class='d-none d-sm-block'>Agregar Campo</span></button> <button type='button' class='btn btn-sm btn-info add-item btn-popover' data-content='Crear Transportador' data-placement='bottom' " . (in_array("19", $this->session->permisos)?"":"disabled") . "><i class='mdi mdi-plus'></i> <span class='d-none d-sm-block'>Crear Transportador</span></button>",
                    "for" => "id_transportador",
                    "required" => "true",
                    "column" => "6",
                    "attr" => [
                        "class" => "form-control id_transportador",
                        "name" => "id_transportador",
                        //"id" => "id_transportador",
                        "required" => "true"
                    ],
                    'options' => $this->Transportador_Model->getAllFormControl(),
                    "btn-info" => true
                ]
            ],
            'form_add' => [
                [
                    "form_control" => "input",
                    "label" => "Nombre<span class='text-danger'>*</span>",
                    "column" => "12",
                    "for" => "transportador",
                    "attr"  => [
                        "type" => "text",
                        "name" => "transportador",
                        "id"   => "transportador",
                        "placeholder" => "Nombre",
                        "required" => "true",
                        "class" => "form-control"
                    ],
                    "value" => "",
                    "extra_attr" => ""
                ]
            ],
            'form_add_titulo' => 'Transportador',
            'form_add_url' => 'transportadores',
            'titulo' => 'Conductores'
        ];

        $this->load->view('admin/conductores/list_new', $data);
    }

    //servicios
    public function new() 
    {
        $r = '';
        if (!$this->form_validation->run('conductores'))
        {
            $r = @json_encode(['res'=>'bad', 'errors'=>$this->form_validation->error_array()]);
            print "<script>parent.postMessage( '".$r."' , '".base_url()."')</script>";
            exit();
        }
        $data = $this->input->post();
        unset($data['id_transportador']);
        if ( $this->General_Model->validateRow( ['numero_documento'=>$data['numero_documento']], 'conductores') ) 
        {
            $r = @json_encode(['res'=>'bad', 'errors'=>['El numero documento ingresado ya se encuentra registrado.']]);
            print "<script>parent.postMessage( '".$r."' , '".base_url()."')</script>";
            exit();
        }
        //busca repetidos en el post
        $list_transportador = $data['list_transportador'];
        $list_transportador = json_decode($list_transportador);
        $list_transportador = objectToArray($list_transportador);
        unset($data['list_transportador']);
        if ( count(array_unique(array_column($list_transportador, 'id_transportador'))) < count($list_transportador) ) {
            $r = @json_encode(['res'=>'bad', 'errors'=>['Hay transportadores repetidos para el conductor.']]);
            print "<script>parent.postMessage( '".$r."' , '".base_url()."')</script>";
            exit();
        }
        $list_ = $this->input->post('list_')!==null ? $this->input->post('list_') : [];
        unset($data['list_']);

        list($type, $firma) = explode(';', $data['firma']);
        list(, $firma)      = explode(',', $firma);
        $firma = base64_decode($firma);
        $url_firma = 'public/firma/'.strtotime("now").'.png';
        file_put_contents(FCPATH.$url_firma, $firma);
        if (!file_exists($url_firma)) {
            $r = @json_encode(['res'=>'bad', 'errors'=>['Problemas para guardar la firma.']]);
            print "<script>parent.postMessage( '".$r."' , '".base_url()."')</script>";
            exit();
        }
        $data['url_firma'] = $url_firma;
        unset($data['firma']);

        $id_conductor = $this->Conductor_Model->store($data);
        if ($id_conductor) {
            foreach ($list_transportador as $k => $v) {
                $list_transportador[$k] = array_merge($list_transportador[$k], ['id_conductor'=>$id_conductor]);
            }
            if ( count($list_)>0 ) {
                foreach ($list_ as $k => $v) {
                    $list_[$k] = array_merge($list_[$k], ['id'=>(string)$id_conductor]);
                }
            }
            if ($this->ConductorTransportador_Model->multiStore($list_transportador)) {
                $r = @json_encode(['res'=>'ok', 'msj'=>'Información guardada.', 'id'=>$id_conductor, 'nombre'=>$data['conductor']." - ".$data['numero_documento'], 'list_'=>$list_]);
            }else{
                $r = @json_encode(['res'=>'bad', 'errors'=>['Problemas para guardar el registro.']]);
            }
        } else {
            $r = @json_encode(['res'=>'bad', 'errors'=>['Problemas para guardar el registro.']]);
        }
        print "<script>parent.postMessage( '".$r."' , '".base_url()."')</script>";
    }

    public function update($id)
    {
        $r = '';
        if (!$this->form_validation->run('conductores'))
        {
            $r = @json_encode(['res'=>'bad', 'errors'=>$this->form_validation->error_array()]);
            print "<script>parent.postMessage( '".$r."' , '".base_url()."')</script>";
            exit();
        }
        $data = $this->input->post();
        unset($data['id_transportador']);
        if ( $this->General_Model->validateRow( ['numero_documento'=>$data['numero_documento']], 'conductores', $id) ) 
        {
            $r = @json_encode(['res'=>'bad', 'errors'=>['El numero documento ingresado ya se encuentra registrado.']]);
            print "<script>parent.postMessage( '".$r."' , '".base_url()."')</script>";
            exit();
        }
        //busca repetidos en el post
        $list_transportador = $data['list_transportador'];
        $list_transportador = json_decode($list_transportador);
        $list_transportador = objectToArray($list_transportador);
        unset($data['list_transportador']);
        if ( count(array_unique(array_column($list_transportador, 'id_transportador'))) < count($list_transportador) ) {
            $r = @json_encode(['res'=>'bad', 'errors'=>['Hay transportadores repetidos para el conductor.']]);
            print "<script>parent.postMessage( '".$r."' , '".base_url()."')</script>";
            exit();
        }

        if ($data['firma']!='') {
            list($type, $firma) = explode(';', $data['firma']);
            list(, $firma)      = explode(',', $firma);
            $firma = base64_decode($firma);
            $url_firma = 'public/firma/'.strtotime("now").'.png';
            file_put_contents(FCPATH.$url_firma, $firma);
            if (!file_exists($url_firma)) {
                $r = @json_encode(['res'=>'bad', 'errors'=>['Problemas para guardar la firma.']]);
                print "<script>parent.postMessage( '".$r."' , '".base_url()."')</script>";
                exit();
            }else{
                $old_url = $this->Conductor_Model->getFirmaById($id);
                unlink(FCPATH.$old_url[0]->url_firma);
            }
            $data['url_firma'] = $url_firma;
        }
        unset($data['firma']);

        //eliminar los que se quitaron en el list
        $all_ = $this->ConductorTransportador_Model->getAllByIdClasificacion($id);
        $all_ = array_column(  objectToArray($all_), 'id' );
        $ids_ = array_column($list_transportador, 'id');
        $diff_ = array_diff($all_, $ids_);
        if (count($diff_)>0) {
            $this->ConductorTransportador_Model->delete($diff_);
        }
        if ($this->Conductor_Model->update($data, $id)) {
            //actualizar o agregar la naviera 
            foreach ($list_transportador as $k => $v) {            
                if (array_key_exists('id', $v)) {
                    $data = [ 'id_conductor' => $v['id_conductor'] ];
                    if (!$this->ConductorTransportador_Model->update($data, $v['id'])) {
                        $r = @json_encode(['res'=>'bad', 'errors'=>['Problemas para actualizar el registro.']]);
                        print "<script>parent.postMessage( '".$r."' , '".base_url()."')</script>";
                        exit();
                    }
                }else{
                    $data = $list_transportador[$k];
                    if (!$this->ConductorTransportador_Model->store($data)) {
                        $r = @json_encode(['res'=>'bad', 'errors'=>['Problemas para actualizar el registro.']]);
                        print "<script>parent.postMessage( '".$r."' , '".base_url()."')</script>";
                        exit();
                    }
                }
            }
            $r = @json_encode(['res'=>'ok', 'msj'=>'Información actualizada.']);
        } else {
            $r = @json_encode(['res'=>'bad', 'errors'=>['Problemas para actualizar el registro.']]);
        }
        print "<script>parent.postMessage( '".$r."' , '".base_url()."')</script>";
    }

    public function delete($id)
    {
        if ($this->Conductor_Model->delete($id)) {
            print json_encode(['res'=>'ok', 'msj'=>'Información eliminado.']);
        } else {
            print json_encode(['res'=>'bad', 'errors'=>['Problemas para eliminar el registro.']]);
        }
    }
}

function pagination_general($total, $url, $per_page, $uri_segment){
    $config['total_rows'] = $total;
    $config["base_url"] = base_url().$url;
    $config['per_page'] = $per_page;
    $config['uri_segment'] = $uri_segment;
    $config['first_link'] = '<< Ir al primero';
    $config['last_link'] = 'Ir al ultimo >';
    $config['next_link'] = ' Siguiente ' . '&gt;';
    $config['prev_link'] = ' &lt;' . ' Atras';
    $config['full_tag_open'] = "<ul  class='pagination justify-content-center'>";
    $config['full_tag_close'] ="</ul>";
    $config['num_tag_open'] = '<li class="page-item">';
    $config['num_tag_close'] = '</li>';
    $config['cur_tag_open'] = "<li class='active'><a href='#'>";
    $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
    $config['next_tag_open'] = '<li class="page-item">';
    $config['next_tag_close'] = "</li>";
    $config['prev_tag_open'] = '<li class="page-item">';
    $config['prev_tag_close'] = "</li>";
    $config['first_tag_open'] = '<li class="page-item">';
    $config['first_tag_close'] = "</li>";
    $config['last_tag_open'] = '<li class="page-item">';
    $config['last_tag_close'] = "</li>";
    $config['use_page_numbers'] = TRUE;
    return $config;
}

function objectToArray ( $object ) {
    if(!is_object($object) && !is_array($object)) {
      return $object;
    }  
    return array_map( 'objectToArray', (array) $object );
}