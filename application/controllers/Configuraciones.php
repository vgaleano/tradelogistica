<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Configuraciones extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Perfil_Model');
        $this->load->model('Vistas_Model');
        $this->load->model('PerfilVista_Model');
    }

    public function index()
    {
        redirect(base_url().'configuraciones/perfiles'); 
    }

    public function perfiles()
    {
        if(!$this->session->logged_in || $this->session->id_perfil!='1') redirect(base_url()."home");
        $data = [
            'url' => 'configuraciones',
            'columns' => [
                [
                    'key' => 'pefil',
                    'name' => 'Perfil'
                ],[
                    'key' => 'btn',
                    'name' => 'Editar'
                ]
            ],
            'lists' => $this->Perfil_Model->getAll(),
            'titulo_view' => 'Perfiles',
            'titulo' => 'Perfiles',
            'from_controller' => true
        ];

        $this->load->view('admin/configuraciones/roles', $data);
    }

    public function permisos($id)
    {
        if(!$this->session->logged_in || $this->session->id_perfil!='1') redirect(base_url()."home");

        $data = [
            'url' => 'configuraciones',
            'columns' => [
                [
                    'key' => 'vista',
                    'name' => 'Vista'
                ],[
                    'key' => 'btn',
                    'name' => 'Permiso'
                ]
            ],
            'lists' => $this->Vistas_Model->getAll(),
            'permisos' => array_column( objectToArray( $this->PerfilVista_Model->getPermisos($id)), 'id_vista'),
            'titulo_view' => 'Permisos',
            'titulo' => 'Permisos',
            'id_rol' => $id
        ];

        $this->load->view('admin/configuraciones/permisos', $data);
    }

    public function permisosByPerfil($id)
    {
        $permisos = $this->input->post('permisos');
        if ($this->PerfilVista_Model->deletePermisosByIdPerfil($id)) {
            if ( $this->PerfilVista_Model->multiStore($permisos)) {
                print json_encode(['res'=>'ok', 'msj'=>'Permisos actualizados.']);
                exit();
            }
        }
        print json_encode(['res'=>'bad', 'errors'=>['Problemas para actualizar los permisos.']]);
    }
}

function objectToArray ( $object ) {
    if(!is_object($object) && !is_array($object)) {
      return $object;
    }  
    return array_map( 'objectToArray', (array) $object );
}