<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Codigodamage extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('CodigoDamage_Model');
        $this->load->model('General_Model');
    }

    public function index()
    {
        redirect(base_url().'codigodamage/list');
    }

    public function list($offset=0)
    {
        if(!$this->session->logged_in) redirect(base_url()."home");        
        if (!in_array('47', $this->session->permisos)) redirect(base_url().'home/permisos');
        
        $this->load->model('General_Model');
        $servicio = $this->General_Model->modalOn('codigodamage');
        $this->session->set_userdata(['menu_pos' => $this->input->post('menu_pos')]);

        $total = $this->CodigoDamage_Model->getCountAll();
        $config = pagination_general($total, 'codigodamage/list', 10, 3);
        $this->pagination->initialize($config);
        $offset_sql = $offset!=0 ? $offset * $config['per_page'] - $config['per_page'] : $offset;
        $list = $this->CodigoDamage_Model->getAll( $config['per_page'], $offset_sql);

        $data = [
            'url' => 'codigodamage',
            'ver_form' => '48',
            'nuevo_form' => '49',
            'editar_form' => '50',
            'eliminar_form' => '51',
            'columns' => [
                [
                    'key' => 'codigo',
                    'name' => 'Código'
                ],[
                    'key' => 'descripcion',
                    'name' => 'Nombre'
                ],[
                    'key' => 'traduccion',
                    'name' => 'Traducción'
                ],
                [
                    'key' => 'descripcion_real',
                    'name' => 'Descripción'
                ],[
                    'key' => 'btn',
                    'name' => 'Acción'
                ],[
                    'key' => '',
                    'name' => 'Editar'
                ],[
                    'key' => '',
                    'name' => 'Eliminar'
                ]
            ],
            'titulo_view' => 'Código de daños',
            'titulo_singular' => 'Código de daño',
            'service' => $servicio,
            'lists' => $list,
            'offset' => $offset,
            'forms' => [
                [
                    "form_control" => "input",
                    "label" => "Código<span class='text-danger'>*</span>",
                    "column" => "6",
                    "for" => "codigo",
                    "attr"  => [
                        "type" => "text",
                        "name" => "codigo",
                        "id"   => "codigo",
                        "placeholder" => "Código",
                        "required" => "true",
                        "class" => "form-control"
                    ],
                    "value" => "",
                    "extra_attr" => ""
                ],
                [
                    "form_control" => "textarea",
                    "label" => "Nombre<span class='text-danger'>*</span>",
                    "column" => "6",
                    "for" => "descripcion",
                    "attr"  => [
                        "type" => "text",
                        "name" => "descripcion",
                        "id"   => "descripcion",
                        "placeholder" => "Nombre",
                        "required" => "true",
                        "class" => "form-control"
                    ],
                    "value" => "",
                    "extra_attr" => ""
                ],
                [
                    "form_control" => "textarea",
                    "label" => "Traducción<span class='text-danger'>*</span>",
                    "column" => "6",
                    "for" => "traduccion",
                    "attr"  => [
                        "type" => "text",
                        "name" => "traduccion",
                        "id"   => "traduccion",
                        "placeholder" => "Traducción",
                        "required" => "true",
                        "class" => "form-control"
                    ],
                    "value" => "",
                    "extra_attr" => ""
                ],
                [
                    "form_control" => "textarea",
                    "label" => "Descripción",
                    "column" => "6",
                    "for" => "descripcion_real",
                    "attr"  => [
                        "type" => "text",
                        "name" => "descripcion_real",
                        "id"   => "descripcion_real",
                        "placeholder" => "Descripción",
                        "class" => "form-control"
                    ],
                    "value" => "",
                    "extra_attr" => ""
                ]
            ],
            'titulo' => 'Código de Daños'
        ];

        $this->load->view('admin/list', $data);
    }

    //servicios
    public function new() 
    {
        if (!$this->form_validation->run('codigos_damage'))
        {
            print json_encode(['res'=>'bad', 'errors'=>$this->form_validation->error_array()]);
            exit();
        }
        $data = $this->input->post();
        if ( $this->General_Model->validateRow( ['codigo'=>$data['codigo']], 'codigos_damage') ) 
        {
            print json_encode(['res'=>'bad', 'errors'=>['El código de daño ingresado ya se encuentra registrado.']]);
            exit();
        }
        if ($this->CodigoDamage_Model->store($data)) {
            print json_encode(['res'=>'ok', 'msj'=>'Información guardada.', 'id'=>$data['codigo'], 'nombre'=>$data['codigo']]);
        } else {
            print json_encode(['res'=>'bad', 'errors'=>['Problemas para guardar el registro.']]);
        }
        
    }

    public function update($id)
    {
        if (!$this->form_validation->run('codigos_damage'))
        {
            print json_encode(['res'=>'bad', 'errors'=>$this->form_validation->error_array()]);
            exit();
        }
        $data = $this->input->post();
        if ( $this->General_Model->validateRow( ['codigo'=>$data['codigo']], 'codigos_damage', $id) ) 
        {
            print json_encode(['res'=>'bad', 'errors'=>['El código de daño ingresado ya se encuentra registrado.']]);
            exit();
        }
        if ($this->CodigoDamage_Model->update($data, $id)) {
            print json_encode(['res'=>'ok', 'msj'=>'Información actualizada.']);
        } else {
            print json_encode(['res'=>'bad', 'errors'=>['Problemas para actualizar el registro.']]);
        }
    }

    public function delete($id)
    {
        if ($this->CodigoDamage_Model->delete($id)) {
            print json_encode(['res'=>'ok', 'msj'=>'Información eliminado.']);
        } else {
            print json_encode(['res'=>'bad', 'errors'=>['Problemas para eliminar el registro.']]);
        }
    }
}

function pagination_general($total, $url, $per_page, $uri_segment){
    $config['total_rows'] = $total;
    $config["base_url"] = base_url().$url;
    $config['per_page'] = $per_page;
    $config['uri_segment'] = $uri_segment;
    $config['first_link'] = '<< Ir al primero';
    $config['last_link'] = 'Ir al ultimo >';
    $config['next_link'] = ' Siguiente ' . '&gt;';
    $config['prev_link'] = ' &lt;' . ' Atras';
    $config['full_tag_open'] = "<ul  class='pagination justify-content-center'>";
    $config['full_tag_close'] ="</ul>";
    $config['num_tag_open'] = '<li class="page-item">';
    $config['num_tag_close'] = '</li>';
    $config['cur_tag_open'] = "<li class='active'><a href='#'>";
    $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
    $config['next_tag_open'] = '<li class="page-item">';
    $config['next_tag_close'] = "</li>";
    $config['prev_tag_open'] = '<li class="page-item">';
    $config['prev_tag_close'] = "</li>";
    $config['first_tag_open'] = '<li class="page-item">';
    $config['first_tag_close'] = "</li>";
    $config['last_tag_open'] = '<li class="page-item">';
    $config['last_tag_close'] = "</li>";
    $config['use_page_numbers'] = TRUE;
    return $config;
}