<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Usuario_Model');
        $this->load->model('Perfil_Model');
        $this->load->model('General_Model');
    }

    public function index()
    {
        redirect(base_url().'usuarios/list'); 
    }

    public function list($offset=0)
    {
        if(!$this->session->logged_in) redirect(base_url()."home");        
        if (!in_array('2', $this->session->permisos)) redirect(base_url().'home/permisos');
        
        $this->load->model('General_Model');
        $servicio = $this->General_Model->modalOn('usuarios');
        $this->session->set_userdata(['menu_pos' => $this->input->post('menu_pos')]);
        
        $total = $this->Usuario_Model->getCountAll();
        $config = pagination_general($total, 'usuarios/list', 10, 3);
        $this->pagination->initialize($config);
        $offset_sql = $offset!=0 ? $offset * $config['per_page'] - $config['per_page'] : $offset;
        $list = $this->Usuario_Model->getAll( $config['per_page'], $offset_sql);

        $data = [
            'url' => 'usuarios',
            'ver_form' => '3',
            'nuevo_form' => '4',
            'editar_form' => '5',
            'eliminar_form' => '6',
            'columns' => [
                [
                    'key' => 'nombre',
                    'name' => 'Nombre'
                ],[
                    'key' => 'email',
                    'name' => 'Email'
                ],[
                    'key' => 'perfil',
                    'name' => 'Perfiles'
                ],[
                    'key' => 'btn',
                    'name' => 'Acción'
                ],[
                    'key' => '',
                    'name' => 'Ver'
                ],[
                    'key' => '',
                    'name' => 'Editar'
                ],[
                    'key' => '',
                    'name' => 'Eliminar'
                ]
            ],
            'titulo_view' => 'Usuarios',
            'titulo_singular' => 'Usuario',
            'service' => $servicio,
            'lists' => $list,
            'offset' => $offset,
            'forms' => [
                [
                    "form_control" => "input",
                    "label" => "Nombre<span class='text-danger'>*</span>",
                    "column" => "6",
                    "for" => "nombre",
                    "attr"  => [
                        "type" => "text",
                        "name" => "nombre",
                        "id"   => "nombre",
                        "placeholder" => "Nombre",
                        "required" => "true",
                        "class" => "form-control"
                    ],
                    "value" => "",
                    "extra_attr" => ""
                ],
                [
                    "form_control" => "input",
                    "label" => "Correo electrónico<span class='text-danger'>*</span>",
                    "for" => "email",
                    "column" => "6",
                    "attr" => [
                        "type" => "text",
                        "name" => "email",
                        "id" => "email",
                        "placeholder" => "Correo electrónico",
                        "required" => "true",
                        "class" => "form-control"
                    ],
                    "value" => "",
                    "extra_attr" => ""
                ],
                [
                    "form_control" => "input",
                    "label" => "Contraseña<span class='text-danger'>*</span>",
                    "for" => "password",
                    "column" => "6",
                    "attr" => [
                        "type" => "password",
                        "name" => "password",
                        "id" => "password",
                        "placeholder" => "Contraseña",
                        "required" => "true",
                        "class" => "form-control"
                    ],
                    "value" => "",
                    "extra_attr" => ""
                ],
                [
                    "form_control" => "radio",
                    "label" => "Perfiles<span class='text-danger'>*</span>",
                    "for" => "id_perfil",
                    "required" => "true",
                    "column" => "3",
                    "attr" => [
                        "class" => "form-check-input",
                        "name" => "id_perfil",
                        "id" => "id_perfil"
                    ],
                    'items' => $this->Perfil_Model->getAll()
                ],
                [
                    "form_control" => "checkbox",
                    "for" => "status_terminos",
                    "required" => "true",
                    "column" => "3",
                    "attr" => [
                        "texto" => "Aceptar los Términos y condiciones<span class='text-danger'>*</span>",
                        "id" => "status_terminos"
                    ]
                ]
            ],
            'titulo' => 'Usuarios'
        ];
        
        $this->load->view('admin/usuarios/list', $data);
    }

    //servicios
    public function new() 
    {
        $r = '';
        $data = $this->input->post();
        $data['password'] = hash('sha256', $data['password']);
        if ($this->General_Model->validateRow( ['email'=>$data['email']], 'usuarios') ) 
        {
            $r = @json_encode(['res'=>'bad', 'errors'=>['El correo electrónico ingresado ya se encuentra registrado.']]);
            print "<script>parent.postMessage( '".$r."' , '".base_url()."')</script>";
            exit();
        }
        list($type, $firma) = explode(';', $data['firma']);
        list(, $firma)      = explode(',', $firma);
        $firma = base64_decode($firma);
        $url_firma = 'public/firma/'.strtotime("now").'.png';
        file_put_contents(FCPATH.$url_firma, $firma);
        if (!file_exists($url_firma)) {
            $r = @json_encode(['res'=>'bad', 'errors'=>['Problemas para guardar la firma.']]);
            print "<script>parent.postMessage( '".$r."' , '".base_url()."')</script>";
            exit();
        }
        $data['url_firma'] = $url_firma;
        unset($data['firma']);
        if ($this->Usuario_Model->store($data)) {
            $r = @json_encode(['res'=>'ok', 'msj'=>'Información guardada.']);
        } else {
            $r = @json_encode(['res'=>'bad', 'errors'=>['Problemas para guardar el registro.']]);
        }
        print "<script>parent.postMessage( '".$r."' , '".base_url()."')</script>";
    }

    public function update($id)
    {
        $r = '';
        $data = $this->input->post();
        if ($data['password']!='') {
            $data['password'] = hash('sha256', $data['password']);
        }else{
            unset($data['password']);
        }        
        if ( $this->General_Model->validateRow( ['email'=>$data['email']], 'usuarios', $id) ) 
        {
            $r = @json_encode(['res'=>'bad', 'errors'=>['El correo electrónico ingresado ya se encuentra registrado.']]);
            print "<script>parent.postMessage( '".$r."' , '".base_url()."')</script>";
            exit();
        }
        if ($data['firma']!='') {
            list($type, $firma) = explode(';', $data['firma']);
            list(, $firma)      = explode(',', $firma);
            $firma = base64_decode($firma);
            $url_firma = 'public/firma/'.strtotime("now").'.png';
            file_put_contents(FCPATH.$url_firma, $firma);
            if (!file_exists($url_firma)) {
                $r = @json_encode(['res'=>'bad', 'errors'=>['Problemas para guardar la firma.']]);
                print "<script>parent.postMessage( '".$r."' , '".base_url()."')</script>";
                exit();
            }else{
                $old_url = $this->Usuario_Model->getFirmaById($id);
                unlink(FCPATH.$old_url[0]->url_firma);
            }
            $data['url_firma'] = $url_firma;
        }
        unset($data['firma']);
        if ($this->Usuario_Model->update($data, $id)) {
            $r = @json_encode(['res'=>'ok', 'msj'=>'Información actualizada.']);
        } else {
            $r = @json_encode(['res'=>'bad', 'errors'=>['Problemas para actualizar el registro.']]);
        }
        print "<script>parent.postMessage( '".$r."' , '".base_url()."')</script>";
    }

    public function delete($id)
    {
        if ($this->Usuario_Model->delete($id)) {
            print json_encode(['res'=>'ok', 'msj'=>'Información eliminado.']);
        } else {
            print json_encode(['res'=>'bad', 'errors'=>['Problemas para eliminar el registro.']]);
        }
    }
}

function pagination_general($total, $url, $per_page, $uri_segment){
    $config['total_rows'] = $total;
    $config["base_url"] = base_url().$url;
    $config['per_page'] = $per_page;
    $config['uri_segment'] = $uri_segment;
    $config['first_link'] = '<< Ir al primero';
    $config['last_link'] = 'Ir al ultimo >';
    $config['next_link'] = ' Siguiente ' . '&gt;';
    $config['prev_link'] = ' &lt;' . ' Atras';
    $config['full_tag_open'] = "<ul  class='pagination justify-content-center'>";
    $config['full_tag_close'] ="</ul>";
    $config['num_tag_open'] = '<li class="page-item">';
    $config['num_tag_close'] = '</li>';
    $config['cur_tag_open'] = "<li class='active'><a href='#'>";
    $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
    $config['next_tag_open'] = '<li class="page-item">';
    $config['next_tag_close'] = "</li>";
    $config['prev_tag_open'] = '<li class="page-item">';
    $config['prev_tag_close'] = "</li>";
    $config['first_tag_open'] = '<li class="page-item">';
    $config['first_tag_close'] = "</li>";
    $config['last_tag_open'] = '<li class="page-item">';
    $config['last_tag_close'] = "</li>";
    $config['use_page_numbers'] = TRUE;
    return $config;
}