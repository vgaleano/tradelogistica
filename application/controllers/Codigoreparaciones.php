<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Codigoreparaciones extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('CodigoReparacion_Model');
        $this->load->model('CodigoDamage_Model');
        $this->load->model('CodigoReparacionDamage_Model');
        $this->load->model('General_Model');
    }

    public function index()
    {
        redirect(base_url().'codigoreparaciones/list');
    }

    public function list($offset=0)
    {
        if(!$this->session->logged_in) redirect(base_url()."home");        
        if (!in_array('52', $this->session->permisos)) redirect(base_url().'home/permisos');
        
        $this->load->model('General_Model');
        $servicio = $this->General_Model->modalOn('codigoreparaciones');
        $this->session->set_userdata(['menu_pos' => $this->input->post('menu_pos')]);
        
        $total = $this->CodigoReparacion_Model->getCountAll();
        $config = pagination_general($total, 'codigoreparaciones/list', 10, 3);
        $this->pagination->initialize($config);
        $offset_sql = $offset!=0 ? $offset * $config['per_page'] - $config['per_page'] : $offset;
        $list = $this->CodigoReparacion_Model->getAll( $config['per_page'], $offset_sql);
        if (count($list)>0) {
            foreach ($list as $k => $v) {
                $list[$k]->damages = $this->CodigoReparacionDamage_Model->getByCodigoReparacion($v->codigo);
            }
        }

        $data = [
            'url' => 'codigoreparaciones',
            'ver_form' => '53',
            'nuevo_form' => '54',
            'editar_form' => '55',
            'eliminar_form' => '56',
            'columns' => [
                [
                    'key' => 'codigo',
                    'name' => 'Código'
                ],[
                    'key' => 'descripcion',
                    'name' => 'Descripción'
                ],[
                    'key' => 'traduccion',
                    'name' => 'Traduccion'
                ],
                [
                    'key' => 'damages',
                    'name' => 'Código de daños'
                ],[
                    'key' => 'btn',
                    'name' => 'Acción'
                ],[
                    'key' => '',
                    'name' => 'Editar'
                ],[
                    'key' => '',
                    'name' => 'Eliminar'
                ]
            ],
            'titulo_view' => 'Código de reparaciones',
            'titulo_singular' => 'Código de reparación',
            'service' => $servicio,
            'lists' => $list,
            'offset' => $offset,
            'forms' => [
                [
                    "form_control" => "input",
                    "label" => "Código<span class='text-danger'>*</span>",
                    "column" => "6",
                    "for" => "codigo",
                    "attr"  => [
                        "type" => "text",
                        "name" => "codigo",
                        "id"   => "codigo",
                        "placeholder" => "Código",
                        "required" => "true",
                        "class" => "form-control"
                    ],
                    "value" => "",
                    "extra_attr" => ""
                ],
                [
                    "form_control" => "textarea",
                    "label" => "Descripción<span class='text-danger'>*</span>",
                    "column" => "6",
                    "for" => "descripcion",
                    "attr"  => [
                        "type" => "text",
                        "name" => "descripcion",
                        "id"   => "descripcion",
                        "placeholder" => "Descripción",
                        "required" => "true",
                        "class" => "form-control"
                    ],
                    "value" => "",
                    "extra_attr" => ""
                ],
                [
                    "form_control" => "textarea",
                    "label" => "Traducción<span class='text-danger'>*</span>",
                    "column" => "6",
                    "for" => "traduccion",
                    "attr"  => [
                        "type" => "text",
                        "name" => "traduccion",
                        "id"   => "traduccion",
                        "placeholder" => "Traducción",
                        "required" => "true",
                        "class" => "form-control"
                    ],
                    "value" => "",
                    "extra_attr" => ""
                ],
                [
                    "form_control" => "select-add",
                    "label" => "Códigos de daños<span class='text-danger'>*</span> <br class='d-none d-sm-block'><button type='button' class='btn btn-success btn-sm addItem btn-popover' data-content='Agregar Campo' data-placement='top'><i class='mdi mdi-plus'></i> <span class='d-none d-sm-block'>Agregar Campo</span></button> <button type='button' class='btn btn-sm btn-info add-item btn-popover' data-content='Crear Código de daño' data-placement='bottom' " . (in_array("49", $this->session->permisos)?"":"disabled") . "><i class='mdi mdi-plus'></i> <span class='d-none d-sm-block'>Crear Código de daño</span></button>",
                    "for" => "codigo_damage",
                    "required" => "true",
                    "column" => "6",
                    "attr" => [
                        "class" => "form-control codigo_damage",
                        "name" => "codigo_damage",
                        //"id" => "codigo_damage",
                        "required" => "true"
                    ],
                    'options' => $this->CodigoDamage_Model->getAllFormControl(),
                    "btn-info" => true
                ]
            ],
            'form_add' => [
                [
                    "form_control" => "input",
                    "label" => "Código<span class='text-danger'>*</span>",
                    "column" => "12",
                    "for" => "codigo",
                    "attr"  => [
                        "type" => "text",
                        "name" => "codigo",
                        "id"   => "codigo",
                        "placeholder" => "Código",
                        "required" => "true",
                        "class" => "form-control"
                    ],
                    "value" => "",
                    "extra_attr" => ""
                ],
                [
                    "form_control" => "textarea",
                    "label" => "Descripción<span class='text-danger'>*</span>",
                    "column" => "12",
                    "for" => "descripcion",
                    "attr"  => [
                        "type" => "text",
                        "name" => "descripcion",
                        //"id"   => "descripcion",
                        "placeholder" => "Descripción",
                        "required" => "true",
                        "class" => "form-control"
                    ],
                    "value" => "",
                    "extra_attr" => ""
                ],
                [
                    "form_control" => "textarea",
                    "label" => "Traducción<span class='text-danger'>*</span>",
                    "column" => "12",
                    "for" => "traduccion",
                    "attr"  => [
                        "type" => "text",
                        "name" => "traduccion",
                        //"id"   => "traduccion",
                        "placeholder" => "Traducción",
                        "required" => "true",
                        "class" => "form-control"
                    ],
                    "value" => "",
                    "extra_attr" => ""
                ]
            ],
            'form_add_titulo' => 'Código de daño',
            'form_add_url' => 'codigodamage',
            'titulo' => 'Código de reparación'
        ];
        
        $this->load->view('admin/codigo_reparaciones/list', $data);
    }

    //servicios
    public function new() 
    {
        if (!$this->form_validation->run('codigos_reparacion'))
        {
            print json_encode(['res'=>'bad', 'errors'=>$this->form_validation->error_array()]);
            exit();
        }
        $data = $this->input->post();
        if ( $this->General_Model->validateRow( ['codigo'=>$data['codigo']], 'codigos_reparacion') ) 
        {
            print json_encode(['res'=>'bad', 'errors'=>['El código de reparación ingresado ya se encuentra registrado.']]);
            exit();
        }
        $list_damage = $data['list_damage'];
        unset($data['list_damage']);
        //repetidos en el post
        $unique_ = array_unique(array_column($list_damage, 'codigo_damage'));
        if ( count($unique_) < count($list_damage) ) {
            print json_encode(['res'=>'bad', 'errors'=>['Hay codigos de daños repetidos para el código de reparación.']]);
            exit();
        }
        /*//repetidos en tabla
        $repetidos_ = objectToArray( $this->CodigoReparacionDamage_Model->getRepetidosCodigoDamage(implode(",",$unique_), $data['codigo']) );
        if ($repetidos_) {
            print json_encode(['res'=>'bad', 'errors'=>['Hay codigos de daños repetidos para el código de reparación: '.implode(", ", array_column($repetidos_
            , 'codigo_damage')).'.']]);
            exit();
        }*/
        $id = $this->CodigoReparacion_Model->store($data);
        if ($id) {
            if ($this->CodigoReparacionDamage_Model->multiStore($list_damage)) {
                print json_encode(['res'=>'ok', 'msj'=>'Información guardada.']);
            }else{
                print json_encode(['res'=>'bad', 'errors'=>['Problemas para guardar el registro.']]);
            }
        } else {
            print json_encode(['res'=>'bad', 'errors'=>['Problemas para guardar el registro.']]);
        }      
    }

    public function update($id)
    {
        if (!$this->form_validation->run('codigos_reparacion'))
        {
            print json_encode(['res'=>'bad', 'errors'=>$this->form_validation->error_array()]);
            exit();
        }
        $data = $this->input->post();
        if ( $this->General_Model->validateRow( ['codigo'=>$data['codigo']], 'codigos_reparacion', $id) ) 
        {
            print json_encode(['res'=>'bad', 'errors'=>['El código de reparación ingresado ya se encuentra registrado.']]);
            exit();
        }
        $list_damage = $data['list_damage'];
        $ids_ = array_column($list_damage, 'id');
        unset($data['list_damage']);
        //repetidos en el post
        $unique_ = array_unique(array_column($list_damage, 'codigo_damage'));
        if ( count($unique_) < count($list_damage) ) {
            print json_encode(['res'=>'bad', 'errors'=>['Hay codigos de daños repetidos para el código de reparación.']]);
            exit();
        }
        /*//repetidos en tabla
        $repetidos_ = objectToArray( $this->CodigoReparacionDamage_Model->getRepetidosCodigoDamage(implode(",",$unique_), $data['codigo'], $ids_) );
        if ($repetidos_) {
            print json_encode(['res'=>'bad', 'errors'=>['Hay codigos de daños repetidos para el código de reparación: '.implode(", ", array_column($repetidos_
            , 'codigo_damage')).'.']]);
            exit();
        }*/
        if (!$this->CodigoReparacion_Model->update($data, $id)) {
            print json_encode(['res'=>'bad', 'errors'=>['Problemas para actualizar el registro.']]);
            exit();
        }
        //eliminar los que se quitaron en el list
        $all_ = $this->CodigoReparacionDamage_Model->getByIdCodigoReparacion($id);
        $all_ = array_column(objectToArray($all_), 'id');
        $diff_ = array_diff($all_, $ids_);
        if (count($diff_)>0) {
            $this->CodigoReparacionDamage_Model->delete($diff_);
        }
        foreach ($list_damage as $k => $v) {            
            if (array_key_exists('id', $v)) {
                $data = [ 'codigo_reparacion' => $v['codigo_reparacion'], 'codigo_damage' => $v['codigo_damage'] ];
                if (!$this->CodigoReparacionDamage_Model->update($data, $v['id'])) {
                    print json_encode(['res'=>'bad', 'errors'=>['Problemas para actualizar el registro.']]);
                    exit();
                }
            }else{
                $data = $list_damage[$k];
                if (!$this->CodigoReparacionDamage_Model->store($data)) {
                    print json_encode(['res'=>'bad', 'errors'=>['Problemas para actualizar el registro.']]);
                    exit();
                }
            }
        }
        print json_encode(['res'=>'ok', 'msj'=>'Información actualizado.']);
    }

    public function delete($id)
    {
        if ($this->CodigoReparacion_Model->delete($id)) {
            print json_encode(['res'=>'ok', 'msj'=>'Información eliminado.']);
        } else {
            print json_encode(['res'=>'bad', 'errors'=>['Problemas para eliminar el registro.']]);
        }
    }
}

function pagination_general($total, $url, $per_page, $uri_segment){
    $config['total_rows'] = $total;
    $config["base_url"] = base_url().$url;
    $config['per_page'] = $per_page;
    $config['uri_segment'] = $uri_segment;
    $config['first_link'] = '<< Ir al primero';
    $config['last_link'] = 'Ir al ultimo >';
    $config['next_link'] = ' Siguiente ' . '&gt;';
    $config['prev_link'] = ' &lt;' . ' Atras';
    $config['full_tag_open'] = "<ul  class='pagination justify-content-center'>";
    $config['full_tag_close'] ="</ul>";
    $config['num_tag_open'] = '<li class="page-item">';
    $config['num_tag_close'] = '</li>';
    $config['cur_tag_open'] = "<li class='active'><a href='#'>";
    $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
    $config['next_tag_open'] = '<li class="page-item">';
    $config['next_tag_close'] = "</li>";
    $config['prev_tag_open'] = '<li class="page-item">';
    $config['prev_tag_close'] = "</li>";
    $config['first_tag_open'] = '<li class="page-item">';
    $config['first_tag_close'] = "</li>";
    $config['last_tag_open'] = '<li class="page-item">';
    $config['last_tag_close'] = "</li>";
    $config['use_page_numbers'] = TRUE;
    return $config;
}

function objectToArray ( $object ) {
    if(!is_object($object) && !is_array($object)) {
      return $object;
    }  
    return array_map( 'objectToArray', (array) $object );
}