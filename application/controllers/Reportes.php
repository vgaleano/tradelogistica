<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once FCPATH.'application/third_party/spout/src/Spout/Autoloader/autoload.php';
use Box\Spout\Writer\Common\Creator\WriterEntityFactory;
use Box\Spout\Common\Entity\Row;
use Box\Spout\Writer\Common\Creator\Style\StyleBuilder;
use Box\Spout\Common\Entity\Style\CellAlignment;
use Box\Spout\Common\Entity\Style\Color;
date_default_timezone_set('America/Bogota');

class Reportes extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Inspecciones_Model');
    }

    public function index()
    {
        $this->session->set_userdata(['menu_pos' => $this->input->post('menu_pos')]);
        $data = [
            'titulo' => 'Reportes'
        ];
        $this->load->view('admin/reportes/list', $data);
    }

    public function exportDataContenedorPatio()
    {
        $fecha_inicio = $this->input->post('fecha_inicio');
        $fecha_fin = $this->input->post('fecha_fin');
        $fecha_inicio = $fecha_inicio!='' ? $fecha_inicio : false;
        $fecha_fin = $fecha_fin!='' ? $fecha_fin : false;

        $this->load->model('Inspecciones_Model');

        $contenedores = $this->Inspecciones_Model->getContenedoresEstanPuerto($fecha_inicio, $fecha_fin);
        $contenedores = objectToArray($contenedores);

        $clasificaciones = array_unique(array_column($contenedores, 'clasificacion'));

        $hoy = new DateTime();
        
        $writer = WriterEntityFactory::createXLSXWriter();
		// $writer = WriterEntityFactory::createODSWriter();
		// $writer = WriterEntityFactory::createCSVWriter();
		$url='export_excel/Reporte_contenedores_patio.xlsx';
		$fileName = FCPATH.$url;
		//chmod($fileName, 0777);
		$writer->openToFile($fileName); // write data to a file or to a PHP stream
		//$writer->openToBrowser($fileName); // stream data directly to the browser

		$style_ok = (new StyleBuilder())
           ->setFontBold()
           ->setFontSize(10)
           //->setCellAlignment(CellAlignment::CENTER)
           ->build();

        $style_titulos = (new StyleBuilder())
           ->setFontBold()
           ->setFontSize(10)
           //->setCellAlignment(CellAlignment::CENTER)
           ->setBackgroundColor(Color::BLUE)
           ->build();
        
        $style_total = (new StyleBuilder())
           ->setFontBold()
           ->setFontSize(10)
           //->setCellAlignment(CellAlignment::CENTER)
           ->setBackgroundColor(Color::YELLOW)
           ->build();

		if (count($contenedores)>0) {
			$singleRow = WriterEntityFactory::createRow([ WriterEntityFactory::createCell('DETALLE CONTENEDORES EN PATIO A LA FECHA') ], $style_ok);
			$writer->addRow($singleRow);

			$keys = ['CLASIFICACION', 'CONTENEDOR', 'TIPO DE MOVIMIENTO', 'CANTIDAD', 'TAMAÑO', 'CLIENTE', 'ESTADO ACTUAL', 'DIAS EN PATIO', 'EN PATIO','Suma de MOVIMIENTO'];
			$rowFromValues = WriterEntityFactory::createRowFromArray($keys, $style_ok);
            $writer->addRow($rowFromValues);
            
            for ($i=0; $i < count($clasificaciones); $i++) { 
                $id_camp = $clasificaciones[$i];
                $activas=array_filter($contenedores, function ($tT) use ($id_camp) 
                {
                    if ($id_camp==$tT['clasificacion']) {
                        return $tT;
                    }
                });
                $activas=array_values($activas);
                foreach ($activas as $contenedor) {
                    list($fecha_creado, $hora) = explode(' ', $contenedor['created_at']);
                    $date2 = new DateTime($fecha_creado);
                    $diff = $date2->diff($hoy); 
                    $temp = [
                        $contenedor['clasificacion'],
                        $contenedor['codigo_contenedor'],
                        $contenedor['estado_in_out']=='1' ? 'INGRESO CONTENEDOR TERRESTRE':'SALIDA CONTENEDOR TERRESTRE',
                        '1',
                        str_replace(['"',"'"],'',$contenedor['largo']),
                        $contenedor['cliente'],
                        $contenedor['clasificacion']=='Dañado'?'No Apto':'Apto',
                        $diff->days,
                        'Si',
                        $contenedor['estado_in_out']
                    ];
                    $rowFromValues = WriterEntityFactory::createRowFromArray($temp);
				    $writer->addRow($rowFromValues);
                }
                $rowFromValues = WriterEntityFactory::createRowFromArray(['Total ' . $id_camp, '','','','','','','','',count($activas)], $style_total);
                $writer->addRow($rowFromValues);
            }
		}		

		$writer->close();

		print json_encode(array('res'=>'ok', 'url'=>$url));
    }

    public function movimientosdiarios()
    {
        $fecha_inicio = $this->input->post('fecha_inicio');
        $fecha_fin = $this->input->post('fecha_fin');
        $fecha_inicio = $fecha_inicio!='' ? $fecha_inicio : false;
        $fecha_fin = $fecha_fin!='' ? $fecha_fin : false;

        $this->load->model('Inspecciones_Model');

        $contenedores = $this->Inspecciones_Model->getMovimientosDiarios($fecha_inicio, $fecha_fin);
        $contenedores = objectToArray($contenedores);
        $codigos_contenedor = array_unique(array_column($contenedores, 'codigo_contenedor'));

        $hoy = new DateTime();
        
        $writer = WriterEntityFactory::createXLSXWriter();
		// $writer = WriterEntityFactory::createODSWriter();
		// $writer = WriterEntityFactory::createCSVWriter();
		$url='export_excel/Reporte_movimientos_diario.xlsx';
		$fileName = FCPATH.$url;
		//chmod($fileName, 0777);
		$writer->openToFile($fileName); // write data to a file or to a PHP stream
		//$writer->openToBrowser($fileName); // stream data directly to the browser

		$style_ok = (new StyleBuilder())
           ->setFontBold()
           ->setFontSize(10)
           //->setCellAlignment(CellAlignment::CENTER)
           ->build();

        $style_titulos = (new StyleBuilder())
           ->setFontBold()
           ->setFontSize(10)
           //->setCellAlignment(CellAlignment::CENTER)
           ->setBackgroundColor(Color::BLUE)
           ->build();
        
        $style_total = (new StyleBuilder())
           ->setFontBold()
           ->setFontSize(10)
           //->setCellAlignment(CellAlignment::CENTER)
           ->setBackgroundColor(Color::YELLOW)
           ->build();

		if (count($contenedores)>0) {
			$singleRow = WriterEntityFactory::createRow([ WriterEntityFactory::createCell('MOVIMIENTOS') ], $style_ok);
			$writer->addRow($singleRow);

            $rowFromValues = WriterEntityFactory::createRowFromArray(['Suma de MOVIMIENTO', '','','','', 'TIPO DE MOVIMIENTO', '',''], $style_ok);
            $writer->addRow($rowFromValues);
			$keys = ['CONTENEDOR', 'FECHA MOVIMIENTO', 'BOOKING/COMODATO', 'CLIENTE', 'CLASIFICACION INICIAL', 'INGRESO CONTENEDOR TERRESTE', 'SALIDA CONTENEDOR TERRESTE', 'Total general'];
			$rowFromValues = WriterEntityFactory::createRowFromArray($keys, $style_ok);
            $writer->addRow($rowFromValues);

            $total_ingreso = 0;
            $total_salida = 0;
            
            for ($i=0; $i < count($codigos_contenedor); $i++) { 
                $id_camp = $codigos_contenedor[$i];
                $activas=array_filter($contenedores, function ($tT) use ($id_camp) 
                {
                    if ($id_camp==$tT['codigo_contenedor']) {
                        return $tT;
                    }
                });
                $activas=array_values($activas);
                foreach ($activas as $contenedor) {
                    $temp = [
                        $contenedor['codigo_contenedor'],
                        $contenedor['created_at'],
                        '',
                        $contenedor['cliente'],
                        $contenedor['clasificacion'],
                        $contenedor['estado_in_out']=='1' ? '1':'',
                        $contenedor['estado_in_out']=='-1' ? '-1':'',
                        $contenedor['estado_in_out']=='1' ? '1':'-1'
                    ];
                    $total_ingreso = $total_ingreso + ($contenedor['estado_in_out']=='1'?1:0);
                    $total_salida = $total_salida + ($contenedor['estado_in_out']=='-1'?-1:0);
                    $rowFromValues = WriterEntityFactory::createRowFromArray($temp);
				    $writer->addRow($rowFromValues);
                }
                $rowFromValues = WriterEntityFactory::createRowFromArray(['Total general', '','','','',$total_ingreso, $total_salida, ($total_salida+$total_ingreso)], $style_total);
                $writer->addRow($rowFromValues);
            }
		}		

		$writer->close();

		print json_encode(array('res'=>'ok', 'url'=>$url));
    }
}

function objectToArray ( $object ) {
    if(!is_object($object) && !is_array($object)) {
      return $object;
    }  
    return array_map( 'objectToArray', (array) $object );
}