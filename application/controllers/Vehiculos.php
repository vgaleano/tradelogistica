<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vehiculos extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Vehiculo_Model');        
        $this->load->model('VehiculoConductor_Model');
        $this->load->model('General_Model');
    }

    public function index()
    {
        redirect(base_url().'vehiculos/list');
    }

    public function list($offset=0)
    {
        if(!$this->session->logged_in) redirect(base_url()."home");        
        if (!in_array('27', $this->session->permisos)) redirect(base_url().'home/permisos');
        
        $this->load->model('General_Model');
        $servicio = $this->General_Model->modalOn('vehiculos');
        $this->session->set_userdata(['menu_pos' => $this->input->post('menu_pos')]);
        
        $this->load->model('Conductor_Model');
        $this->load->model('Transportador_Model');

        $total = $this->Vehiculo_Model->getCountAll();
        $config = pagination_general($total, 'vehiculos/list', 10, 3);
        $this->pagination->initialize($config);
        $offset_sql = $offset!=0 ? $offset * $config['per_page'] - $config['per_page'] : $offset;
        $list = $this->Vehiculo_Model->getAll( $config['per_page'], $offset_sql);
        if (count($list)>0) {
            foreach ($list as $k => $v) {
                $list[$k]->conductores = $this->VehiculoConductor_Model->getByIdVehiculo($v->id);
            }
        }
        
        $data = [
            'url' => 'vehiculos',
            'ver_form' => '28',
            'nuevo_form' => '29',
            'editar_form' => '30',
            'eliminar_form' => '31',
            'columns' => [
                [
                    'key' => 'placa',
                    'name' => 'Placa'
                ],[
                    'key' => 'conductor',
                    'name' => 'Conductor'
                ],[
                    'key' => 'btn',
                    'name' => 'Acción'
                ],[
                    'key' => '',
                    'name' => 'Editar'
                ],[
                    'key' => '',
                    'name' => 'Eliminar'
                ]
            ],
            'titulo_view' => 'Vehículos',
            'titulo_singular' => 'Vehículo',
            'service' => $servicio,
            'lists' => $list,
            'offset' => $offset,
            'forms' => [
                [
                    "form_control" => "input",
                    "label" => "Placa",
                    "column" => "4",
                    "for" => "placa",
                    "attr"  => [
                        "type" => "text",
                        "name" => "placa",
                        "id"   => "placa",
                        "placeholder" => "Placa",
                        "required" => "true",
                        "class" => "form-control"
                    ],
                    "value" => "",
                    "extra_attr" => ""
                ],
                [
                    "form_control" => "select-add",
                    "label" => "Conductor <button type='button' class='btn btn-success btn-sm addItem btn-popover' data-content='Agregar Campo' data-placement='top'><i class='mdi mdi-plus'></i> <span class='d-none d-sm-block'>Agregar Campo</span></button> <button type='button' class='btn btn-sm btn-info add-item btn-popover' data-content='Crear Conductor' data-placement='bottom' " . (in_array("24", $this->session->permisos)?"":"disabled") . "><i class='mdi mdi-plus'></i> <span class='d-none d-sm-block'>Crear Conductor</span></button>",
                    "for" => "id_conductor",
                    "required" => "true",
                    "column" => "6",
                    "attr" => [
                        "class" => "form-control id_conductor",
                        "name" => "id_conductor",
                        //"id" => "id_conductor",
                        "required" => "true"
                    ],
                    'options' => $this->Conductor_Model->getAllFormControl(),
                    "btn-info" => true,
                ]
            ],
            'form_add' => [
                [
                    "form_control" => "input",
                    "label" => "Nombre",
                    "column" => "12",
                    "for" => "conductor<span class=\'text-danger\'>*</span>",
                    "attr"  => [
                        "type" => "text",
                        "name" => "conductor",
                        "id"   => "conductor",
                        "placeholder" => "Nombre",
                        "required" => "true",
                        "class" => "form-control"
                    ],
                    "value" => "",
                    "extra_attr" => ""
                ],
                [
                    "form_control" => "input",
                    "label" => "Número de documento<span class=\'text-danger\'>*</span>",
                    "column" => "12",
                    "for" => "numero_documento",
                    "attr"  => [
                        "type" => "text",
                        "name" => "numero_documento",
                        "id"   => "numero_documento",
                        "placeholder" => "Número de documento",
                        "required" => "true",
                        "class" => "form-control"
                    ],
                    "value" => "",
                    "extra_attr" => "onkeypress=\'return onlyNumber(event)\'"
                ],
                [
                    "form_control" => "select",
                    "label" => "Transportador<span class=\'text-danger\'>*</span>",
                    "for" => "id_transportador",
                    "required" => "true",
                    "column" => "12",
                    "attr" => [
                        "class" => "form-control",
                        "name" => "id_transportador",
                        "id" => "id_transportador",
                        "required" => "true"
                    ],
                    'options' => $this->Transportador_Model->getAllFormControl()
                ]
            ],
            'form_add_titulo' => 'Conductor',
            'form_add_url' => 'conductores',
            'titulo' => 'Vehículos'
        ];

        $this->load->view('admin/vehiculos/list', $data);
    }

    //servicios
    public function new() 
    {
        if (!$this->form_validation->run('vehiculos'))
        {
            print json_encode(['res'=>'bad', 'errors'=>$this->form_validation->error_array()]);
            exit();
        }        
        $placa = $this->input->post('placa', true);
        if ( $this->General_Model->validateRow( ['placa'=>$placa], 'vehiculos') ) 
        {
            print json_encode(['res'=>'bad', 'errors'=>['La placa ingresada ya se encuentra registrado.']]);
            exit();
        }
        //busca repetidos en el post
        $list_conductor = $this->input->post('list_conductor');
        if ( count(array_unique(array_column($list_conductor, 'id_conductor'))) < count($list_conductor) ) {
            print json_encode(['res'=>'bad', 'errors'=>['Hay conductores repetidos para el vehículo.']]);
            exit();
        }
        $id_vehiculo = $this->Vehiculo_Model->store(['placa'=>$placa]);
        $list_ = $this->input->post('list_')!==null ? $this->input->post('list_') : [];
        if ($id_vehiculo) {
            foreach ($list_conductor as $k => $v) {
                $list_conductor[$k] = array_merge($list_conductor[$k], ['id_vehiculo'=>$id_vehiculo]);
            }
            if ($this->VehiculoConductor_Model->multiStore($list_conductor)) {
                print json_encode(['res'=>'ok', 'msj'=>'Información guardada.', 'id'=>$placa, 'nombre'=>$placa, 'list_'=>$list_]);
            }else{
                print json_encode(['res'=>'bad', 'errors'=>['Problemas para guardar el registro.']]);
            }
        } else {
            print json_encode(['res'=>'bad', 'errors'=>['Problemas para guardar el registro.']]);
        }
    }

    public function update($id)
    {
        if (!$this->form_validation->run('vehiculos'))
        {
            print json_encode(['res'=>'bad', 'errors'=>$this->form_validation->error_array()]);
            exit();
        }
        $placa = $this->input->post('placa', true);
        if ( $this->General_Model->validateRow( ['placa'=>$placa], 'vehiculos', $id) ) 
        {
            print json_encode(['res'=>'bad', 'errors'=>['El numero documento ingresado ya se encuentra registrado.']]);
            exit();
        }
        //busca repetidos en el post
        $list_conductor = $this->input->post('list_conductor');
        if ( count(array_unique(array_column($list_conductor, 'id_conductor'))) < count($list_conductor) ) {
            print json_encode(['res'=>'bad', 'errors'=>['Hay conductores repetidos para el vehículo.']]);
            exit();
        }
        //eliminar los que se quitaron en el list
        $all_ = $this->VehiculoConductor_Model->getAllByIdVehiculo($id);
        $all_ = array_column(objectToArray($all_), 'id');
        $ids_ = array_column($list_conductor, 'id');
        $diff_ = array_diff($all_, $ids_);
        if (count($diff_)>0) {
            $this->VehiculoConductor_Model->delete($diff_);
        }
        if ( $this->Vehiculo_Model->update(['placa'=>$placa], $id) ) {
            //actualizar o agregar el conductor 
            foreach ($list_conductor as $k => $v) {            
                if (array_key_exists('id', $v)) {
                    $data = [ 'id_conductor' => $v['id_conductor'] ];
                    if (!$this->VehiculoConductor_Model->update($data, $v['id'])) {
                        print json_encode(['res'=>'bad', 'errors'=>['Problemas para actualizar el registro.']]);
                        exit();
                    }
                }else{
                    $data = $list_conductor[$k];
                    if (!$this->VehiculoConductor_Model->store($data)) {
                        print json_encode(['res'=>'bad', 'errors'=>['Problemas para actualizar el registro.']]);
                        exit();
                    }
                }
            }
            print json_encode(['res'=>'ok', 'msj'=>'Información actualizado.']);
        } else {
            print json_encode(['res'=>'bad', 'errors'=>['Problemas para actualizar el registro.']]);
        }        
    }

    public function delete($id)
    {
        if ($this->Vehiculo_Model->delete($id)) {
            print json_encode(['res'=>'ok', 'msj'=>'Información eliminado.']);
        } else {
            print json_encode(['res'=>'bad', 'errors'=>['Problemas para eliminar el registro.']]);
        }       
    }
}

function pagination_general($total, $url, $per_page, $uri_segment){
    $config['total_rows'] = $total;
    $config["base_url"] = base_url().$url;
    $config['per_page'] = $per_page;
    $config['uri_segment'] = $uri_segment;
    $config['first_link'] = '<< Ir al primero';
    $config['last_link'] = 'Ir al ultimo >';
    $config['next_link'] = ' Siguiente ' . '&gt;';
    $config['prev_link'] = ' &lt;' . ' Atras';
    $config['full_tag_open'] = "<ul  class='pagination justify-content-center'>";
    $config['full_tag_close'] ="</ul>";
    $config['num_tag_open'] = '<li class="page-item">';
    $config['num_tag_close'] = '</li>';
    $config['cur_tag_open'] = "<li class='active'><a href='#'>";
    $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
    $config['next_tag_open'] = '<li class="page-item">';
    $config['next_tag_close'] = "</li>";
    $config['prev_tag_open'] = '<li class="page-item">';
    $config['prev_tag_close'] = "</li>";
    $config['first_tag_open'] = '<li class="page-item">';
    $config['first_tag_close'] = "</li>";
    $config['last_tag_open'] = '<li class="page-item">';
    $config['last_tag_close'] = "</li>";
    $config['use_page_numbers'] = TRUE;
    return $config;
}

function objectToArray ( $object ) {
    if(!is_object($object) && !is_array($object)) {
      return $object;
    }  
    return array_map( 'objectToArray', (array) $object );
}