<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once FCPATH.'application/third_party/spout/src/Spout/Autoloader/autoload.php';
use Box\Spout\Writer\Common\Creator\WriterEntityFactory;
use Box\Spout\Common\Entity\Row;
use Box\Spout\Writer\Common\Creator\Style\StyleBuilder;
use Box\Spout\Common\Entity\Style\CellAlignment;
use Box\Spout\Common\Entity\Style\Color;
require_once FCPATH.'application/libraries/dompdf/autoload.inc.php';
use Dompdf\Dompdf;// reference the Dompdf namespace
use Dompdf\Options;

date_default_timezone_set('America/Bogota');

class Inspecciones extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Inspecciones_Model');
        $this->load->model('General_Model');
        $this->load->model('Inspeccioninfodamage_Model');
    }

    public function index()
    {
        redirect(base_url().'inspecciones/list'); 
    }

    public function list($offset=0)
    {
        if(!$this->session->logged_in) redirect(base_url()."home");        
        if (!in_array('72', $this->session->permisos)) redirect(base_url().'home/permisos');
        
        $this->load->model('Transportador_Model');
        $this->load->model('Conductor_Model');
        $this->load->model('Clasificacion_Model');
        $this->load->model('Naviera_Model');
        $this->load->model('Cliente_Model');

        $this->session->set_userdata(['menu_pos' => $this->input->post('menu_pos')]);
        $this->session->set_userdata(['modal_on'=>false]);
        $this->session->unset_userdata("count_modal_on");

        $fecha_inicio = $this->input->post('fecha_inicio');
        $fecha_fin = $this->input->post('fecha_fin');
        $id_transportador = $this->input->post('id_transportador');
        $id_conductor = $this->input->post('id_conductor');
        $id_clasificacion = $this->input->post('id_clasificacion');
        $id_naviera = $this->input->post('id_naviera');
        $id_cliente = $this->input->post('id_cliente');
        $tipo_movimiento = $this->input->post('tipo_movimiento');

        $total = $this->Inspecciones_Model->getCountAll($fecha_inicio, $fecha_fin, $id_transportador, $id_conductor, $id_clasificacion, $id_naviera, $id_cliente, $tipo_movimiento);
        $config = pagination_general($total, 'inspecciones/list', 10, 3);
        $this->pagination->initialize($config);
        $offset_sql = $offset!=0 ? $offset * $config['per_page'] - $config['per_page'] : $offset;
        $list = $this->Inspecciones_Model->getAll($fecha_inicio, $fecha_fin, $id_transportador, $id_conductor, $id_clasificacion, $id_naviera, $id_cliente, $tipo_movimiento, $config['per_page'], $offset_sql);
        if (count($list)>0) {
            foreach ($list as $k => $v) {
                $list[$k]->no_damage = $this->Inspeccioninfodamage_Model->getByIdInspeccion($v->id);
            }
        }

        $data = [
            'url' => 'inspecciones',
            'ver_form' => '73',
            'nuevo_form' => '74',
            'editar_form' => '75',
            'eliminar_form' => '76',
            'columns' => [
                [
                    'key' => 'placa_vehiculo',
                    'name' => 'Placa'
                ],[
                    'key' => 'conductor',
                    'name' => 'Conductor'
                ],[
                    'key' => 'transportador',
                    'name' => 'Transportador'
                ],[
                    'key' => 'naviera',
                    'name' => 'Naviera'
                ],[
                    'key' => 'codigo_contenedor',
                    'name' => 'Contenedor'
                ],[
                    'key' => 'cliente',
                    'name' => 'Cliente'
                ],[
                    'key' => 'no_damage',
                    'name' => 'NO. Daños'
                ],[
                    'key' => 'btn',
                    'name' => 'Acción'
                ],[
                    'key' => '',
                    'name' => 'Descargar PDF'
                ],[
                    'key' => '',
                    'name' => 'Editar'
                ],[
                    'key' => '',
                    'name' => 'Eliminar'
                ]
            ],
            'titulo_view' => 'Inspecciones',
            'titulo_singular' => 'Inspección',
            'service' => isset($_POST['service']) ? $_POST['service'] : '',
            'lists' => $list,
            'offset' => $offset,
            'view_page' => true,
            'titulo' => 'Inspecciones',
            'array_transportador' => $this->Transportador_Model->getAllFormControl(),
            'array_conductor' => $this->Conductor_Model->getAllSelect(),
            'array_clasificacion' => $this->Clasificacion_Model->getAllFormControl(),
            'array_naviera' => $this->Naviera_Model->getAllSelect(),
            'array_cliente' => $this->Cliente_Model->getAllFormControl()
        ];
        
        $this->load->view('admin/inspecciones/list', $data);
    }

    public function create() 
    {
        if(!$this->session->logged_in) redirect(base_url()."home");        
        if (!in_array('74', $this->session->permisos)) redirect(base_url().'home/permisos');

        $this->load->model('Conductor_Model');
        $this->load->model('VehiculoConductor_Model');
        $this->load->model('CodigoReparacionDamage_Model');
        $this->load->model('CodigoIso_Model');
        $this->load->model('Clasificacion_Model');
        $this->load->model('Transportador_Model');
        $this->load->model('Cliente_Model');
        $this->load->model('Naviera_Model');
        $this->load->model('CodigoResponsabilidad_Model');
        $this->load->model('CodigoDamage_Model');
        $this->load->model('ControlDigitosContenedor_Model');
        $this->load->model('NavieraCodigo_Model');

        $this->session->set_userdata(['menu_pos' => $this->input->post('menu_pos')]);
        $this->session->set_userdata(['modal_on'=>false]);
        $this->session->unset_userdata("count_modal_on");

        $control_digitos_contenedor = objectToArray( $this->ControlDigitosContenedor_Model->getAll() );
        $letras = array_column($control_digitos_contenedor, 'letra');
        $valores = array_column($control_digitos_contenedor, 'valor');
        $control_digitos_contenedor = array_combine($letras, $valores);

        $data = [
            'url' => 'inspecciones',
            'titulo_view' => 'Crear Inspección',
            'list_conductores' => $this->Conductor_Model->getAllSelect(),
            'list_vehiculos' => $this->VehiculoConductor_Model->getAllSelect(),
            'list_codigo_reparacion' => $this->CodigoReparacionDamage_Model->getAll(),
            'list_naviera_codigo' => $this->NavieraCodigo_Model->getAll(),
            'forms_1' => [
                [
                    "form_control" => "input",
                    "label" => "CONTAINER No.<span class='text-danger'>*</span> <button type='button' class='btn btn-sm btn-info add-item btn-popover' data-url='contenedores' data-toggle='modal' data-target='#staticBackdrop' data-content='Crear Contenedor' data-placement='top' " . (in_array('44', $this->session->permisos)?'':'disabled') . "><i class='mdi mdi-plus'></i> <span class='d-none d-sm-block'>Crear Contenedor</span></button>",
                    "column" => "12",
                    "for" => "codigo_contenedor",
                    "attr"  => [
                        "type" => "text",
                        "name" => "codigo_contenedor",
                        "id"   => "codigo_contenedor",
                        "placeholder" => "Código Container",
                        "required" => "true",
                        "class" => "form-control"
                    ],
                    "value" => "",
                    "extra_attr" => "",
                    "btn-info" => true
                ],
                [
                    "form_control" => "select",
                    "label" => "TIPO<span class='text-danger'>*</span> <br class='d-none d-sm-block'> <button type='button' class='btn btn-sm btn-info add-item btn-popover' data-url='codigoiso' data-toggle='modal' data-target='#staticBackdrop' data-content='Crear Código ISO' data-placement='top' " . (in_array("39", $this->session->permisos)?"":"disabled") . "><i class='mdi mdi-plus'></i> <span class='d-none d-sm-block'>Crear Código ISO</span></button>",
                    "for" => "id_tipo",
                    "required" => "true",
                    "column" => "6",
                    "attr" => [
                        "class" => "form-control",
                        "name" => "id_tipo",
                        "id" => "id_tipo",
                        "required" => "true",
                        "disabled" => "true"
                    ],
                    'options' => $this->CodigoIso_Model->getAllFormControl(),
                    "btn-info" => true
                ],
                [
                    "form_control" => "select",
                    "label" => "Clasificación<span class='text-danger'>*</span> <br class='d-none d-sm-block'> <button type='button' class='btn btn-sm btn-info add-item btn-popover' data-url='clasificaciones' data-toggle='modal' data-target='#staticBackdrop' data-content='Crear Clasificación' data-placement='top' " . (in_array("14", $this->session->permisos)?"":"disabled") . "><i class='mdi mdi-plus'></i> <span class='d-none d-sm-block'>Crear Clasificación</span></button>",
                    "for" => "id_clasificacion",
                    "required" => "true",
                    "column" => "6",
                    "attr" => [
                        "class" => "form-control s-normal",
                        "name" => "id_clasificacion",
                        "id" => "id_clasificacion",
                        "required" => "true",
                        "disabled" => "true"
                    ],
                    'options' => $this->Clasificacion_Model->getAllFormControl(),
                    "btn-info" => true
                ],
                [
                    "form_control" => "radio",
                    "for" => "estado_ingreso",
                    "required" => "true",
                    "column" => "6",
                    "attr" => [
                        "class" => "form-check-input",
                        "name" => "estado_ingreso",
                        "id" => "estado_ingreso",
                        "disabled" => "true"
                    ],
                    'items' => [
                        (object)[
                            "id" => "ingreso",
                            "perfil" => "Ingreso"
                        ],
                        (object)[
                            "id" => "salida",
                            "perfil" => "Salida"
                        ]
                    ]
                ],
                [
                    "form_control" => "radio",
                    "for" => "estado_llenado",
                    "required" => "true",
                    "column" => "6",
                    "attr" => [
                        "class" => "form-check-input",
                        "name" => "estado_llenado",
                        "id" => "estado_llenado",
                        "disabled" => "true"
                    ],
                    'items' => [
                        (object)[
                            "id" => "lleno",
                            "perfil" => "Lleno"
                        ],
                        (object)[
                            "id" => "vacio",
                            "perfil" => "Vacío"
                        ]
                    ]
                ],
                [
                    "form_control" => "select",
                    "label" => "Código ISO<span class='text-danger'>*</span> <button type='button' class='btn btn-sm btn-info add-item btn-popover' data-url='codigoiso' data-content='Crear Código ISO' data-placement='top' " . (in_array("39", $this->session->permisos)?"":"disabled") . "><i class='mdi mdi-plus'></i> <span class='d-none d-sm-block'>Crear Código ISO</span></button>",
                    "for" => "codigo_iso",
                    "required" => "true",
                    "column" => "6",
                    "attr" => [
                        "class" => "form-control d-none",
                        "name" => "codigo_iso",
                        "id" => "codigo_iso",
                        "required" => "true",
                        "disabled" => "true"
                    ],
                    'options' => $this->CodigoIso_Model->getAllFormControl(),
                    "btn-info" => true,
                ]
            ],
            'forms_2' => [
                [
                    "form_control" => "select",
                    "label" => "Transportador<span class='text-danger'>*</span> <button type='button' class='btn btn-sm btn-info add-item btn-popover' data-url='transportadores' data-toggle='modal' data-target='#staticBackdrop' data-content='Crear Transportador' data-placement='top' " . (in_array("19", $this->session->permisos)?"":"disabled") . "><i class='mdi mdi-plus'></i> <span class='d-none d-sm-block'>Crear Transportador</span></button>",
                    "for" => "id_transportador",
                    "required" => "true",
                    "column" => "12",
                    "attr" => [
                        "class" => "form-control s-normal",
                        "name" => "id_transportador",
                        "id" => "id_transportador",
                        "required" => "true",
                        "disabled" => "true"
                    ],
                    'options' => $this->Transportador_Model->getAllFormControl(),
                    "btn-info" => true
                ],
                [
                    "form_control" => "select",
                    "label" => "Conductor<span class='text-danger'>*</span> <br class='d-none d-sm-block'> <button type='button' class='btn btn-sm btn-info add-item btn-popover' data-url='conductores'  data-toggle='modal' data-target='#staticBackdrop' data-content='Crear Conductores' data-placement='top' " . (in_array("24", $this->session->permisos)?"":"disabled") . "><i class='mdi mdi-plus'></i> <span class='d-none d-sm-block'>Crear Conductores</span></button>",
                    "for" => "id_conductor",
                    "required" => "true",
                    "column" => "6",
                    "attr" => [
                        "class" => "form-control s-normal",
                        "name" => "id_conductor",
                        "id" => "id_conductor",
                        "required" => "true",
                        "disabled" => "true"
                    ],
                    'options' => [],
                    "btn-info" => true
                ],
                [
                    "form_control" => "select",
                    "label" => "Placa<span class='text-danger'>*</span> <br class='d-none d-sm-block'> <button type='button' class='btn btn-sm btn-info add-item btn-popover' data-content='Crear Vehículo' data-placement='top' data-url='vehiculos'  data-toggle='modal' data-target='#staticBackdrop' " . (in_array("29", $this->session->permisos)?"":"disabled") . "><i class='mdi mdi-plus'></i> <span class='d-none d-sm-block'>Crear Vehículo</span></button>",
                    "for" => "placa_vehiculo",
                    "required" => "true",
                    "column" => "6",
                    "attr" => [
                        "class" => "form-control s-normal",
                        "name" => "placa_vehiculo",
                        "id" => "placa_vehiculo",
                        "required" => "true",
                        "disabled" => "true"
                    ],
                    'options' => [],
                    "btn-info" => true
                ],
                [
                    "form_control" => "select",
                    "label" => "Cliente<span class='text-danger'>*</span> <button type='button' class='btn btn-sm btn-info add-item btn-popover' data-content='Crear Cliente' data-placement='top' data-url='clientes' data-toggle='modal' data-target='#staticBackdrop' " . (in_array("9", $this->session->permisos)?"":"disabled") . "><i class='mdi mdi-plus'></i> <span class='d-none d-sm-block'>Crear Cliente</span></button>",
                    "for" => "id_cliente",
                    "required" => "true",
                    "column" => "12",
                    "attr" => [
                        "class" => "form-control s-normal",
                        "name" => "id_cliente",
                        "id" => "id_cliente",
                        "required" => "true",
                        "disabled" => "true"
                    ],
                    'options' => $this->Cliente_Model->getAllFormControl(),
                    "btn-info" => true
                ],
                [
                    "form_control" => "select",
                    "label" => "Naviera<span class='text-danger'>*</span> <button type='button' class='btn btn-sm btn-info add-item btn-popover' data-url='navieras' data-toggle='modal' data-target='#staticBackdrop' data-content='Crear Naviera' data-placement='top' " . (in_array("34", $this->session->permisos)?"":"disabled") . "><i class='mdi mdi-plus'></i> <span class='d-none d-sm-block'>Crear Naviera</span></button>",
                    "for" => "codigo_naviera",
                    "required" => "true",
                    "column" => "12",
                    "attr" => [
                        "class" => "form-control",
                        "name" => "codigo_naviera",
                        "id" => "codigo_naviera",
                        "required" => "true",
                        "disabled" => "true"
                    ],
                    'options' => $this->Naviera_Model->getAllFormControl(),
                    "btn-info" => true
                ]
            ],
            'forms_3' => [
                [
                    "form_control" => "input",
                    "label" => "Observaciones",
                    "column" => "6",
                    "for" => "observacion",
                    "attr"  => [
                        "type" => "text",
                        "name" => "observacion",
                        "id"   => "observacion",
                        "placeholder" => "Observaciones",
                        "required" => "true",
                        "class" => "form-control"
                    ],
                    "value" => "",
                    "extra_attr" => ""
                ],
                [
                    "form_control" => "select",
                    "label" => "Adaptar a<span class='text-danger'>*</span>",
                    "for" => "id_adaptar",
                    "required" => "true",
                    "column" => "6",
                    "attr" => [
                        "class" => "form-control",
                        "name" => "id_adaptar",
                        "id" => "id_adaptar"
                    ],
                    'options' => $this->Clasificacion_Model->getAllFormControl()
                ]
            ],
            'forms_4' => [
                [
                    "form_control" => "select",
                    "label" => "Descripción del daño<span class='text-danger'>*</span>",
                    "for" => "codigo_damage",
                    "required" => "true",
                    "column" => "4",
                    "attr" => [
                        "class" => "form-control",
                        "name" => "codigo_damage",
                        "id" => "codigo_damage",
                        "required" => "true"
                    ],
                    'options' => $this->CodigoDamage_Model->getAllFormControl()
                ],
                [
                    "form_control" => "select",
                    "label" => "Posible reparación<span class='text-danger'>*</span>",
                    "for" => "codigo_reparacion",
                    "required" => "true",
                    "column" => "4",
                    "attr" => [
                        "class" => "form-control form-disabled",
                        "name" => "codigo_reparacion",
                        "id" => "codigo_reparacion",
                        "required" => "true",
                        "disabled" => "true"
                    ],
                    'options' => []
                ],
                [
                    "form_control" => "select",
                    "label" => "Cargo a<span class='text-danger'>*</span>",
                    "for" => "id_cargo",
                    "required" => "true",
                    "column" => "4",
                    "attr" => [
                        "class" => "form-control",
                        "name" => "id_cargo",
                        "id" => "id_cargo",
                        "required" => "true"
                    ],
                    'options' => $this->CodigoResponsabilidad_Model->getAllFormControl()
                ],
                [
                    "form_control" => "input",
                    "label" => "Localización del daño<span class='text-danger'>*</span>",
                    "for" => "localizacion_damage",
                    "column" => "4",
                    "attr" => [
                        "type" => "text",
                        "name" => "localizacion_damage",
                        "id" => "localizacion_damage",
                        "placeholder" => "",
                        "required" => "true",
                        "class" => "form-control form-readonly",
                        "readonly" => "true"
                    ],
                    "value" => "",
                    "extra_attr" => ""
                ],
                [
                    "form_control" => "select",
                    "label" => "Componente<span class='text-danger'>*</span>",
                    "for" => "id_componente",
                    "required" => "true",
                    "column" => "4",
                    "attr" => [
                        "class" => "form-control form-disabled",
                        "name" => "id_componente",
                        "id" => "id_componente",
                        "required" => "true",
                        "disabled" => "true"
                    ],
                    'options' => []
                ],
                [
                    "form_control" => "select",
                    "label" => "Medida<span class='text-danger'>*</span>",
                    "for" => "id_medida",
                    "required" => "true",
                    "column" => "4",
                    "attr" => [
                        "class" => "form-control form-disabled",
                        "name" => "id_medida",
                        "id" => "id_medida",
                        "required" => "true",
                        "disabled" => "true"
                    ],
                    'options' => []
                ]
            ],
            'control_digitos_contenedor' => $control_digitos_contenedor,
            'forms_add' => [
                'contenedores' => [
                    'form_add' => [
                        [
                            "form_control" => "select",
                            "label" => "Código ISO<span class='text-danger'>*</span> <button type='button' class='btn btn-sm btn-info add-item btn-popover' data-url='codigoiso' data-content='Crear Código ISO' data-placement='top' " . (in_array("39", $this->session->permisos)?"":"disabled") . "><i class='mdi mdi-plus'></i> <span class='d-none d-sm-block'>Crear Código ISO</span></button>",
                            "for" => "codigo_iso",
                            "required" => "true",
                            "column" => "6",
                            "attr" => [
                                "class" => "form-control",
                                "name" => "codigo_iso",
                                "id" => "codigo_iso",
                                "required" => "true",
                                "disabled" => "true"
                            ],
                            'options' => $this->CodigoIso_Model->getAllFormControl(),
                            "btn-info" => true,
                        ],
                        [
                            "form_control" => "select",
                            "label" => "Naviera<span class='text-danger'>*</span> <button type='button' class='btn btn-sm btn-info add-item btn-popover' data-content='Crear Naviera' data-placement='top' data-url='navieras' " . (in_array("34", $this->session->permisos)?"":"disabled") . "><i class='mdi mdi-plus'></i> <span class='d-none d-sm-block'>Crear Naviera</span></button>",
                            "for" => "id_naviera",
                            "required" => "",
                            "column" => "6",
                            "attr" => [
                                "class" => "form-control",
                                "name" => "id_naviera",
                                "id" => "id_naviera",
                                "required" => "true",
                                "disabled" => "true"
                            ],
                            'options' => []
                        ]
                    ],
                    'form_add_titulo' => 'Contenedor',
                    'form_add_url' => 'contenedores'
                ],
                'transportadores' => [
                    'form_add' => [
                        [
                            "form_control" => "input",
                            "label" => "Nombre<span class='text-danger'>*</span>",
                            "column" => "12",
                            "for" => "transportador",
                            "attr"  => [
                                "type" => "text",
                                "name" => "transportador",
                                "id"   => "transportador",
                                "placeholder" => "Nombre",
                                "required" => "true",
                                "class" => "form-control"
                            ],
                            "value" => "",
                            "extra_attr" => ""
                        ]
                    ],
                    'form_add_titulo' => 'Transportador',
                    'form_add_url' => 'transportadores'
                ],
                'codigoiso' => [
                    'form_add' => [
                        [
                            "form_control" => "input",
                            "label" => "Código<span class='text-danger'>*</span>",
                            "column" => "6",
                            "for" => "codigo",
                            "attr"  => [
                                "type" => "text",
                                "name" => "codigo",
                                "id"   => "codigo",
                                "placeholder" => "Código",
                                "required" => "true",
                                "class" => "form-control"
                            ],
                            "value" => "",
                            "extra_attr" => ""
                        ],
                        [
                            "form_control" => "input-add",
                            "label" => "Dimensión<span class='text-danger'>*</span>",
                            "column" => "6",
                            "inputs" => [
                                [
                                    "type" => "text",
                                    "name" => "dimension_largo",
                                    "id"   => "dimension_largo",
                                    "placeholder" => "Largo",
                                    "required" => "true",
                                    "class" => "form-control"
                                ],
                                [
                                    "type" => "text",
                                    "name" => "dimension_alto",
                                    "id"   => "dimension_alto",
                                    "placeholder" => "Alto",
                                    "required" => "true",
                                    "class" => "form-control"
                                ],
                                [
                                    "type" => "text",
                                    "name" => "dimension_ancho",
                                    "id"   => "dimension_ancho",
                                    "placeholder" => "Ancho",
                                    "required" => "true",
                                    "class" => "form-control"
                                ]
                            ]
                        ],
                        [
                            "form_control" => "select-add",
                            "label" => "Navieras <button type='button' class='btn btn-success btn-sm addItem btn-popover' data-content='Agregar Campo' data-placement='top' data-select-add='codigoiso' data-select-position='2'><i class='mdi mdi-plus'></i> <span class='d-none d-sm-block'>Agregar Campo</span></button> <button type='button' class='btn btn-sm btn-info add-item btn-popover' data-content='Crear Naviera' data-placement='top' data-url='navieras' " . (in_array("34", $this->session->permisos)?"":"disabled") . "><i class='mdi mdi-plus'></i> <span class='d-none d-sm-block'>Crear Naviera</span></button>",
                            "for" => "id_naviera",
                            "required" => "true",
                            "column" => "6",
                            "attr" => [
                                "class" => "form-control id_naviera",
                                "name" => "id_naviera",
                                //"id" => "id_naviera",
                                "required" => "true"
                            ],
                            'options' => $this->Naviera_Model->getAllFormControl()
                        ]
                    ],
                    'form_add_titulo' => 'Código ISO',
                    'form_add_url' => 'codigoiso'
                ],
                'clasificaciones' => [
                    'form_add' => [
                        [
                            "form_control" => "input",
                            "label" => "Nombre<span class='text-danger'>*</span>",
                            "column" => "6",
                            "for" => "clasificacion",
                            "attr"  => [
                                "type" => "text",
                                "name" => "clasificacion",
                                "id"   => "clasificacion",
                                "placeholder" => "Nombre",
                                "required" => "true",
                                "class" => "form-control"
                            ],
                            "value" => "",
                            "extra_attr" => ""
                        ],
                        [
                            "form_control" => "select-add",
                            "label" => "Naviera<span class='text-danger'>*</span> <button type='button' class='btn btn-success btn-sm addItem btn-popover' data-content='Agregar Campo' data-placement='top' data-select-add='clasificaciones' data-select-position='1'><i class='mdi mdi-plus'></i> <span class='d-none d-sm-block'>Agregar Campo</span></button> <button type='button' class='btn btn-sm btn-info add-item btn-popover' data-content='Crear Naviera' data-placement='top' data-url='navieras' " . (in_array("34", $this->session->permisos)?"":"disabled") . "><i class='mdi mdi-plus'></i> <span class='d-none d-sm-block'>Crear Naviera</span></button>",
                            "btn-info" => true,
                            "for" => "id_naviera",
                            "required" => "true",
                            "column" => "6",
                            "attr" => [
                                "class" => "form-control id_naviera",
                                "name" => "id_naviera",
                                //"id" => "id_naviera",
                                "required" => "true"
                            ],
                            'options' => $this->Naviera_Model->getAllFormControl()
                        ]
                    ],
                    'form_add_titulo' => 'Clasificación',
                    'form_add_url' => 'clasificaciones'
                ],
                'conductores' => [
                    'form_add' => [
                        [
                            "form_control" => "input",
                            "label" => "Nombre<span class='text-danger'>*</span>",
                            "column" => "6",
                            "for" => "conductor",
                            "attr"  => [
                                "type" => "text",
                                "name" => "conductor",
                                "id"   => "conductor",
                                "placeholder" => "Nombre",
                                "required" => "true",
                                "class" => "form-control"
                            ],
                            "value" => "",
                            "extra_attr" => ""
                        ],
                        [
                            "form_control" => "input",
                            "label" => "Número de documento<span class='text-danger'>*</span>",
                            "column" => "6",
                            "for" => "numero_documento",
                            "attr"  => [
                                "type" => "text",
                                "name" => "numero_documento",
                                "id"   => "numero_documento",
                                "placeholder" => "Número de documento",
                                "required" => "true",
                                "class" => "form-control"
                            ],
                            "value" => "",
                            "extra_attr" => "onkeypress='return onlyNumber(event)'"
                        ],
                        [
                            "form_control" => "select-add",
                            "label" => "Transportador<span class='text-danger'>*</span> <br class='d-none d-sm-block'> <button type='button' class='btn btn-success btn-sm addItem btn-popover' data-content='Agregar Campo' data-placement='top' data-select-add='conductores' data-select-position='2'><i class='mdi mdi-plus'></i> <span class='d-none d-sm-block'>Agregar Campo</span></button> <button type='button' class='btn btn-sm btn-info add-item btn-popover' data-content='Crear Transportador' data-placement='top' data-url='transportadores' " . (in_array("19", $this->session->permisos)?"":"disabled") . "><i class='mdi mdi-plus'></i> <span class='d-none d-sm-block'>Crear Transportador</span></button>",
                            "for" => "id_transportador",
                            "required" => "true",
                            "column" => "6",
                            "attr" => [
                                "class" => "form-control id_transportador",
                                "name" => "id_transportador",
                                "id" => "id_transportador",
                                "required" => "true"
                            ],
                            'options' => $this->Transportador_Model->getAllFormControl()
                        ]
                    ],
                    'form_add_titulo' => 'Conductor',
                    'form_add_url' => 'conductores'
                ],
                'vehiculos' => [
                    'form_add' => [
                        [
                            "form_control" => "input",
                            "label" => "Placa<span class='text-danger'>*</span>",
                            "column" => "6",
                            "for" => "placa",
                            "attr"  => [
                                "type" => "text",
                                "name" => "placa",
                                "id"   => "placa",
                                "placeholder" => "Placa",
                                "required" => "true",
                                "class" => "form-control"
                            ],
                            "value" => "",
                            "extra_attr" => ""
                        ],
                        [
                            "form_control" => "select-add",
                            "label" => "Conductor<span class='text-danger'>*</span> <button type='button' class='btn btn-success btn-sm addItem btn-popover' data-content='Agregar Campo' data-placement='top' data-select-add='vehiculos' data-select-position='1'><i class='mdi mdi-plus'></i> <span class='d-none d-sm-block'>Agregar Campo</span></button> <button type='button' class='btn btn-sm btn-info add-item btn-popover' data-content='Crear Conductor' data-placement='top' data-url='conductores' " . (in_array("24", $this->session->permisos)?"":"disabled") . "><i class='mdi mdi-plus'></i> <span class='d-none d-sm-block'>Crear Conductor</span></button>",
                            "for" => "id_conductor",
                            "required" => "true",
                            "column" => "6",
                            "attr" => [
                                "class" => "form-control id_conductor",
                                "name" => "id_conductor",
                                "required" => "true"
                            ],
                            'options' => $this->Conductor_Model->getAllFormControl(),
                            "btn-info" => true
                        ]
                    ],
                    'form_add_titulo' => 'Vehículo',
                    'form_add_url' => 'vehiculos'
                ],
                'clientes' => [
                    'form_add' => [
                        [
                            "form_control" => "input",
                            "label" => "Nombre<span class='text-danger'>*</span>",
                            "column" => "12",
                            "for" => "cliente",
                            "attr"  => [
                                "type" => "text",
                                "name" => "cliente",
                                "id"   => "cliente",
                                "placeholder" => "Nombre",
                                "required" => "true",
                                "class" => "form-control"
                            ],
                            "value" => "",
                            "extra_attr" => ""
                        ]
                    ],
                    'form_add_titulo' => 'Cliente',
                    'form_add_url' => 'clientes'
                ],
                'navieras' => [
                    'form_add' => [
                        [
                            "form_control" => "input",
                            "label" => "Nombre<span class='text-danger'>*</span>",
                            "column" => "12",
                            "for" => "naviera",
                            "attr"  => [
                                "type" => "text",
                                "name" => "naviera",
                                "id"   => "naviera",
                                "placeholder" => "Nombre",
                                "required" => "true",
                                "class" => "form-control"
                            ],
                            "value" => "",
                            "extra_attr" => ""
                        ]
                    ],
                    'form_add_titulo' => 'Naviera',
                    'form_add_url' => 'navieras'
                ]
            ],
            'titulo' => 'Crear Inspección',
            'titulo_2' => 'Crear Inspección',
            'titulo_1' => 'Inspecciones'
        ];
        
        $this->load->view('admin/inspecciones/create', $data);
    }

    public function edit($id) 
    {
        if(!$this->session->logged_in) redirect(base_url()."home");        
        if (!in_array('75', $this->session->permisos)) redirect(base_url().'home/permisos');

        $this->load->model('Conductor_Model');
        $this->load->model('VehiculoConductor_Model');
        $this->load->model('CodigoReparacionDamage_Model');
        $this->load->model('CodigoIso_Model');
        $this->load->model('Clasificacion_Model');
        $this->load->model('Transportador_Model');
        $this->load->model('Cliente_Model');
        $this->load->model('Naviera_Model');
        $this->load->model('CodigoResponsabilidad_Model');
        $this->load->model('CodigoDamage_Model');
        $this->load->model('ControlDigitosContenedor_Model');
        $this->load->model('NavieraCodigo_Model');
        $this->load->model('Inspeccioninfodamage_Model');

        $this->session->set_userdata(['menu_pos' => 1]);
        $this->session->set_userdata(['modal_on'=>false]);
        $this->session->unset_userdata("count_modal_on");

        $control_digitos_contenedor = objectToArray( $this->ControlDigitosContenedor_Model->getAll() );
        $letras = array_column($control_digitos_contenedor, 'letra');
        $valores = array_column($control_digitos_contenedor, 'valor');
        $control_digitos_contenedor = array_combine($letras, $valores);
        $inspeccion = $this->Inspecciones_Model->getInspeccionById($id);
        if (count($inspeccion)>0) {
            $inspeccion[0]->damages = $this->Inspeccioninfodamage_Model->getInfoByIdInspeccion($id);
        }

        $data = [
            'url' => 'inspecciones',
            'titulo_view' => 'Editar Inspección',
            'list_conductores' => $this->Conductor_Model->getAllSelect(),
            'list_vehiculos' => $this->VehiculoConductor_Model->getAllSelect(),
            'list_codigo_reparacion' => $this->CodigoReparacionDamage_Model->getAll(),
            'list_naviera_codigo' => $this->NavieraCodigo_Model->getAll(),
            'forms_1' => [
                [
                    "form_control" => "input",
                    "label" => "CONTAINER No.<span class='text-danger'>*</span> <button type='button' class='btn btn-sm btn-info add-item btn-popover' data-url='contenedores' data-toggle='modal' data-target='#staticBackdrop' data-content='Crear Contenedor' data-placement='top' " . (in_array("44", $this->session->permisos)?"":"disabled") . "><i class='mdi mdi-plus'></i> <span class='d-none d-sm-block'>Crear Contenedor</span></button>",
                    "column" => "12",
                    "for" => "codigo_contenedor",
                    "attr"  => [
                        "type" => "text",
                        "name" => "codigo_contenedor",
                        "id"   => "codigo_contenedor",
                        "placeholder" => "Código Container",
                        "required" => "true",
                        "class" => "form-control"
                    ],
                    "value" => "",
                    "extra_attr" => "",
                    "btn-info" => true
                ],
                [
                    "form_control" => "select",
                    "label" => "TIPO<span class='text-danger'>*</span> <br class='d-none d-sm-block'> <button type='button' class='btn btn-sm btn-info add-item btn-popover' data-url='codigoiso' data-toggle='modal' data-target='#staticBackdrop' data-content='Crear Código ISO' data-placement='top' " . (in_array("39", $this->session->permisos)?"":"disabled") . "><i class='mdi mdi-plus'></i> <span class='d-none d-sm-block'>Crear Código ISO</span></button>",
                    "for" => "id_tipo",
                    "required" => "true",
                    "column" => "6",
                    "attr" => [
                        "class" => "form-control",
                        "name" => "id_tipo",
                        "id" => "id_tipo",
                        "required" => "true",
                        "disabled" => "true"
                    ],
                    'options' => $this->CodigoIso_Model->getAllFormControl(),
                    "btn-info" => true
                ],
                [
                    "form_control" => "select",
                    "label" => "Clasificación<span class='text-danger'>*</span> <br class='d-none d-sm-block'> <button type='button' class='btn btn-sm btn-info add-item btn-popover' data-url='clasificaciones' data-toggle='modal' data-target='#staticBackdrop' data-content='Crear Clasificación' data-placement='top' " . (in_array("14", $this->session->permisos)?"":"disabled") . "><i class='mdi mdi-plus'></i> <span class='d-none d-sm-block'>Crear Clasificación</span></button>",
                    "for" => "id_clasificacion",
                    "required" => "true",
                    "column" => "6",
                    "attr" => [
                        "class" => "form-control s-normal",
                        "name" => "id_clasificacion",
                        "id" => "id_clasificacion",
                        "required" => "true",
                        "disabled" => "true"
                    ],
                    'options' => $this->Clasificacion_Model->getAllFormControl(),
                    "btn-info" => true
                ],
                [
                    "form_control" => "radio",
                    "for" => "estado_ingreso",
                    "required" => "true",
                    "column" => "6",
                    "attr" => [
                        "class" => "form-check-input",
                        "name" => "estado_ingreso",
                        "id" => "estado_ingreso",
                        "disabled" => "true"
                    ],
                    'items' => [
                        (object)[
                            "id" => "ingreso",
                            "perfil" => "Ingreso"
                        ],
                        (object)[
                            "id" => "salida",
                            "perfil" => "Salida"
                        ]
                    ]
                ],
                [
                    "form_control" => "radio",
                    "for" => "estado_llenado",
                    "required" => "true",
                    "column" => "6",
                    "attr" => [
                        "class" => "form-check-input",
                        "name" => "estado_llenado",
                        "id" => "estado_llenado",
                        "disabled" => "true"
                    ],
                    'items' => [
                        (object)[
                            "id" => "lleno",
                            "perfil" => "Lleno"
                        ],
                        (object)[
                            "id" => "vacio",
                            "perfil" => "Vacío"
                        ]
                    ]
                ]
            ],
            'forms_2' => [
                [
                    "form_control" => "select",
                    "label" => "Transportador<span class='text-danger'>*</span> <button type='button' class='btn btn-sm btn-info add-item btn-popover' data-url='transportadores' data-toggle='modal' data-target='#staticBackdrop' data-content='Crear Transportador' data-placement='top' " . (in_array("19", $this->session->permisos)?"":"disabled") . "><i class='mdi mdi-plus'></i> <span class='d-none d-sm-block'>Crear Transportador</span></button>",
                    "for" => "id_transportador",
                    "required" => "true",
                    "column" => "12",
                    "attr" => [
                        "class" => "form-control s-normal",
                        "name" => "id_transportador",
                        "id" => "id_transportador",
                        "required" => "true",
                        "disabled" => "true"
                    ],
                    'options' => $this->Transportador_Model->getAllFormControl(),
                    "btn-info" => true
                ],
                [
                    "form_control" => "select",
                    "label" => "Conductor<span class='text-danger'>*</span> <br class='d-none d-sm-block'> <button type='button' class='btn btn-sm btn-info add-item btn-popover' data-url='conductores'  data-toggle='modal' data-target='#staticBackdrop' data-content='Crear Conductores' data-placement='top' " . (in_array("24", $this->session->permisos)?"":"disabled") . "><i class='mdi mdi-plus'></i> <span class='d-none d-sm-block'>Crear Conductores</span></button>",
                    "for" => "id_conductor",
                    "required" => "true",
                    "column" => "6",
                    "attr" => [
                        "class" => "form-control s-normal",
                        "name" => "id_conductor",
                        "id" => "id_conductor",
                        "required" => "true",
                        "disabled" => "true"
                    ],
                    'options' => [],
                    "btn-info" => true
                ],
                [
                    "form_control" => "select",
                    "label" => "Placa<span class='text-danger'>*</span> <br class='d-none d-sm-block'> <button type='button' class='btn btn-sm btn-info add-item btn-popover' data-content='Crear Vehículo' data-placement='top' data-url='vehiculos'  data-toggle='modal' data-target='#staticBackdrop' " . (in_array("29", $this->session->permisos)?"":"disabled") . "><i class='mdi mdi-plus'></i> <span class='d-none d-sm-block'>Crear Vehículo</span></button>",
                    "for" => "placa_vehiculo",
                    "required" => "true",
                    "column" => "6",
                    "attr" => [
                        "class" => "form-control s-normal",
                        "name" => "placa_vehiculo",
                        "id" => "placa_vehiculo",
                        "required" => "true",
                        "disabled" => "true"
                    ],
                    'options' => [],
                    "btn-info" => true
                ],
                [
                    "form_control" => "select",
                    "label" => "Cliente<span class='text-danger'>*</span> <button type='button' class='btn btn-sm btn-info add-item btn-popover' data-content='Crear Cliente' data-placement='top' data-url='clientes' data-toggle='modal' data-target='#staticBackdrop' " . (in_array("9", $this->session->permisos)?"":"disabled") . "><i class='mdi mdi-plus'></i> <span class='d-none d-sm-block'>Crear Cliente</span></button>",
                    "for" => "id_cliente",
                    "required" => "true",
                    "column" => "12",
                    "attr" => [
                        "class" => "form-control s-normal",
                        "name" => "id_cliente",
                        "id" => "id_cliente",
                        "required" => "true",
                        "disabled" => "true"
                    ],
                    'options' => $this->Cliente_Model->getAllFormControl(),
                    "btn-info" => true
                ],
                [
                    "form_control" => "select",
                    "label" => "Naviera<span class='text-danger'>*</span> <button type='button' class='btn btn-sm btn-info add-item btn-popover' data-url='navieras' data-toggle='modal' data-target='#staticBackdrop' data-content='Crear Naviera' data-placement='top' " . (in_array("34", $this->session->permisos)?"":"disabled") . "><i class='mdi mdi-plus'></i> <span class='d-none d-sm-block'>Crear Naviera</span></button>",
                    "for" => "codigo_naviera",
                    "required" => "true",
                    "column" => "12",
                    "attr" => [
                        "class" => "form-control",
                        "name" => "codigo_naviera",
                        "id" => "codigo_naviera",
                        "required" => "true",
                        "disabled" => "true"
                    ],
                    'options' => $this->Naviera_Model->getAllFormControl(),
                    "btn-info" => true
                ]
            ],
            'forms_3' => [
                [
                    "form_control" => "input",
                    "label" => "Observaciones",
                    "column" => "6",
                    "for" => "observacion",
                    "attr"  => [
                        "type" => "text",
                        "name" => "observacion",
                        "id"   => "observacion",
                        "placeholder" => "Observaciones",
                        "required" => "true",
                        "class" => "form-control"
                    ],
                    "value" => "",
                    "extra_attr" => ""
                ],
                [
                    "form_control" => "select",
                    "label" => "Adaptar a<span class='text-danger'>*</span>",
                    "for" => "id_adaptar",
                    "required" => "true",
                    "column" => "6",
                    "attr" => [
                        "class" => "form-control",
                        "name" => "id_adaptar",
                        "id" => "id_adaptar"
                    ],
                    'options' => $this->Clasificacion_Model->getAllFormControl()
                ]
            ],
            'forms_4' => [
                [
                    "form_control" => "select",
                    "label" => "Descripción del daño<span class='text-danger'>*</span>",
                    "for" => "codigo_damage",
                    "required" => "true",
                    "column" => "4",
                    "attr" => [
                        "class" => "form-control",
                        "name" => "codigo_damage",
                        "id" => "codigo_damage",
                        "required" => "true"
                    ],
                    'options' => $this->CodigoDamage_Model->getAllFormControl()
                ],
                [
                    "form_control" => "select",
                    "label" => "Posible reparación<span class='text-danger'>*</span>",
                    "for" => "codigo_reparacion",
                    "required" => "true",
                    "column" => "4",
                    "attr" => [
                        "class" => "form-control form-disabled",
                        "name" => "codigo_reparacion",
                        "id" => "codigo_reparacion",
                        "required" => "true",
                        "disabled" => "true"
                    ],
                    'options' => []
                ],
                [
                    "form_control" => "select",
                    "label" => "Cargo a<span class='text-danger'>*</span>",
                    "for" => "id_cargo",
                    "required" => "true",
                    "column" => "4",
                    "attr" => [
                        "class" => "form-control",
                        "name" => "id_cargo",
                        "id" => "id_cargo",
                        "required" => "true"
                    ],
                    'options' => $this->CodigoResponsabilidad_Model->getAllFormControl()
                ],
                [
                    "form_control" => "input",
                    "label" => "Localización del daño<span class='text-danger'>*</span>",
                    "for" => "localizacion_damage",
                    "column" => "4",
                    "attr" => [
                        "type" => "text",
                        "name" => "localizacion_damage",
                        "id" => "localizacion_damage",
                        "placeholder" => "",
                        "required" => "true",
                        "class" => "form-control form-readonly",
                        "readonly" => "true"
                    ],
                    "value" => "",
                    "extra_attr" => ""
                ],
                [
                    "form_control" => "select",
                    "label" => "Componente<span class='text-danger'>*</span>",
                    "for" => "id_componente",
                    "required" => "true",
                    "column" => "4",
                    "attr" => [
                        "class" => "form-control form-disabled",
                        "name" => "id_componente",
                        "id" => "id_componente",
                        "required" => "true",
                        "disabled" => "true"
                    ],
                    'options' => []
                ],
                [
                    "form_control" => "select",
                    "label" => "Medida<span class='text-danger'>*</span>",
                    "for" => "id_medida",
                    "required" => "true",
                    "column" => "4",
                    "attr" => [
                        "class" => "form-control form-disabled",
                        "name" => "id_medida",
                        "id" => "id_medida",
                        "required" => "true",
                        "disabled" => "true"
                    ],
                    'options' => []
                ]
            ],
            'control_digitos_contenedor' => $control_digitos_contenedor,
            'forms_add' => [
                'contenedores' => [
                    'form_add' => [
                        [
                            "form_control" => "select",
                            "label" => "Código ISO<span class='text-danger'>*</span> <button type='button' class='btn btn-sm btn-info add-item btn-popover' data-url='codigoiso' data-content='Crear Código ISO' data-placement='top' " . (in_array("39", $this->session->permisos)?"":"disabled") . "><i class='mdi mdi-plus'></i> <span class='d-none d-sm-block'>Crear Código ISO</span></button>",
                            "for" => "codigo_iso",
                            "required" => "true",
                            "column" => "6",
                            "attr" => [
                                "class" => "form-control",
                                "name" => "codigo_iso",
                                "id" => "codigo_iso",
                                "required" => "true",
                                "disabled" => "true"
                            ],
                            'options' => $this->CodigoIso_Model->getAllFormControl(),
                            "btn-info" => true,
                        ],
                        [
                            "form_control" => "select",
                            "label" => "Naviera<span class='text-danger'>*</span> <button type='button' class='btn btn-sm btn-info add-item btn-popover' data-content='Crear Naviera' data-placement='top' data-url='navieras' " . (in_array("34", $this->session->permisos)?"":"disabled") . "><i class='mdi mdi-plus'></i> <span class='d-none d-sm-block'>Crear Naviera</span></button>",
                            "for" => "id_naviera",
                            "required" => "",
                            "column" => "6",
                            "attr" => [
                                "class" => "form-control",
                                "name" => "id_naviera",
                                "id" => "id_naviera",
                                "required" => "true",
                                "disabled" => "true"
                            ],
                            'options' => []
                        ]
                    ],
                    'form_add_titulo' => 'Contenedor',
                    'form_add_url' => 'contenedores'
                ],
                'transportadores' => [
                    'form_add' => [
                        [
                            "form_control" => "input",
                            "label" => "Nombre<span class='text-danger'>*</span>",
                            "column" => "12",
                            "for" => "transportador",
                            "attr"  => [
                                "type" => "text",
                                "name" => "transportador",
                                "id"   => "transportador",
                                "placeholder" => "Nombre",
                                "required" => "true",
                                "class" => "form-control"
                            ],
                            "value" => "",
                            "extra_attr" => ""
                        ]
                    ],
                    'form_add_titulo' => 'Transportador',
                    'form_add_url' => 'transportadores'
                ],
                'codigoiso' => [
                    'form_add' => [
                        [
                            "form_control" => "input",
                            "label" => "Código<span class='text-danger'>*</span>",
                            "column" => "6",
                            "for" => "codigo",
                            "attr"  => [
                                "type" => "text",
                                "name" => "codigo",
                                "id"   => "codigo",
                                "placeholder" => "Código",
                                "required" => "true",
                                "class" => "form-control"
                            ],
                            "value" => "",
                            "extra_attr" => ""
                        ],
                        [
                            "form_control" => "input-add",
                            "label" => "Dimensión<span class='text-danger'>*</span>",
                            "column" => "6",
                            "inputs" => [
                                [
                                    "type" => "text",
                                    "name" => "dimension_largo",
                                    "id"   => "dimension_largo",
                                    "placeholder" => "Largo",
                                    "required" => "true",
                                    "class" => "form-control"
                                ],
                                [
                                    "type" => "text",
                                    "name" => "dimension_alto",
                                    "id"   => "dimension_alto",
                                    "placeholder" => "Alto",
                                    "required" => "true",
                                    "class" => "form-control"
                                ],
                                [
                                    "type" => "text",
                                    "name" => "dimension_ancho",
                                    "id"   => "dimension_ancho",
                                    "placeholder" => "Ancho",
                                    "required" => "true",
                                    "class" => "form-control"
                                ]
                            ]
                        ],
                        [
                            "form_control" => "select-add",
                            "label" => "Navieras <button type='button' class='btn btn-success btn-sm addItem btn-popover' data-content='Agregar Campo' data-placement='top' data-select-add='codigoiso' data-select-position='2'><i class='mdi mdi-plus'></i> <span class='d-none d-sm-block'>Agregar Campo</span></button> <button type='button' class='btn btn-sm btn-info add-item btn-popover' data-content='Crear Naviera' data-placement='top' data-url='navieras' " . (in_array("34", $this->session->permisos)?"":"disabled") . "><i class='mdi mdi-plus'></i> <span class='d-none d-sm-block'>Crear Naviera</span></button>",
                            "for" => "id_naviera",
                            "required" => "true",
                            "column" => "6",
                            "attr" => [
                                "class" => "form-control id_naviera",
                                "name" => "id_naviera",
                                //"id" => "id_naviera",
                                "required" => "true"
                            ],
                            'options' => $this->Naviera_Model->getAllFormControl()
                        ]
                    ],
                    'form_add_titulo' => 'Código ISO',
                    'form_add_url' => 'codigoiso'
                ],
                'clasificaciones' => [
                    'form_add' => [
                        [
                            "form_control" => "input",
                            "label" => "Nombre<span class='text-danger'>*</span>",
                            "column" => "6",
                            "for" => "clasificacion",
                            "attr"  => [
                                "type" => "text",
                                "name" => "clasificacion",
                                "id"   => "clasificacion",
                                "placeholder" => "Nombre",
                                "required" => "true",
                                "class" => "form-control"
                            ],
                            "value" => "",
                            "extra_attr" => ""
                        ],
                        [
                            "form_control" => "select-add",
                            "label" => "Naviera<span class='text-danger'>*</span> <button type='button' class='btn btn-success btn-sm addItem btn-popover' data-content='Agregar Campo' data-placement='top' data-select-add='clasificaciones' data-select-position='1'><i class='mdi mdi-plus'></i> <span class='d-none d-sm-block'>Agregar Campo</span></button> <button type='button' class='btn btn-sm btn-info add-item btn-popover' data-content='Crear Naviera' data-placement='top' data-url='navieras' " . (in_array("34", $this->session->permisos)?"":"disabled") . "><i class='mdi mdi-plus'></i> <span class='d-none d-sm-block'>Crear Naviera</span></button>",
                            "btn-info" => true,
                            "for" => "id_naviera",
                            "required" => "true",
                            "column" => "6",
                            "attr" => [
                                "class" => "form-control id_naviera",
                                "name" => "id_naviera",
                                //"id" => "id_naviera",
                                "required" => "true"
                            ],
                            'options' => $this->Naviera_Model->getAllFormControl()
                        ]
                    ],
                    'form_add_titulo' => 'Clasificación',
                    'form_add_url' => 'clasificaciones'
                ],
                'conductores' => [
                    'form_add' => [
                        [
                            "form_control" => "input",
                            "label" => "Nombre<span class='text-danger'>*</span>",
                            "column" => "6",
                            "for" => "conductor",
                            "attr"  => [
                                "type" => "text",
                                "name" => "conductor",
                                "id"   => "conductor",
                                "placeholder" => "Nombre",
                                "required" => "true",
                                "class" => "form-control"
                            ],
                            "value" => "",
                            "extra_attr" => ""
                        ],
                        [
                            "form_control" => "input",
                            "label" => "Número de documento<span class='text-danger'>*</span>",
                            "column" => "6",
                            "for" => "numero_documento",
                            "attr"  => [
                                "type" => "text",
                                "name" => "numero_documento",
                                "id"   => "numero_documento",
                                "placeholder" => "Número de documento",
                                "required" => "true",
                                "class" => "form-control"
                            ],
                            "value" => "",
                            "extra_attr" => "onkeypress='return onlyNumber(event)'"
                        ],
                        [
                            "form_control" => "select-add",
                            "label" => "Transportador<span class='text-danger'>*</span> <br class='d-none d-sm-block'> <button type='button' class='btn btn-success btn-sm addItem btn-popover' data-content='Agregar Campo' data-placement='top' data-select-add='conductores' data-select-position='2'><i class='mdi mdi-plus'></i> <span class='d-none d-sm-block'>Agregar Campo</span></button> <button type='button' class='btn btn-sm btn-info add-item btn-popover' data-content='Crear Transportador' data-placement='top' data-url='transportadores' " . (in_array("19", $this->session->permisos)?"":"disabled") . "><i class='mdi mdi-plus'></i> <span class='d-none d-sm-block'>Crear Transportador</span></button>",
                            "for" => "id_transportador",
                            "required" => "true",
                            "column" => "6",
                            "attr" => [
                                "class" => "form-control id_transportador",
                                "name" => "id_transportador",
                                "id" => "id_transportador",
                                "required" => "true"
                            ],
                            'options' => $this->Transportador_Model->getAllFormControl()
                        ]
                    ],
                    'form_add_titulo' => 'Conductor',
                    'form_add_url' => 'conductores'
                ],
                'vehiculos' => [
                    'form_add' => [
                        [
                            "form_control" => "input",
                            "label" => "Placa<span class='text-danger'>*</span>",
                            "column" => "6",
                            "for" => "placa",
                            "attr"  => [
                                "type" => "text",
                                "name" => "placa",
                                "id"   => "placa",
                                "placeholder" => "Placa",
                                "required" => "true",
                                "class" => "form-control"
                            ],
                            "value" => "",
                            "extra_attr" => ""
                        ],
                        [
                            "form_control" => "select-add",
                            "label" => "Conductor<span class='text-danger'>*</span> <button type='button' class='btn btn-success btn-sm addItem btn-popover' data-content='Agregar Campo' data-placement='top' data-select-add='vehiculos' data-select-position='1'><i class='mdi mdi-plus'></i> <span class='d-none d-sm-block'>Agregar Campo</span></button> <button type='button' class='btn btn-sm btn-info add-item btn-popover' data-content='Crear Conductor' data-placement='top' data-url='conductores' " . (in_array("24", $this->session->permisos)?"":"disabled") . "><i class='mdi mdi-plus'></i> <span class='d-none d-sm-block'>Crear Conductor</span></button>",
                            "for" => "id_conductor",
                            "required" => "true",
                            "column" => "6",
                            "attr" => [
                                "class" => "form-control id_conductor",
                                "name" => "id_conductor",
                                "required" => "true"
                            ],
                            'options' => $this->Conductor_Model->getAllFormControl(),
                            "btn-info" => true
                        ]
                    ],
                    'form_add_titulo' => 'Vehículo',
                    'form_add_url' => 'vehiculos'
                ],
                'clientes' => [
                    'form_add' => [
                        [
                            "form_control" => "input",
                            "label" => "Nombre<span class='text-danger'>*</span>",
                            "column" => "12",
                            "for" => "cliente",
                            "attr"  => [
                                "type" => "text",
                                "name" => "cliente",
                                "id"   => "cliente",
                                "placeholder" => "Nombre",
                                "required" => "true",
                                "class" => "form-control"
                            ],
                            "value" => "",
                            "extra_attr" => ""
                        ]
                    ],
                    'form_add_titulo' => 'Cliente',
                    'form_add_url' => 'clientes'
                ],
                'navieras' => [
                    'form_add' => [
                        [
                            "form_control" => "input",
                            "label" => "Nombre<span class='text-danger'>*</span>",
                            "column" => "12",
                            "for" => "naviera",
                            "attr"  => [
                                "type" => "text",
                                "name" => "naviera",
                                "id"   => "naviera",
                                "placeholder" => "Nombre",
                                "required" => "true",
                                "class" => "form-control"
                            ],
                            "value" => "",
                            "extra_attr" => ""
                        ]
                    ],
                    'form_add_titulo' => 'Naviera',
                    'form_add_url' => 'navieras'
                ]
            ],
            'titulo' => 'Editar Inspección',
            'titulo_2' => 'Editar Inspección',
            'titulo_1' => 'Inspecciones',
            'inspeccion' => $inspeccion[0],
            'id' => $id
        ];

        $this->load->view('admin/inspecciones/edit', $data);
    }

    //servicios
    public function new() 
    {
        $data = $this->input->post();
        $array_damage = $data['array_damage'];
        unset($data['array_damage']);

        //valida que sea nuevo registro del contenedor
        if ( $this->Inspecciones_Model->validateInspeccionContenedor( $data['codigo_contenedor'], $data['estado_in_out'] ) ) {
            print json_encode(['res'=>'bad', 'errors'=>['Ya se encuentra un registro del contenedor con el movimiento.']]);
            exit();
        }

        if ( array_key_exists( 'codigo_iso', $data ) ) {
            $this->load->model('Contenedor_Model');
            $data_conainer = [
                'codigo' => $data['codigo_contenedor'],
                'codigo_iso' => $data['codigo_iso'],
                'id_naviera' => $data['id_naviera']
            ];
            unset($data['codigo_iso']);
            $this->Contenedor_Model->store( $data_conainer );
        }        

        $id_inspeccion = $this->Inspecciones_Model->store( $data );
        if ( $id_inspeccion )
        {
            for ($i=0; $i < count($array_damage); $i++) { 
                $array_damage[$i] = array_merge($array_damage[$i], ['id_inspeccion'=>$id_inspeccion]);
            }
            if ($this->Inspeccioninfodamage_Model->multiStore($array_damage)) {
                print json_encode(['res'=>'ok', 'msj'=>'Información guardada.']);
            }else{
                print json_encode(['res'=>'bad', 'errors'=>['Problemas para guardar el registro.']]);
            }
        }else{
            print json_encode(['res'=>'bad', 'errors'=>['Problemas para guardar el registro.']]);
        }
    }

    public function update($id) 
    {
        $data = $this->input->post();
        $array_damage = $data['array_damage'];
        unset($data['array_damage']);

        $this->load->model('Inspeccioninfodamage_Model');

        //valida que sea nuevo registro del contenedor
        /*if ( $this->Inspecciones_Model->validateInspeccionContenedor( $data['codigo_contenedor'], $data['estado_in_out'], $id ) ) {
            print json_encode(['res'=>'bad', 'errors'=>['Ya se encuentra un registro del contenedor con el movimiento.']]);
            exit();
        }*/

        //eliminar los que se quitaron en el list
        $all_ = $this->Inspeccioninfodamage_Model->getAllByIdInspeccion($id);
        $all_ = array_column(objectToArray($all_), 'id');
        $ids_ = array_column($array_damage, 'id');
        $diff_ = array_diff($all_, $ids_);
        if (count($diff_)>0) {
            $this->Inspeccioninfodamage_Model->delete($diff_);
        }

        if ($this->Inspecciones_Model->update($data, $id)) {
            //actualizar o agregar el daño 
            foreach ($array_damage as $k => $v) {            
                if (array_key_exists('id', $v)) {
                    $data = $array_damage[$k];
                    $id_ = $v['id'];
                    unset($data['id']);
                    if (!$this->Inspeccioninfodamage_Model->update($data, $id)) {
                        print json_encode(['res'=>'bad', 'errors'=>['Problemas para actualizar el registro.']]);
                        exit();
                    }
                }else{
                    $data = $array_damage[$k];
                    $data = array_merge($data, ['id_inspeccion'=>$id]);
                    if (!$this->Inspeccioninfodamage_Model->store($data)) {
                        print json_encode(['res'=>'bad', 'errors'=>['Problemas para actualizar el registro.']]);
                        exit();
                    }
                }
            }
            print json_encode(['res'=>'ok', 'msj'=>'Información actualizada.']);
        } else {
            print json_encode(['res'=>'bad', 'errors'=>['Problemas para actualizar el registro.']]);
        }
    }

    public function delete($id)
    {
        if ($this->Inspecciones_Model->delete($id)) {
            print json_encode(['res'=>'ok', 'msj'=>'Información eliminado.']);
        } else {
            print json_encode(['res'=>'bad', 'errors'=>['Problemas para eliminar el registro.']]);
        }
    }

    public function getComponentsByLocation() {
        $codigo_localizacion = $this->input->post('codigo_localizacion');

        $letter_1 = $codigo_localizacion[0];
        $letter_2 = $codigo_localizacion[1];

        $this->load->model('Componente_Model');

        $codigos = $this->Componente_Model->getComponenteByLocation($letter_1);
        print json_encode(['res'=>'ok', 'datos'=>$codigos]);
    }

    public function getMedidasByComponentReparacion() 
    {
        $codigo_componente = $this->input->post('codigo_componente');
        $codigo_reparacion = $this->input->post('codigo_reparacion');

        $this->load->model('Medidas_Model');

        $codigos = $this->Medidas_Model->getMedidasByComponentReparacion($codigo_componente, $codigo_reparacion);
        print json_encode(['res'=>'ok', 'datos'=>$codigos]);
    }
    //EXCEL
    public function exportarInspecciones()
    {
        $fecha_inicio = $this->input->post('fecha_inicio');
        $fecha_fin = $this->input->post('fecha_fin');
        $id_transportador = $this->input->post('id_transportador');
        $id_conductor = $this->input->post('id_conductor');
        $id_clasificacion = $this->input->post('id_clasificacion');
        $id_naviera = $this->input->post('id_naviera');
        $id_cliente = $this->input->post('id_cliente');
        $tipo_movimiento = $this->input->post('tipo_movimiento');

        $inspecciones = $this->Inspecciones_Model->getAllInspecciones($fecha_inicio, $fecha_fin, $id_transportador, $id_conductor, $id_clasificacion, $id_naviera, $id_cliente, $tipo_movimiento);
        $inspecciones = objectToArray($inspecciones);

        $writer = WriterEntityFactory::createXLSXWriter();
		// $writer = WriterEntityFactory::createODSWriter();
		// $writer = WriterEntityFactory::createCSVWriter();
		$url='export_excel/Reporte_inspecciones.xlsx';
		$fileName = FCPATH.$url;
		//chmod($fileName, 0777);
		$writer->openToFile($fileName); // write data to a file or to a PHP stream
		//$writer->openToBrowser($fileName); // stream data directly to the browser

		$style_ok = (new StyleBuilder())
           ->setFontBold()
           ->setFontSize(10)
           //->setCellAlignment(CellAlignment::CENTER)
           ->build();

        $style_titulos = (new StyleBuilder())
           ->setFontBold()
           ->setFontSize(10)
           //->setCellAlignment(CellAlignment::CENTER)
           ->setBackgroundColor(Color::BLUE)
           ->build();
        
        $style_total = (new StyleBuilder())
           ->setFontBold()
           ->setFontSize(10)
           //->setCellAlignment(CellAlignment::CENTER)
           ->setBackgroundColor(Color::YELLOW)
           ->build();

		if (count($inspecciones)>0) {
			$singleRow = WriterEntityFactory::createRow([ WriterEntityFactory::createCell('INSPECCIONES') ], $style_ok);
			$writer->addRow($singleRow);

			$keys = ['CONTENEDOR', 'FECHA MOVIMIENTO', 'TRANSPORTADOR', 'CONDUCTOR', 'VEHICULO', 'CLASIFICACION', 'NAVIERA', 'CODIGO ISO', 'TIPO DE MOVIMIENTO', 'ESTADO', 'OBSERVACIONES', 'CODIGO DAÑO', 'CODIGO LOCALIZACION DE DAÑO', 'CODIGO COMPONENTE', 'MEDIDA', 'CODIGO REPARACION', 'CODIGO RESPONSABILIDAD'];
			$rowFromValues = WriterEntityFactory::createRowFromArray($keys, $style_ok);
            $writer->addRow($rowFromValues);
            
            foreach ($inspecciones as $inspeccion) {
                $temp = [
                    $inspeccion['codigo_contenedor'],
                    $inspeccion['created_at'],
                    $inspeccion['transportador'],
                    $inspeccion['conductor'],
                    $inspeccion['placa_vehiculo'],
                    $inspeccion['clasificacion'],
                    $inspeccion['naviera'],
                    $inspeccion['codigo_iso'],
                    $inspeccion['estado_in_out']=='1'?'Ingreso':'Salida',
                    $inspeccion['estado_llenado']=='1'?'Lleno':'Vacio',
                    $inspeccion['observaciones'],
                    $inspeccion['codigo_damage'],
                    $inspeccion['codigo_localizacion_damage'],
                    $inspeccion['codigo_componente'],
                    $inspeccion['medida'],
                    $inspeccion['codigo_reparacion'],
                    $inspeccion['codigo_responsabilidad'],
                ];
                $rowFromValues = WriterEntityFactory::createRowFromArray($temp);
                $writer->addRow($rowFromValues);
            }
		}		

		$writer->close();

		print json_encode(array('res'=>'ok', 'url'=>$url));

    }

    public function getPDFInspeccion($id=false)
    {
        $inspeccion = $this->Inspecciones_Model->getById($id);
        $inspeccion[0]->largo = str_replace(['"',"'"], '', $inspeccion[0]->largo);
        $damages = objectToArray($this->Inspeccioninfodamage_Model->getInfoByIdInspeccion($id));
        $codigo_localizacion_damage = array_column($damages, 'codigo_localizacion_damage');
        $list_damages = [];
        echo "<pre>";
        print_r($damages);
        echo "</pre>";
        exit();

        $data['inspeccion'] = $inspeccion[0];

        // instantiate and use the dompdf class
        
        $dompdf = new Dompdf();
        $options = $dompdf->getOptions(); 
        $options->set(array('isRemoteEnabled' => true));
        $dompdf->setOptions($options);
        $html = $this->load->view('admin/inspecciones/table_pdf', $data, true);
        $dompdf->loadHtml($html);
        $dompdf->setBasePath(FCPATH);
        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        $output = $dompdf->output();
        $file_to_save = 'export_excel/Inspeccion_'.$inspeccion[0]->codigo_contenedor.'.pdf';
        file_put_contents(FCPATH.$file_to_save, $output);

        print json_encode(array('res'=>'ok', 'url'=>$file_to_save));
    }
}

function pagination_general($total, $url, $per_page, $uri_segment){
    $config['total_rows'] = $total;
    $config["base_url"] = base_url().$url;
    $config['per_page'] = $per_page;
    $config['uri_segment'] = $uri_segment;
    $config['first_link'] = '<< Ir al primero';
    $config['last_link'] = 'Ir al ultimo >';
    $config['next_link'] = ' Siguiente ' . '&gt;';
    $config['prev_link'] = ' &lt;' . ' Atras';
    $config['full_tag_open'] = "<ul  class='pagination justify-content-center'>";
    $config['full_tag_close'] ="</ul>";
    $config['num_tag_open'] = '<li class="page-item">';
    $config['num_tag_close'] = '</li>';
    $config['cur_tag_open'] = "<li class='active'><a href='#'>";
    $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
    $config['next_tag_open'] = '<li class="page-item">';
    $config['next_tag_close'] = "</li>";
    $config['prev_tag_open'] = '<li class="page-item">';
    $config['prev_tag_close'] = "</li>";
    $config['first_tag_open'] = '<li class="page-item">';
    $config['first_tag_close'] = "</li>";
    $config['last_tag_open'] = '<li class="page-item">';
    $config['last_tag_close'] = "</li>";
    $config['use_page_numbers'] = TRUE;
    return $config;
}

function objectToArray ( $object ) {
    if(!is_object($object) && !is_array($object)) {
      return $object;
    }  
    return array_map( 'objectToArray', (array) $object );
}