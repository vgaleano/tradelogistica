<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clasificaciones extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Clasificacion_Model');
        $this->load->model('ClasificacionNaviera_Model');
        $this->load->model('General_Model');
    }

    public function index()
    {
        redirect( base_url().'clasificaciones/list' );
    }

    public function list( $offset=0 )
    {
        if( !$this->session->logged_in) redirect(base_url()."home" );        
		if ( !in_array('12', $this->session->permisos) ) redirect(base_url().'home/permisos');
        
        $this->load->model('General_Model');
        $servicio = $this->General_Model->modalOn('clasificaciones');
        $this->session->set_userdata(['menu_pos' => $this->input->post('menu_pos')]);

        $this->load->model('Naviera_Model');

        $total = $this->Clasificacion_Model->getCountAll();
        $config = pagination_general( $total, 'clasificaciones/list', 10, 3 );
        $this->pagination->initialize( $config );
        $offset_sql = $offset!=0 ? $offset * $config['per_page'] - $config['per_page'] : $offset;
        $list = $this->Clasificacion_Model->getAll( $config['per_page'], $offset_sql );
        if ( count($list)>0 ) {
            foreach ($list as $k => $v) {
                $list[$k]->navieras = $this->ClasificacionNaviera_Model->getByIdClasificacion( $v->id );
            }
        }

        $data = [
            'url' => 'clasificaciones',
            'ver_form' => '13',
            'nuevo_form' => '14',
            'editar_form' => '15',
            'eliminar_form' => '16',
            'columns' => [
                [
                    'key' => 'clasificacion',
                    'name' => 'Nombre'
                ],[
                    'key' => 'navieras',
                    'name' => 'Naviera'
                ],[
                    'key' => 'btn',
                    'name' => 'Acción'
                ],[
                    'key' => '',
                    'name' => 'Editar'
                ],[
                    'key' => '',
                    'name' => 'Eliminar'
                ]
            ],
            'titulo_view' => 'Clasificaciones',
            'titulo_singular' => 'Clasificación',
            'service' => $servicio,
            'lists' => $list,
            'offset' => $offset,
            'list_naviera' => $this->Naviera_Model->getAllFormControl(),
            'forms' => [
                [
                    "form_control" => "input",
                    "label" => "Nombre<span class='text-danger'>*</span>",
                    "column" => "12",
                    "for" => "clasificacion",
                    "attr"  => [
                        "type" => "text",
                        "name" => "clasificacion",
                        "id"   => "clasificacion",
                        "placeholder" => "Nombre",
                        "required" => "true",
                        "class" => "form-control"
                    ],
                    "value" => "",
                    "extra_attr" => ""
                ],
                [
                    "form_control" => "select-add",
                    "label" => "Naviera<span class='text-danger'>*</span> <button type='button' class='btn btn-success btn-sm addItem btn-popover' data-content='Agregar Campo' data-placement='top'><i class='mdi mdi-plus'></i> <span class='d-none d-sm-block'>Agregar Campo</span></button> <button type='button' class='btn btn-sm btn-info add-item btn-popover' data-content='Crear Naviera' data-placement='bottom' " . (in_array("34", $this->session->permisos)?"":"disabled") . "><i class='mdi mdi-plus'></i> <span class='d-none d-sm-block'>Crear Naviera</span></button>",
                    "btn-info" => true,
                    "for" => "id_naviera",
                    "required" => "true",
                    "column" => "6",
                    "attr" => [
                        "class" => "form-control id_naviera",
                        "name" => "id_naviera",
                        "required" => "true"
                    ],
                    'options' => $this->Naviera_Model->getAllFormControl()
                ]
            ],
            'titulo' => 'Clasificaciones',
            'form_add' => [
                [
                    "form_control" => "input",
                    "label" => "Nombre<span class='text-danger'>*</span>",
                    "column" => "12",
                    "for" => "naviera",
                    "attr"  => [
                        "type" => "text",
                        "name" => "naviera",
                        "id"   => "naviera",
                        "placeholder" => "Nombre",
                        "required" => "true",
                        "class" => "form-control"
                    ],
                    "value" => "",
                    "extra_attr" => ""
                ]
            ],
            'form_add_titulo' => 'Naviera',
            'form_add_url' => 'navieras'
        ];

        $this->load->view('admin/clasificaciones/list', $data);
    }

    //servicios
    public function new() 
    {
        if ( !$this->form_validation->run('clasificaciones') )
        {
            print json_encode(['res'=>'bad', 'errors'=>$this->form_validation->error_array()]);
            exit();
        }
        $nombre = $this->input->post('clasificacion');
        if ( $this->General_Model->validateRow( ['clasificacion'=>$nombre], 'clasificaciones') ) 
        {
            print json_encode(['res'=>'bad', 'errors'=>['El nombre de la clasificación ya se encuentra registrado.']]);
            exit();
        }
        //busca repetidos en el post
        $list_naviera = $this->input->post('list_naviera');
        if ( count(array_unique(array_column($list_naviera, 'id_naviera'))) < count($list_naviera) ) {
            print json_encode(['res'=>'bad', 'errors'=>['Hay navieras repetidos para la clasificación.']]);
            exit();
        }
        $id_clasificacion = $this->Clasificacion_Model->store(['clasificacion'=>$nombre]);
        if ( $id_clasificacion ) {
            foreach ($list_naviera as $k => $v) {
                $list_naviera[$k] = array_merge($list_naviera[$k], ['id_clasificacion'=>$id_clasificacion]);
            }
            if ( $this->ClasificacionNaviera_Model->multiStore($list_naviera) ) {
                print json_encode(['res'=>'ok', 'msj'=>'Información guardada.', 'id'=>$id_clasificacion, 'nombre'=>$nombre]);
            }else{
                print json_encode(['res'=>'bad', 'errors'=>['Problemas para guardar el registro.']]);
            }
        } else {
            print json_encode(['res'=>'bad', 'errors'=>['Problemas para guardar el registro.']]);
        }
    }

    public function update($id)
    {
        if (!$this->form_validation->run('clasificaciones'))
        {
            print json_encode(['res'=>'bad', 'errors'=>$this->form_validation->error_array()]);
            exit();
        }
        $nombre = $this->input->post('clasificacion');
        if ( $this->General_Model->validateRow( ['clasificacion'=>$nombre], 'clasificaciones', $id) ) 
        {
            print json_encode(['res'=>'bad', 'errors'=>['El nombre de la clasificación ya se encuentra registrado.']]);
            exit();
        }
        //busca repetidos en el post
        $list_naviera = $this->input->post('list_naviera');
        if ( count(array_unique(array_column($list_naviera, 'id_naviera'))) < count($list_naviera) ) {
            print json_encode(['res'=>'bad', 'errors'=>['Hay navieras repetidos para la clasificación.']]);
            exit();
        }
        //eliminar los que se quitaron en el list
        $all_ = $this->ClasificacionNaviera_Model->getAllByIdClasificacion($id);
        $all_ = array_column(  objectToArray($all_), 'id' );
        $ids_ = array_column($list_naviera, 'id');
        $diff_ = array_diff($all_, $ids_);
        if (count($diff_)>0) {
            $this->ClasificacionNaviera_Model->delete($diff_);
        }
        if ($this->Clasificacion_Model->update(['clasificacion'=>$nombre], $id)) {
            //actualizar o agregar la naviera 
            foreach ($list_naviera as $k => $v) {            
                if (array_key_exists('id', $v)) {
                    $data = [ 'id_clasificacion' => $v['id_clasificacion'] ];
                    if (!$this->ClasificacionNaviera_Model->update($data, $v['id'])) {
                        print json_encode(['res'=>'bad', 'errors'=>['Problemas para actualizar el registro.']]);
                        exit();
                    }
                }else{
                    $data = $list_naviera[$k];
                    if (!$this->ClasificacionNaviera_Model->store($data)) {
                        print json_encode(['res'=>'bad', 'errors'=>['Problemas para actualizar el registro.']]);
                        exit();
                    }
                }
            }
            print json_encode(['res'=>'ok', 'msj'=>'Información actualizada.']);
        } else {
            print json_encode(['res'=>'bad', 'errors'=>['Problemas para actualizar el registro.']]);
        }
    }

    public function delete($id)
    {
        if ( $id == '8' ) {
            print json_encode(['res'=>'bad', 'errors'=>['No se puede eliminar la clasificación Dañados.']]);
            exit();
        }
        if ($this->Clasificacion_Model->delete($id)) {
            print json_encode(['res'=>'ok', 'msj'=>'Información eliminado.']);
        } else {
            print json_encode(['res'=>'bad', 'errors'=>['Problemas para eliminar el registro.']]);
        }
    }
}

function pagination_general($total, $url, $per_page, $uri_segment){
    $config['total_rows'] = $total;
    $config["base_url"] = base_url().$url;
    $config['per_page'] = $per_page;
    $config['uri_segment'] = $uri_segment;
    $config['first_link'] = '<< Ir al primero';
    $config['last_link'] = 'Ir al ultimo >';
    $config['next_link'] = ' Siguiente ' . '&gt;';
    $config['prev_link'] = ' &lt;' . ' Atras';
    $config['full_tag_open'] = "<ul  class='pagination justify-content-center'>";
    $config['full_tag_close'] ="</ul>";
    $config['num_tag_open'] = '<li class="page-item">';
    $config['num_tag_close'] = '</li>';
    $config['cur_tag_open'] = "<li class='active'><a href='#'>";
    $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
    $config['next_tag_open'] = '<li class="page-item">';
    $config['next_tag_close'] = "</li>";
    $config['prev_tag_open'] = '<li class="page-item">';
    $config['prev_tag_close'] = "</li>";
    $config['first_tag_open'] = '<li class="page-item">';
    $config['first_tag_close'] = "</li>";
    $config['last_tag_open'] = '<li class="page-item">';
    $config['last_tag_close'] = "</li>";
    $config['use_page_numbers'] = TRUE;
    return $config;
}

function objectToArray ( $object ) {
    if(!is_object($object) && !is_array($object)) {
      return $object;
    }  
    return array_map( 'objectToArray', (array) $object );
}