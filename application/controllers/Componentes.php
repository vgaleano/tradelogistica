<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Componentes extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Componente_Model');
        $this->load->model('ComponenteDamage_Model');
        $this->load->model('General_Model');
    }

    public function index()
    {
        redirect(base_url().'componentes/list');
    }

    public function list($offset=0)
    {
        if(!$this->session->logged_in) redirect(base_url()."home");        
        if (!in_array('57', $this->session->permisos)) redirect(base_url().'home/permisos');
        
        $this->load->model('General_Model');
        $servicio = $this->General_Model->modalOn('componentes');
        $this->session->set_userdata(['menu_pos' => $this->input->post('menu_pos')]);
        
        $this->load->model('CodigoDamage_Model');
        $this->load->model('TypeDesignation_Model');
        $this->load->model('Assembly_Model');

        $total = $this->Componente_Model->getCountAll();
        $config = pagination_general($total, 'componentes/list', 10, 3);
        $this->pagination->initialize($config);
        $offset_sql = $offset!=0 ? $offset * $config['per_page'] - $config['per_page'] : $offset;
        $list = $this->Componente_Model->getAll( $config['per_page'], $offset_sql);
        if (count($list)>0) {
            foreach ($list as $k => $v) {
                $list[$k]->damages = $this->ComponenteDamage_Model->getByCodigoComponente($v->codigo);
            }
        }
        
        $data = [
            'url' => 'componentes',
            'ver_form' => '58',
            'nuevo_form' => '59',
            'editar_form' => '60',
            'eliminar_form' => '61',
            'titulo_view' => 'Componentes',
            'titulo_singular' => 'Componente',
            'service' => $servicio,
            'lists' => $list,
            'offset' => $offset,
            'forms' => [
                [
                    "form_control" => "input",
                    "label" => "Nombre<span class='text-danger'>*</span>",
                    "column" => "6",
                    "for" => "nombre",
                    "attr"  => [
                        "type" => "text",
                        "name" => "nombre",
                        "id"   => "nombre",
                        "placeholder" => "Nombre",
                        "required" => "true",
                        "class" => "form-control"
                    ],
                    "value" => "",
                    "extra_attr" => ""
                ],
                [
                    "form_control" => "textarea",
                    "label" => "Traducción<span class='text-danger'>*</span>",
                    "column" => "6",
                    "for" => "traduccion",
                    "attr"  => [
                        "type" => "text",
                        "name" => "traduccion",
                        "id"   => "traduccion",
                        "placeholder" => "Traducción",
                        "required" => "true",
                        "class" => "form-control"
                    ],
                    "value" => "",
                    "extra_attr" => ""
                ],
                [
                    "form_control" => "input",
                    "label" => "Código<span class='text-danger'>*</span>",
                    "column" => "6",
                    "for" => "codigo",
                    "attr"  => [
                        "type" => "text",
                        "name" => "codigo",
                        "id"   => "codigo",
                        "placeholder" => "Código",
                        "required" => "true",
                        "class" => "form-control"
                    ],
                    "value" => "",
                    "extra_attr" => ""
                ],
                [
                    "form_control" => "input",
                    "label" => "Código localización<span class='text-danger'>*</span>",
                    "column" => "6",
                    "for" => "codigo_localizacion",
                    "attr"  => [
                        "type" => "text",
                        "name" => "codigo_localizacion",
                        "id"   => "codigo_localizacion",
                        "placeholder" => "Código localización",
                        "required" => "true",
                        "class" => "form-control"
                    ],
                    "value" => "",
                    "extra_attr" => ""
                ],
                [
                    "form_control" => "select",
                    "label" => "Assembly<span class='text-danger'>*</span>",
                    "for" => "id_assembly",
                    "required" => "true",
                    "column" => "6",
                    "attr" => [
                        "class" => "form-control",
                        "name" => "id_assembly",
                        "id" => "id_assembly",
                        "required" => "true"
                    ],
                    'options' => $this->Assembly_Model->getAllFormControl()
                ],
                [
                    "form_control" => "select",
                    "label" => "Type designation<span class='text-danger'>*</span>",
                    "for" => "id_type_designation",
                    "required" => "true",
                    "column" => "6",
                    "attr" => [
                        "class" => "form-control",
                        "name" => "id_type_designation",
                        "id" => "id_type_designation",
                        "required" => "true"
                    ],
                    'options' => $this->TypeDesignation_Model->getAllFormControl()
                ],
                [
                    "form_control" => "select-add",
                    "label" => "Códigos de daños<span class='text-danger'>*</span> <br class='d-none d-sm-block'><button type='button' class='btn btn-success btn-sm addItem btn-popover'  data-content='Agregar Campo' data-placement='top'><i class='mdi mdi-plus'></i> <span class='d-none d-sm-block'>Agregar campo</span></button> <button type='button' class='btn btn-sm btn-info add-item btn-popover' data-content='Crear Código de daño' data-placement='bottom' " . (in_array("49", $this->session->permisos)?"":"disabled") . "><i class='mdi mdi-plus'></i> <span class='d-none d-sm-block'>Crear Código de daño</span></button>",
                    "for" => "codigo_damage",
                    "required" => "true",
                    "column" => "6",
                    "attr" => [
                        "class" => "form-control codigo_damage",
                        "name" => "codigo_damage",
                        "id" => "codigo_damage",
                        "required" => "true"
                    ],
                    'options' => $this->CodigoDamage_Model->getAllFormControl(),
                    "btn-info" => true
                ]
            ],
            'list_damage' => $this->CodigoDamage_Model->getAllFormControl(),
            'form_add' => [
                [
                    "form_control" => "input",
                    "label" => "Código<span class='text-danger'>*</span>",
                    "column" => "12",
                    "for" => "codigo",
                    "attr"  => [
                        "type" => "text",
                        "name" => "codigo",
                        "id"   => "codigo",
                        "placeholder" => "Código",
                        "required" => "true",
                        "class" => "form-control"
                    ],
                    "value" => "",
                    "extra_attr" => ""
                ],
                [
                    "form_control" => "textarea",
                    "label" => "Descripción<span class='text-danger'>*</span>",
                    "column" => "12",
                    "for" => "descripcion",
                    "attr"  => [
                        "type" => "text",
                        "name" => "descripcion",
                        //"id"   => "descripcion",
                        "placeholder" => "Descripción",
                        "required" => "true",
                        "class" => "form-control"
                    ],
                    "value" => "",
                    "extra_attr" => ""
                ],
                [
                    "form_control" => "textarea",
                    "label" => "Traducción<span class='text-danger'>*</span>",
                    "column" => "12",
                    "for" => "traduccion",
                    "attr"  => [
                        "type" => "text",
                        "name" => "traduccion",
                        //"id"   => "traduccion",
                        "placeholder" => "Traducción",
                        "required" => "true",
                        "class" => "form-control"
                    ],
                    "value" => "",
                    "extra_attr" => ""
                ]
            ],
            'form_add_titulo' => 'Código de daño',
            'form_add_url' => 'codigodamage',
            'titulo' => 'Componentes'
        ];
        
        $this->load->view('admin/componentes/list', $data);
    }

    //servicios
    public function new() 
    {
        if (!$this->form_validation->run('componentes'))
        {
            print json_encode(['res'=>'bad', 'errors'=>$this->form_validation->error_array()]);
            exit();
        }
        $data = $this->input->post();
        if ( $this->General_Model->validateRow( ['codigo'=>$data['codigo']], 'componentes') ) 
        {
            print json_encode(['res'=>'bad', 'errors'=>['El código de componente ingresado ya se encuentra registrado.']]);
            exit();
        }
        $list_relaciones = isset($data['list_relacion']) ? $data['list_relacion'] : [];
        unset($data['list_relacion']);
        //repetidos en el post
        if (count($list_relaciones)>0) {
            $unique_ = array_unique(array_column($list_relaciones, 'codigo_damage'));
            if ( count($unique_) < count($list_relaciones) ) {
                print json_encode(['res'=>'bad', 'errors'=>['Hay codigos de daños repetidos para el componente.']]);
                exit();
            }
        }
        $id = $this->Componente_Model->store($data);
        if (!$id) {
            print json_encode(['res'=>'bad', 'errors'=>['Problemas para guardar el registro.']]);
            exit();
        }
        if (count($list_relaciones)>0) {
            if (!$this->ComponenteDamage_Model->multiStore($list_relaciones)) {
                print json_encode(['res'=>'ok', 'msj'=>'Información guardada.']);
                exit();
            }
        }
        print json_encode(['res'=>'ok', 'msj'=>'Información guardada.']);
    }

    public function update($id)
    {
        if (!$this->form_validation->run('componentes'))
        {
            print json_encode(['res'=>'bad', 'errors'=>$this->form_validation->error_array()]);
            exit();
        }
        $data = $this->input->post();
        if ( $this->General_Model->validateRow( ['codigo'=>$data['codigo']], 'componentes', $id) ) 
        {
            print json_encode(['res'=>'bad', 'errors'=>['El código de componente ingresado ya se encuentra registrado.']]);
            exit();
        }
        $list_relaciones = isset($data['list_relacion']) ? $data['list_relacion'] : [];
        unset($data['list_relacion']);
        //repetidos en el post
        if (count($list_relaciones)>0) {
            $unique_ = array_unique(array_column($list_relaciones, 'codigo_damage'));
            if ( count($unique_) < count($list_relaciones) ) {
                print json_encode(['res'=>'bad', 'errors'=>['Hay codigos de daños repetidos para el componente.']]);
                exit();
            }
        }
        if (!$this->Componente_Model->update($data, $id)) {
            print json_encode(['res'=>'bad', 'errors'=>['Problemas para actualizar el registro.']]);
            exit();
        }
        //eliminar los que se quitaron en el list
        if (count($list_relaciones)>0) {
            $all_ = $this->ComponenteDamage_Model->getByIdCodigoComponente($id);
            $all_ = array_column(objectToArray($all_), 'id');
            $ids_ = array_column($list_relaciones, 'id');
            $diff_ = array_diff($all_, $ids_);
            if (count($diff_)>0) {
                $this->ComponenteDamage_Model->delete($diff_);
            }
            foreach ($list_relaciones as $k => $v) {            
                if (array_key_exists('id', $v)) {
                    $data = [ 'codigo_componente' => $v['codigo_componente'], 'codigo_damage' => $v['codigo_damage'] ];
                    if (!$this->ComponenteDamage_Model->update($data, $v['id'])) {
                        print json_encode(['res'=>'bad', 'errors'=>['Problemas para actualizar el registro.']]);
                        exit();
                    }
                }else{
                    $data = $list_relaciones[$k];
                    if (!$this->ComponenteDamage_Model->store($data)) {
                        print json_encode(['res'=>'bad', 'errors'=>['Problemas para actualizar el registro.']]);
                        exit();
                    }
                }
            }
        }
        print json_encode(['res'=>'ok', 'msj'=>'Información actualizado.']);
    }

    public function delete($id)
    {
        if ($this->Componente_Model->delete($id)) {
            print json_encode(['res'=>'ok', 'msj'=>'Información eliminado.']);
        } else {
            print json_encode(['res'=>'bad', 'errors'=>['Problemas para eliminar el registro.']]);
        }
    }
}

function pagination_general($total, $url, $per_page, $uri_segment){
    $config['total_rows'] = $total;
    $config["base_url"] = base_url().$url;
    $config['per_page'] = $per_page;
    $config['uri_segment'] = $uri_segment;
    $config['first_link'] = '<< Ir al primero';
    $config['last_link'] = 'Ir al ultimo >';
    $config['next_link'] = ' Siguiente ' . '&gt;';
    $config['prev_link'] = ' &lt;' . ' Atras';
    $config['full_tag_open'] = "<ul  class='pagination justify-content-center'>";
    $config['full_tag_close'] ="</ul>";
    $config['num_tag_open'] = '<li class="page-item">';
    $config['num_tag_close'] = '</li>';
    $config['cur_tag_open'] = "<li class='active'><a href='#'>";
    $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
    $config['next_tag_open'] = '<li class="page-item">';
    $config['next_tag_close'] = "</li>";
    $config['prev_tag_open'] = '<li class="page-item">';
    $config['prev_tag_close'] = "</li>";
    $config['first_tag_open'] = '<li class="page-item">';
    $config['first_tag_close'] = "</li>";
    $config['last_tag_open'] = '<li class="page-item">';
    $config['last_tag_close'] = "</li>";
    $config['use_page_numbers'] = TRUE;
    return $config;
}

function objectToArray ( $object ) {
    if(!is_object($object) && !is_array($object)) {
      return $object;
    }  
    return array_map( 'objectToArray', (array) $object );
}