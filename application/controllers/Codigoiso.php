<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Codigoiso extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('CodigoIso_Model');
        $this->load->model('NavieraCodigo_Model');
        $this->load->model('General_Model');
    }

    public function index()
    {
        redirect(base_url().'codigoiso/list');
    }

    public function list($offset=0)
    {
        if(!$this->session->logged_in) redirect(base_url()."home");        
        if (!in_array('37', $this->session->permisos)) redirect(base_url().'home/permisos');
        
        $this->load->model('General_Model');
        $servicio = $this->General_Model->modalOn('codigoiso');
        $this->session->set_userdata(['menu_pos' => $this->input->post('menu_pos')]);
        
        $this->load->model('Naviera_Model');

        $total = $this->CodigoIso_Model->getCountAll();
        $config = pagination_general($total, 'codigoiso/list', 10, 3);

        $this->pagination->initialize($config);
        $offset_sql = $offset!=0 ? $offset * $config['per_page'] - $config['per_page'] : $offset;
        $list = $this->CodigoIso_Model->getAll( $config['per_page'], $offset_sql);
        if (count($list)>0) {
            foreach ($list as $k => $v) {
                $list[$k]->navieras = objectToArray( $this->NavieraCodigo_Model->getByCodigoIso($v->codigo) );
            }
        }
        
        $data = [
            'url' => 'codigoiso',
            'ver_form' => '38',
            'nuevo_form' => '39',
            'editar_form' => '40',
            'eliminar_form' => '41',
            'list_naviera' => $this->Naviera_Model->getAllFormControl(),
            'titulo_view' => 'Códigos ISO',
            'titulo_singular' => 'Código ISO',
            'service' => $servicio,
            'lists' => $list,
            'offset' => $offset,
            'forms' => [
                [
                    "form_control" => "input",
                    "label" => "Código<span class='text-danger'>*</span>",
                    "column" => "6",
                    "for" => "codigo",
                    "attr"  => [
                        "type" => "text",
                        "name" => "codigo",
                        "id"   => "codigo",
                        "placeholder" => "Código",
                        "required" => "true",
                        "class" => "form-control"
                    ],
                    "value" => "",
                    "extra_attr" => ""
                ],
                [
                    "form_control" => "input",
                    "label" => "Dimension<span class='text-danger'>*</span>",
                    "for" => "dimensiones",
                    "column" => "6",
                    "attr" => [
                        "type" => "dimensiones",
                        "name" => "dimensiones",
                        "id" => "dimensiones",
                        "placeholder" => "Dimensiones",
                        "required" => "true",
                        "class" => "form-control"
                    ],
                    "value" => "",
                    "extra_attr" => ""
                ],
                [
                    "form_control" => "select-add",
                    "label" => "Navieras <button type='button' class='btn btn-success btn-sm addItem btn-popover' data-content='Agregar Campo' data-placement='top'><i class='mdi mdi-plus'></i> <span class='d-none d-sm-block'>Agregar Campo</span></button> <button type='button' class='btn btn-sm btn-info add-item btn-popover' data-content='Crear Naviera' data-placement='bottom' " . (in_array("34", $this->session->permisos)?"":"disabled") . "><i class='mdi mdi-plus'></i> <span class='d-none d-sm-block'>Crear Naviera</span></button>",
                    "for" => "id_naviera",
                    "required" => "true",
                    "column" => "12",
                    "attr" => [
                        "class" => "form-control id_naviera",
                        "name" => "id_naviera",
                        //"id" => "id_naviera",
                        "required" => "true"
                    ],
                    'options' => $this->Naviera_Model->getAllFormControl(),
                    "btn-info" => true,
                ]
            ],
            'form_add' => [
                [
                    "form_control" => "input",
                    "label" => "Nombre<span class=\'text-danger\'>*</span>",
                    "column" => "12",
                    "for" => "naviera",
                    "attr"  => [
                        "type" => "text",
                        "name" => "naviera",
                        "id"   => "naviera",
                        "placeholder" => "Nombre",
                        "required" => "true",
                        "class" => "form-control"
                    ],
                    "value" => "",
                    "extra_attr" => ""
                ]
            ],
            'form_add_titulo' => 'Naviera',
            'form_add_url' => 'navieras',
            'titulo' => 'Codigos ISO'
        ];
        
        $this->load->view('admin/codigos_iso/list', $data);
    }

    //servicios
    public function new() 
    {
        if (!$this->form_validation->run('codigos_iso'))
        {
            print json_encode([
                'res'=>'bad', 
                'errors'=>$this->form_validation->error_array()
            ]);
            exit();
        }
        $data = $this->input->post();
        if ( $this->General_Model->validateRow( ['codigo'=>$data['codigo']], 'codigos_iso') ) 
        {
            print json_encode([
                'res'=>'bad', 
                'errors'=>['El código iso ingresado ya se encuentra registrado.']
            ]);
            exit();
        }
        $list_relaciones = [];
        if (isset($data['list_relacion'])) {
            $list_relaciones = $data['list_relacion'];
            unset($data['list_relacion']);
            $unique_ = array_unique(array_column($list_relaciones, 'id_naviera'));
            if ( count($unique_) < count($list_relaciones) ) {
                print json_encode([
                    'res'=>'bad', 
                    'errors'=>['Hay navieras repetidos para el código ISO.']
                ]);
                exit();
            }
            /*//repetidos en el post
            $unique_ = array_unique(array_column($list_relaciones, 'codigo_naviera'));
            if ( count($unique_) < count($list_relaciones) ) {
                print json_encode([
                    'res'=>'bad', 
                    'errors'=>['Hay codigos de naviera repetidos para el código ISO.']
                ]);
                exit();
            }
            //repetidos en tabla
            $repetidos_ = objectToArray( $this->NavieraCodigo_Model->getRepetidosCodigoNaviera(implode(",",$unique_)) );
            if ($repetidos_) {
                print json_encode([
                    'res'=>'bad', 
                    'errors'=>['Hay codigos de naviera repetidos para el código ISO:'.implode(", ", array_column($repetidos_, 'codigo_naviera')).'.']
                ]);
                exit();
            }*/
        }        
        $array_ = isset($data['list_add_modal']) ? $data['list_add_modal'] : [];
        unset($data['list_add_modal']);
        $list_ = isset($data['list_']) ? $data['list_'] : [];
        unset($data['list_']);
        $id = $this->CodigoIso_Model->store($data);
        if ($id) {
            if (count($list_relaciones)>0 && !$this->NavieraCodigo_Model->multiStore($list_relaciones)) {
                print json_encode([
                    'res'=>'bad', 
                    'errors'=>['Problemas para guardar el registro.']
                    ]);
                exit();
            }
            print json_encode([
                'res'=>'ok', 
                'msj'=>'Información guardada.', 
                'array_'=>$array_, 
                'id'=>$data['codigo'], 
                'nombre'=>$data['codigo'], 
                'extra_data'=>$data['dimensiones'], 
                'list_'=>$list_,
                'largo'=>$data['largo']
                ]);
        } else {
            print json_encode([
                'res'=>'bad', 
                'errors'=>['Problemas para guardar el registro.']
                ]);
        }
    }

    public function update($id)
    {
        if (!$this->form_validation->run('codigos_iso'))
        {
            print json_encode(['res'=>'bad', 'errors'=>$this->form_validation->error_array()]);
            exit();
        }
        $data = $this->input->post();
        if ( $this->General_Model->validateRow( ['codigo'=>$data['codigo']], 'codigos_iso', $id) ) 
        {
            print json_encode(['res'=>'bad', 'errors'=>['El código iso ingresado ya se encuentra registrado.']]);
            exit();
        }
        $list_relaciones = [];
        if (isset($data['list_relacion'])) {
            $list_relaciones = $data['list_relacion'];
            unset($data['list_relacion']);
            $unique_ = array_unique(array_column($list_relaciones, 'id_naviera'));
            if ( count($unique_) < count($list_relaciones) ) {
                print json_encode(['res'=>'bad', 'errors'=>['Hay navieras repetidos para el código ISO.']]);
                exit();
            }
            /*//repetidos en el post
            $unique_ = array_unique(array_column($list_relaciones, 'codigo_naviera'));
            if ( count($unique_) < count($list_relaciones) ) {
                print json_encode(['res'=>'bad', 'errors'=>['Hay codigos de naviera repetidos para el código ISO.']]);
                exit();
            }
            //repetidos en tabla
            $repetidos_ = objectToArray( $this->NavieraCodigo_Model->getRepetidosCodigoNaviera(implode(",",$unique_)) );
            if ($repetidos_) {
                print json_encode(['res'=>'bad', 'errors'=>['Hay codigos de naviera repetidos para el código ISO:'.implode(", ", array_column($repetidos_
                , 'codigo_naviera')).'.']]);
                exit();
            }*/
        }
        
        if (!$this->CodigoIso_Model->update($data, $id)) {
            print json_encode(['res'=>'bad', 'errors'=>['Problemas para actualizar el registro.']]);
            exit();
        }
        //eliminar los que se quitaron en el list
        $all_ = $this->NavieraCodigo_Model->getByIdCodigoIso($id);
        $all_ = array_column(objectToArray($all_), 'id');
        $ids_ = array_column($list_relaciones, 'id');
        $diff_ = array_diff($all_, $ids_);
        if (count($diff_)>0) {
            $this->NavieraCodigo_Model->delete($diff_);
        }
        foreach ($list_relaciones as $k => $v) {            
            if (array_key_exists('id', $v)) {
                $data = [ 'codigo_iso' => $v['codigo_iso'], 'id_naviera' => $v['id_naviera'], 'dimensiones' => $v['dimensiones'], 'codigo_naviera' => $v['codigo_naviera'] ];
                if (!$this->NavieraCodigo_Model->update($data, $v['id'])) {
                    print json_encode(['res'=>'bad', 'errors'=>['Problemas para actualizar el registro.']]);
                    exit();
                }
            }else{
                $data = $list_relaciones[$k];
                if (!$this->NavieraCodigo_Model->store($data)) {
                    print json_encode(['res'=>'bad', 'errors'=>['Problemas para actualizar el registro.']]);
                    exit();
                }
            }
        }
        print json_encode(['res'=>'ok', 'msj'=>'Información actualizado.']);
    }

    public function delete($id)
    {
        if ($this->CodigoIso_Model->delete($id)) {
            print json_encode(['res'=>'ok', 'msj'=>'Información eliminado.']);
        } else {
            print json_encode(['res'=>'bad', 'errors'=>['Problemas para eliminar el registro.']]);
        }
    }
}

function pagination_general($total, $url, $per_page, $uri_segment){
    $config = array();
    $config['total_rows'] = $total;
    $config["base_url"] = base_url().$url;
    $config['per_page'] = $per_page;
    $config['uri_segment'] = $uri_segment;
    $config['first_link'] = '<< Ir al primero';
    $config['last_link'] = 'Ir al ultimo >';
    $config['next_link'] = ' Siguiente ' . '&gt;';
    $config['prev_link'] = ' &lt;' . ' Atras';
    $config['full_tag_open'] = "<ul  class='pagination justify-content-center'>";
    $config['full_tag_close'] ="</ul>";
    $config['num_tag_open'] = '<li class="page-item">';
    $config['num_tag_close'] = '</li>';
    $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
    $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
    $config['next_tag_open'] = '<li class="page-item">';
    $config['next_tag_close'] = "</li>";
    $config['prev_tag_open'] = '<li class="page-item">';
    $config['prev_tag_close'] = "</li>";
    $config['first_tag_open'] = '<li class="page-item">';
    $config['first_tag_close'] = "</li>";
    $config['last_tag_open'] = '<li class="page-item">';
    $config['last_tag_close'] = "</li>";
    $config['use_page_numbers'] = TRUE;
    return $config;
}

function objectToArray ( $object ) {
    if(!is_object($object) && !is_array($object)) {
      return $object;
    }  
    return array_map( 'objectToArray', (array) $object );
}