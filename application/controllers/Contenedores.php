<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once FCPATH.'application/third_party/spout/src/Spout/Autoloader/autoload.php';
use Box\Spout\Writer\Common\Creator\WriterEntityFactory;
use Box\Spout\Common\Entity\Row;
use Box\Spout\Writer\Common\Creator\Style\StyleBuilder;
use Box\Spout\Common\Entity\Style\CellAlignment;
use Box\Spout\Common\Entity\Style\Color;
date_default_timezone_set('America/Bogota');

class Contenedores extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Contenedor_Model');
        $this->load->model('General_Model');
    }

    public function index()
    {
        redirect(base_url().'contenedores/list');
    }

    public function list($offset=0)
    {
        if(!$this->session->logged_in) redirect(base_url()."home");        
        if (!in_array('42', $this->session->permisos)) redirect(base_url().'home/permisos');
        
        $this->load->model('General_Model');
        $servicio = $this->General_Model->modalOn('contenedores');
        $this->session->set_userdata(['menu_pos' => $this->input->post('menu_pos')]);
        
        $this->load->model('CodigoIso_Model');
        $this->load->model('NavieraCodigo_Model');
        $this->load->model('ControlDigitosContenedor_Model');
        $this->load->model('Naviera_Model');

        $total = $this->Contenedor_Model->getCountAll();
        $config = pagination_general($total, 'contenedores/list', 10, 3);
        $this->pagination->initialize($config);
        $offset_sql = $offset!=0 ? $offset * $config['per_page'] - $config['per_page'] : $offset;
        $list = $this->Contenedor_Model->getAll($config['per_page'], $offset_sql);
        $control_digitos_contenedor = objectToArray( $this->ControlDigitosContenedor_Model->getAll() );
        $letras = array_column($control_digitos_contenedor, 'letra');
        $valores = array_column($control_digitos_contenedor, 'valor');
        $control_digitos_contenedor = array_combine($letras, $valores);

        $data = [
            'url' => 'contenedores',
            'ver_form' => '43',
            'nuevo_form' => '44',
            'editar_form' => '45',
            'eliminar_form' => '46',
            'columns' => [
                [
                    'key' => 'codigo',
                    'name' => 'Container No.'
                ],[
                    'key' => 'codigo_iso',
                    'name' => 'Código ISO'
                ],[
                    'key' => 'naviera',
                    'name' => 'Naviera'
                ],[
                    'key' => 'btn',
                    'name' => 'Acción'
                ],[
                    'key' => '',
                    'name' => 'Editar'
                ],[
                    'key' => '',
                    'name' => 'Eliminar'
                ]
            ],
            'titulo_view' => 'Contenedores',
            'titulo_singular' => 'Contenedor',
            'service' => $servicio,
            'lists' => $list,
            'offset' => $offset,
            'forms' => [
                [
                    "form_control" => "select",
                    "label" => "Código ISO<span class='text-danger'>*</span> <button type='button' class='btn btn-sm btn-info add-item btn-popover' data-content='Crear Código ISO' data-placement='top' " . (in_array("39", $this->session->permisos)?"":"disabled") . "><i class='mdi mdi-plus'></i> <span class='d-none d-sm-block'>Crear Código ISO</span></button>",
                    "for" => "codigo_iso",
                    "required" => "true",
                    "column" => "6",
                    "attr" => [
                        "class" => "form-control",
                        "name" => "codigo_iso",
                        "id" => "codigo_iso",
                        "required" => "true",
                        "disabled" => "true"
                    ],
                    'options' => $this->CodigoIso_Model->getAllFormControl(),
                    "btn-info" => true,
                ],
                [
                    "form_control" => "select",
                    "label" => "Naviera<span class='text-danger'>*</span>",
                    "for" => "id_naviera",
                    "required" => "",
                    "column" => "6",
                    "attr" => [
                        "class" => "form-control",
                        "name" => "id_naviera",
                        "id" => "id_naviera",
                        "required" => "true",
                        "disabled" => "true"
                    ],
                    'options' => []
                ]
            ],
            'list_naviera_codigo' => $this->NavieraCodigo_Model->getAll(),
            'control_digitos_contenedor' => $control_digitos_contenedor,
            'form_add' => [
                [
                    "form_control" => "input",
                    "label" => "Código<span class=\'text-danger\'>*</span>",
                    "column" => "12",
                    "for" => "codigo",
                    "attr"  => [
                        "type" => "text",
                        "name" => "codigo",
                        "id"   => "codigo",
                        "placeholder" => "Código",
                        "required" => "true",
                        "class" => "form-control"
                    ],
                    "value" => "",
                    "extra_attr" => ""
                ],
                [
                    "form_control" => "input",
                    "label" => "Dimension<span class=\'text-danger\'>*</span>",
                    "for" => "dimensiones",
                    "column" => "12",
                    "attr" => [
                        "type" => "dimensiones",
                        "name" => "dimensiones",
                        "id" => "dimensiones",
                        "placeholder" => "Dimensiones",
                        "required" => "true",
                        "class" => "form-control"
                    ],
                    "value" => "",
                    "extra_attr" => ""
                ],
                [
                    "form_control" => "select-add",
                    "label" => "Navieras<span class=\'text-danger\'>*</span> <button type=\'button\' class=\'btn btn-success btn-sm addItem btn-popover\' data-content=\'Agregar Campo\' data-placement=\'top\'><i class=\'mdi mdi-plus\'></i> <span class=\'d-none d-sm-block\'>Agregar Campo</span></button>",
                    "for" => "id_naviera",
                    "required" => "true",
                    "column" => "12",
                    "attr" => [
                        "class" => "form-control id_naviera",
                        "name" => "id_naviera",
                        //"id" => "id_naviera",
                        "required" => "true"
                    ],
                    'options' => $this->Naviera_Model->getAllFormControl(),
                    "btn-info" => true
                ]
            ],
            'form_add_titulo' => 'Código Iso',
            'form_add_url' => 'codigoiso',
            'list_naviera' => $this->Naviera_Model->getAllFormControl(),
            'titulo' => 'Contenedores'
        ];

        $this->load->view('admin/contenedores/list', $data);
    }

    //servicios
    public function new() 
    {
        if (!$this->form_validation->run('contenedores'))
        {
            print json_encode(['res'=>'bad', 'errors'=>$this->form_validation->error_array()]);
            exit();
        }
        $data = $this->input->post();
        if ( $this->General_Model->validateRow( ['codigo'=>$data['codigo']], 'contenedores') ) 
        {
            print json_encode(['res'=>'bad', 'errors'=>['El código del contenedor ingresado ya se encuentra registrado.']]);
            exit();
        }
        if ($this->Contenedor_Model->store($data)) {
            print json_encode(['res'=>'ok', 'msj'=>'Información guardada.']);
        } else {
            print json_encode(['res'=>'bad', 'errors'=>['Problemas para guardar el registro.']]);
        }
        
    }

    public function update($id)
    {
        if (!$this->form_validation->run('contenedores'))
        {
            print json_encode(['res'=>'bad', 'errors'=>$this->form_validation->error_array()]);
            exit();
        }
        $data = $this->input->post();
        if ( $this->General_Model->validateRow( ['codigo'=>$data['codigo']], 'contenedores', $id) ) 
        {
            print json_encode(['res'=>'bad', 'errors'=>['El código del contenedor ingresado ya se encuentra registrado.']]);
            exit();
        }
        if ($this->Contenedor_Model->update($data, $id)) {
            print json_encode(['res'=>'ok', 'msj'=>'Información actualizada.']);
        } else {
            print json_encode(['res'=>'bad', 'errors'=>['Problemas para actualizar el registro.']]);
        }
    }

    public function delete($id)
    {
        if ($this->Contenedor_Model->delete($id)) {
            print json_encode(['res'=>'ok', 'msj'=>'Información eliminado.']);
        } else {
            print json_encode(['res'=>'bad', 'errors'=>['Problemas para eliminar el registro.']]);
        }
    }

    public function viewContainer() {
        $text = $this->input->post('text');
        $array_like = $this->Contenedor_Model->getLike( $text, 'contenedores.codigo' );
        print json_encode(['res'=>'ok', 'like'=>$array_like]);
    }
}

function pagination_general($total, $url, $per_page, $uri_segment){
    $config['total_rows'] = $total;
    $config["base_url"] = base_url().$url;
    $config['per_page'] = $per_page;
    $config['uri_segment'] = $uri_segment;
    $config['first_link'] = '<< Ir al primero';
    $config['last_link'] = 'Ir al ultimo >';
    $config['next_link'] = ' Siguiente ' . '&gt;';
    $config['prev_link'] = ' &lt;' . ' Atras';
    $config['full_tag_open'] = "<ul  class='pagination justify-content-center'>";
    $config['full_tag_close'] ="</ul>";
    $config['num_tag_open'] = '<li class="page-item">';
    $config['num_tag_close'] = '</li>';
    $config['cur_tag_open'] = "<li class='active'><a href='#'>";
    $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
    $config['next_tag_open'] = '<li class="page-item">';
    $config['next_tag_close'] = "</li>";
    $config['prev_tag_open'] = '<li class="page-item">';
    $config['prev_tag_close'] = "</li>";
    $config['first_tag_open'] = '<li class="page-item">';
    $config['first_tag_close'] = "</li>";
    $config['last_tag_open'] = '<li class="page-item">';
    $config['last_tag_close'] = "</li>";
    $config['use_page_numbers'] = TRUE;
    return $config;
}

function objectToArray ( $object ) {
    if(!is_object($object) && !is_array($object)) {
      return $object;
    }  
    return array_map( 'objectToArray', (array) $object );
}