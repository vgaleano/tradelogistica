<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clientes extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Cliente_Model');
        $this->load->model('General_Model');
    }

    public function index()
    {
        redirect(base_url().'clientes/list');
    }

    public function list($offset=0)
    {
        if(!$this->session->logged_in) redirect(base_url()."home");        
        if (!in_array('7', $this->session->permisos)) redirect(base_url().'home/permisos');
        
        $this->load->model('General_Model');
        $servicio = $this->General_Model->modalOn('clientes');
        $this->session->set_userdata(['menu_pos' => $this->input->post('menu_pos')]);

        $total = $this->Cliente_Model->getCountAll();
        $config = pagination_general($total, 'clientes/list/', 10, '4');
        $this->pagination->initialize($config);
        $offset_sql = $offset!=0 ? $offset * $config['per_page'] - $config['per_page'] : $offset;
        $list = $this->Cliente_Model->getAll( $config['per_page'], $offset_sql);

        $data = [
            'url' => 'clientes',
            'ver_form' => '8',
            'nuevo_form' => '9',
            'editar_form' => '10',
            'eliminar_form' => '11',
            'columns' => [
                [
                    'key' => 'cliente',
                    'name' => 'Nombre'
                ],[
                    'key' => 'btn',
                    'name' => 'Acción'
                ],[
                    'key' => '',
                    'name' => 'Editar'
                ],[
                    'key' => '',
                    'name' => 'Eliminar'
                ]
            ],
            'titulo_view' => 'Clientes',
            'titulo_singular' => 'Cliente',
            'service' => $servicio,
            'lists' => $list,
            'offset' => $offset,
            'forms' => [
                [
                    "form_control" => "input",
                    "label" => "Nombre<span class='text-danger'>*</span>",
                    "column" => "12",
                    "for" => "cliente",
                    "attr"  => [
                        "type" => "text",
                        "name" => "cliente",
                        "id"   => "cliente",
                        "placeholder" => "Nombre",
                        "required" => "true",
                        "class" => "form-control"
                    ],
                    "value" => "",
                    "extra_attr" => ""
                ]
            ],
            'titulo' => 'Clientes'
        ];

        $this->load->view('admin/list', $data);
    }

    //servicios
    public function new() 
    {
        if (!$this->form_validation->run('clientes'))
        {
            print json_encode(['res'=>'bad', 'errors'=>$this->form_validation->error_array()]);
            exit();
        }
        $data = $this->input->post();
        if ( $this->General_Model->validateRow( ['cliente'=>$data['cliente']], 'clientes') ) 
        {
            print json_encode(['res'=>'bad', 'errors'=>['El nombre del cliente ya se encuentra registrado.']]);
            exit();
        }
        $id = $this->Cliente_Model->store($data);
        if ( $id ) {
            print json_encode(['res'=>'ok', 'msj'=>'Información guardada.', 'id'=>$id, 'nombre'=>$data['cliente']]);
        } else {
            print json_encode(['res'=>'bad', 'errors'=>['Problemas para guardar el registro.']]);
        }
        
    }

    public function update($id)
    {
        if (!$this->form_validation->run('clientes'))
        {
            print json_encode(['res'=>'bad', 'errors'=>$this->form_validation->error_array()]);
            exit();
        }
        $data = $this->input->post();
        if ( $this->General_Model->validateRow( ['cliente'=>$data['cliente']], 'clientes', $id) ) 
        {
            print json_encode(['res'=>'bad', 'errors'=>['El nombre del cliente ya se encuentra registrado.']]);
            exit();
        }
        if ($this->Cliente_Model->update($data, $id)) {
            print json_encode(['res'=>'ok', 'msj'=>'Información actualizada.']);
        } else {
            print json_encode(['res'=>'bad', 'errors'=>['Problemas para actualizar el registro.']]);
        }
    }

    public function delete($id)
    {
        if ($this->Cliente_Model->delete($id)) {
            print json_encode(['res'=>'ok', 'msj'=>'Información eliminado.']);
        } else {
            print json_encode(['res'=>'bad', 'errors'=>['Problemas para eliminar el registro.']]);
        }
    }
}

function pagination_general($total, $url, $per_page, $uri_segment){
    $config['total_rows'] = $total;
    $config["base_url"] = base_url().$url;
    $config['per_page'] = $per_page;
    $config['uri_segment'] = $uri_segment;
    $config['first_link'] = '<< Ir al primero';
    $config['last_link'] = 'Ir al ultimo >';
    $config['next_link'] = ' Siguiente ' . '&gt;';
    $config['prev_link'] = ' &lt;' . ' Atras';
    $config['full_tag_open'] = "<ul  class='pagination justify-content-center'>";
    $config['full_tag_close'] ="</ul>";
    $config['num_tag_open'] = '<li class="page-item">';
    $config['num_tag_close'] = '</li>';
    $config['cur_tag_open'] = "<li class='active'><a href='#'>";
    $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
    $config['next_tag_open'] = '<li class="page-item">';
    $config['next_tag_close'] = "</li>";
    $config['prev_tag_open'] = '<li class="page-item">';
    $config['prev_tag_close'] = "</li>";
    $config['first_tag_open'] = '<li class="page-item">';
    $config['first_tag_close'] = "</li>";
    $config['last_tag_open'] = '<li class="page-item">';
    $config['last_tag_close'] = "</li>";
    $config['use_page_numbers'] = TRUE;
    return $config;
}