<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function index()
	{		
		if($this->session->logged_in && in_array('1', $this->session->permisos)) redirect(base_url()."home/dashboard");
		$this->load->model('Usuario_Model');
		//ON Recordar
		if( get_cookie('id_user')!==null || get_cookie('marca')!==null ){
			$cookie_ = $this->Usuario_Model->getUserByCookie(get_cookie('id_user'), get_cookie('marca'));
			if ( $cookie_ ) {
				authUser($cookie_[0]);
				redirect(base_url()."home/dashboard");
			}
		}

		$this->load->view('public/login');
	}

	//--------------------------------------------------------------------
	public function dashboard(){
        if(!$this->session->logged_in) redirect(base_url()."home");        
		if (!in_array('1', $this->session->permisos)) redirect(base_url().'home/permisos');
		$this->session->set_userdata(['modal_on'=>0]);
		$data = [
			'titulo' => 'Dashboard'
		];
		$this->load->view('admin/dashboard', $data);
	}

	public function permisos()
	{
		$data['titulo'] = 'Denegado';
		$this->load->view('admin/permisos_denegado', $data);
	}

	//servicios
	public function login()
	{
		$email = $this->input->post('email', TRUE);
		$password = hash('sha256', $this->input->post('password', TRUE) );
		$recordar = $this->input->post('recordar', TRUE);

		$this->load->model('Usuario_Model');
		$usuario = $this->Usuario_Model->login($email, $password);
		if (count($usuario)==0) {
			print json_encode(['res'=>'bad', 'errors'=>['Correo electrónico o contraseña incorrecta.']]);
            exit();
		}

		//cookies - recordar
		if ($recordar) {
			delete_cookie('id_user');
			delete_cookie('marca');
			mt_srand(time());
			$rand = mt_rand(1000000,9999999);
			$this->Usuario_Model->update( [ 'cookie' => $rand ], $usuario[0]->id );			
			set_cookie("id_user", $usuario[0]->id, time()+(60*60*24*365));
			set_cookie("marca", $rand, time()+(60*60*24*365));
		}
		
		authUser($usuario[0]);

		print json_encode(['res'=>'ok']);
	}

	public function loginRecordar(){
		$this->load->model('Usuario_Model');
		//ON Recordar
		if( get_cookie('id_user')!==null || get_cookie('marca')!==null ){
			$cookie_ = $this->Usuario_Model->getUserByCookie(get_cookie('id_user'), get_cookie('marca'));
			if ( $cookie_ ) {
				authUser($cookie_[0]);
				print json_encode(['res'=>'ok']);
				exit();
			}
		}
		print json_encode(array("res"=>"bad", "msj" => "Debe ingresar las credenciales."));
	}

	public function logout() {
		$this->session->sess_destroy();
		print json_encode(array("res"=>"ok", "msj" => "La sesion ha terminado."));
	}

	public function modo_off()
	{
		$this->session->set_userdata(['modal_on'=>false]);
		print json_encode(array("res"=>"ok"));
	}

	/*public function insertPerfil()
	{
		$data = [];
		for ($i=1; $i < 83; $i++) { 
			$data[] = ['id_perfil'=>1, 'id_vista'=>$i];
		}
		$this->db->insert_batch('perfil_vista', $data);
	} */
}

function objectToArray ( $object ) {
	if(!is_object($object) && !is_array($object)) {
	  return $object;
	}  
	return array_map( 'objectToArray', (array) $object );
}

function authUser($usuario)
{
	$ci =& get_instance();
	$ci->load->model('PerfilVista_Model');
	$permisos = $ci->PerfilVista_Model->getPermisos($usuario->id_perfil);

	$newdata = [
		'nombre' => $usuario->nombre,
		'id_perfil' => $usuario->id_perfil,
		'id' => $usuario->id,
		'logged_in' => TRUE,
		'permisos' => array_column( objectToArray($permisos), 'id_vista' ),
		'modal_open' => 0
	];
	$ci->session->set_userdata($newdata);
}