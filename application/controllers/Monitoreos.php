<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Monitoreos extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Monitoreo_Model');
        $this->load->model('General_Model');
    }

    public function index()
    {
        redirect(base_url().'monitoreos/list'); 
    }

    public function list($offset=0)
    {
        if(!$this->session->logged_in) redirect(base_url()."home");        
        if (!in_array('77', $this->session->permisos)) redirect(base_url().'home/permisos');
        
        $this->load->model('General_Model');
        $this->load->model('CodigoIso_Model');
        $this->load->model('ControlDigitosContenedor_Model');
        $this->load->model('NavieraCodigo_Model');
        $this->load->model('Naviera_Model');
        
        $servicio = $this->General_Model->modalOn('monitoreos');
        $this->session->set_userdata(['menu_pos' => $this->input->post('menu_pos')]);

        $total = $this->Monitoreo_Model->getCountAll();
        $config = pagination_general($total, 'monitoreos/list', 10, 3);
        $this->pagination->initialize($config);
        $offset_sql = $offset!=0 ? $offset * $config['per_page'] - $config['per_page'] : $offset;
        $list = $this->Monitoreo_Model->getAll( $config['per_page'], $offset_sql);
        $control_digitos_contenedor = objectToArray( $this->ControlDigitosContenedor_Model->getAll() );
        $letras = array_column($control_digitos_contenedor, 'letra');
        $valores = array_column($control_digitos_contenedor, 'valor');
        $control_digitos_contenedor = array_combine($letras, $valores);

        $data = [
            'url' => 'monitoreos',
            'ver_form' => '78',
            'nuevo_form' => '79',
            'editar_form' => '80',
            'eliminar_form' => '81',
            'columns' => [
                [
                    'key' => 'codigo_contenedor',
                    'name' => 'No Container'
                ],[
                    'key' => 'temperatura_suministro',
                    'name' => 'Temperatura suministro (°C)'
                ],[
                    'key' => 'temperatura_retorno',
                    'name' => 'Temperatura retorno (°C)'
                ],[
                    'key' => 'evento',
                    'name' => 'Evento'
                ],[
                    'key' => 'observaciones',
                    'name' => 'Observaciones'
                ],[
                    'key' => 'btn',
                    'name' => 'Acción'
                ],[
                    'key' => '',
                    'name' => 'Editar'
                ],[
                    'key' => '',
                    'name' => 'Eliminar'
                ]
            ],
            'titulo_view' => 'Monitoreos',
            'titulo_singular' => 'Monitoreo',
            'service' => $servicio,
            'lists' => $list,
            'offset' => $offset,
            'forms' => [
                [
                    "form_control" => "input",
                    "label" => "CONTAINER No.<span class='text-danger'>*</span> <button type='button' class='btn btn-sm btn-info add-item btn-popover' data-url='contenedores' data-content='Crear Contenedor' data-placement='top' " . (in_array('44', $this->session->permisos)?'':'disabled') . "><i class='mdi mdi-plus'></i> <span class='d-none d-sm-block'>Crear Contenedor</span></button>",
                    "column" => "6",
                    "for" => "codigo_contenedor",
                    "attr"  => [
                        "type" => "text",
                        "name" => "codigo_contenedor",
                        "id"   => "codigo_contenedor",
                        "placeholder" => "Código Container",
                        "required" => "true",
                        "class" => "form-control",
                        "maxlength" => "11"
                    ],
                    "value" => "",
                    "extra_attr" => "",
                    "btn-info" => true
                ],
                [
                    "form_control" => "input",
                    "label" => "Temperatura Suministro (°C)<span class='text-danger'>*</span>",
                    "column" => "6",
                    "for" => "temperatura_suministro",
                    "attr"  => [
                        "type" => "text",
                        "name" => "temperatura_suministro",
                        "id"   => "temperatura_suministro",
                        "placeholder" => "Temperatura Suministro",
                        "required" => "true",
                        "class" => "form-control"
                    ],
                    "value" => "",
                    "extra_attr" => ""
                ],
                [
                    "form_control" => "input",
                    "label" => "Temperatura Retorno (°C)<span class='text-danger'>*</span>",
                    "column" => "6",
                    "for" => "temperatura_retorno",
                    "attr"  => [
                        "type" => "text",
                        "name" => "temperatura_retorno",
                        "id"   => "temperatura_retorno",
                        "placeholder" => "Temperatura Retorno",
                        "required" => "true",
                        "class" => "form-control"
                    ],
                    "value" => "",
                    "extra_attr" => ""
                ],
                [
                    "form_control" => "radio",
                    "label" => "Evento<span class='text-danger'>*</span>",
                    "for" => "evento",
                    "required" => "true",
                    "column" => "3",
                    "attr" => [
                        "class" => "form-check-input",
                        "name" => "evento",
                        "id" => "evento"
                    ],
                    'items' => [ (object)['valor'=>'Conexión', 'nombre'=>'Conexión'], (object)['valor'=>'Desconexión', 'nombre'=>'Desconexión'], (object)['valor'=>'Monitoreo', 'nombre'=>'Monitoreo'] ]
                ],
                [
                    "form_control" => "textarea",
                    "label" => "Observaciones",
                    "column" => "6",
                    "for" => "observaciones",
                    "attr"  => [
                        "type" => "text",
                        "name" => "observaciones",
                        "id"   => "observaciones",
                        "placeholder" => "Observaciones",
                        "required" => "true",
                        "class" => "form-control"
                    ],
                    "value" => "",
                    "extra_attr" => ""
                ],
                [
                    "form_control" => "select",
                    "label" => "Código ISO<span class='text-danger'>*</span> <button type='button' class='btn btn-sm btn-info add-item btn-popover' data-url='codigoiso' data-content='Crear Código ISO' data-placement='top' " . (in_array("39", $this->session->permisos)?"":"disabled") . "><i class='mdi mdi-plus'></i> <span class='d-none d-sm-block'>Crear Código ISO</span></button>",
                    "for" => "codigo_iso",
                    "required" => "true",
                    "column" => "6",
                    "attr" => [
                        "class" => "form-control form-container",
                        "name" => "codigo_iso",
                        "id" => "codigo_iso",
                        "required" => "true"
                    ],
                    'options' => $this->CodigoIso_Model->getAllFormControl(),
                    "btn-info" => true,
                ],
                [
                    "form_control" => "select",
                    "label" => "Naviera<span class='text-danger'>*</span>",
                    "for" => "id_naviera",
                    "required" => "",
                    "column" => "6",
                    "attr" => [
                        "class" => "form-control form-container",
                        "name" => "id_naviera",
                        "id" => "id_naviera",
                        "required" => "true"
                    ],
                    'options' => []
                ]
            ],
            'list_naviera_codigo' => $this->NavieraCodigo_Model->getAll(),
            'control_digitos_contenedor' => $control_digitos_contenedor,
            'titulo' => 'Monitoreos',
            'forms_add' => [
                'contenedores' => [
                    'form_add' => [
                        [
                            "form_control" => "select",
                            "label" => "Código ISO<span class='text-danger'>*</span> <button type='button' class='btn btn-sm btn-info add-item btn-popover' data-url='codigoiso' data-content='Crear Código ISO' data-placement='top' " . (in_array("39", $this->session->permisos)?"":"disabled") . "><i class='mdi mdi-plus'></i> <span class='d-none d-sm-block'>Crear Código ISO</span></button>",
                            "for" => "codigo_iso",
                            "required" => "true",
                            "column" => "6",
                            "attr" => [
                                "class" => "form-control",
                                "name" => "codigo_iso",
                                "id" => "codigo_iso",
                                "required" => "true",
                                "disabled" => "true"
                            ],
                            'options' => $this->CodigoIso_Model->getAllFormControl(),
                            "btn-info" => true,
                        ],
                        [
                            "form_control" => "select",
                            "label" => "Naviera<span class='text-danger'>*</span> <button type='button' class='btn btn-sm btn-info add-item btn-popover' data-content='Crear Naviera' data-placement='top' data-url='navieras' " . (in_array("34", $this->session->permisos)?"":"disabled") . "><i class='mdi mdi-plus'></i> <span class='d-none d-sm-block'>Crear Naviera</span></button>",
                            "for" => "id_naviera",
                            "required" => "",
                            "column" => "6",
                            "attr" => [
                                "class" => "form-control",
                                "name" => "id_naviera",
                                "id" => "id_naviera",
                                "required" => "true",
                                "disabled" => "true"
                            ],
                            'options' => []
                        ]
                    ],
                    'form_add_titulo' => 'Contenedor',
                    'form_add_url' => 'contenedores'
                ],
                'codigoiso' => [
                    'form_add' => [
                        [
                            "form_control" => "input",
                            "label" => "Código<span class='text-danger'>*</span>",
                            "column" => "6",
                            "for" => "codigo",
                            "attr"  => [
                                "type" => "text",
                                "name" => "codigo",
                                "id"   => "codigo",
                                "placeholder" => "Código",
                                "required" => "true",
                                "class" => "form-control"
                            ],
                            "value" => "",
                            "extra_attr" => ""
                        ],
                        [
                            "form_control" => "input-add",
                            "label" => "Dimensión<span class='text-danger'>*</span>",
                            "column" => "6",
                            "inputs" => [
                                [
                                    "type" => "text",
                                    "name" => "dimension_largo",
                                    "id"   => "dimension_largo",
                                    "placeholder" => "Largo",
                                    "required" => "true",
                                    "class" => "form-control"
                                ],
                                [
                                    "type" => "text",
                                    "name" => "dimension_alto",
                                    "id"   => "dimension_alto",
                                    "placeholder" => "Alto",
                                    "required" => "true",
                                    "class" => "form-control"
                                ],
                                [
                                    "type" => "text",
                                    "name" => "dimension_ancho",
                                    "id"   => "dimension_ancho",
                                    "placeholder" => "Ancho",
                                    "required" => "true",
                                    "class" => "form-control"
                                ]
                            ]
                        ],
                        [
                            "form_control" => "select-add",
                            "label" => "Navieras <button type='button' class='btn btn-success btn-sm addItem btn-popover' data-content='Agregar Campo' data-placement='top' data-select-add='codigoiso' data-select-position='2'><i class='mdi mdi-plus'></i> <span class='d-none d-sm-block'>Agregar Campo</span></button> <button type='button' class='btn btn-sm btn-info add-item btn-popover' data-content='Crear Naviera' data-placement='top' data-url='navieras' " . (in_array("34", $this->session->permisos)?"":"disabled") . "><i class='mdi mdi-plus'></i> <span class='d-none d-sm-block'>Crear Naviera</span></button>",
                            "for" => "id_naviera",
                            "required" => "true",
                            "column" => "6",
                            "attr" => [
                                "class" => "form-control id_naviera",
                                "name" => "id_naviera",
                                //"id" => "id_naviera",
                                "required" => "true"
                            ],
                            'options' => $this->Naviera_Model->getAllFormControl()
                        ]
                    ],
                    'form_add_titulo' => 'Código ISO',
                    'form_add_url' => 'codigoiso'
                ]
            ]
        ];
        
        $this->load->view('admin/monitoreos/list', $data);
    }

    //servicios
    public function new() 
    {
        if (!$this->form_validation->run('monitores'))
        {
            print json_encode(['res'=>'bad', 'errors'=>$this->form_validation->error_array()]);
            exit();
        }
        $data = $this->input->post();
        if ( array_key_exists('codigo_iso', $data) )
        {
            $data_container = [ 'codigo'=>$data['codigo_contenedor'], 'codigo_iso'=>$data['codigo_iso'], 'id_naviera'=>$data['id_naviera'] ];
            if (!$this->form_validation->run('contenedores'))
            {
                print json_encode(['res'=>'bad', 'errors'=>$this->form_validation->error_array()]);
                exit();
            }
            $this->load->model('Contenedor_Model');
            if (!$this->Contenedor_Model->store($data_container)) {
                print json_encode(['res'=>'bad', 'errors'=>['Problemas para guardar el registro del contenedor.']]);
                exit();
            }
        }
        unset($data['codigo_iso']);
        unset($data['id_naviera']);
        
        if ($this->Monitoreo_Model->store($data)) {
            print json_encode(['res'=>'ok', 'msj'=>'Información guardada.']);
        } else {
            print json_encode(['res'=>'bad', 'errors'=>['Problemas para guardar el registro.']]);
        }
    }

    public function update($id)
    {
        if (!$this->form_validation->run('monitores'))
        {
            print json_encode(['res'=>'bad', 'errors'=>$this->form_validation->error_array()]);
            exit();
        }
        $data = $this->input->post();
        if ($this->Monitoreo_Model->update($data, $id)) {
            print json_encode(['res'=>'ok', 'msj'=>'Información actualizada.']);
        } else {
            print json_encode(['res'=>'bad', 'errors'=>['Problemas para actualizar el registro.']]);
        }
    }

    public function delete($id)
    {
        if ($this->Monitoreo_Model->delete($id)) {
            print json_encode(['res'=>'ok', 'msj'=>'Información eliminado.']);
        } else {
            print json_encode(['res'=>'bad', 'errors'=>['Problemas para eliminar el registro.']]);
        }
    }
}

function pagination_general($total, $url, $per_page, $uri_segment){
    $config['total_rows'] = $total;
    $config["base_url"] = base_url().$url;
    $config['per_page'] = $per_page;
    $config['uri_segment'] = $uri_segment;
    $config['first_link'] = '<< Ir al primero';
    $config['last_link'] = 'Ir al ultimo >';
    $config['next_link'] = ' Siguiente ' . '&gt;';
    $config['prev_link'] = ' &lt;' . ' Atras';
    $config['full_tag_open'] = "<ul  class='pagination justify-content-center'>";
    $config['full_tag_close'] ="</ul>";
    $config['num_tag_open'] = '<li class="page-item">';
    $config['num_tag_close'] = '</li>';
    $config['cur_tag_open'] = "<li class='active'><a href='#'>";
    $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
    $config['next_tag_open'] = '<li class="page-item">';
    $config['next_tag_close'] = "</li>";
    $config['prev_tag_open'] = '<li class="page-item">';
    $config['prev_tag_close'] = "</li>";
    $config['first_tag_open'] = '<li class="page-item">';
    $config['first_tag_close'] = "</li>";
    $config['last_tag_open'] = '<li class="page-item">';
    $config['last_tag_close'] = "</li>";
    $config['use_page_numbers'] = TRUE;
    return $config;
}

function objectToArray ( $object ) {
    if(!is_object($object) && !is_array($object)) {
      return $object;
    }  
    return array_map( 'objectToArray', (array) $object );
}