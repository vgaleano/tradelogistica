<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Medidas extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Medidas_Model');
        $this->load->model('General_Model');
    }

    public function index()
    {
        redirect(base_url().'medidas/list'); 
    }

    public function list($offset=0)
    {
        if(!$this->session->logged_in) redirect(base_url()."home");        
        if (!in_array('62', $this->session->permisos)) redirect(base_url().'home/permisos');
        
        $this->load->model('General_Model');
        $servicio = $this->General_Model->modalOn('medidas');
        $this->session->set_userdata(['menu_pos' => $this->input->post('menu_pos')]);

        $this->load->model('Componente_Model');
        $this->load->model('CodigoReparacion_Model');

        $total = $this->Medidas_Model->getCountAll();
        $config = pagination_general($total, 'medidas/list', 10, 3);
        $this->pagination->initialize($config);
        $offset_sql = $offset!=0 ? $offset * $config['per_page'] - $config['per_page'] : $offset;
        $list = $this->Medidas_Model->getAll( $config['per_page'], $offset_sql);

        $data = [
            'url' => 'medidas',
            'ver_form' => '63',
            'nuevo_form' => '64',
            'editar_form' => '65',
            'eliminar_form' => '66',
            'columns' => [
                [
                    'key' => 'medida',
                    'name' => 'Medida'
                ],[
                    'key' => 'codigo_componente',
                    'name' => 'Componente'
                ],[
                    'key' => 'codigo_reparacion',
                    'name' => 'Código de reparación'
                ],[
                    'key' => 'numero_reparacion',
                    'name' => 'Número de reparación'
                ],[
                    'key' => 'tiempo_reparacion',
                    'name' => 'Tiempo de reparación'
                ],[
                    'key' => 'precio_material',
                    'name' => 'Precio de material'
                ],[
                    'key' => 'btn',
                    'name' => 'Acción'
                ],[
                    'key' => '',
                    'name' => 'Editar'
                ],[
                    'key' => '',
                    'name' => 'Eliminar'
                ]
            ],
            'titulo_view' => 'Medidas',
            'titulo_singular' => 'Medida',
            'service' => $servicio,
            'lists' => $list,
            'offset' => $offset,
            'forms' => [
                [
                    "form_control" => "input",
                    "label" => "Medida<span class='text-danger'>*</span>",
                    "column" => "6",
                    "for" => "medida",
                    "attr"  => [
                        "type" => "text",
                        "name" => "medida",
                        "id"   => "medida",
                        "placeholder" => "Medida",
                        "required" => "true",
                        "class" => "form-control"
                    ],
                    "value" => "",
                    "extra_attr" => ""
                ],
                [
                    "form_control" => "select",
                    "label" => "Componentes<span class='text-danger'>*</span>",
                    "for" => "codigo_componente",
                    "required" => "true",
                    "column" => "6",
                    "attr" => [
                        "class" => "form-control",
                        "name" => "codigo_componente",
                        "id" => "codigo_componente",
                        "required" => "true"
                    ],
                    'options' => $this->Componente_Model->getAllFormControl()
                ],
                [
                    "form_control" => "select",
                    "label" => "Código de reparación<span class='text-danger'>*</span>",
                    "for" => "codigo_reparacion",
                    "required" => "true",
                    "column" => "6",
                    "attr" => [
                        "class" => "form-control",
                        "name" => "codigo_reparacion",
                        "id" => "codigo_reparacion",
                        "required" => "true"
                    ],
                    'options' => $this->CodigoReparacion_Model->getAllFormControl()
                ],
                [
                    "form_control" => "input",
                    "label" => "Número de reparación<span class='text-danger'>*</span>",
                    "column" => "6",
                    "for" => "numero_reparacion",
                    "attr"  => [
                        "type" => "text",
                        "name" => "numero_reparacion",
                        "id"   => "numero_reparacion",
                        "placeholder" => "Número de reparación",
                        "required" => "true",
                        "class" => "form-control"
                    ],
                    "value" => "",
                    "extra_attr" => ""
                ],
                [
                    "form_control" => "input",
                    "label" => "Tiempo de reparación<span class='text-danger'>*</span>",
                    "column" => "6",
                    "for" => "tiempo_reparacion",
                    "attr"  => [
                        "type" => "text",
                        "name" => "tiempo_reparacion",
                        "id"   => "tiempo_reparacion",
                        "placeholder" => "Tiempo de reparación",
                        "required" => "true",
                        "class" => "form-control"
                    ],
                    "value" => "",
                    "extra_attr" => ""
                ],
                [
                    "form_control" => "input",
                    "label" => "Precio material<span class='text-danger'>*</span>",
                    "column" => "6",
                    "for" => "precio_material",
                    "attr"  => [
                        "type" => "text",
                        "name" => "precio_material",
                        "id"   => "precio_material",
                        "placeholder" => "Precio material",
                        "required" => "true",
                        "class" => "form-control"
                    ],
                    "value" => "",
                    "extra_attr" => ""
                ]
            ],
            'titulo' => 'Medidas'
        ];
        
        $this->load->view('admin/list', $data);
    }

    //servicios
    public function new() 
    {
        if (!$this->form_validation->run('medidas'))
        {
            print json_encode(['res'=>'bad', 'errors'=>$this->form_validation->error_array()]);
            exit();
        }
        $medida = $this->input->post('medida');
        if ( $this->General_Model->validateRow( ['medida'=>$medida], 'medidas') ) 
        {
            print json_encode(['res'=>'bad', 'errors'=>['El nombre de la medida ya se encuentra registrado.']]);
            exit();
        }
        $data = $this->input->post();
        if ($this->Medidas_Model->store($data)) {
            print json_encode(['res'=>'ok', 'msj'=>'Información guardada.']);
        } else {
            print json_encode(['res'=>'bad', 'errors'=>['Problemas para guardar el registro.']]);
        }
        
    }

    public function update($id)
    {
        if (!$this->form_validation->run('medidas'))
        {
            print json_encode(['res'=>'bad', 'errors'=>$this->form_validation->error_array()]);
            exit();
        }
        $medida = $this->input->post('medida');
        if ( $this->General_Model->validateRow( ['medida'=>$medida], 'medidas', $id) ) 
        {
            print json_encode(['res'=>'bad', 'errors'=>['El nombre de la medida ya se encuentra registrado.']]);
            exit();
        }
        $data = $this->input->post();
        if ($this->Medidas_Model->update($data, $id)) {
            print json_encode(['res'=>'ok', 'msj'=>'Información actualizada.']);
        } else {
            print json_encode(['res'=>'bad', 'errors'=>['Problemas para actualizar el registro.']]);
        }
    }

    public function delete($id)
    {
        if ($this->Medidas_Model->delete($id)) {
            print json_encode(['res'=>'ok', 'msj'=>'Información eliminado.']);
        } else {
            print json_encode(['res'=>'bad', 'errors'=>['Problemas para eliminar el registro.']]);
        }
    }
}

function pagination_general($total, $url, $per_page, $uri_segment){
    $config['total_rows'] = $total;
    $config["base_url"] = base_url().$url;
    $config['per_page'] = $per_page;
    $config['uri_segment'] = $uri_segment;
    $config['first_link'] = '<< Ir al primero';
    $config['last_link'] = 'Ir al ultimo >';
    $config['next_link'] = ' Siguiente ' . '&gt;';
    $config['prev_link'] = ' &lt;' . ' Atras';
    $config['full_tag_open'] = "<ul  class='pagination justify-content-center'>";
    $config['full_tag_close'] ="</ul>";
    $config['num_tag_open'] = '<li class="page-item">';
    $config['num_tag_close'] = '</li>';
    $config['cur_tag_open'] = "<li class='active'><a href='#'>";
    $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
    $config['next_tag_open'] = '<li class="page-item">';
    $config['next_tag_close'] = "</li>";
    $config['prev_tag_open'] = '<li class="page-item">';
    $config['prev_tag_close'] = "</li>";
    $config['first_tag_open'] = '<li class="page-item">';
    $config['first_tag_close'] = "</li>";
    $config['last_tag_open'] = '<li class="page-item">';
    $config['last_tag_close'] = "</li>";
    $config['use_page_numbers'] = TRUE;
    return $config;
}