<script>
//Seccion dinamica para agregar un item cuando se hace click en agregar campo
    function addItemContent(name_select, options=[], pos_=false, codigo_naviera=false, id=false, id_select=false) {
        var content = '<div class="input-group mb-3">'
                content += '<select class="form-control ' + name_select + '" name="' + name_select + '" required="true" '
                content += id ? 'data-id="' + id + '"' : ''
                content +='>'
                    content += '<option value="" style="display:none">Seleccionar</option>'
                    for (let j = 0; j < options.length; j++) {
                    content += '<option value="' + options[j].value + '"'
                    content += id_select && id_select==options[j].value ? 'selected' : '' 
                    content += '>' + options[j].name + '</option>'
                    }
                content += '</select>'
                if ( pos_=='codigoiso' )
                    content += '<input type="text" name="codigo_naviera" placeholder="Código" class="form-control" value="'+( codigo_naviera ? codigo_naviera:'')+'">'
                content += '<div class="input-group-append">'
                    content += '<span class="input-group-text h-100">'
                        content += '<button type="button" class="btn btn-danger h-100 removeItem"><i class="mdi mdi-close"></i></button>'
                    content += '</span>'
                content += '</div>'
            content += '</div>'
        return content;
    }
    //END
</script>