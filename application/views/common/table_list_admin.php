<table class="table cf">
    <thead class="cf">
        <tr>
        <?php foreach ( $columns as $column ):
            if (empty($column['key'])) { continue; } ?>
            <th><?=$column['name']?></th>
        <?php endforeach; ?>
        </tr>
    </thead>
    <tbody>
    <?php if ( !empty($lists) && is_array($lists) ):
        $array_keys = array_column($columns, 'key');
        foreach ( $lists as $key_list => $list ): $pos=0; ?>
            <tr>
                <?php foreach ( $list as $key_item => $item ):
                    if (!in_array($key_item, $array_keys)) { continue; } ?>
                    <td data-title="<?=$columns[$pos]['name']?>"><?=$item==''?'----':$item;?></td>
                <?php $pos++;
                endforeach; ?>                                
                <td data-title="Acciones">
                <?php if (in_array('Ver', array_column($columns, 'name'))) { ?>
                    <button type="button" class="btn btn-success ver mb-1" data-pos="<?=$key_list?>" <?=in_array($ver_form, $this->session->permisos)?'':'disabled'?> data-toggle="tooltip" data-placement="top" title="Ver"><i class="mdi mdi-eye"></i></button>
                <?php }
                if (in_array('Descargar PDF', array_column($columns, 'name'))) { ?>
                    <button type="button" class="btn btn-success generarPDF mb-1" data-id="<?=$list->id?>" <?php in_array($ver_form, $this->session->permisos)?'':''?> data-toggle="tooltip" data-placement="top" title="Descargar PDF"><i class="mdi mdi-file"></i></button>
                <?php }?>
                    <button type="button" class="btn btn-warning editar mb-1" data-pos="<?=$key_list?>" data-id="<?=$list->id?>" <?php in_array($editar_form, $this->session->permisos) || isset($from_controller) ?'':'disabled'?> data-toggle="tooltip" data-placement="top" title="Editar"><i class="mdi mdi-pencil"></i></button>
                <?php if ( !isset($from_controller) ) { ?>
                    <button type="button" class="btn btn-danger eliminar mb-1" data-id="<?=$list->id?>" <?php in_array($eliminar_form, $this->session->permisos)?'':'disabled'?> data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="mdi mdi-delete"></i></button>
                <?php } ?>
                </td>                  
            </tr>
        <?php endforeach;
    else : ?>
        <tr>
            <td colspan="<?=count($columns);?>" class="text-center font-weight-bold text-break">No hay registros</td>
        </tr>
    <?php endif ?>
    </tbody>
</table>