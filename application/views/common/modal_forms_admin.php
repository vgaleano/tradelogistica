<div class="modal" tabindex="-1" id="modalNew" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><i class="mdi mdi-plus"></i> Crear <?=$titulo_singular?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="<?=$url;?>/new" method="POST" onsubmit="return false">
                    <div class="row">
                    <?php foreach ( $forms as $form_ ): ?>
                        <div class="col-12 col-lg-<?=$form_['column']?> form-group">
                            <?php if ( isset($form_['label']) ): ?>
                                <label for="<?=$form_['for']?>"><?=$form_['label']?></label>
                            <?php endif;
                            if ( isset($form_['btn-info']) && $form_['btn-info'] ) { ?>
                                <button type="button" class="btn btn-outline-warning view-info-btn float-right d-block d-sm-none"><i class="mdi mdi-alert-circle-outline"></i></button>
                            <?php }                            
                            $data['form'] = $form_;
                            if ($form_['form_control']!='select-add') {                                  
                                $this->load->view('common/form_control/'.$form_['form_control'], $data);
                            }else{ ?>
                                <div class="input-group mb-3">
                                    <?php $this->load->view('common/form_control/select', $data); ?>
                                    <div class="input-group-append">
                                        <span class="input-group-text h-100">
                                            <button type="button" class="btn btn-danger h-100 removeItem"><i class="mdi mdi-close"></i></button>
                                        </span>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    <?php endforeach; ?>
                        <div class="col-12 text-right">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                            <button type="submit" class="btn btn-info save" id="save">Guardar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>