<div class="row page-titles mb-0">
    <div class="col-md-5 col-8 align-self-center">
        <h3 class="text-themecolor"><?=$titulo?></h3>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Inicio</a></li>
            <?php if ( isset($titulo_2) ) { ?> 
                <li class="breadcrumb-item">
                    <a href="<?=base_url().$url?>"><?=$titulo_1?></a> 
                </li>
                <li class="breadcrumb-item active"><?=$titulo_2?></li>
            <?php }else{
                echo '<li class="breadcrumb-item active">'.$titulo.'</li>';
            } ?>
        </ol>
    </div>
    <div class="col-md-7 col-4 align-self-center p-0-xs">
        <?php if (isset($titulo_singular) && isset($url)) {
            echo isset($view_page)?'<a href="'.base_url().$url.'/create" class="btn btn-info pull-right"'.(in_array($nuevo_form, $this->session->permisos)?'':' disabled').'><i class="mdi mdi-plus"></i> <span class="d-none d-sm-block">Nuevo '.$titulo_singular.'</span></a>':'<button type="button" class="btn btn-info pull-right" id="btn-newModal" data-toggle="modal" data-target="#modalNew"'.(in_array($nuevo_form, $this->session->permisos)?'':' disabled').'><i class="mdi mdi-plus"></i> <span class="d-none d-sm-block">Nuevo '.$titulo_singular.'</span></button>';
        }
        if (!isset($titulo_singular) && isset($url) && !isset($from_controller)) { ?>
            <a href="<?=base_url().$url?>" class="btn btn-dark pull-right"><i class="mdi mdi-arrow-left"></i></a>
        <?php } ?>
    </div>
</div>