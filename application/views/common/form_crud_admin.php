<script>
    var lists = <?=json_encode($lists);?>;
    var forms = <?=json_encode($forms);?>;
    var input = ['input', 'textarea', 'select'];
    var add_input = false;

    $('document').ready(function () {
        var service = '<?=$service?>'
        if (service!='') 
            $('#modalNew').modal('show')
    })

    //Nuevo Modal
    $('body').on('click', '#btn-newModal', function () {
        $('form')[0].reset()
        $('form input[type=radio]').attr('checked', false)
        $('#modalNew h5').html('<i class="mdi mdi-plus"></i> Nuevo <?=$titulo_singular?>')
    })

    //Editar Modal
    $('body').on('click', 'table .editar', function () {
        $('#modalNew').modal('show')
        $('form')[0].reset()
        $('form input[type=radio]').attr('checked', false)
        var pos_item = $(this).attr('data-pos'),
        item = lists[pos_item];
        $('#modalNew form').attr('action', '<?=$url?>/update/'+item.id)
        $('#modalNew h5').html('<i class="mdi mdi-pencil"></i> Editar <?=$titulo_singular?>').append('<input type="hidden" name="id" value="'+item.id+'">')
        for (let i = 0; i < forms.length; i++) {
            if (input.includes(forms[i].form_control)) 
                $('form [name='+forms[i].attr.name+']').val(item[forms[i].attr.name])
            if (forms[i].form_control=='radio') 
                $('form #'+forms[i].attr.id+item[forms[i].attr.name]).attr('checked', true)
            if (forms[i].form_control=='select-add') 
                addItemEditarModal(pos_item)
        }
    })

    //reset form when open modal
    $(document).on('show.bs.modal', '.modal', function () {
        resetModal()
        <?php if (isset($form_add)) { ?>
        removeAddItemModal()
        <?php } ?>
    });
</script>