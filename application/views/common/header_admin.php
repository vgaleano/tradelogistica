<header class="topbar">
    <nav class="navbar top-navbar navbar-toggleable-sm navbar-light">
        <div class="navbar-header">
            <a class="navbar-brand" href="<?=base_url()?>home/dashboard">
                <b class="text-white">
                    T
                </b>
                <span class="text-white">  
                    Tradelogistica
                </span>
            </a>
        </div>
        <div class="navbar-collapse">
            <ul class="navbar-nav mr-auto mt-md-0">
                <li class="nav-item">
                    <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)">
                        <i class="mdi mdi-menu"></i>
                    </a> 
                </li>
            </ul>
            <ul class="navbar-nav my-lg-0">
                <li class="nav-item d-flex align-items-center">
                    <a class="btn btn-danger log-out" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Cerrar Sesión</a>
                </li>
            </ul>
        </div>
    </nav>
</header>