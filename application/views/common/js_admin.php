    <script src="<?=base_url()?>public/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?=base_url()?>public/plugins/bootstrap/js/tether.min.js"></script>
    <script src="<?=base_url()?>public/plugins/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?=base_url()?>public/js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="<?=base_url()?>public/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="<?=base_url()?>public/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="<?=base_url()?>public/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <!--Custom JavaScript -->
    <script src="<?=base_url()?>public/js/custom.min.js"></script>
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
    <script type="text/javascript">var base_url = "<?=base_url();?>"; </script>
    <script src="<?=base_url()?>public/js/scripts.js"></script>
    <script src="<?=base_url()?>public/js/sweetalert2.min.js"></script>    
    <script src="<?=base_url()?>public/js/jquery.redirect.js"></script>
    <script>
        //cerrar sesion
        $('body').on('click', '.log-out', function() {
            ajax('home/logout',
            {},
            function(data){
                Swal.fire( { icon: 'success', title: data.msj, showConfirmButton: false, timer: 1500 } )
                setTimeout(function() { window.location.href=base_url; }, 1500);
            },10000);
        })
        
        $('.new').on('click', function () {
            sessionStorage.setItem('AnteriorView', typeof(sessionStorage.ActualView)!='undefined' ? sessionStorage.ActualView : 0 )
            sessionStorage.setItem('ActualView', typeof(sessionStorage.AnteriorView)!='undefined' ? (parseInt(sessionStorage.AnteriorView) + 1) : 0 )
            var _url = base_url+$(this).attr('data-url'),
                data_pos = $(this).attr('data-pos');
            $.redirect(_url, {service: true, menu_pos: data_pos}, "POST"); 
        })

        $('.list-menu').on('click', function (e) {
            e.preventDefault();
            var _url = $(this).attr('href'),
                data_pos = $(this).attr('data-pos');
            $.redirect(_url, {menu_pos: data_pos}, "POST"); 
        })

        $(document).ready(function () {            
            $('[data-toggle="tooltip"]').tooltip();
            
            <?php if ( $this->session->modal_on ) { ?>
                ajax('home/modo_off', {}, function(data){},10000);
            <?php } ?>

            //menu
            $('.slimScrollDiv>.scroll-sidebar>nav.sidebar-nav>ul>li').eq('<?=$this->session->menu_pos?>').addClass('selected-option')
            <?php if ( $this->session->menu_pos > 5) { ?>
            $('.slimScrollDiv>.scroll-sidebar').scrollTop( $('.slimScrollDiv>.scroll-sidebar>nav.sidebar-nav>ul>li').eq('<?=$this->session->menu_pos?>').offset().top )
            <?php } ?>
            <?php if ( $this->session->menu_pos == 1) { ?>
                var titulo_view = '<?=$titulo_view?>';
                $('.slimScrollDiv>.scroll-sidebar>nav.sidebar-nav>ul>li').eq(1).addClass('active').children('a').attr('aria-expanded', true)
                if ( ['Crear Inspección', 'Inspecciones'].includes(titulo_view) ) 
                    $('.slimScrollDiv>.scroll-sidebar>nav.sidebar-nav>ul>li').eq(1).children('ul').addClass('in').attr('aria-expanded', true).children('li').eq( ( titulo_view=='Crear Inspección' ? 1 : 0 ) ).addClass('active').children().addClass('active')
            <?php } ?>
            
        })

        //en input y textarea, la primera letra va en mayuscula
        $('body').on('change', 'input[type=text], textarea', function () {
            if ( $(this).attr('name') != 'email' ) {
                var str = $(this).val();
                $(this).val( str.charAt(0).toUpperCase() + str.substr(1) )
            }            
        })

        //en input type checkbox
        $('body').on('change', 'input[type=checkbox]', function () {
            $( '[name='+$(this).attr('id')+']' ).val( $(this).is(":checked")?'1':'0' )
        })

        //Eliminar
        <?php if (isset($url)) { ?>
        $('body').on('click', 'table .eliminar', function () {
            var id = $(this).attr('data-id')
            ajax('<?=$url?>/delete/'+id, {}, function (data) {
                if (data.res=='ok') {
                    Swal.fire({ icon: 'success',  title: 'Registro eliminado', showConfirmButton: false, timer: 1500 })
                    setTimeout(function() { location.reload(); }, 1500);
                } else {
                    var content = '';
                    for(let i in data.errors) 
                        content+=data.errors[i]+'\n'
                    Swal.fire({ icon: 'error', title: 'Oops...', text: content })
                }
            })
        })
        <?php } ?>

        //view info botones
        $('body').on('click', '.view-info-btn', function () {
            $('.btn-popover').popover('show')
            setTimeout(function() { $('.btn-popover').popover('hide') }, 1000);
        })
    </script>