<script>
//ADD item de la sección agregar campo del modal
    $('body').on('click', '.addItem', function () {
        var pos_ = $(this).attr('data-select-add'),
            pos_select_add = $(this).attr('data-select-position'),
            item = forms_add[pos_].form_add[pos_select_add];
        if ( $(this).parent().parent().children( '.input-group' ).length < item.options.length ) {
            var content = addItemContent( item.for, item.options, pos_ )
            $(this).closest('.form-group').append(content)
        }                
    })
    //END

    //Elimina un item de la sección agregar campo del modal
    $('body').on('click', '.removeItem', function () {
        $(this).closest('.input-group').remove()
    })
    //END
</script>