<script>    
    var form_add = <?=json_encode($form_add);?>;
    //AGREGAR al modal un formulario dinamico
    $('body').on('click', '.add-item', function(){
        $('.modal form .row').eq(1).remove()
        add_input = true
        $('.modal form').addClass('row')
        $('.modal form .row').eq(0).addClass('col-12 col-lg-6 mx-0 w-100')
        $('.modal form .row .col-lg-4').removeClass('col-lg-4').addClass('col-lg-12')
        $('.modal form .row .col-lg-6').removeClass('col-lg-6').addClass('col-lg-12')
        var content = '<div class="row col-12 col-lg-6 mx-0 w-100 border-left">'
            content += '<form action="<?=$form_add_url;?>/new" method="POST" onsubmit="return false" class="w-100">'
                content += '<h3 class="mb-0">Nuevo <?=$form_add_titulo?></h3>'
            for (let i = 0; i < form_add.length; i++) {
                content += '<div class="col-12 col-lg-'+form_add[i]['column']+' form-group">'
                if (typeof form_add[i]['label'] !== undefined ) 
                    content += '<label for="'+form_add[i].for+'">'+form_add[i]['label']+'</label>'
                if ( form_add[i]['form_control'] == 'input' ) {
                    content += '<input value="'+form_add[i]['value']+'" '+(form_add[i]['extra_attr']!=''?form_add[i]['extra_attr']:'')+' ';
                    for (attr in form_add[i]['attr']) 
                        content += attr+'="'+form_add[i]['attr'][attr]+'" ';
                    content += '>';
                }
                if ( form_add[i]['form_control'] == 'select' ) {
                    content += '<select ';
                    for (attr in form_add[i]['attr']) 
                        content += attr+'="'+form_add[i]['attr'][attr]+'" ';
                    content += '><option value="" style="display:none">Seleccionar</option>';
                    for (let j = 0; j < form_add[i]['options'].length; j++) 
                        content += '<option value="'+form_add[i]['options'][j]['value']+'">'+form_add[i]['options'][j]['name']+'</option>';
                    content += '</select>';
                }
                if ( form_add[i]['form_control'] == 'select-add' ) {
                    content += addItemContent()
                }
                if ( form_add[i]['form_control'] == 'textarea' ) {
                    content += '<textarea ';
                    for (attr in form_add[i]['attr']) 
                        content += attr+'="'+form_add[i]['attr'][attr]+'" ';
                    content += '>'+form_add[i]['value']+'</textarea>';
                }
                content += '</div>'
            }
                content += '<div class="col-12 text-right">'
                    content += '<button type="button" class="btn btn-secondary remove-add-item">Cancelar</button>'
                    content += '<button type="submit" class="btn btn-info ml-2 save">Guardar</button>'
                content += '</div>'
            content += '</form>'
        content += '</div>'
        
        $('.modal form').append(content)
    })

    $('body').on('click', '.remove-add-item', function (){
        removeAddItemModal()
    })

    function removeAddItemModal() {
        $('.modal form .row').eq(1).remove()
        $('.modal form .row').eq(0).attr('class', 'row mx-0 w-100')
        $('.modal form .row .col-lg-12').removeClass('col-lg-12').addClass('col-lg-6')
    }
</script>