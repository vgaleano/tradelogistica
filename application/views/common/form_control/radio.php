<?php
if ( !empty($form['items']) && is_array($form['items']) ):
    $form_control = '';
    foreach ( $form['items'] as $k_i => $item ):
        $form_control .= '<div class="form-check"><input class="'.$form['attr']['class'].'" type="radio" name="'.$form['attr']['name'].'" value="'.$item->valor.'" id="'.$form['attr']['id'].$item->valor.'" '. (isset($form['attr']['disabled']) ? 'disabled="'.$form['attr']['disabled'].'"' : '') .'><label class="form-check-label" for="'.$form['attr']['id'].$item->valor.'">'.$item->nombre.'</label></div>';
    endforeach;
    echo $form_control;
endif;