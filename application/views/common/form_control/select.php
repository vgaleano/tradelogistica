<?php
$form_control = '<select ';
foreach ($form['attr'] as $attr => $val) {
    $form_control .= $attr.'="'.$val.'" ';
}
$form_control .= '><option value="" style="display:none">Seleccionar</option>';
foreach ($form['options'] as $opt) {
    $form_control .= '<option value="'.$opt->value.'" '.( isset($opt->extra_data) ? 'data="'.$opt->extra_data.'"' : '' ).'>'.$opt->name.'</option>';
}
$form_control .= '</select>';
echo $form_control;