<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?=base_url()?>public/images/favicon.png">
    <title><?=APP_NAME." | ".(isset($titulo) ? $titulo : "" )?></title>
    <!-- Bootstrap Core CSS -->
    <link href="<?=base_url()?>public/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?=base_url()?>public/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?=base_url()?>public/css/style.css" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="<?=base_url()?>public/css/colors/blue.css" id="theme" rel="stylesheet">
    <link href="<?=base_url()?>public/css/style_custom.css" id="theme" rel="stylesheet">
    <link href="<?=base_url()?>public/css/sweetalert2.min.css" id="theme" rel="stylesheet">