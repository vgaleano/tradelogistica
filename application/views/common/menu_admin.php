<aside class="left-sidebar">
    <div class="scroll-sidebar">
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li> 
                    <a class="waves-effect waves-dark list-menu" href="<?=base_url()?>" aria-expanded="false" data-pos="0"><i class="mdi mdi-gauge"></i><span class="hide-menu">Dashboard</span></a>
                </li>
                <li class="sidebar-item"> 
                    <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-account-multiple"></i><span class="hide-menu">Inspecciones</span></a>
                    <ul aria-expanded="false" class="collapse  first-level">
                        <li class="sidebar-item">
                            <a href="<?=base_url()?>inspecciones/list" class="sidebar-link list-menu" data-pos="1">
                                <i class="mdi mdi-format-list-bulleted"></i>
                                <span> Lista</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="#" class="sidebar-link new" data-url="inspecciones/create" data-pos="1">
                                <i class="mdi mdi-account-plus"></i>
                                <span> Añadir</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li> 
                    <a class="waves-effect waves-dark list-menu" href="<?=base_url()?>reportes" aria-expanded="false" data-pos="2"><i class="mdi mdi-file-excel"></i><span class="hide-menu">Reportes</span></a>
                </li>
                <li class="sidebar-item"> 
                    <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-account-check"></i><span class="hide-menu">Usuarios</span></a>
                    <ul aria-expanded="false" class="collapse  first-level">
                        <li class="sidebar-item">
                            <a href="<?=base_url()?>usuarios/list" class="sidebar-link list-menu" data-pos="3">
                                <i class="mdi mdi-format-list-bulleted"></i>
                                <span> Lista</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="#" class="sidebar-link new" data-url="usuarios/list" data-pos="3">
                                <i class="mdi mdi-account-plus"></i>
                                <span> Añadir</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="sidebar-item"> 
                    <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-account-multiple"></i><span class="hide-menu">Clientes</span></a>
                    <ul aria-expanded="false" class="collapse  first-level">
                        <li class="sidebar-item">
                            <a href="<?=base_url()?>clientes/list" class="sidebar-link list-menu" data-pos="4">
                                <i class="mdi mdi-format-list-bulleted"></i>
                                <span> Lista</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="#" class="sidebar-link new" data-url="clientes/list" data-pos="4">
                                <i class="mdi mdi-account-plus"></i>
                                <span> Añadir</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="sidebar-item"> 
                    <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-ferry"></i><span class="hide-menu">Navieras</span></a>
                    <ul aria-expanded="false" class="collapse  first-level">
                        <li class="sidebar-item">
                            <a href="<?=base_url()?>navieras/list" class="sidebar-link list-menu" data-pos="5">
                                <i class="mdi mdi-format-list-bulleted"></i>
                                <span> Lista</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="#" class="sidebar-link new" data-url="navieras/list" data-pos="5">
                                <i class="mdi mdi-account-plus"></i>
                                <span> Añadir</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="sidebar-item"> 
                    <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-apps"></i><span class="hide-menu">Clasificaciones</span></a>
                    <ul aria-expanded="false" class="collapse  first-level">
                        <li class="sidebar-item">
                            <a href="<?=base_url()?>clasificaciones/list" class="sidebar-link list-menu" data-pos="6">
                                <i class="mdi mdi-format-list-bulleted"></i>
                                <span> Lista</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="#" class="sidebar-link new" data-url="clasificaciones/list" data-pos="6">
                                <i class="mdi mdi-account-plus"></i>
                                <span> Añadir</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="sidebar-item"> 
                    <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-account-multiple"></i><span class="hide-menu">Transportadores</span></a>
                    <ul aria-expanded="false" class="collapse  first-level">
                        <li class="sidebar-item">
                            <a href="<?=base_url()?>transportadores/list" class="sidebar-link list-menu" data-pos="7">
                                <i class="mdi mdi-format-list-bulleted"></i>
                                <span> Lista</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="#" class="sidebar-link new" data-url="transportadores/list" data-pos="7">
                                <i class="mdi mdi-account-plus"></i>
                                <span> Añadir</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="sidebar-item"> 
                    <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-steering"></i><span class="hide-menu">Conductores</span></a>
                    <ul aria-expanded="false" class="collapse  first-level">
                        <li class="sidebar-item">
                            <a href="<?=base_url()?>conductores/list" class="sidebar-link list-menu" data-pos="8">
                                <i class="mdi mdi-format-list-bulleted"></i>
                                <span> Lista</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="#" class="sidebar-link new" data-url="conductores/list" data-pos="8">
                                <i class="mdi mdi-account-plus"></i>
                                <span> Añadir</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="sidebar-item"> 
                    <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-car"></i><span class="hide-menu">Vehículos</span></a>
                    <ul aria-expanded="false" class="collapse  first-level">
                        <li class="sidebar-item">
                            <a href="<?=base_url()?>vehiculos/list" class="sidebar-link list-menu" data-pos="9">
                                <i class="mdi mdi-format-list-bulleted"></i>
                                <span> Lista</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="#" class="sidebar-link new" data-url="vehiculos/list" data-pos="9">
                                <i class="mdi mdi-account-plus"></i>
                                <span> Añadir</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="sidebar-item"> 
                    <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-account-multiple"></i><span class="hide-menu">Código ISOs</span></a>
                    <ul aria-expanded="false" class="collapse  first-level">
                        <li class="sidebar-item">
                            <a href="<?=base_url()?>codigoiso/list" class="sidebar-link list-menu" data-pos="10">
                                <i class="mdi mdi-format-list-bulleted"></i>
                                <span> Lista</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="#" class="sidebar-link new" data-url="codigoiso/list" data-pos="10">
                                <i class="mdi mdi-account-plus"></i>
                                <span> Añadir</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="sidebar-item"> 
                    <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-checkbox-multiple-blank-outline"></i><span class="hide-menu">Contenedores</span></a>
                    <ul aria-expanded="false" class="collapse  first-level">
                        <li class="sidebar-item">
                            <a href="<?=base_url()?>contenedores/list" class="sidebar-link list-menu" data-pos="11">
                                <i class="mdi mdi-format-list-bulleted"></i>
                                <span> Lista</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="#" class="sidebar-link new" data-url="contenedores/list" data-pos="11">
                                <i class="mdi mdi-account-plus"></i>
                                <span> Añadir</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="sidebar-item"> 
                    <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-information"></i><span class="hide-menu">Código de Daños</span></a>
                    <ul aria-expanded="false" class="collapse  first-level">
                        <li class="sidebar-item">
                            <a href="<?=base_url()?>codigodamage/list" class="sidebar-linklist-menu" data-pos="12">
                                <i class="mdi mdi-format-list-bulleted"></i>
                                <span> Lista</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="#" class="sidebar-link new" data-url="codigodamage/list" data-pos="12">
                                <i class="mdi mdi-account-plus"></i>
                                <span> Añadir</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="sidebar-item"> 
                    <a class="sidebar-link has-arrow waves-effect waves-dark d-flex" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-check-all"></i><span class="hide-menu">Código de <br>Reparaciones</span></a>
                    <ul aria-expanded="false" class="collapse  first-level">
                        <li class="sidebar-item">
                            <a href="<?=base_url()?>codigoreparaciones/list" class="sidebar-link list-menu" data-pos="13">
                                <i class="mdi mdi-format-list-bulleted"></i>
                                <span> Lista</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="#" class="sidebar-link new" data-url="codigoreparaciones/list" data-pos="13">
                                <i class="mdi mdi-account-plus"></i>
                                <span> Añadir</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="sidebar-item"> 
                    <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-apps"></i><span class="hide-menu">Componentes</span></a>
                    <ul aria-expanded="false" class="collapse  first-level">
                        <li class="sidebar-item">
                            <a href="<?=base_url()?>componentes/list" class="sidebar-link list-menu" data-pos="14">
                                <i class="mdi mdi-format-list-bulleted"></i>
                                <span> Lista</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="#" class="sidebar-link new" data-url="componentes/list" data-pos="14">
                                <i class="mdi mdi-account-plus"></i>
                                <span> Añadir</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="sidebar-item"> 
                    <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-ruler"></i><span class="hide-menu">Medidas</span></a>
                    <ul aria-expanded="false" class="collapse  first-level">
                        <li class="sidebar-item">
                            <a href="<?=base_url()?>medidas/list" class="sidebar-link list-menu" data-pos="15">
                                <i class="mdi mdi-format-list-bulleted"></i>
                                <span> Lista</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="#" class="sidebar-link new" data-url="medidas/list" data-pos="15">
                                <i class="mdi mdi-account-plus"></i>
                                <span> Añadir</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="sidebar-item"> 
                    <a class="sidebar-link has-arrow waves-effect waves-dark d-flex" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-account-multiple"></i><span class="hide-menu">Código de <br>Responsabilidad</span></a>
                    <ul aria-expanded="false" class="collapse  first-level">
                        <li class="sidebar-item">
                            <a href="<?=base_url()?>codigoresponsabilidad/list" class="sidebar-link list-menu" data-pos="16">
                                <i class="mdi mdi-format-list-bulleted"></i>
                                <span> Lista</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="#" class="sidebar-link new" data-url="codigoresponsabilidad/list" data-pos="16">
                                <i class="mdi mdi-account-plus"></i>
                                <span> Añadir</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="sidebar-item"> 
                    <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-account-multiple"></i><span class="hide-menu">Monitoreos</span></a>
                    <ul aria-expanded="false" class="collapse  first-level">
                        <li class="sidebar-item">
                            <a href="<?=base_url()?>monitoreos/list" class="sidebar-link list-menu" data-pos="17">
                                <i class="mdi mdi-format-list-bulleted"></i>
                                <span> Lista</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="#" class="sidebar-link new" data-url="monitoreos/list" data-pos="17">
                                <i class="mdi mdi-account-plus"></i>
                                <span> Añadir</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="sidebar-item"> 
                    <a class="sidebar-link has-arrow waves-effect waves-dark d-flex" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-account-multiple"></i><span class="hide-menu">Guías de <br>cotización</span></a>
                    <ul aria-expanded="false" class="collapse  first-level">
                        <li class="sidebar-item">
                            <a href="#" class="sidebar-link list-menu" data-pos="18">
                                <i class="mdi mdi-format-list-bulleted"></i>
                                <span> Lista</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="#" class="sidebar-link new" data-url="#" data-pos="18">
                                <i class="mdi mdi-account-plus"></i>
                                <span> Añadir</span>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>
    </div>
    <div class="sidebar-footer">
        <a href="<?=base_url()?>configuraciones" class="link" data-toggle="tooltip" title="Settings"><i class="ti-settings"></i></a>
        <a href="" class="link" data-toggle="tooltip" title="Email"><i class="mdi mdi-gmail"></i></a>
        <a href="" class="link" data-toggle="tooltip" title="Logout"><i class="mdi mdi-power"></i></a>
    </div>
</aside>