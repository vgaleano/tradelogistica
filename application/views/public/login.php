<?php $this->load->view('common/head_admin'); ?>

</head>

<body class="fix-header fix-sidebar">
    <?php $this->load->view('common/preloader'); ?>
    <div id="main-wrapper">
        <div class="auth-wrapper d-flex justify-content-center align-items-center">
            <div class="card">
                <div class="card-block p-4">
                    <form action="#" onsubmit="return false">
                        <h2 class="card-title text-center">Iniciar sesión</h2>
                        <div class="form-group">
                            <input type="text" class="form-control" name="email" placeholder="Correo electrónico" require>
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" name="password" placeholder="Contraseña" require>
                        </div>
                        <div class="form-group">
                            <input type="checkbox" id="recordar">
                            <label for="recordar">Recuérdeme</label>
                        </div>
                        <div class="form-group mb-0">
                            <button class="btn btn-success btn-block" id="login">Iniciar sesión</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <?php $this->load->view('common/js_admin');?>
    <script>
        $('#login').on('click', function(){
            var bandera = true
            if ( $('[name=email]').val().trim() == '' ) {
                bandera = false
                return Swal.fire({ icon: 'error', title: 'Oops...', text: 'Ingresar el correo electrónico.' })
            }else if( !validarCorreo($('[name=email]').val().trim()) ) {
                bandera = false
                return Swal.fire({ icon: 'error', title: 'Oops...', text: 'Ingresar un correo electrónico válido.' })
            }
            if ( $('[name=password]').val().trim() == '' ) {
                bandera = false
                return Swal.fire({ icon: 'error', title: 'Oops...', text: 'Ingresar una contraseña.' })
            }
            if (bandera) {
                ajax('home/login',{
                    email: $('[name=email]').val().trim(),
                    password: $('[name=password]').val().trim(),
                    recordar: $('#recordar').is(":checked")
                }, function (data) {
                    if (data.res=='ok') {
                        window.location.href=base_url+'home/dashboard';
                    }else{
                        return Swal.fire({ icon: 'error', title: 'Oops...', text: data.msj })
                    }
                })
            }
        })

        function loginRecordar() {
            ajax('home/loginRecordar',{}, function (data) {
                if (data.res=='ok') {
                    window.location.href=base_url+'home/dashboard';
                }else{
                    return Swal.fire({ icon: 'error', title: 'Oops...', text: data.msj })
                }
            })
        }
    </script>
    </body>

</html>