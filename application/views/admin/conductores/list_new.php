<?php $this->load->view('common/head_admin'); ?>
<link href="<?=base_url()?>public/css/signature-pad.css" rel="stylesheet">
</head>

<body class="fix-header fix-sidebar card-no-border">
    <?php $this->load->view('common/preloader'); ?>
    <div id="main-wrapper">

        <?php $this->load->view('common/header_admin');
        $this->load->view('common/menu_admin'); ?>

        <div class="page-wrapper">
            <div class="container-fluid">

                <?php $this->load->view('common/titulo_page_admin');?>

                <div class="row py-3 px-5">
                    <div class="col-12">
                        <div class="card" id="no-more-tables">
                            <div class="card-block">
                                <table class="table">
                                    <thead>
                                        <tr>
                                        <?php foreach ( $columns as $column ): 
                                            if (empty($column['key'])) { continue; } ?>
                                            <th><?=$column['name']?></th>
                                        <?php endforeach; ?>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php if ( !empty($lists) && is_array($lists) ):
                                        foreach ( $lists as $key_list => $list ):  ?>
                                            <tr>
                                                <td data-title="<?=$columns[0]['name']?>"><?=$list->conductor?></td>
                                                <td data-title="<?=$columns[1]['name']?>"><?=$list->numero_documento?></td>
                                                <td data-title="<?=$columns[2]['name']?>">
                                                    <?php if (count($list->transportadores)>0) { ?>
                                                        <ul>
                                                        <?php foreach ($list->transportadores as$v) { ?>
                                                            <?="<li>".$v->transportador."</li>"?>
                                                        <?php } ?>
                                                        </ul>
                                                    <?php }else{
                                                        echo '---';
                                                    } ?>
                                                </td>
                                                <td data-title="<?=$columns[3]['name']?>">
                                                    <button type="button" class="btn btn-warning editar mb-1" data-pos="<?=$key_list?>" data-toggle="tooltip" data-placement="top" title="Editar" <?php in_array($editar_form, $this->session->permisos)?'':'disabled'?>><i class="mdi mdi-pencil"></i></button>
                                                    <button type="button" class="btn btn-danger eliminar mb-1" data-id="<?=$list->id?>" data-toggle="tooltip" data-placement="top" title="Eliminar" <?php in_array($eliminar_form, $this->session->permisos)?'':'disabled'?>><i class="mdi mdi-delete"></i></button>
                                                </td>
                                            </tr>
                                        <?php endforeach;
                                    else : ?>
                                        <tr>
                                            <td colspan="<?=count($columns);?>" class="text-center font-weight-bold text-break">No hay registros</td>
                                        </tr>
                                    <?php endif ?>
                                    </tbody>
                                </table>
                            </div>
                            <?php if ( $this->pagination->create_links() !== null && $this->pagination->create_links() != '' ) { ?>
                            <div class="card-footer">
                                <?php echo $this->pagination->create_links(); ?>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="modal" tabindex="-1" id="modalNew" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title"><i class="mdi mdi-plus"></i> Crear <?=$titulo_singular?></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="<?=base_url().$url;?>/new" onsubmit="return false" method="POST" target="my_iframe" enctype="multipart/form-data">
                            <div class="row">
                                <input type="hidden" name="list_transportador">
                            <?php foreach ( $forms as $form_ ): ?>
                                <div class="col-12 col-lg-<?=$form_['column']?> form-group">

                                    <?php if ( isset($form_['label']) ): ?>
                                        <label for="<?=$form_['for']?>"><?=$form_['label']?></label>
                                    <?php endif;
                                    if ( isset($form_['btn-info']) && $form_['btn-info'] ) { ?>
                                        <button type="button" class="btn btn-outline-warning view-info-btn float-right d-block d-sm-none"><i class="mdi mdi-alert-circle-outline"></i></button>
                                    <?php }                            
                                    $data['form'] = $form_;
                                    if ($form_['form_control']!='select-add') {                                  
                                        $this->load->view('common/form_control/'.$form_['form_control'], $data);
                                    }else{ ?>
                                        <div class="input-group mb-3">
                                            <?php $this->load->view('common/form_control/select', $data); ?>
                                            <div class="input-group-append">
                                                <span class="input-group-text h-100">
                                                    <button type="button" class="btn btn-danger h-100 removeItem"><i class="mdi mdi-close"></i></button>
                                                </span>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            <?php endforeach; ?>
                                <div class="col-12 d-none pb-3">
                                    <label>Firma<span class='text-danger'>*</span></label>
                                    <img src="" class="img-fluid" id="firma_user">
                                </div>
                                <div class="col-12 mb-4">
                                    <div id="signature-pad" class="m-signature-pad">
                                        <div class="m-signature-pad--body">
                                            <canvas></canvas>
                                        </div>
                                        <div class="m-signature-pad--footer">
                                            <div class="description">Agregar Firma Digital Aquí</div>
                                            <button type="button" class="button clear" data-action="clear">Limpiar</button>
                                        </div>
                                    </div>
                                </div>
                                <input  type="text" class="hide" id="firma" name="firma" value="">
                                <div class="col-12 text-right">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                    <button type="submit" class="btn btn-info save" id="save">Guardar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <iframe id="my_iframe" name="my_iframe" src="" class="d-none" style="width: 100%; height: 500px;"></iframe>
        <?php $this->load->view('common/js_admin');?>
        <script src="<?=base_url()?>public/js/signature_pad.js"></script>
        <script>

            var list_transportador = <?=json_encode($list_transportador);?>;
            var wrapper,
                canvas,
                signaturePad;
            var lists = <?=json_encode($lists);?>;
            var forms = <?=json_encode($forms);?>;
            var input = ['input', 'textarea', 'select'],
                add_input = false;

            //Nuevo Modal
            $('body').on('click', '#btn-newModal', function () {
                $('#modalNew form').attr( 'action', '<?=base_url().$url?>/new' )
                $('form input[type=radio]').attr('checked', false)
                $('#modalNew h5').html('<i class="mdi mdi-plus"></i> Nuevo <?=$titulo_singular?>')
                $('#firma_user').attr('src', '').parent().removeClass('d-none')
                $('#signature-pad').parent().removeClass('d-none')
                $('.modal form .row>div').last().removeClass('d-none')
            })

            //Editar Modal
            $('body').on('click', 'table .editar', function () {
                $('#modalNew').modal('show')
                $('form input[type=radio]').attr('checked', false)
                var pos_item = $(this).attr('data-pos'),
                item = lists[pos_item];

                $('#modalNew form').attr('action', '<?=base_url().$url?>/update/'+item.id)
                $('#modalNew h5').html('<i class="mdi mdi-pencil"></i> Editar <?=$titulo_singular?>').append('<input type="hidden" name="id" value="'+item.id+'">')
                for (let i = 0; i < forms.length; i++) {
                    if (input.includes(forms[i].form_control)) 
                        $('form [name='+forms[i].attr.name+']').val(item[forms[i].attr.name])
                    if (forms[i].form_control=='radio') 
                        $('form #'+forms[i].attr.id+item[forms[i].attr.name]).attr('checked', true)
                    if (forms[i].form_control=='select-add') 
                        addItemEditarModal(pos_item)
                }
                $('#firma_user').attr( 'src', item['url_firma'] ? base_url+item['url_firma'] : '' ).parent().removeClass('d-none')

                $('#signature-pad').parent().removeClass('d-none')
                $('.modal form .row>div').last().removeClass('d-none')
                resetFirma()
            })

            //Formulario - guardar
            $( "body" ).on( "click", ".save", function( event ) {
                event.preventDefault();
                var url = $(this).closest('form').eq(0).attr('action'),
                    bandera = true,
                    list_ = [],
                    eso = $(this);
                
                if (eso.attr('id')!==undefined) { //guardar form principal
                    $('form .id_transportador').each(function(){
                        if ( $(this).val()=='' ) {
                            bandera = false
                            return Swal.fire({ icon: 'error', title: 'Oops...', text: 'Debe seleccionar un transportador.' })
                        }else{
                            var tmp = {id_transportador: $(this).val()}
                            if ( $('.modal [name=id]').val()!==undefined ) 
                                tmp['id_conductor'] = $('.modal [name=id]').val()
                            if ( $(this).attr('data-id')!==undefined ) 
                                tmp.id = $(this).attr('data-id')
                            list_.push(tmp)
                        }
                    })
                    if (list_.length==0) {
                        bandera = false
                        return Swal.fire({ icon: 'error', title: 'Oops...', text: 'Debe agregar un transportador para guardar el conductor.' })
                    }
                    //Firma
                    if (signaturePad.isEmpty()) {
                        bandera = false
                        return Swal.fire({ icon: 'error', title: 'Oops...', text: 'Olvidó agregar su firma digital.' })
                    }else{
                        var png= signaturePad.toDataURL("image/png", 100); 
                        document.getElementById("firma").value =png;
                    }
                    //console.log(bandera)
                    if (bandera) {
                        $('[name=list_transportador]').val( JSON.stringify(list_) )
                        $('form')[0].submit()
                    }
                }else{ //editar
                    var data = $(this).closest('form').eq(0).serialize()
                    ajax(url, data, function (data) {
                        if (data.res=='ok') {
                            Swal.fire({ icon: 'success',  title: data.msj, showConfirmButton: false, timer: 1500 })
                            if (add_input && eso.attr('id')===undefined) {
                                removeAddItemModal()
                                appendAddItemModal(data.id, data.nombre)
                            }else
                                setTimeout(function() { location.reload(); }, 1500);    
                        } else {
                            var content = '';
                            for(let i in data.errors) 
                                content+=data.errors[i]+'\n'
                            Swal.fire({ icon: 'error', title: 'Oops...', text: content })
                        }
                    })
                }
            });

            function appendAddItemModal(id, nombre) {
                $('.modal form .id_transportador').append('<option value="'+id+'">'+nombre+'</option>')
                list_transportador.push({value: id, name: nombre})
            }

            //ADD item dinamico
            $('body').on('click', '.addItem', function () {
                if ( $('.modal form .id_transportador').length < list_transportador.length ) {
                    var content = addItemContent()
                    $(this).closest('.form-group').append(content)
                }                
            })

            //REMOVE item dinamico
            $('body').on('click', '.removeItem', function () {
                $(this).closest('.input-group').remove()
            })

            function resetModal() {
                $('form')[0].reset()
                $('form .form-group .input-group').remove()
                var content = addItemContent()
                $('form .form-group').eq(2).append(content)
            }

            function addItemContent(id=false, id_transportador=false) {
                var content = '<div class="input-group mb-3">'
                        content += '<select class="form-control id_transportador" name="id_transportador" required="true" '
                        content += id?'data-id="'+id+'"':''
                        content +='>'
                            content += '<option value="" style="display:none">Seleccionar</option>'
                            for (let j = 0; j < list_transportador.length; j++) {
                            content += '<option value="'+list_transportador[j].value+'"'
                            content += id_transportador && id_transportador==list_transportador[j].value?'selected':'' 
                            content += '>'+list_transportador[j].name+'</option>'
                            }
                        content += '</select>'
                        content += '<div class="input-group-append">'
                            content += '<span class="input-group-text h-100">'
                                content += '<button type="button" class="btn btn-danger h-100 removeItem"><i class="mdi mdi-close"></i></button>'
                            content += '</span>'
                        content += '</div>'
                    content += '</div>'
                return content;
            }

            function addItemEditarModal(pos_item) {
                var item = lists[pos_item];
                if ( item.transportadores.length>0 ) {
                    var content = ''
                    for (let i = 0; i < item.transportadores.length; i++) {
                        if ( i==0 ) {
                            $('.modal form>.row>.form-group .id_transportador').val( item.transportadores[i].id_transportador )
                            continue;
                        }
                        content += addItemContent(item.transportadores[i].id, item.transportadores[i].id_transportador)
                    }
                    $('.modal form>.row>.form-group').last().append(content)
                }
            }

            document.addEventListener('DOMContentLoaded', () => {
                document.querySelectorAll('input[type=text]').forEach( node => node.addEventListener('keypress', e => {
                    if(e.keyCode == 13) 
                        e.preventDefault();
                }))
            });

            //reset form when open modal
            $(document).on('show.bs.modal', '.modal', function () {
                $('form')[0].reset()
                $('#modalNew input, #modalNew textarea, #modalNew select, #modalNew input[type=radio], #modalNew input[type=checkbox]').removeAttr('disabled')
                
            });
            
            $(document).on('shown.bs.modal', '.modal', function () {
                //Inicializa la firma para editar y nuevo usuario
                if ( $('#modalNew form').attr( 'action').indexOf('new')!==-1 || $('#modalNew form').attr( 'action').indexOf('update')!==-1 ) 
                    resetFirma()
                resetModal()
                <?php if (isset($form_add)) { ?>
                removeAddItemModal()
                <?php } ?>
            });

            function resetFirma() {
                //Inicializa la firma
                wrapper = document.getElementById("signature-pad")
                canvas = wrapper.querySelector("canvas")
                signaturePad = new SignaturePad(canvas);
                resizeCanvas()
                signaturePad = new SignaturePad(canvas);
            }

            // Adjust canvas coordinate space taking into account pixel ratio,
            // to make it look crisp on mobile devices.
            // This also causes canvas to be cleared.
            function resizeCanvas() {
                // When zoomed out to less than 100%, for some very strange reason,
                // some browsers report devicePixelRatio as less than 1
                // and only part of the canvas is cleared then.
                var ratio =  Math.max(window.devicePixelRatio || 1, 1);
                canvas.width = canvas.offsetWidth * ratio;
                canvas.height = canvas.offsetHeight * ratio;
                canvas.getContext("2d").scale(ratio, ratio);
                signaturePad.clear();
            }

            window.onresize = function() { resizeCanvas };

            $('body').on('click', '#signature-pad .clear', function () {
                signaturePad.clear();
            })
            //Luego del guardado del form entra por acá para la respuesta
            $(window).on("message onmessage", function(e) {
                var data1 = e.originalEvent.data;
                datos = JSON.stringify(data1);    
                if (datos!=='""') {
                    datos = JSON.parse(datos);
                    datos = JSON.parse(datos);
                    if(datos.res == "ok"){
                        $('form')[0].reset();
                        signaturePad.clear();
                        Swal.fire( { icon: 'success', title: datos.msj, showConfirmButton: false, timer: 3000 } )
                        setTimeout(function() { location.reload(); }, 3000);   
                    }else
                        return Swal.fire( { title: 'Error!', text: datos.msj, icon: 'error', confirmButtonText: 'Ok' } )
                }
                $('#my_iframe').attr('src', '');
            }); 
            $('document').ready(function () {
                var service = '<?=$service?>'
                if (service!='') 
                    $('#modalNew').modal('show')
            })
        </script>
        <?php if (isset($form_add)) {
            $this->load->view('common/js_modal_forms_add');
        } ?>
        
    </body>

</html>