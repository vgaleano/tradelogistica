<?php $this->load->view('common/head_admin'); ?>

</head>

<body class="fix-header fix-sidebar card-no-border">
    <?php $this->load->view('common/preloader'); ?>
    <div id="main-wrapper">

        <?php $this->load->view('common/header_admin');
        $this->load->view('common/menu_admin'); ?>

        <div class="page-wrapper">
            <div class="container-fluid">

                <?php $this->load->view('common/titulo_page_admin');?>

                <div class="row py-3 px-5">
                    <div class="col-12">
                        <div class="card" id="no-more-tables">
                            <div class="card-block">
                                <table class="table">
                                    <thead>
                                        <tr>
                                        <?php foreach ( $columns as $column ): ?>
                                            <th><?=$column['name']?></th>
                                        <?php endforeach; ?>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php if ( !empty($lists) && is_array($lists) ):
                                        foreach ( $lists as $key_list => $list ):  ?>
                                            <tr>
                                                <td data-title="<?=$columns[0]['name']?>"><?=$list->conductor?></td>
                                                <td data-title="<?=$columns[1]['name']?>"><?=$list->numero_documento?></td>
                                                <td data-title="<?=$columns[2]['name']?>">
                                                    <?php if (count($list->transportadores)>0) { ?>
                                                        <ul>
                                                        <?php foreach ($list->transportadores as$v) { ?>
                                                            <?="<li>".$v->transportador."</li>"?>
                                                        <?php } ?>
                                                        </ul>
                                                    <?php }else{
                                                        echo '---';
                                                    } ?>
                                                </td>
                                                <td data-title="<?=$columns[3]['name']?>"><button type="button" class="btn btn-warning editar" data-pos="<?=$key_list?>"><i class="mdi mdi-pencil"></i></button></td>
                                                <td data-title="<?=$columns[4]['name']?>"><button type="button" class="btn btn-danger eliminar" data-id="<?=$list->id?>"><i class="mdi mdi-delete"></i></button></td>
                                            </tr>
                                        <?php endforeach;
                                    else : ?>
                                        <tr>
                                            <td colspan="<?=count($columns);?>" class="text-center font-weight-bold text-break">No hay registros</td>
                                        </tr>
                                    <?php endif ?>
                                    </tbody>
                                </table>
                            </div>
                            <?php if ( $this->pagination->create_links() !== null && $this->pagination->create_links() != '' ) { ?>
                            <div class="card-footer">
                                <?php echo $this->pagination->create_links(); ?>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

        <?php $this->load->view('common/modal_forms_admin');
        $this->load->view('common/js_admin');
        $this->load->view('common/form_crud_admin');?>
        <script>

            var list_transportador = <?=json_encode($list_transportador);?>;

            //Formulario - guardar
            $( "body" ).on( "click", ".save", function( event ) {
                event.preventDefault();
                var url = $(this).closest('form').eq(0).attr('action'),
                    bandera = true,
                    list_ = [],
                    eso = $(this);
                
                if (eso.attr('id')!==undefined) { //guardar form principal
                    $('form .id_transportador').each(function(){
                        if ( $(this).val()=='' ) {
                            bandera = false
                            return Swal.fire({ icon: 'error', title: 'Oops...', text: 'Debe seleccionar un transportador.' })
                        }else{
                            var tmp = {id_transportador: $(this).val()}
                            if ( $('.modal [name=id]').val()!==undefined ) 
                                tmp['id_conductor'] = $('.modal [name=id]').val()  
                            if ( $(this).attr('data-id')!==undefined ) 
                                tmp.id = $(this).attr('data-id')
                            list_.push(tmp)
                        }
                    })
                    if (list_.length==0) {
                        bandera = false
                        return Swal.fire({ icon: 'error', title: 'Oops...', text: 'Debe agregar un transportador para guardar el conductor.' })
                    }
                    var data = {
                        conductor: $('form [name=conductor]').val(),
                        list_transportador: list_,
                        numero_documento: $('form [name=numero_documento]').val()
                    }
                }else
                    var data = $(this).closest('form').eq(0).serialize() //editar
                
                if (bandera) {
                    ajax(url, data, function (data) {
                        if (data.res=='ok') {
                            Swal.fire({ icon: 'success',  title: data.msj, showConfirmButton: false, timer: 1500 })
                            if (add_input && eso.attr('id')===undefined) {
                                removeAddItemModal()
                                appendAddItemModal(data.id, data.nombre)
                            }else
                                setTimeout(function() { location.reload(); }, 1500);             
                        } else {
                            var content = '';
                            for(let i in data.errors) 
                                content+=data.errors[i]+'\n'
                            Swal.fire({ icon: 'error', title: 'Oops...', text: content })
                        }
                    })
                }
            });

            function appendAddItemModal(id, nombre) {
                $('.modal form .id_transportador').append('<option value="'+id+'">'+nombre+'</option>')
                list_transportador.push({value: id, name: nombre})
            }

            //ADD item dinamico
            $('body').on('click', '.addItem', function () {
                if ( $('.modal form .id_transportador').length < list_transportador.length ) {
                    var content = addItemContent()
                    $(this).closest('.form-group').append(content)
                }                
            })

            //REMOVE item dinamico
            $('body').on('click', '.removeItem', function () {
                $(this).closest('.input-group').remove()
            })

            function resetModal() {
                $('form')[0].reset()
                $('form .form-group .input-group').remove()
                var content = addItemContent()
                $('form .form-group').eq(2).append(content)
            }

            function addItemContent(id=false, id_transportador=false) {
                var content = '<div class="input-group mb-3">'
                        content += '<select class="form-control id_transportador" name="id_transportador" required="true" '
                        content += id?'data-id="'+id+'"':''
                        content +='>'
                            content += '<option value="" style="display:none">Seleccionar</option>'
                            for (let j = 0; j < list_transportador.length; j++) {
                            content += '<option value="'+list_transportador[j].value+'"'
                            content += id_transportador && id_transportador==list_transportador[j].value?'selected':'' 
                            content += '>'+list_transportador[j].name+'</option>'
                            }
                        content += '</select>'
                        content += '<div class="input-group-append">'
                            content += '<span class="input-group-text h-100">'
                                content += '<button type="button" class="btn btn-danger h-100 removeItem"><i class="mdi mdi-close"></i></button>'
                            content += '</span>'
                        content += '</div>'
                    content += '</div>'
                return content;
            }

            function addItemEditarModal(pos_item) {
                var item = lists[pos_item];
                if ( item.transportadores.length>0 ) {
                    var content = ''
                    for (let i = 0; i < item.transportadores.length; i++) {
                        if ( i==0 ) {
                            $('.modal form>.row>.form-group .id_transportador').val( item.transportadores[i].id_transportador )
                            continue;
                        }
                        content += addItemContent(item.transportadores[i].id, item.transportadores[i].id_transportador)
                    }
                    $('.modal form>.row>.form-group').last().append(content)
                }
            }

            document.addEventListener('DOMContentLoaded', () => {
                document.querySelectorAll('input[type=text]').forEach( node => node.addEventListener('keypress', e => {
                    if(e.keyCode == 13) 
                        e.preventDefault();
                }))
            });
        </script>
        <?php if (isset($form_add)) {
            $this->load->view('common/js_modal_forms_add');
        } ?>
        
    </body>

</html>