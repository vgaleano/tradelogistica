<?php $this->load->view('common/head_admin'); ?>
<link rel="stylesheet" href="<?=base_url(); ?>public/css/typeahead.css">
</head>

<body class="fix-header fix-sidebar card-no-border">
    <?php $this->load->view('common/preloader'); ?>
    <div id="main-wrapper">
        
        <?php $this->load->view('common/header_admin');
        $this->load->view('common/menu_admin'); ?>

        <div class="page-wrapper">
            <div class="container-fluid">

                <?php $this->load->view('common/titulo_page_admin');?>

                <div class="row py-3 px-5">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-block" id="no-more-tables">
                                <?php $this->load->view('common/table_list_admin'); ?>
                            </div>
                            <?php if ( $this->pagination->create_links() !== null && $this->pagination->create_links() != '' ) { ?>
                            <div class="card-footer">
                                <?php echo $this->pagination->create_links(); ?>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
        
        <!-- Modal -->
        <div class="modal fade" id="modalNew" tabindex="-1" aria-labelledby="modalNewLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modalNewLabel"></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="#" onsubmit="return false">
                            <input type="hidden" id="modo">
                            <div class="row mx-0 w-100">
                                <?php foreach ( $forms as $form_ ): ?>
                                    <div class="col-12 col-lg-<?=$form_['column']?> form-group">
                                        <?php if ( isset($form_['label']) ): ?>
                                            <label for="<?=$form_['for']?>"><?=$form_['label']?></label>
                                        <?php endif;
                                        if ( isset($form_['btn-info']) && $form_['btn-info'] ) { ?>
                                            <button type="button" class="btn btn-outline-warning view-info-btn float-right d-block d-sm-none"><i class="mdi mdi-alert-circle-outline"></i></button>
                                        <?php }                            
                                        $data['form'] = $form_;
                                        if ($form_['form_control']!='select-add') {                                  
                                            $this->load->view('common/form_control/'.$form_['form_control'], $data);
                                        }else{ ?>
                                            <div class="input-group mb-3">
                                                <?php $this->load->view('common/form_control/select', $data); ?>
                                                <div class="input-group-append">
                                                    <span class="input-group-text h-100">
                                                        <button type="button" class="btn btn-danger h-100 removeItem"><i class="mdi mdi-close"></i></button>
                                                    </span>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn btn-info save" id="save">Guardar</button>
                    </div>
                </div>
            </div>
        </div>

        <?php $this->load->view('common/js_admin'); ?>
        <script src="<?=base_url()?>public/js/typeahead.js"></script>
        <script src="<?=base_url()?>public/js/campos_modal_dinamico.js"></script>
        <script>           
            var forms_add = <?=json_encode($forms_add);?>;
            var lists = <?=json_encode($lists);?>;
            var control_digitos_c = <?=json_encode($control_digitos_contenedor);?>;
            var list_naviera_codigo = <?=json_encode($list_naviera_codigo);?>;
            var input = ['input', 'textarea', 'password', 'select'],
                codigosT = [],
                add_input = false,
                modal_lg = ['codigoiso', 'contenedores']; 
            
            //Hide los campos para crear el contenedor desde el principal
            $('.form-container').parent().addClass('d-none');

            $('document').ready(function () {
                var service = '<?=$service?>'
                if (service!='') 
                    $('#modalNew').modal('show')
            })

            //Nuevo Modal
            $('body').on('click', '#btn-newModal', function () {
                resetModal()
                $('#modalNew h5').html('<i class="mdi mdi-plus"></i> Nuevo <?=$titulo_singular?>')
            })

            //Editar Modal
            $('body').on('click', 'table .editar', function () {
                $('#modalNew').modal('show')
                $('.form-container').parent().addClass('d-none');
                resetModal()
                var pos_item = $(this).attr('data-pos'),
                item = lists[pos_item];
                $('#modalNew form').attr('action', '<?=$url?>/update/'+item.id)
                $('#modalNew h5').html('<i class="mdi mdi-pencil"></i> Editar <?=$titulo_singular?>').append('<input type="hidden" name="id" value="'+item.id+'">')
                $('form [name=1_contenedor]').val( item.codigo.substring(0, 4) )
                $('form [name=2_contenedor]').val( item.codigo.substring(4, 10) )
                $('form [name=3_contenedor]').val( item.codigo.substring(10, 11) )
                $('form [name=codigo_iso]').val(item.codigo_iso).attr('disabled', false)
                active = list_naviera_codigo.filter(naviera => naviera.codigo_iso === item.codigo_iso),
                content = '<option value="" style="display:none">Seleccionar</option>';
                if (active.length>0) {
                    for (let i = 0; i < active.length; i++) 
                        content += '<option value="'+active[i].id_naviera+'" '+(item.id_naviera==active[i].id_naviera?'selected':'')+'>'+active[i].naviera+' - '+active[i].codigo_naviera+'</option>';
                }
                $('form [name=id_naviera]').empty().append(content).attr('disabled', false)
                validateContainerNo()
            })

            //Formulario - guardar
            $( "body" ).on( "click", ".save", function( event ) {
                event.preventDefault();
                var url = $(this).closest('form').eq(0).attr('action'),
                    data = $(this).closest('form').eq(0).serialize(),
                    eso = $(this), 
                    bandera = true;
                
                if ($(this).attr('id')!=undefined) { //guardar principal
                    if ( $('[name=codigo_iso]').parent().attr('class').indexOf('d-none')===-1 && $('[name=codigo_iso]').val()=='' ) {
                        bandera = false
                        Swal.fire({ icon: 'warning', title: 'Oops...', text: 'Debe seleccionar un código ISO para el container.' })
                    }   
                    if ( $('[name=id_naviera]').parent().attr('class').indexOf('d-none')===-1 && $('[name=id_naviera]').val()=='' ) {
                        bandera = false
                        Swal.fire({ icon: 'warning', title: 'Oops...', text: 'Debe seleccionar la naviera para el container.' })
                    }
                }
                if (bandera) {
                    ajax(url, data, function (data) {
                        if (data.res=='ok') {
                            Swal.fire({ icon: 'success',  title: data.msj, showConfirmButton: false, timer: 1500 })
                            if (add_input && eso.attr('id')===undefined) {
                                <?php if (isset($form_add)) { ?>
                                removeAddItemModal()
                                <?php } ?>
                            }else
                                setTimeout(function() { location.reload(); }, 1500);        
                        } else {
                            var content = '';
                            for(let i in data.errors) 
                                content+=data.errors[i]+'\n'
                            Swal.fire({ icon: 'error', title: 'Oops...', text: content })
                        }
                    })
                }               
            });

            //reset form when open modal
            $(document).on('show.bs.modal', '.modal', function () {
                resetModal()
                removeAddItemModal()
            });

            function resetModal(){
                $('form')[0].reset()
                $('form [name=id_naviera]').empty().append('<option value="" style="display:none">Seleccionar</option>')
            }

            //Elimina el 2 form del modal, cuando se da crear campo
            $('body').on('click', '.remove-add-item', function (){
                removeAddItemModal()
            })
            function removeAddItemModal() {
                $('.modal form .row').eq(1).remove()
                $('.modal form .row').eq(0).attr('class', 'row mx-0 w-100')
                $('.modal form .row .col-lg-12').removeClass('col-lg-12').addClass('col-lg-6')
            }
            //END

            function addInputContent( data_input ) {
                var content = '<div class="input-group mb-3">'
                    for (let i = 0; i < data_input.length; i++) 
                        content += '<input type="' + data_input[i].type + '" name="' + data_input[i].name + '" placeholder="' + data_input[i].placeholder + '" class="' + data_input[i].class + '">'
                    content += '</div>'
                return content;
            }

        </script>
        <?php $this->load->view('admin/inspecciones/buscador_contenedor');?>
        <?php $this->load->view('admin/contenedores/rules_container_number');?>
        <?php $this->load->view('admin/contenedores/change_codigo_iso');?>        
        <?php $this->load->view('admin/inspecciones/add-item_modal');?>
        <?php $this->load->view('common/addItemContent');?>
        <?php $this->load->view('common/add_remove_campo');?>
    </body>

</html>