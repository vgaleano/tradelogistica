<script>
//Modal contenedor - change codigo ISO
$('body').on('change', '.modal form [name=codigo_iso]', function () {
    console.log('entra')
    var codigo_iso = $(this).val(),
    active = list_naviera_codigo.filter(naviera => naviera.codigo_iso === codigo_iso),
    content = '<option value="" style="display:none">Seleccionar</option>';
    if (active.length>0) {
        for (let i = 0; i < active.length; i++) 
            content += '<option value="'+active[i].id_naviera+'">'+active[i].naviera+' - '+active[i].codigo_naviera+'</option>';
    }
    $('.modal form [name=id_naviera]').empty().append(content)
})
//END
</script>