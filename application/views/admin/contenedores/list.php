<?php $this->load->view('common/head_admin'); ?>
</head>

<body class="fix-header fix-sidebar card-no-border">
    <?php $this->load->view('common/preloader'); ?>
    <div id="main-wrapper">
        
        <?php $this->load->view('common/header_admin');
        $this->load->view('common/menu_admin'); ?>

        <div class="page-wrapper">
            <div class="container-fluid">

                <?php $this->load->view('common/titulo_page_admin');?>

                <div class="row py-3 px-5">
                    <div class="col-12">
                        <div class="card" id="no-more-tables">
                            <div class="card-block">
                                <?php $this->load->view('common/table_list_admin'); ?>
                            </div>
                            <?php if ( $this->pagination->create_links() !== null && $this->pagination->create_links() != '' ) { ?>
                            <div class="card-footer">
                                <?php echo $this->pagination->create_links(); ?>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

        <div class="modal" tabindex="-1" id="modalNew" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title"><i class="mdi mdi-plus"></i> Crear <?=$titulo_singular?></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="<?=$url;?>/new" method="POST" onsubmit="return false">
                            <div class="row">
                                <div class="col-12 col-lg-6 form-group">
                                    <label>Container No.<span class='text-danger'>*</span> </label>
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control n_contenedor text-uppercase" name="1_contenedor" onkeypress="return onlyText(event)" maxlength="4" size="4" placeholder="XXXX">
                                        <input type="text" class="form-control n_contenedor" name="2_contenedor" onkeypress="return onlyNumber(event)" maxlength="6" size="6" placeholder="######">
                                        <div class="input-group-append">
                                            <span class="input-group-text h-100 d-flex align-items-center px-2">-</span>
                                        </div>
                                        <input type="text" class="form-control n_contenedor" name="3_contenedor" onkeypress="return onlyNumber(event)" maxlength="1" size="1" placeholder="#">
                                    </div>
                                </div>
                            <?php foreach ( $forms as $form ): ?>
                                <div class="col-12 col-lg-<?=$form['column']?> form-group">

                                    <?php if ( isset($form['label']) ): ?>
                                        <label for="<?=$form['for']?>"><?=$form['label']?></label>
                                    <?php endif;
                                    if ( isset($form['btn-info']) && $form['btn-info'] ) { ?>
                                        <button type="button" class="btn btn-outline-warning view-info-btn float-right d-block d-sm-none"><i class="mdi mdi-alert-circle-outline"></i></button>
                                    <?php }  
                                    $data['form'] = $form;
                                    $this->load->view('common/form_control/'.$form['form_control'], $data); ?>
                                </div>
                            <?php endforeach; ?>
                                <div class="col-12 text-right">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                    <button type="submit" class="btn btn-info save" id="save">Guardar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <?php $this->load->view('common/js_admin');?>
        <script>
            var lists = <?=json_encode($lists);?>;
            var forms = <?=json_encode($forms);?>;
            var input = ['input', 'textarea', 'password', 'select'];
            var list_naviera_codigo = <?=json_encode($list_naviera_codigo);?>;
            var control_digitos_c = <?=json_encode($control_digitos_contenedor);?>;
            var array_naviera = <?=json_encode($list_naviera);?>;
            var add_input = false;

            $('document').ready(function () {
                var service = '<?=$service?>'
                if (service!='') 
                    $('#modalNew').modal('show')
            })

            //Nuevo Modal
            $('body').on('click', '#btn-newModal', function () {
                resetModal()
                $('#modalNew h5').html('<i class="mdi mdi-plus"></i> Nuevo <?=$titulo_singular?>')
            })

            //Editar Modal
            $('body').on('click', 'table .editar', function () {
                $('#modalNew').modal('show')
                resetModal()
                var pos_item = $(this).attr('data-pos'),
                item = lists[pos_item];
                $('#modalNew form').attr('action', '<?=$url?>/update/'+item.id)
                $('#modalNew h5').html('<i class="mdi mdi-pencil"></i> Editar <?=$titulo_singular?>').append('<input type="hidden" name="id" value="'+item.id+'">')
                $('form [name=1_contenedor]').val( item.codigo.substring(0, 4) )
                $('form [name=2_contenedor]').val( item.codigo.substring(4, 10) )
                $('form [name=3_contenedor]').val( item.codigo.substring(10, 11) )
                $('form [name=codigo_iso]').val(item.codigo_iso).attr('disabled', false)
                active = list_naviera_codigo.filter(naviera => naviera.codigo_iso === item.codigo_iso),
                content = '<option value="" style="display:none">Seleccionar</option>';
                if (active.length>0) {
                    for (let i = 0; i < active.length; i++) 
                        content += '<option value="'+active[i].id_naviera+'" '+(item.id_naviera==active[i].id_naviera?'selected':'')+'>'+active[i].naviera+' - '+active[i].codigo_naviera+'</option>';
                }
                $('form [name=id_naviera]').empty().append(content).attr('disabled', false)
                validateContainerNo()
            })

            //Formulario - guardar
            $( "body" ).on( "click", ".save", function( event ) {
                event.preventDefault();
                var url = $(this).closest('form').eq(0).attr('action'),
                    eso = $(this),
                    list_relacion = [],
                    list_add_modal = [];

                if (eso.attr('id')!==undefined) {
                    var data = {
                        codigo: $('form [name=1_contenedor]').val().toUpperCase() + $('form [name=2_contenedor]').val() + $('form [name=3_contenedor]').val(),
                        codigo_iso: $('form [name=codigo_iso]').val(),
                        id_naviera: $('form [name=id_naviera').val()
                    }
                }else{
                    $('form .id_naviera').each(function(){
                        if ($(this).val()=='' && $(this).next('[name=codigo_naviera]').val()=='') {
                        }else{
                            if ($(this).val().trim()=='') {
                                bandera = false
                                return Swal.fire({ icon: 'error', title: 'Oops...', text: 'Debe seleccionar la naviera.' })
                            }
                            if ($(this).next('[name=codigo_naviera]').val().trim()=='') {
                                bandera = false
                                return Swal.fire({ icon: 'error', title: 'Oops...', text: 'Debe agregar un código ISo para la naviera.' })
                            }
                            var tmp = { id_naviera: $(this).val(), codigo_naviera: $(this).next('[name=codigo_naviera]').val(), codigo_iso: $('form [name=codigo]').val(), dimensiones: $('form [name=dimensiones]').val() }
                            var tmp_ = { id_naviera: $(this).val(), codigo_naviera: $(this).next('[name=codigo_naviera]').val(), codigo_iso: $('form [name=codigo]').val(), naviera: $('option:selected', this).text() }
                            if ( $(this).attr('data-id')!==undefined ) 
                                tmp.id = $(this).attr('data-id')
                            list_relacion.push(tmp)
                            list_add_modal.push(tmp_)
                        }
                    })
                    /*if (list_relacion.length==0) {
                        bandera = false
                        Swal.fire({ icon: 'error', title: 'Oops...', text: 'Debe agregar una naviera para guardar el código ISO.' })
                    }*/
                    var data = {
                        codigo: $('form [name=codigo]').val(),
                        dimensiones: $('form [name=dimensiones]').val(),
                        list_relacion: list_relacion,
                        list_add_modal: list_add_modal
                    }
                }
                ajax(url, data, function (data) {
                    if (data.res=='ok') {
                        Swal.fire({ icon: 'success',  title: data.msj, showConfirmButton: false, timer: 1500 })
                        if (add_input && eso.attr('id')===undefined) {
                            removeAddItemModal()
                            $('.modal form #codigo_iso').append('<option value="'+data.id+'">'+data.nombre+'</option>')
                            list_naviera_codigo = list_naviera_codigo.concat(data.array_)
                        }else
                            setTimeout(function() { location.reload(); }, 1500);
                    } else {
                        var content = '';
                        for(let i in data.errors) 
                            content+=data.errors[i]+'\n'
                        Swal.fire({ icon: 'error', title: 'Oops...', text: content })
                    }
                })
            });

            //reset form when open modal
            $(document).on('show.bs.modal', '.modal', function () {
                resetModal()
                removeAddItemModal()
            });

            function resetModal(){
                $('form')[0].reset()
                $('form [name=id_naviera]').empty().append('<option value="" style="display:none">Seleccionar</option>')
            }


            //ADD item
            $('body').on('click', '.addItem', function () {
                var content = addItemContent()
                $(this).closest('.form-group').append(content)
            })
            //REMOVE item
            $('body').on('click', '.removeItem', function () {
                $(this).closest('.input-group').remove()
            })

            function addItemContent(id=false, id_naviera=false, codigo_naviera=false) {
                var content = '<div class="input-group mb-3">'
                    content += '<select class="form-control id_naviera" name="id_naviera" required="true" '+ ( id?'data-id="'+id+'"':'' )+'>'
                        content += '<option value="" style="display:none">Seleccionar</option>'
                        for (let j = 0; j < array_naviera.length; j++) {
                        content += '<option value="'+array_naviera[j].value+'" '+ ( id_naviera && id_naviera==array_naviera[j].value?'selected':'' ) +'>'+array_naviera[j].name+'</option>'
                        }
                    content += '</select><input type="text" name="codigo_naviera" placeholder="Código" class="form-control" value="'+( codigo_naviera ? codigo_naviera:'')+'">'
                    content += '<div class="input-group-append"><span class="input-group-text h-100"><button type="button" class="btn btn-danger h-100 removeItem"><i class="mdi mdi-close"></i></button></span></div></div>'
                return content;
            }

            <?php if (isset($form_add)) { ?>
            $('body').on('click', '.add-item', function(){
                $('.modal form .row').eq(1).remove()
                add_input = true
                $('.modal form').addClass('row')
                $('.modal form .row').eq(0).addClass('col-12 col-lg-6 mx-0 w-100')
                $('.modal form .row .col-lg-6').removeClass('col-lg-6').addClass('col-lg-12')
                var content = '<div class="row col-12 col-lg-6  mx-0 w-100 border-left">'
                        content += '<form action="<?=$form_add_url;?>/new" method="POST" onsubmit="return false">'
                            content += '<h3 class="mb-0">Nuevo <?=$form_add_titulo?></h3>'
                            <?php foreach ( $form_add as $form ): ?>
                            content += '<div class="col-12 col-lg-<?=$form['column']?> form-group">'
                                <?php if ( isset($form['label']) ): ?>
                                content += '<label for="<?=$form['for']?>"><?=$form['label']?></label>'
                                <?php endif;
                                if ( isset($form['btn-info']) && $form['btn-info'] ) { ?>
                                    content +='<button type="button" class="btn btn-outline-warning view-info-btn float-right d-block d-sm-none"><i class="mdi mdi-alert-circle-outline"></i></button>'
                                <?php }  
                                $data['form'] = $form;
                                if ($form['form_control']!='select-add') { ?>
                                    content += '<?php $this->load->view('common/form_control/'.$form['form_control'], $data); ?>'
                                <?php }else{ ?>
                                    content += '<div class="input-group mb-3">'
                                        content += '<?php $this->load->view('common/form_control/select', $data); ?>'
                                            content += '<input type="text" name="codigo_naviera" placeholder="Código" class="form-control">'
                                            content += '<div class="input-group-append">'
                                                content += '<span class="input-group-text h-100">'
                                                content += '<button type="button" class="btn btn-danger h-100 removeItem"><i class="mdi mdi-close"></i></button>'
                                            content += '</span>'
                                        content += '</div>'
                                    content += '</div>'
                                <?php }?>
                            content += '</div>'
                            <?php endforeach; ?>
                            content += '<div class="col-12 text-right">'
                                content += '<button type="button" class="btn btn-secondary remove-add-item">Cancelar</button>'
                                content += '<button type="submit" class="btn btn-info ml-2 save">Guardar</button>'
                            content += '</div>'
                        content += '</form>'
                    content += '</div>'
                $('.modal form').append(content)
            })
            <?php } ?>

            $('body').on('click', '.remove-add-item', function (){
                removeAddItemModal()
            })

            function removeAddItemModal() {
                $('.modal form .row').eq(1).remove()
                $('.modal form .row').eq(0).attr('class', 'row mx-0 w-100')
                $('.modal form .row .col-lg-12').removeClass('col-lg-12').addClass('col-lg-6')
            }

            document.addEventListener('DOMContentLoaded', () => {
                document.querySelectorAll('input[type=text]').forEach( node => node.addEventListener('keypress', e => {
                    if(e.keyCode == 13) 
                        e.preventDefault();
                }))
            });
        </script>
        <?php $this->load->view('admin/contenedores/change_codigo_iso');?>
        <?php $this->load->view('admin/contenedores/rules_container_number');?>
    </body>

</html>