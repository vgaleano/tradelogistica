<script>
//IN reglas container No.
$('body').on('change','form .n_contenedor', function (){
    var parte_1 = $('form [name=1_contenedor]').val().trim().toUpperCase(),
        parte_2 = $('form [name=2_contenedor]').val().trim(),
        parte_3 = $('form [name=3_contenedor]').val().trim();
    $('form .n_contenedor').removeClass('border-danger').removeClass('border-success')
    $('form [name=3_contenedor]').tooltip({trigger:'manual'}).tooltip('hide');

    if ( parte_1!='' && parte_2!='' && parte_3!='' ) {
        if (validateContainerNo(parte_1, parte_2, parte_3))
        {
            $(this).closest('form').find('select').removeAttr('disabled')
        }else{
            $(this).closest('form').find('select').attr('disabled', true)
        }
    }
        
})
function validateContainerNo(parte_1, parte_2, parte_3) {
    var f1 = ( parseInt( control_digitos_c[parte_1.substr(0,1)] ) *1 ) + ( parseInt( control_digitos_c[parte_1.substr(1,1)] ) *2 ) + ( parseInt( control_digitos_c[parte_1.substr(2,1)] ) *4 ) + ( parseInt( control_digitos_c[parte_1.substr(3,1)] ) *8 ) + ( parseInt( parte_2.substr(0,1) ) *16 ) + ( parseInt( parte_2.substr(1,1) ) *32 ) + ( parseInt( parte_2.substr(2,1) ) *64 ) + ( parseInt( parte_2.substr(3,1) ) *128 ) + ( parseInt( parte_2.substr(4,1) ) *256 ) + ( parseInt( parte_2.substr(5,1) ) *512 ),
        f2 = f1*+1/11,
        f3 = parseInt( f2.toString().substr(0,3) ) * 11,
        dvreal =  (f1 - f3) == 10 ? 0 : (f1 - f3);
    if ( dvreal == parte_3 ) {
        $('form .n_contenedor').addClass('border-success')
        return true
    }else{
        $('form .n_contenedor').addClass('border-danger')
        $('form [name=3_contenedor]').attr("data-toggle", "tooltip").attr("title", dvreal).tooltip({trigger:'manual'}).tooltip('show');
    }
    return false
}
//END reglas container No.
</script>