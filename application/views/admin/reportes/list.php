<?php $this->load->view('common/head_admin'); ?>
<link rel="stylesheet" href="<?=base_url(); ?>public/css/jquery.datetimepicker.css">
</head>

<body class="fix-header fix-sidebar card-no-border">
    <?php $this->load->view('common/preloader'); ?>
    <div id="main-wrapper">
        
        <?php $this->load->view('common/header_admin');
        $this->load->view('common/menu_admin'); ?>

        <div class="page-wrapper">
            <div class="container-fluid">

                <?php $this->load->view('common/titulo_page_admin');?>

                <div class="row mt-3">
                    <div class="col-12 col-lg-6">
                        <div class="card">
                            <div class="card-block">
                                <form action="reportes/exportDataContenedorPatio" method="POST" onsubmit="return false">
                                    <div class="row">
                                        <div class="col-12 form-group">
                                            <h5>Reporte de contenedores en el patio</h5>
                                        </div>
                                        <div class="col-12 col-lg-6 form-group">
                                            <label for="fecha_inicio">Fecha inicio</label>
                                            <input type="text" class="form-control fecha" name="fecha_inicio">
                                        </div>
                                        <div class="col-12 col-lg-6 form-group">
                                            <label for="fecha_fin">Fecha fin</label>
                                            <input type="text" class="form-control fecha" name="fecha_fin">
                                        </div>
                                        <div class="col-12 d-flex align-items-center justify-content-end">
                                            <button type="button" class="btn btn-danger reset-search" data-toggle="tooltip" data-placement="top" title="Resetear"><i class="mdi mdi-delete"></i></button>
                                            <button type="button" class="btn btn-info ml-1 exportar-excel" data-modo="1" data-toggle="tooltip" data-placement="top" title="Descargar Excel"><i class="mdi mdi-file-excel"></i></button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6">
                        <div class="card">
                            <div class="card-block">
                                <form action="reportes/movimientosdiarios" method="POST" onsubmit="return false">
                                    <div class="row">
                                        <div class="col-12 form-group">
                                            <h5 class="color-success">Reporte de movimientos</h5>
                                        </div>
                                        <div class="col-12 col-lg-6 form-group">
                                            <label for="fecha_inicio_2">Fecha inicio</label>
                                            <input type="text" class="form-control fecha" name="fecha_inicio_2">
                                        </div>
                                        <div class="col-12 col-lg-6 form-group">
                                            <label for="fecha_fin_2">Fecha fin</label>
                                            <input type="text" class="form-control fecha" name="fecha_fin_2">
                                        </div>
                                        <div class="col-12 d-flex align-items-center justify-content-end">
                                            <button type="button" class="btn btn-danger reset-search" data-toggle="tooltip" data-placement="top" title="Resetear"><i class="mdi mdi-delete"></i></button>
                                            <button type="button" class="btn btn-info ml-1 exportar-excel" data-modo="2" data-toggle="tooltip" data-placement="top" title="Descargar Excel"><i class="mdi mdi-file-excel"></i></button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <?php $this->load->view('common/modal_forms_admin');
        $this->load->view('common/js_admin');?>
        <script type="text/javascript" src="<?=base_url();?>public/js/jquery.datetimepicker.full.min.js"></script>
        <script>

            $('.reset-search').on('click', function () {
                $('[name=fecha_inicio], [name=fecha_fin]').val('')
                $(this).closest('form')[0].submit()
            })

            $('.exportar-excel').on('click', function () {
                var url = $(this).closest('form').attr('action'),
                    modo = $(this).attr('data-modo'),
                    bandera = true;
                
                if (modo==1) {
                    if ($("[name=fecha_inicio]").val()!='' && $("[name=fecha_fin]").val()!='' && validate_fechaMayorQue($("[name=fecha_inicio]").val(),$("[name=fecha_fin]").val())===0) {
                        bandera = false
                        return Swal.fire({ icon: 'error', title: 'Oops...', text: "La fecha inicio debe ser menor que la fecha fin"});
                    }else{
                        var data = {
                            fecha_inicio: $('[name=fecha_inicio]').val(),
                            fecha_fin: $('[name=fecha_fin]').val()
                        }
                    }
                }
                //console.log(modo)
                if (modo==2) {
                    if ($("[name=fecha_inicio_2]").val()!='' && $("[name=fecha_fin_2]").val()!='' && validate_fechaMayorQue($("[name=fecha_inicio_2]").val(),$("[name=fecha_fin_2]").val())===0) {
                        bandera = false
                        return Swal.fire({ icon: 'error', title: 'Oops...', text: "La fecha inicio debe ser menor que la fecha fin"});
                    }else{
                        var data = {
                            fecha_inicio: $('[name=fecha_inicio_2]').val(),
                            fecha_fin: $('[name=fecha_fin_2]').val()
                        }
                    }
                }
                if (bandera) {
                    ajax(url, data,
                    function(data){
                        if(data.res=="ok"){
                            var urlF = data.url;
                            window.open(base_url+urlF); 
                        }else
                            mensaje(data.msj);
                    },10000);
                } 
            })
            
            $(document).ready(function () {
                $('body').on('focus',".fecha", function(){
                    $(this).datetimepicker({
                        format:'Y-m-d',
                        timepicker:false,
                        autoclose: true
                    });
                });

                //validar fechas_fin sea mayor a fecha inicio
                jQuery(function(){
                    jQuery('[name=fecha_inicio]').datetimepicker({
                        format:'Y-m-d',
                        onShow:function( ct ){
                            this.setOptions({
                                minDate:jQuery('[name=fecha_inicio]').val()?jQuery('[name=fecha_inicio]').val():false
                            })
                        },
                        timepicker:false
                    });
                    jQuery('[name=fecha_fin]').datetimepicker({
                        format:'Y-m-d',
                        onShow:function( ct ){
                            this.setOptions({
                                maxDate:jQuery('[name=fecha_inicio]').val()?jQuery('[name=fecha_inicio]').val():false
                            })
                        },
                        timepicker:false
                    });
                });
                jQuery(function(){
                    jQuery('[name=fecha_inicio_2]').datetimepicker({
                        format:'Y-m-d',
                        onShow:function( ct ){
                            this.setOptions({
                                minDate:jQuery('[name=fecha_inicio_2]').val()?jQuery('[name=fecha_inicio_2]').val():false
                            })
                        },
                        timepicker:false
                    });
                    jQuery('[name=fecha_fin_2]').datetimepicker({
                        format:'Y-m-d',
                        onShow:function( ct ){
                            this.setOptions({
                                maxDate:jQuery('[name=fecha_inicio_2]').val()?jQuery('[name=fecha_inicio_2]').val():false
                            })
                        },
                        timepicker:false
                    });
                });

                jQuery.datetimepicker.setLocale('es');
            })

            function validate_fechaMayorQue(fechaInicial,fechaFinal)
            {
                //dd/mm/yyyy
                valuesStart=fechaInicial.split("-");
                valuesEnd=fechaFinal.split("-");

                // Verificamos que la fecha no sea posterior a la actual
                var dateStart = new Date(valuesStart[0],(valuesStart[1]-1),valuesStart[2]);
                var dateEnd = new Date(valuesEnd[0],(valuesEnd[1]-1),valuesEnd[2]);
                if(dateStart>=dateEnd)
                    return 0;
                return 1;
            }
        </script>
    </body>

</html>