<?php $this->load->view('common/head_admin'); ?>
</head>

<body class="fix-header fix-sidebar card-no-border">
    <?php $this->load->view('common/preloader'); ?>
    <div id="main-wrapper">
        
        <?php $this->load->view('common/header_admin');
        $this->load->view('common/menu_admin'); ?>

        <div class="page-wrapper">
            <div class="container-fluid">

                <?php $this->load->view('common/titulo_page_admin');?>

                <div class="row py-3 px-5">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-block" id="no-more-tables">
                                <table class="table cf">
                                    <thead class="cf">
                                        <tr>
                                        <?php foreach ( $columns as $column ): ?>
                                            <th><?=$column['name']?></th>
                                        <?php endforeach; ?>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ( $lists as $list ): ?>
                                        <tr>
                                            <td data-title="Perfil"><?=$list->perfil?></td>
                                            <td data-title="Editar">
                                                <a href="<?=base_url().'configuraciones/permisos/'.$list->id?>" type="button" class="btn btn-warning editar" <?php $list->id!='1' ?'':'disabled'?> data-toggle="tooltip" data-placement="top" title="Editar"><i class="mdi mdi-pencil"></i></button>
                                            </td>
                                        </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

        <?php $this->load->view('common/js_admin');?>
        
    </body>

</html>