<?php $this->load->view('common/head_admin'); ?>
<link rel="stylesheet" href="<?php echo base_url(); ?>public/css/bootstrap-switch.min.css">
</head>

<body class="fix-header fix-sidebar card-no-border">
    <?php $this->load->view('common/preloader'); ?>
    <div id="main-wrapper">
        
        <?php $this->load->view('common/header_admin');
        $this->load->view('common/menu_admin'); ?>

        <div class="page-wrapper">
            <div class="container-fluid">

                <?php $this->load->view('common/titulo_page_admin');?>

                <div class="row py-3 px-5">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-block" id="no-more-tables">
                                <table class="table cf">
                                    <thead class="cf">
                                        <tr>
                                        <?php foreach ( $columns as $column ): ?>
                                            <th><?=$column['name']?></th>
                                        <?php endforeach; ?>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if(!empty($lists)){
                                        if(is_array($lists) && count($lists)) {
                                            foreach ($lists as $list) {?>
                                        <tr>
                                            <td data-title="Vista"><?=$list->vista ?></td>
                                            <td data-title="Permiso"><input type="checkbox" class="estado" data-id="<?=$list->id; ?>" name="my-checkbox" <?php if(in_array($list->id, $permisos)){ echo 'checked'; } ?>></td>	
                                        </tr>
                                        <?php }}}else{?>
                                        <tr>
                                            <td colspan="5"><?=$this->lang->line('bad_table_vista')?></td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="card-footer d-flex justify-content-end">
                                <a href="<?=base_url().'configuraciones/perfiles'?>" type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</a>
                                <button type="button" class="btn btn-info save ml-2" id="save">Guardar</button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

        <?php $this->load->view('common/js_admin');?>
        <script type="text/javascript" src="<?php echo base_url();?>public/js/bootstrap-switch.min.js"></script>
        <script  type="text/javascript" language="javascript">
			$("[name='my-checkbox']").bootstrapSwitch();

            $('#save').on('click', function () {
                var tmp = []

                $('.bootstrap-switch-on').each( function () {
                    tmp.push( { id_perfil: '<?=$id_rol?>', id_vista: $(this).find('.estado').attr('data-id') } )
                })
                
                if (tmp.length>0) {
                    ajax('configuraciones/permisosByPerfil/<?=$id_rol?>',
                    {   
                        permisos:tmp
                    },
                    function(data){
                        if(data.res=="ok"){  
                            Swal.fire({ icon: 'success',  title: data.msj, showConfirmButton: false, timer: 1500 }) 
                            setTimeout(function() { location.reload(); }, 3000);                                   
                        }else{
                            var content = '';
                            for(let i in data.errors) 
                                content+=data.errors[i]+'\n'
                            Swal.fire({ icon: 'error', title: 'Oops...', text: content })
                        }
                    },10000);
                }
            })
		</script>
        
    </body>

</html>