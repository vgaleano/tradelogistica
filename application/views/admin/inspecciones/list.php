<?php $this->load->view('common/head_admin'); ?>
<link rel="stylesheet" href="<?=base_url(); ?>public/css/jquery.datetimepicker.css">
</head>

<body class="fix-header fix-sidebar card-no-border">
    <?php $this->load->view('common/preloader'); ?>
    <div id="main-wrapper">
        
        <?php $this->load->view('common/header_admin');
        $this->load->view('common/menu_admin'); ?>

        <div class="page-wrapper">
            <div class="container-fluid">

                <?php $this->load->view('common/titulo_page_admin');?>

                <div class="row py-3 px-0">
                    <div class="col-12 px-0">
                        <div class="card" id="no-more-tables">
                            <div class="card-header">
                                <form action="<?=base_url()?>inspecciones/list" method="POST" onsubmit="return false">
                                    <div class="row">
                                        <div class="col-12 col-lg-3 form-group">
                                            <label for="fecha_inicio">Fecha inicio</label>
                                            <input type="text" class="form-control fecha" name="fecha_inicio">
                                        </div>
                                        <div class="col-12 col-lg-3 form-group">
                                            <label for="fecha_fin">Fecha fin</label>
                                            <input type="text" class="form-control fecha" name="fecha_fin">
                                        </div>
                                        <div class="col-12 col-lg-3 form-group">
                                            <label for="id_transportador">Transportador</label>
                                            <select name="id_transportador" id="id_transportador" class="form-control">
                                                <option value="" style="display:none;">Seleccionar</option>
                                                <?php foreach ($array_transportador as $a_t) { ?>
                                                    <option value="<?=$a_t->value?>"><?=$a_t->name?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="col-12 col-lg-3 form-group">
                                            <label for="id_conductor">Conductor</label>
                                            <select name="id_conductor" id="id_conductor" class="form-control">
                                                <option value="" style="display:none;">Seleccionar</option>
                                            </select>
                                        </div>
                                        <div class="col-12 col-lg-3 form-group">
                                            <label for="id_clasificacion">Clasificación</label>
                                            <select name="id_clasificacion" id="id_clasificacion" class="form-control">
                                                <option value="" style="display:none;">Seleccionar</option>
                                                <?php foreach ($array_clasificacion as $a_c) { ?>
                                                    <option value="<?=$a_c->value?>"><?=$a_c->name?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="col-12 col-lg-3 form-group">
                                            <label for="id_naviera">Naviera</label>
                                            <select name="id_naviera" id="id_naviera" class="form-control">
                                                <option value="" style="display:none;">Seleccionar</option>
                                            </select>
                                        </div>
                                        <div class="col-12 col-lg-3 form-group">
                                            <label for="id_cliente">Cliente</label>
                                            <select name="id_cliente" id="id_cliente" class="form-control">
                                                <option value="" style="display:none;">Seleccionar</option>
                                                <?php foreach ($array_cliente as $a_c) { ?>
                                                    <option value="<?=$a_c->value?>"><?=$a_c->name?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="col-12 col-lg-3 form-group">
                                            <label for="tipo_movimiento">Tipo movimiento</label>
                                            <select name="tipo_movimiento" id="tipo_movimiento" class="form-control">
                                                <option value="" style="display:none;">Seleccionar</option>
                                                <option value="1">Ingreso</option>
                                                <option value="-1">Salida</option>
                                            </select>
                                        </div>
                                        <div class="col-12 col-lg-3 d-flex align-items-center justify-content-end">
                                            <button type="button" class="btn btn-danger reset-search" data-toggle="tooltip" data-placement="top" title="Resetear"><i class="mdi mdi-delete"></i></button>
                                            <button type="button" class="btn btn-info ml-1" data-toggle="tooltip" data-placement="top" title="Buscar" id="buscar">Buscar</button>
                                            <button type="button" class="btn btn-success ml-1" data-toggle="tooltip" data-placement="top" title="Descargar Excel" id="exportar-excel"><i class="mdi mdi-file-excel"></i></button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="card-block">                        
                                <?php $this->load->view('common/table_list_admin'); ?>
                            </div>
                            <?php if ( $this->pagination->create_links() !== null && $this->pagination->create_links() != '' ) { ?>
                            <div class="card-footer">
                                <?php echo $this->pagination->create_links(); ?>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <?php $this->load->view('common/modal_forms_admin');
        $this->load->view('common/js_admin');?>
        <script type="text/javascript" src="<?=base_url();?>public/js/jquery.datetimepicker.full.min.js"></script>
        <script>
            var array_conductor = <?=json_encode($array_conductor);?>;
            var array_naviera = <?=json_encode($array_naviera);?>;

            //FILTROS
            $('.reset-search').on('click', function () {
                $(this).closest('form')[0].reset().submit()
            })

            $('#buscar').on('click', function () {
                var url = $(this).closest('form').attr('action'),
                    bandera = true;
                
                if ($("[name=fecha_inicio]").val()!='' && $("[name=fecha_fin]").val()!='' && validate_fechaMayorQue($("[name=fecha_inicio]").val(),$("[name=fecha_fin]").val())===0) {
                    bandera = false
                    return Swal.fire({ icon: 'error', title: 'Oops...', text: "La fecha inicio debe ser menor que la fecha fin"});
                }
                if (bandera) 
                    $('form')[0].submit()
            })

            $('#exportar-excel').on('click', function () {
                var bandera = true;
                
                if ($("[name=fecha_inicio]").val()!='' && $("[name=fecha_fin]").val()!='' && validate_fechaMayorQue($("[name=fecha_inicio]").val(),$("[name=fecha_fin]").val())===0) {
                    bandera = false
                    return Swal.fire({ icon: 'error', title: 'Oops...', text: "La fecha inicio debe ser menor que la fecha fin"});
                }
                if (bandera) {
                    ajax('inspecciones/exportarInspecciones', {
                        fecha_inicio: $('[name=fecha_inicio]').val(),
                        fecha_fin: $('[name=fecha_fin]').val(),
                        id_transportador: $('#id_transportador').val(),
                        id_conductor: $('#id_conductor').val(),
                        id_clasificacion: $('#id_clasificacion').val(),
                        id_naviera: $('#id_naviera').val(),
                        id_cliente: $('#id_cliente').val(),
                        tipo_movimiento: $('#tipo_movimiento').val()
                    },
                    function(data){
                        if(data.res=="ok"){
                            var urlF = data.url;                      
                            console.log(base_url+urlF);
                            window.open(base_url+urlF); 
                        }else
                            mensaje(data.msj);
                    },10000);
                } 
            })
            //End Filtros

            $(document).ready(function () {
                $('body').on('focus',".fecha", function(){
                    $(this).datetimepicker({
                        format:'Y-m-d',
                        timepicker:false,
                        autoclose: true
                    });
                });

                //validar fechas_fin sea mayor a fecha inicio
                jQuery(function(){
                    jQuery('[name=fecha_inicio]').datetimepicker({
                        format:'Y-m-d',
                        onShow:function( ct ){
                            this.setOptions({
                                minDate:jQuery('[name=fecha_inicio]').val()?jQuery('[name=fecha_inicio]').val():false
                            })
                        },
                        timepicker:false
                    });
                    jQuery('[name=fecha_fin]').datetimepicker({
                        format:'Y-m-d',
                        onShow:function( ct ){
                            this.setOptions({
                                maxDate:jQuery('[name=fecha_inicio]').val()?jQuery('[name=fecha_inicio]').val():false
                            })
                        },
                        timepicker:false
                    });
                });

                jQuery.datetimepicker.setLocale('es');
            })

            function validate_fechaMayorQue(fechaInicial,fechaFinal)
            {
                //dd/mm/yyyy
                valuesStart=fechaInicial.split("-");
                valuesEnd=fechaFinal.split("-");

                // Verificamos que la fecha no sea posterior a la actual
                var dateStart = new Date(valuesStart[0],(valuesStart[1]-1),valuesStart[2]);
                var dateEnd = new Date(valuesEnd[0],(valuesEnd[1]-1),valuesEnd[2]);
                if(dateStart>=dateEnd)
                    return 0;
                return 1;
            }

            //Change Transportador
            $('#id_transportador').on('change', function() {
                var id_transportador = $(this).val(),
                    content = '<option value="" style="display:none;">Seleccionar</option>';
                if (id_transportador!='') {
                    var activas = array_conductor.filter(conductor => conductor['id_transportador'] === id_transportador)
                    for (let i = 0; i < activas.length; i++) 
                        content += '<option value="' + activas[i].id + '">' + activas[i].conductor + ' - ' + activas[i].numero_documento + '</option>'
                }
                $('#id_conductor').val('').empty().append(content);
            })
            //End Change Transportador

            //Change Clasificación
            $('#id_clasificacion').on('change', function() {
                var id_clasificacion = $(this).val(),
                    content = '<option value="" style="display:none;">Seleccionar</option>';
                if (id_clasificacion!='') {
                    var activas = array_naviera.filter(naviera => naviera['id_clasificacion'] === id_clasificacion)
                    for (let i = 0; i < activas.length; i++) 
                        content += '<option value="' + activas[i].id + '">' + activas[i].naviera + '</option>'
                }
                $('#id_naviera').val('').empty().append(content);
            })
            //End Change Clasificación

            $('body').on('click', '.editar', function () {
                window.location.href = base_url + 'inspecciones/edit/' + $(this).attr('data-id');
            })

            $('body').on('click', '.generarPDF', function () {
                var id = $(this).attr('data-id')
                ajax('inspecciones/getPDFInspeccion/' + id, {},function (data){
                    if(data.res=="ok"){
                        var urlF = data.url;
                        window.open(base_url+urlF); 
                    }
                })
            })
        </script>
    </body>

</html>