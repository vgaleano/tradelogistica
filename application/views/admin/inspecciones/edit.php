<?php $this->load->view('common/head_admin'); ?>
<link rel="stylesheet" href="<?=base_url(); ?>public/css/typeahead.css">
</head>

<body class="fix-header fix-sidebar card-no-border">
    <?php $this->load->view('common/preloader'); ?>
    <div id="main-wrapper">
        
        <?php $this->load->view('common/header_admin');
        $this->load->view('common/menu_admin'); ?>

        <div class="page-wrapper">
            <div class="container-fluid">

                <?php $this->load->view('common/titulo_page_admin');?>

                <div class="row py-3 px-5">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-block">
                                <form action="<?=$url;?>/update/<?=$id?>" method="POST" onsubmit="return false" id="principal" data-id="<?=$id?>">
                                    <div class="row">

                                        <div class="col-12 col-lg-6">
                                            <div class="row">
                                                <div class="col-12">
                                                    <h3 class="border-bottom">Contenedor Info</h3>
                                                </div>
                                                <?php foreach ( $forms_1 as $form ): ?>
                                                    <div class="col-12 col-lg-<?=$form['column']?> form-group">

                                                        <?php if ( isset($form['label']) ): ?>
                                                            <label for="<?=$form['for']?>"><?=$form['label']?></label>
                                                        <?php endif;
                                                        $data['form'] = $form;
                                                        $this->load->view('common/form_control/'.$form['form_control'], $data); ?>
                                                    </div>
                                                <?php endforeach; ?>
                                            </div>
                                        </div>

                                        <div class="col-12 col-lg-6">
                                            <div class="row">
                                                <div class="col-12">
                                                    <h3 class="border-bottom">Transporte Info</h3>
                                                </div>
                                                <?php foreach ( $forms_2 as $form ): ?>
                                                    <div class="col-12 col-lg-<?=$form['column']?> form-group">

                                                        <?php if ( isset($form['label']) ): ?>
                                                            <label for="<?=$form['for']?>"><?=$form['label']?></label>
                                                        <?php endif;
                                                        $data['form'] = $form;
                                                        $this->load->view('common/form_control/'.$form['form_control'], $data); ?>
                                                    </div>
                                                <?php endforeach; ?>
                                            </div>
                                        </div>

                                        <div class="col-12 box-damage d-none">
                                            <h3 class="border-bottom">Daño Info <button type="button" class="btn btn-sm btn-success addItemTable pull-right"><i class='mdi mdi-plus'></i> Agregar Campo</button></h3>
                                            <div class="table-responsive">
                                                <table class="table table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th>Descripción del Daño</th>
                                                            <th>Locacización del Daño</th>
                                                            <th>Componente</th>
                                                            <th>Medida</th>
                                                            <th>Posible Reparación</th>
                                                            <th>Cargo a</th>
                                                            <th>Acciones</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody class="text-center" id="table-damage">
                                                        <tr class="datos-none">
                                                            <td colspan="7">Ningún daño añadido...</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>

                                        <div class="col-12 box-damage-campo d-none mt-3">
                                            <div class="row">
                                                <?php foreach ( $forms_4 as $form ): ?>
                                                    <div class="col-12 col-lg-<?=$form['column']?> form-group">

                                                        <?php if ( isset($form['label']) ): ?>
                                                            <label for="<?=$form['for']?>"><?=$form['label']?></label>
                                                        <?php endif;
                                                        $data['form'] = $form;
                                                        $this->load->view('common/form_control/'.$form['form_control'], $data); ?>
                                                    </div>
                                                <?php endforeach; ?>
                                                <div class="col-12 col-lg-6">
                                                    <div class="row">
                                                        <div class="col-3 form-group">
                                                            <button type="button" class="btn btn-warning btn-block btn-ubicacion" data-toggle="modal" data-target="#modalUbicacionDamage" data-first-letter="T">Techo</button>
                                                        </div>
                                                        <div class="col-3 form-group">
                                                            <button type="button" class="btn btn-warning btn-block btn-ubicacion" data-toggle="modal" data-target="#modalUbicacionDamage" data-first-letter="L">Lado Izq.</button>
                                                        </div>
                                                        <div class="col-12"></div>
                                                        <div class="col-3 form-group">
                                                            <button type="button" class="btn btn-warning btn-block btn-ubicacion" data-toggle="modal" data-target="#modalUbicacionDamage" data-first-letter="B">Piso</button>
                                                        </div>
                                                        <div class="col-3 form-group">
                                                            <button type="button" class="btn btn-warning btn-block btn-ubicacion" data-toggle="modal" data-target="#modalUbicacionDamage" data-first-letter="R">Lado Der.</button>
                                                        </div>
                                                        <div class="col-12"></div>
                                                        <div class="col-3 form-group">
                                                            <button type="button" class="btn btn-warning btn-block btn-ubicacion" data-toggle="modal" data-target="#modalUbicacionDamage" data-first-letter="E">Reefer</button>
                                                        </div>
                                                        <div class="col-3 form-group">
                                                            <button type="button" class="btn btn-warning btn-block btn-ubicacion" data-toggle="modal" data-target="#modalUbicacionDamage" data-first-letter="I">Dentro</button>
                                                        </div>
                                                        <div class="col-3 form-group">
                                                            <button type="button" class="btn btn-warning btn-block btn-ubicacion" data-toggle="modal" data-target="#modalUbicacionDamage" data-first-letter="D">Puerta</button>
                                                        </div>
                                                        <div class="col-3 form-group">
                                                            <button type="button" class="btn btn-warning btn-block btn-ubicacion" data-toggle="modal" data-target="#modalUbicacionDamage" data-first-letter="F">Frente</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <div class="form-group text-center">
                                                        <button type="button" class="btn btn-secondary" id="cancel-box-damage-campo">Cancel</button>
                                                        <button type="button" class="btn btn-info" id="save-box-damage-campo">Crear Daño</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-12">
                                            <div class="row" id="adicional-info">
                                                <div class="col-12">
                                                    <h3 class="border-bottom">Adicional</h3>
                                                </div>
                                                <?php foreach ( $forms_3 as $form ): ?>
                                                    <div class="col-12 col-lg-<?=$form['column']?> form-group">

                                                        <?php if ( isset($form['label']) ): ?>
                                                            <label for="<?=$form['for']?>"><?=$form['label']?></label>
                                                        <?php endif;
                                                        $data['form'] = $form;
                                                        $this->load->view('common/form_control/'.$form['form_control'], $data); ?>
                                                    </div>
                                                <?php endforeach; ?>
                                            </div>
                                        </div>
                                    
                                        <div class="col-12 text-center">
                                            <a href="<?=base_url()?>inspecciones/list" type="button" class="btn btn-secondary">Cancelar</a>
                                            <button type="submit" class="btn btn-info save" id="save">Guardar</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>  
                    </div> 
                </div>

            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="staticBackdrop" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="#" onsubmit="return false">
                        <input type="hidden" id="modo">
                        <div class="row mx-0 w-100"></div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-info save" id="save">Guardar</button>
                </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="modalUbicacionDamage" tabindex="-1" aria-labelledby="modalUbicacionDamageLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalUbicacionDamageLabel">Localice los daños</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="#" onsubmit="return false">
                        <input type="hidden" name="letter_1_damage">
                        <div class="row mx-0 w-100">
                            <div class="col-12 form-group text-center">
                                <h6>Código generado <span id="codigo_generado_damage"></span></h6>
                            </div>
                            <div class="col-12 form-group text-center">
                                <button class="btn btn-info" id="damage_todo">TODO</button>
                                <button class="btn btn-info" id="damage_reset">LIMPIAR</button>
                            </div>
                        </div>                        
                        <div class="row w-100 mx-0 px-4" id="box-damage"></div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-info" id="save-codigo-localizacion">Guardar</button>
                </div>
                </div>
            </div>
        </div>
        <?php $this->load->view('common/js_admin');?>
        <script src="<?=base_url()?>public/js/typeahead.js"></script>
        <?php $this->load->view('admin/inspecciones/js');?>
        <script>
            var inspeccion = <?=json_encode($inspeccion);?>;
            //Carga las cosas
            $(document).ready(function(){
                //console.log(inspeccion)
                //container
                $('#codigo_contenedor').val( inspeccion.codigo_contenedor ).attr('data-largo', inspeccion.largo.match(/(\d+)/g) )
                $('#id_tipo').val( inspeccion.codigo_iso )
                $('#id_clasificacion').val( inspeccion.id_clasificacion )
                inspeccion.estado_in_out=='1' ? $('#estado_ingresoingreso').attr('checked', true) : $('#estado_ingresosalida').attr('checked', true)
                inspeccion.estado_llenado=='1' ? $('#estado_llenadolleno').attr('checked', true) : $('#estado_llenadovacio').attr('checked', true)
                //transporte
                $('#id_transportador').val( inspeccion.id_transportador )
                changeTransportador( inspeccion.id_transportador )
                $('#id_conductor').val( inspeccion.id_conductor )
                changeConductor( inspeccion.id_conductor )
                $('#placa_vehiculo').val( inspeccion.placa_vehiculo )
                $('#id_cliente').val( inspeccion.id_cliente )
                $('#codigo_naviera').val( inspeccion.id_naviera )
                $('#observaciones').val( inspeccion.observaciones )

                changeClasificacion (inspeccion.id_clasificacion )
                if ( inspeccion.damages.length>0 )
                {
                    if ($('#table-damage tr.datos-none').length==1) 
                        $('#table-damage tr.datos-none').remove()
                    var content = ''
                    for (let i = 0; i < inspeccion.damages.length; i++) {
                        content += '<tr id="hijo-' + hijo_tr +'">'
                            content += '<td>' + inspeccion.damages[i].codigo_damage + '</td>'
                            content += '<td>' + inspeccion.damages[i].codigo_localizacion_damage + '</td>'
                            content += '<td>' + inspeccion.damages[i].codigo_componente + '</td>'
                            content += '<td>' + inspeccion.damages[i].medida + '<input type="hidden" value="' + inspeccion.damages[i].id_medida + '">' + '</td>'
                            content += '<td>' + inspeccion.damages[i].codigo_reparacion + '</td>'
                            content += '<td>' + inspeccion.damages[i].codigo_responsabilidad + '</td>'
                            content += '<td>'
                                content += '<button type="button" class="btn btn-warning edit-row-table" data-id="' + inspeccion.damages[i].id + '"><i class="mdi mdi-pencil"></i></button>'
                                content += '<button type="button" class="btn btn-danger delete-row-table"><i class="mdi mdi-delete"></i></button>'
                            content += '</td>'
                        content += '</tr>'
                        hijo_tr++;
                    }
                    $('#table-damage').append(content);
                }

                $('#id_clasificacion, #estado_ingresoingreso, #estado_ingresosalida, #estado_llenadolleno, #estado_llenadovacio, #id_transportador, #id_conductor, #placa_vehiculo, #id_cliente').removeAttr('disabled')
            })
        </script>
    </body>

</html>