<script>
    //Seccion
    $('body').on('click', '.box-section-damage.btn', function () {      
         
        //var secciones_contenedor = largo < 30 ? 5 : 10
        //inicio = secciones_contenedor==5 ? 1 : 0,
        //fin = secciones_contenedor==5 ? 6 : 10,
        var codigo_ = '',
            largo = parseInt( $('[name=codigo_contenedor]' ).attr('data-largo') ),
            homologacion_ = ['L','R','H','T','B','G'],
            letter_1 = $('[name=letter_1_damage]').val(),
            cantidad_letter = ['T', 'B'].includes(letter_1) ? 2 : 4;

        if ( ['L', 'R', 'T', 'B'].includes(letter_1 ) ) 
            var secciones_contenedor = largo < 30 ? 5 : 10
        else if ( ['F', 'D'].includes(letter_1 ) ) 
            var secciones_contenedor = 4
        else
            var secciones_contenedor = 1
        
        if ( $(this).attr('class').indexOf('active')!==-1 )
            $(this).removeClass('active')
        else
            $(this).addClass('active')
        
        //horizontal 0->L, 1->R, 2->H, 3->T, 4->B, 5->G,
        var horizontal = [];
        for (let i = 0; i < 6; i++) 
            horizontal.push([])
        //Solo hay un cuadrito
        if ( $('.box-section-damage.btn.active').length == 1 ) 
            codigo = letter_1 + $(this).attr('data-letter') + $(this).attr('data-seccion') + ( ['I', 'E'].includes(letter_1) ? 'X' : 'N' ) //seccion
        else if ( $('.box-section-damage.btn.active').length == 0 ) 
            codigo = ''
        else {
            //trae todos los que está activos
            $('.box-section-damage.btn.active').each( function () {
                var seccion = $(this).attr('data-seccion'),
                    letter = $(this).attr('data-letter'),
                    pos_ = $(this).attr('data-horizontal');

                horizontal[ pos_ ].push( parseInt(seccion) )
            })
            //console.log('horizontal', horizontal)

            //horizontal
            var active_h = horizontal.filter( letter_=>letter_.length>0 ),
                longitud_x_h = 0,
                seccion_h = 0,
                longitud_y_h = 0,
                codigo = '';
            if ( active_h.length>0 ) {
                var new_array_y = [];
                for (let i = 0; i < active_h.length; i++) {
                    if ( active_h[i].length==1 ) 
                        new_array_y.push(active_h[i][0])
                }
                var unique = new_array_y.filter(onlyUnique),
                    repetidos_y = active_h.filter(variable => variable.length >1 );
                
                //Es toda la vertical y es sólo un y 
                if ( active_h.length == cantidad_letter && unique.length==1 && repetidos_y.length==0 ) {
                    codigo = letter_1 + 'X' + unique[0] + 'N'
                //Es una seccion de la vertical
                }else if ( repetidos_y.length==0  ) {
                    var new_array_keys_y = []
                    for (let i = 0; i < horizontal.length; i++) {
                        if ( horizontal[i].length==1 ) 
                            new_array_keys_y.push(i)
                    }
                    var primera = homologacion_[ new_array_keys_y[0] ],
                        segunda = homologacion_[ new_array_keys_y[(new_array_keys_y.length-1)] ]
                    codigo = letter_1 + primera + segunda + unique[0]
                }else if ( repetidos_y.length>0 && active_h.length>1 ) 
                    codigo = ''
                else{ //es una seccion horizontal
                    for (let i = 0; i < horizontal.length; i++) {
                        if ( horizontal[i].length==0 )
                            continue;
                        if ( horizontal[i].length==secciones_contenedor ) {
                            codigo = letter_1 + homologacion_[i] + 'XX'; //longitud horizontal
                            longitud_x_h++;
                        }else{
                            var new_array = horizontal[i]
                            //cuando el contenedor tiene 10 secciones y es del lado izquierdo
                            if ( new_array.includes(0) && letter_1=='L' ) 
                                new_array[0] = 10
                            //cuando el contenedor tiene 10 secciones y no es del lado izquierdo
                            if ( new_array.includes(0) && letter_1!='L' ) 
                                new_array[ (new_array.length-1) ] = 10
                            //Valida que el array venga consecutivo
                            if ( sonConsecutivos(new_array) == 0) {
                                var letter_3 = horizontal[i][ (horizontal[i].length-1) ] == 10 ? 0 : horizontal[i][ (horizontal[i].length-1) ],
                                    letter_4 = horizontal[i][0] == 10 ? 0 : horizontal[i][0];
                                codigo = letter_1!='L' ? letter_1 + homologacion_[i] + letter_3 + letter_4 : letter_1 + homologacion_[i] + letter_4 + letter_3 //porcion de la longitud horizontal
                                seccion_h++;
                            }
                        }
                    }
                }
            }
            if ( codigo == '') 
                codigo = 'ERROR'
            

            function sonConsecutivos(arrayNumeros)
            {
                if (arrayNumeros.length<=1)
                    return false;
                // si el orden es creciente lo giramos
                if (arrayNumeros[0]<arrayNumeros[1]) 
                    arrayNumeros.reverse();
                // devuelve 0 si son consecutivos
                return arrayNumeros.reduce((acum, el, index, array)=>{
                    if (index==1) 
                        acum=0;
                    return +acum+array[index-1]-(el+1);
                });
            }

            function onlyUnique(value, index, self) {
                return self.indexOf(value) === index;
            }
        }

        $('#codigo_generado_damage').text( codigo )
    })

    $('body').on('click', '#damage_todo', function () {
        var letter_1 = $('[name=letter_1_damage]').val()
        $('.box-section-damage.btn').removeClass('active').addClass('active')
        $('#codigo_generado_damage').text( letter_1 + 'XXX' )
    })

    $('body').on('click', '#damage_reset', function () {
        $('.box-section-damage.btn').removeClass('active')
        $('#codigo_generado_damage').text( '' )
    })
</script>