<script>
//Change codigo de daño: resetea posible reparacion
$('#principal #codigo_damage').on('change', function () {
    var codigo_damage = $(this).val()
    changeCodigoDamage (codigo_damage)
})
function changeCodigoDamage (codigo_damage){
    var items = list_codigo_reparacion.filter(damage => damage['codigo_damage'] === codigo_damage),
        content = '<option value="" style="display:none">Seleccionar</option>';
    if ( items.length>0 ) {
        for ( let i = 0; i < items.length; i++ ) 
            content += '<option value="' + items[i].codigo_reparacion + '">' + items[i].codigo_reparacion + '</option>';
    }
    $('#principal #codigo_reparacion').removeAttr('disabled').empty().append( content )
    if ( items.length==0 ) 
        Swal.fire({ icon: 'warning', text: 'Debe agregar un código de reparación al código de daño ' + codigo_damage + '...' })
}
//END
</script>