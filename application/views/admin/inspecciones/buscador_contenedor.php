<script>
//Contenedor - buscador
$('[name=codigo_contenedor]').typeahead({
    hint: true,
    highlight: true,
    minLength: 4,
},{
    limit: 12,
    async: true,
    source: function(query, processSync, processAsync){
        return $.ajax({
            url: "<?=base_url();?>contenedores/viewContainer",
            type: 'POST',
            data:{
                text: query //texto
            },
            dataType: 'JSON',
            success: function(data){
                $('#principal [name=codigo_iso]').addClass('d-none')
                $(document).ajaxStop(function(){
                    $("body").removeClass("loading");
                });
                <?php if ($url=='monitoreos') { ?>
                    $('.form-container').parent().addClass('d-none');
                <?php } ?>
                if (data.res === "ok" && data.like.length>0) {
                    var tags_auto = [];
                    for (var i = 0; i < data.like.length; i++) {
                        tags_auto.push(data.like[i].codigo);
                        var largo = data.like[i].largo.match(/(\d+)/g)
                        codigosT[data.like[i].codigo] = { 
                            naviera: data.like[i].naviera, 
                            id: data.like[i].id_naviera,
                            codigo_iso: data.like[i].codigo_iso,
                            largo: largo[0]
                        };
                    }
                    return processAsync(tags_auto);	
                }else
                    codigosT = []
            }
        });
    }
});

//selecciona una sugerencia del contenedor: automaticamente selecciona el codigo de la naviera y se desbloquea los demas campos
$('[name=codigo_contenedor]').bind('typeahead:select', function(ev, suggestion) {
    $(this).val(suggestion).attr( 'data-largo', codigosT[suggestion].largo );
    $('[name=codigo_naviera]').val( codigosT[suggestion].id )
    $('[name=id_tipo]').val( codigosT[suggestion].codigo_iso )
    $('input, select.s-normal').removeAttr('disabled')
});	
//END

$('[name=codigo_contenedor]').bind('typeahead:close', function() {
    if ( $(this).val()!='' && Object.keys(codigosT).length>0 && ($(this).attr('data-largo')==undefined || $(this).attr('data-largo')=='') )
        Swal.fire({ icon: 'warning', title: 'Oops...', text: 'Debe seleccionar la opción del container.' })
    else if ($(this).val()!='' && $(this).val().length!=11) {
        Swal.fire({ icon: 'warning', title: 'Oops...', text: 'Debe ingresar 11 caracteres del número del container.' })
    }else if ( $(this).val()!='' && ($(this).attr('data-largo')==undefined || $(this).attr('data-largo')=='') && Object.keys(codigosT).length==0 ) {
        var codigo_contenedor = $(this).val().trim().toUpperCase(),
            parte_1 = codigo_contenedor.substr(0, 4),
            parte_2 = codigo_contenedor.substr(4, 6),
            parte_3 = codigo_contenedor.substr(-1, 1);

        if ( validateContainerNoInput(parte_1, parte_2, parte_3) ){
            <?php if ($url=='inspecciones') { ?>
                $('[name=codigo_naviera], [name=id_tipo], #principal [name=codigo_iso], input, select.s-normal').removeAttr('disabled')
            <?php }elseif ($url=='monitoreos') { ?>
                $('.form-container').parent().removeClass('d-none');
            <?php } ?>
        }else{
            Swal.fire({ icon: 'warning', title: 'Oops...', text: 'Debe ingresar un código del container válido.' }) 
        }   
    }else{
        <?php if ($url=='monitoreos') { ?>
            $('.form-container').parent().addClass('d-none');
        <?php } ?>
    }
});

function validateContainerNoInput(parte_1, parte_2, parte_3)
{
    var f1 = ( parseInt( control_digitos_c[parte_1.substr(0,1)] ) *1 ) + ( parseInt( control_digitos_c[parte_1.substr(1,1)] ) *2 ) + ( parseInt( control_digitos_c[parte_1.substr(2,1)] ) *4 ) + ( parseInt( control_digitos_c[parte_1.substr(3,1)] ) *8 ) + ( parseInt( parte_2.substr(0,1) ) *16 ) + ( parseInt( parte_2.substr(1,1) ) *32 ) + ( parseInt( parte_2.substr(2,1) ) *64 ) + ( parseInt( parte_2.substr(3,1) ) *128 ) + ( parseInt( parte_2.substr(4,1) ) *256 ) + ( parseInt( parte_2.substr(5,1) ) *512 ),
        f2 = f1*+1/11,
        f3 = parseInt( f2.toString().substr(0,3) ) * 11,
        dvreal =  (f1 - f3) == 10 ? 0 : (f1 - f3);
    
        if ( dvreal == parte_3 )
        return true
    return false
}
</script>