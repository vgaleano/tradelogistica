<script>
//CHANGE transporador: resetea conductor y placa
$('#principal #id_transportador').on('change', function () {
    var content = '<option value="" style="display:none">Seleccionar</option>'
    $('#principal #placa_vehiculo').attr('disabled', true).empty().append( content )
    var id_transportador = $(this).val()
    changeTransportador( id_transportador )
})
function changeTransportador(id_transportador){
    var content = '<option value="" style="display:none">Seleccionar</option>'
    var items = list_conductores.filter(conductor => conductor.id_transportador === id_transportador);
    if ( items.length > 0 ) {
        for (let i = 0; i < items.length; i++) 
            content += '<option value="' + items[i].id + '">' + items[i].conductor + ' - ' + items[i].numero_documento + '</option>';
    }
    
    $('#principal #id_conductor').removeAttr('disabled').empty().append( content )
}
//END
</script>