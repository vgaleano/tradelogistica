<?php date_default_timezone_set('America/Bogota'); ?>
<style>
    table {
        width: 100%;
        max-width: 100%;
        margin-bottom: 1rem;
    }
    table td, table th {
        border-color: #f3f1f1;
        font-size: 12px;
    }
    .table-border td, .table-border th {
        border: 1px solid black;
    }
    table td, table th {
        padding: .5rem;
        vertical-align: top;
    }
    .center td {
        text-align: center;
    }
    .back-lleno {
        background-color: black;
    }
    .mt-1 {
        margin-top: 10px;
    }
    .back-gris {
        background-color: #f3f3f3;
    }
    table:not(.table-border) {
        padding: 0.2rem;
    }
    .border-puntas {
        border: 1px dotted black;
    }
    .border-t {
        border-top: 1px solid black;
    }
    .border-l {
        border-left: 1px solid black;
    }
    .border-r {
        border-right: 1px solid black;
    }
    .border-b {
        border-bottom: 1px solid black;
    }
    .p-0 {
        padding: 5px;
    }
    .m-0 {
        margin: 0px;
    }
    .pt-0 {
        padding-top: 0px!important;
    }
    .pb-0 {
        padding-bottom: 0px!important;
    }
    .observaciones {
        text-align: left!important;
    }
    img {
        max-width: 100px
    }
    .box-firma {
        width: 100%;
        height: 90px;
    }
</style>
<table class="table center table-border">
    <tbody>
        <tr>
            <td width="15">DÍA<br><?=date('d', strtotime($inspeccion->created_at))?></td>
            <td width="15">MES<br><?=date('m', strtotime($inspeccion->created_at))?></td>
            <td width="15">AÑO<br><?=date('Y', strtotime($inspeccion->created_at))?></td>
            <td class="py-auto">EQUIPMENT INSPECTION REPORT - EIR</td>
            <td  width="15" rowspan="2"><?=$inspeccion->id?><br>CAS</td>
        </tr>
        <tr>
            <td colspan="3">Ciudad: Barranquilla</td>
            <td colspan="1">GERENCIA DE OPERACIONES</td>
        </tr>
    </tbody>
</table>
<table class="table center mt-1 table-border">
    <tbody>
        <tr>
            <td colspan="6" width="50%" class="back-gris">CONTAINER No. <?=$inspeccion->codigo_contenedor?></td>
            <td width="20%" class="back-gris">TRANSPORTADOR</td>
            <td width="30%" class="back-gris"><?=$inspeccion->transportador?></td>
        </tr>
        <tr>
            <td>INGRESO</td>
            <td class="<?=$inspeccion->estado_in_out=='1'?'back-lleno':''?>"></td>
            <td>LLENO</td>
            <td class="<?=$inspeccion->estado_llenado=='1'?'back-lleno':''?>"></td>
            <td>TIPO</td>
            <td><?=$inspeccion->codigo_iso?></td>
            <td>CONDUCTOR</td>
            <td><?=$inspeccion->conductor?></td>
        </tr>
        <tr>
            <td>SALIDA</td>
            <td class="<?=$inspeccion->estado_in_out=='-1'?'back-lleno':''?>"></td>
            <td>VACÍO</td>
            <td class="<?=$inspeccion->estado_llenado=='-1'?'back-lleno':''?>"></td>
            <td>TAMAÑO</td>
            <td><?=$inspeccion->largo?></td>
            <td>C.C.</td>
            <td><?=$inspeccion->numero_documento?></td>
        </tr>
        <tr>
            <td>HORA</td>
            <td><?=date('H:i:s', strtotime($inspeccion->created_at))?></td>
            <td colspan="4">CLASIFICACIÓN: <?=$inspeccion->clasificacion?></td>
            <td>PLACAS</td>
            <td><?=$inspeccion->placa_vehiculo?></td>
        </tr>
        <tr>
            <td colspan="6"></td>
            <td>CLIENTE</td>
            <td><?=$inspeccion->cliente?></td>
        </tr>
        <tr>
            <td colspan="6"></td>
            <td>LÍNEA MARÍTIMA</td>
            <td><?=$inspeccion->naviera?></td>
        </tr>
    </tbody>
</table>
<table class="table center m-0">
    <tbody>
        <tr>
            <td class="p-0 pt-0" width="50%">
                <table class="m-0 pt-0 pb-0">
                    <tbody>
                        <tr>
                            <td rowspan="3" class="p-0 vertical-1">Roof<br>(Techo)</td>
                            <td></td>
                            <td class="p-0 pb-0">1</td>
                            <td class="p-0 pb-0">2</td>
                            <td class="p-0 pb-0">3</td>
                            <td class="p-0 pb-0">4</td>
                            <td class="p-0 pb-0">5</td>
                            <?php if ((int)$inspeccion->largo>=30) { ?>
                            <td class="p-0 pb-0">6</td>
                            <td class="p-0 pb-0">7</td>
                            <td class="p-0 pb-0">8</td>
                            <td class="p-0 pb-0">9</td>
                            <td class="p-0 pb-0">0</td>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td class="p-0">L</td>
                            <td class="border-l border-t border-puntas p-0"></td>
                            <td class="border-t border-puntas p-0"></td>
                            <td class="border-t border-puntas p-0"></td>
                            <td class="border-t border-puntas p-0"></td>
                            <td class="border-t border-puntas p-0"></td>
                            <?php if ((int)$inspeccion->largo>=30) { ?>
                            <td class="border-t border-puntas p-0"></td>
                            <td class="border-t border-puntas p-0"></td>
                            <td class="border-t border-puntas p-0"></td>
                            <td class="border-t border-puntas p-0"></td>
                            <td class="border-t border-r border-puntas p-0"></td>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td class="p-0">R</td>
                            <td class="border-l border-b border-puntas p-0"></td>
                            <td class="border-b border-puntas p-0"></td>
                            <td class="border-b border-puntas p-0"></td>
                            <td class="border-b border-puntas p-0"></td>
                            <td class="border-b border-puntas p-0"></td>
                            <?php if ((int)$inspeccion->largo>=30) { ?>
                            <td class="border-b border-puntas p-0"></td>
                            <td class="border-b border-puntas p-0"></td>
                            <td class="border-b border-puntas p-0"></td>
                            <td class="border-b border-puntas p-0"></td>
                            <td class="border-b border-r border-puntas p-0"></td>
                            <?php } ?>
                        </tr>
                    </tbody>
                </table>
            </td>
            <td class="p-0 pt-0" width="50%">
                <table class="m-0 pt-0 pb-0">
                    <tbody>
                        <tr>
                            <td class="vertical" rowspan="5"><span>Left Side<br>(Lado Izq)</span></td>
                            <td class="p-0"></td>
                            <?php if ((int)$inspeccion->largo>=30) { ?>
                            <td class="p-0  pb-0">0</td>
                            <td class="p-0 pb-0">9</td>
                            <td class="p-0 pb-0">8</td>
                            <td class="p-0 pb-0">7</td>
                            <td class="p-0 pb-0">6</td>
                            <?php } ?>
                            <td class="p-0 pb-0">5</td>
                            <td class="p-0 pb-0">4</td>
                            <td class="p-0 pb-0">3</td>
                            <td class="p-0 pb-0">2</td>
                            <td class="p-0 pb-0">1</td>
                        </tr>
                        <tr >
                            <td class="p-0">H</td>
                            <?php if ((int)$inspeccion->largo>=30) { ?>
                            <td class="border-l border-t border-b border-r p-0"></td>
                            <td class="border-t border-l border-b border-r p-0"></td>
                            <td class="border-t border-l border-b border-r p-0"></td>
                            <td class="border-t border-l border-b border-r p-0"></td>
                            <td class="border-t border-l border-b border-r p-0"></td>
                            <?php } ?>
                            <td class="border-t border-l border-b border-r p-0"></td>
                            <td class="border-t border-l border-b border-r p-0"></td>
                            <td class="border-t border-l border-b border-r p-0"></td>
                            <td class="border-t border-l border-b border-r p-0"></td>
                            <td class="border-t border-r border-l border-b p-0"></td>
                        </tr>
                        <tr>
                            <td class="p-0">T</td>
                            <?php if ((int)$inspeccion->largo>=30) { ?>
                            <td class="border-l border-puntas p-0"></td>
                            <td class="border-puntas p-0"></td>
                            <td class="border-puntas p-0"></td>
                            <td class="border-puntas p-0"></td>
                            <td class="border-puntas p-0"></td>
                            <?php } ?>
                            <td class="border-puntas p-0"></td>
                            <td class="border-puntas p-0"></td>
                            <td class="border-puntas p-0"></td>
                            <td class="border-puntas p-0"></td>
                            <td class="border-r border-puntas p-0"></td>
                        </tr>
                        <tr>
                            <td class="p-0">B</td>
                            <?php if ((int)$inspeccion->largo>=30) { ?>
                            <td class="border-l border-puntas p-0"></td>
                            <td class="border-puntas p-0"></td>
                            <td class="border-puntas p-0"></td>
                            <td class="border-puntas p-0"></td>
                            <td class="border-puntas p-0"></td>
                            <?php } ?>
                            <td class="border-puntas p-0"></td>
                            <td class="border-puntas p-0"></td>
                            <td class="border-puntas p-0"></td>
                            <td class="border-puntas p-0"></td>
                            <td class="border-r border-puntas p-0"></td>
                        </tr>
                        <tr >
                            <td class="p-0">G</td>
                            <?php if ((int)$inspeccion->largo>=30) { ?>
                            <td class="border-l border-b border-t border-r p-0"></td>
                            <td class="border-b border-t border-l border-r p-0"></td>
                            <td class="border-b border-t border-l border-r p-0"></td>
                            <td class="border-b border-t border-l border-r p-0"></td>
                            <td class="border-b border-t border-l border-r p-0"></td>
                            <?php } ?>
                            <td class="border-b border-t border-l border-r p-0"></td>
                            <td class="border-b border-t border-l border-r p-0"></td>
                            <td class="border-b border-t border-l border-r p-0"></td>
                            <td class="border-b border-t border-l border-r p-0"></td>
                            <td class="border-b border-r border-t border-l p-0"></td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td class="p-0 pt-0" width="50%">
                <table class="m-0">
                    <tbody>
                        <tr>
                            <td rowspan="3" class="p-0 vertical-1">Floor<br>(Piso)</td>
                            <td></td>
                            <td class="p-0 pb-0">1</td>
                            <td class="p-0 pb-0">2</td>
                            <td class="p-0 pb-0">3</td>
                            <td class="p-0 pb-0">4</td>
                            <td class="p-0 pb-0">5</td>
                            <?php if ((int)$inspeccion->largo>=30) { ?>
                            <td class="p-0 pb-0">6</td>
                            <td class="p-0 pb-0">7</td>
                            <td class="p-0 pb-0">8</td>
                            <td class="p-0 pb-0">9</td>
                            <td class="p-0 pb-0">0</td>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td class="p-0">L</td>
                            <td class="border-l border-t border-puntas p-0"></td>
                            <td class="border-t border-puntas p-0"></td>
                            <td class="border-t border-puntas p-0"></td>
                            <td class="border-t border-puntas p-0"></td>
                            <td class="border-t border-puntas p-0"></td>
                            <?php if ((int)$inspeccion->largo>=30) { ?>
                            <td class="border-t border-puntas p-0"></td>
                            <td class="border-t border-puntas p-0"></td>
                            <td class="border-t border-puntas p-0"></td>
                            <td class="border-t border-puntas p-0"></td>
                            <td class="border-t border-r border-puntas p-0"></td>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td class="p-0">R</td>
                            <td class="border-l border-b border-puntas p-0"></td>
                            <td class="border-b border-puntas p-0"></td>
                            <td class="border-b border-puntas p-0"></td>
                            <td class="border-b border-puntas p-0"></td>
                            <td class="border-b border-puntas p-0"></td>
                            <?php if ((int)$inspeccion->largo>=30) { ?>
                            <td class="border-b border-puntas p-0"></td>
                            <td class="border-b border-puntas p-0"></td>
                            <td class="border-b border-puntas p-0"></td>
                            <td class="border-b border-puntas p-0"></td>
                            <td class="border-b border-r border-puntas p-0"></td>
                            <?php } ?>
                        </tr>
                    </tbody>
                </table>
            </td>
            <td class="p-0 pt-0" width="50%">
                <table class="m-0">
                    <tbody>
                        <tr>
                            <td class="vertical" rowspan="5"><span>Right Side<br>(Lado Der)</span></td>
                            <td class="p-0"></td>
                            <td class="p-0 pb-0">1</td>
                            <td class="p-0 pb-0">2</td>
                            <td class="p-0 pb-0">3</td>
                            <td class="p-0 pb-0">4</td>
                            <td class="p-0 pb-0">5</td>
                            <?php if ((int)$inspeccion->largo>=30) { ?>
                            <td class="p-0 pb-0">6</td>
                            <td class="p-0 pb-0">7</td>
                            <td class="p-0 pb-0">8</td>
                            <td class="p-0 pb-0">9</td>
                            <td class="p-0 pb-0">0</td>
                            <?php } ?>
                        </tr>
                        <tr >
                            <td class="p-0">H</td>
                            <td class="border-l border-t border-b border-r p-0"></td>
                            <td class="border-t border-l border-b border-r p-0"></td>
                            <td class="border-t border-l border-b border-r p-0"></td>
                            <td class="border-t border-l border-b border-r p-0"></td>
                            <td class="border-t border-l border-b border-r p-0"></td>
                            <?php if ((int)$inspeccion->largo>=30) { ?>
                            <td class="border-t border-l border-b border-r p-0"></td>
                            <td class="border-t border-l border-b border-r p-0"></td>
                            <td class="border-t border-l border-b border-r p-0"></td>
                            <td class="border-t border-l border-b border-r p-0"></td>
                            <td class="border-t border-r border-l border-b p-0"></td>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td class="p-0">T</td>
                            <td class="border-l border-puntas p-0"></td>
                            <td class="border-puntas p-0"></td>
                            <td class="border-puntas p-0"></td>
                            <td class="border-puntas p-0"></td>
                            <td class="border-puntas p-0"></td>
                            <?php if ((int)$inspeccion->largo>=30) { ?>
                            <td class="border-puntas p-0"></td>
                            <td class="border-puntas p-0"></td>
                            <td class="border-puntas p-0"></td>
                            <td class="border-puntas p-0"></td>
                            <td class="border-r border-puntas p-0"></td>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td class="p-0">B</td>
                            <td class="border-l border-puntas p-0"></td>
                            <td class="border-puntas p-0"></td>
                            <td class="border-puntas p-0"></td>
                            <td class="border-puntas p-0"></td>
                            <td class="border-puntas p-0"></td>
                            <?php if ((int)$inspeccion->largo>=30) { ?>
                            <td class="border-puntas p-0"></td>
                            <td class="border-puntas p-0"></td>
                            <td class="border-puntas p-0"></td>
                            <td class="border-puntas p-0"></td>
                            <td class="border-r border-puntas p-0"></td>
                            <?php } ?>
                        </tr>
                        <tr >
                            <td class="p-0">G</td>
                            <td class="border-l border-b border-t border-r p-0"></td>
                            <td class="border-b border-t border-l border-r p-0"></td>
                            <td class="border-b border-t border-l border-r p-0"></td>
                            <td class="border-b border-t border-l border-r p-0"></td>
                            <td class="border-b border-t border-l border-r p-0"></td>
                            <?php if ((int)$inspeccion->largo>=30) { ?>
                            <td class="border-b border-t border-l border-r p-0"></td>
                            <td class="border-b border-t border-l border-r p-0"></td>
                            <td class="border-b border-t border-l border-r p-0"></td>
                            <td class="border-b border-t border-l border-r p-0"></td>
                            <td class="border-b border-r border-t border-l p-0"></td>
                            <?php } ?>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2" class=" pt-0">
                <table class="m-0">
                    <tbody>
                        <tr>
                            <td width="5%"></td>
                            <td class="pt-0 pb-0">Reefer</td>
                            <td width="5%"></td>
                            <td class="pt-0 pb-0">Inside (Dentro)</td>
                            <td width="5%"></td>
                            <td class="pt-0 pb-0">Door (Puerta)</td>
                            <td width="5%"></td>
                            <td class="pt-0 pb-0">Front (Frente)</td>
                            <td width="5%"></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td class="pt-0 pb-0">
                                <table class="m-0 pt-0 pb-0">
                                    <tbody>
                                        <tr>
                                            <td rowspan="4" class="border-b border-r border-t border-l"></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                            <td></td>
                            <td class="pt-0 pb-0">
                                <table class="m-0 pt-0 pb-0">
                                    <tbody>
                                        <tr>
                                            <td rowspan="4" class="border-b border-r border-t border-l"></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                            <td></td>
                            <td class="pt-0 pb-0">
                                <table class="m-0 pt-0 pb-0">
                                    <tbody>
                                        <tr>
                                            <td class="p-0"></td>
                                            <td class="p-0 pb-0">1</td>
                                            <td class="p-0 pb-0">2</td>
                                            <td class="p-0 pb-0">3</td>
                                            <td class="p-0 pb-0">4</td>
                                        </tr>
                                        <tr>
                                            <td class="p-0">H</td>
                                            <td class="p-0 border-l border-t border-b border-puntas"></td>
                                            <td class="p-0 border-t border-b border-puntas"></td>
                                            <td class="p-0 border-t border-b border-puntas"></td>
                                            <td class="p-0 border-r border-t border-b border-puntas"></td>
                                        </tr>
                                        <tr>
                                            <td class="p-0">T</td>
                                            <td class="p-0 border-l border-puntas"></td>
                                            <td class="p-0 border-puntas"></td>
                                            <td class="p-0 border-puntas"></td>
                                            <td class="p-0 border-r border-puntas"></td>
                                        </tr>
                                        <tr>
                                            <td class="p-0">B</td>
                                            <td class="p-0 border-l border-puntas"></td>
                                            <td class="p-0 border-puntas"></td>
                                            <td class="p-0 border-puntas"></td>
                                            <td class="p-0 border-r border-puntas"></td>
                                        </tr>
                                        <tr>
                                            <td class="p-0">G</td>
                                            <td class="p-0 border-l border-t border-b border-puntas"></td>
                                            <td class="p-0 border-t border-b border-puntas"></td>
                                            <td class="p-0 border-t border-b border-puntas"></td>
                                            <td class="p-0 border-r border-t border-b border-puntas"></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                            <td></td>
                            <td class="pt-0 pb-0">
                                <table class="m-0 pt-0 pb-0">
                                    <tbody>
                                    <tr>
                                            <td class="p-0"></td>
                                            <td class="p-0 pb-0">4</td>
                                            <td class="p-0 pb-0">3</td>
                                            <td class="p-0 pb-0">2</td>
                                            <td class="p-0 pb-0">1</td>
                                        </tr>
                                        <tr>
                                            <td class="p-0">H</td>
                                            <td class="p-0 border-l border-t border-b border-puntas"></td>
                                            <td class="p-0 border-t border-b border-puntas"></td>
                                            <td class="p-0 border-t border-b border-puntas"></td>
                                            <td class="p-0 border-r border-t border-b border-puntas"></td>
                                        </tr>
                                        <tr>
                                            <td class="p-0">T</td>
                                            <td class="p-0 border-l border-puntas"></td>
                                            <td class="p-0 border-puntas"></td>
                                            <td class="p-0 border-puntas"></td>
                                            <td class="p-0 border-r border-puntas"></td>
                                        </tr>
                                        <tr>
                                            <td class="p-0">B</td>
                                            <td class="p-0 border-l border-puntas"></td>
                                            <td class="p-0 border-puntas"></td>
                                            <td class="p-0 border-puntas"></td>
                                            <td class="p-0 border-r border-puntas"></td>
                                        </tr>
                                        <tr>
                                            <td class="p-0">G</td>
                                            <td class="p-0 border-l border-t border-b border-puntas"></td>
                                            <td class="p-0 border-t border-b border-puntas"></td>
                                            <td class="p-0 border-t border-b border-puntas"></td>
                                            <td class="p-0 border-r border-t border-b border-puntas"></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>
<table class="table center">
    <tbody>
        <tr>
            <td colspan="3" class="border-t border-b border-l border-r observaciones">OBSERVACIONES: <?=$inspeccion->observaciones?></td>
        </tr>
    </tbody>
</table>
<table class="table center">
    <tbody>
        <tr>
            <td width="33.33% mt-2">
                <?php if ( $inspeccion->url_firma != '' ) { ?>
                <img width="150px" height="100px" src="<?=base_url().$inspeccion->url_firma?>">
                <?php }else{ ?>
                <div style="width:150px; height:100px;"></div>
                <?php } ?>
                
                <br>
                <hr>
                CONDUCTOR
            </td>
            <td width="33.33%"></td>
            <td width="33.33%">
                <div class="box-firma"></div>
                <br>
                <hr>
                TÉCNICO
            </td>
        </tr>
    </tbody>
</table>