<script>
    //Modal change content
    <?php if (isset($forms_add)) { ?>
    $('body').on('click', '.add-item', function(){
        var pos_ = $(this).attr('data-url'),
            item = forms_add[pos_],
            content = '',
            bandera_lg = false;
            
        add_input = true

        if ( $('.modal form .row').length > 1)
            $('.modal form .row').eq(1).remove()

        //viene desde el boton crear en el modal cuando es diferente de 0, para agregar el otro form y convertir lo que hay en una columna
        if ( $(this).closest('.modal')!==undefined && $(this).closest('.modal').length != 0 ) {
            $('.modal form').addClass('row')
            $('.modal form .row').eq(0).addClass('col-12 col-lg-6') //pasarlo todo a 1 columna
            $('.modal form .row .col-lg-6').removeClass('col-lg-6').addClass('col-lg-12')
            content = '<div class="row col-12 col-lg-6 mx-0 w-100 border-left">'
                content += '<form action="' + item['form_add_url'] + '/new" method="POST" onsubmit="return false" class="w-100">'
                    content += '<h3 class="mb-0">Nuevo ' + item['form_add_titulo'] + '</h3>'
                    content += '<div class="row mx-0 w-100">'
        }else{
            $('.modal form #modo').val( $(this).attr('data-url') )
            $('.modal form .row').empty().removeClass('col-12 col-lg-6')
            $('.modal h5').text( 'Nuevo ' + item['form_add_titulo'] )
            $('.modal form').attr( 'action', item['form_add_url'] + '/new' )
        }
        //END
                        
        //no es contenedor y viene directamente del boton de la vista
        if ( pos_ != 'contenedores' && ( $(this).closest('.modal')===undefined || ( $(this).closest('.modal')!==undefined && $(this).closest('.modal').length==0 ) ) ) 
            $('.modal .modal-dialog').removeClass('modal-lg')
        //Si es contenedores o vehiculos se pone grande el modal
        if ( modal_lg.includes( pos_ ) ) 
            $('.modal .modal-dialog').addClass('modal-lg')
        if ( modal_lg.includes( pos_) && $(this).closest('.modal')!==undefined && $(this).closest('.modal').length != 0 ) 
            bandera_lg = true         
        //Si es contenedores para agregar el input del codigo del contenedor
        if ( pos_ == 'contenedores' ) {
            content += '<div class="col-12 col-lg-<?=$url=="inspecciones"?6:12?> form-group">'
                content += '<label>Container No.<span class="text-danger">*</span> </label>'
                content += '<div class="input-group mb-3">'
                    content += '<input type="text" class="form-control n_contenedor text-uppercase" name="1_contenedor" onkeypress="return onlyText(event)" maxlength="4" size="4" placeholder="XXXX">'
                    content += '<input type="text" class="form-control n_contenedor" name="2_contenedor" onkeypress="return onlyNumber(event)" maxlength="6" size="6" placeholder="######">'
                    content += '<div class="input-group-append">'
                        content += '<span class="input-group-text h-100 d-flex align-items-center px-2">-</span>'
                    content += '</div>'
                    content += '<input type="text" class="form-control n_contenedor" name="3_contenedor" onkeypress="return onlyNumber(event)" maxlength="1" size="1" placeholder="#">'
                content += '</div>'
            content += '</div>'
        }
        //END
        //Crean los form control: select, input, label y select-add
        for ( let i = 0; i < item.form_add.length; i++ ) {
            var column_form_control = bandera_lg ? '12' : item.form_add[i]['column']
            //Crea el div form-group
            content += '<div class="col-12 col-lg-' + column_form_control + ' form-group">'
            //Si existe lavel y lo pinta
            if (typeof item.form_add[i]['label'] !== undefined ) 
                content += '<label for="' + item.form_add[i].for + '">' + item.form_add[i]['label'] + '</label>'
            //Si el form_control es un input, attr: value y extra attr
            if ( item.form_add[i]['form_control'] == 'input' ) 
                content += '<input value="' + item.form_add[i]['value'] + '" ' + (item.form_add[i]['extra_attr']!='' ? item.form_add[i]['extra_attr'] : '' ) + ' ';
            //Si el form_control es un select
            if ( item.form_add[i]['form_control'] == 'select' ) 
                content += '<select ';
            //Si el form_control es select o input pintara el resto de los attr
            if ( item.form_add[i]['form_control'] == 'select' || item.form_add[i]['form_control'] == 'input' ) {
                for ( attr in item.form_add[i]['attr'] ) 
                    content += attr + "='" + item.form_add[i]['attr'][attr] + "' ";
            }
            //Si el form_control es un input y lo cierra
            if ( item.form_add[i]['form_control'] == 'input' ) 
                content += '>';
            //Si el form_control es un select, pinta las opciones
            if ( item.form_add[i]['form_control'] == 'select' ) {
                content += '><option value="" style="display:none">Seleccionar</option>';
                for ( let j = 0; j < item.form_add[i]['options'].length; j++ ) 
                    content += '<option value="' + item.form_add[i]['options'][j]['value'] + '">' + item.form_add[i]['options'][j]['name'] + '</option>';
                content += '</select>';
            }
            //Si el form_control es un select-add y lo pinta
            if ( item.form_add[i]['form_control'] == 'select-add' ) 
                content += addItemContent( item.form_add[i]['for'], item.form_add[i]['options'], pos_ )
            //Si el form_control es un input-add y lo pinta
            if ( item.form_add[i]['form_control'] == 'input-add' )
                content += addInputContent( item.form_add[i]['inputs'] )
            //Cierra el div de form-group
            content += '</div>'
        }
        //END
        //Viene desde el boton crear en el modal
        if ( $(this).closest('.modal') !== undefined && $(this).closest('.modal').length != 0 ) {
                        content += '<div class="col-12 text-right">'
                            content += '<button type="button" class="btn btn-secondary remove-add-item">Cancelar</button>'
                            content += '<button type="submit" class="btn btn-info ml-2 save">Guardar</button>'
                        content += '</div>'
                    content += '</div>'
                content += '</form>'
            content += '</div>'
            $('.modal form').append(content)
            $('.modal form .row').eq(1).find('.add-item').addClass('d-none')
        }else
            $('.modal form .row').append(content)            

        if ( pos_=="contenedores") {
            
        }
    })
    <?php } ?>
</script>