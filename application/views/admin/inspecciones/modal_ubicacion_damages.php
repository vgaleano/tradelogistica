<script>
//IN INFORMACION DE DAÑO
//Click la ubicacion del daño
$('#principal .btn-ubicacion').on('click', function () {
    var largo = parseInt( $('[name=codigo_contenedor]' ).attr('data-largo') ),
        letter_1 = $(this).attr( 'data-first-letter' ),
        box_seccion = '<div class="col-12 d-flex justify-content-center align-items-center">',
        content_seccion_btn = content_seccion = '';
    
    if ( ['L', 'R', 'T', 'B'].includes(letter_1 ) ) 
        var secciones_contenedor = largo < 30 ? 6 : 11
    else if ( ['F', 'D'].includes(letter_1 ) ) 
        var secciones_contenedor = 5
    else
        var secciones_contenedor = 1
    //horizontal 0->L, 1->R, 2->H, 3->T, 4->B, 5->G
    btn_select_damage = ['T', 'B'].includes(letter_1) ? [ { letter: 'L', pos_: 0 }, { letter: 'R', pos_: 1 } ] : [ { letter: 'H', pos_: 2 }, { letter: 'T', pos_: 3 }, { letter: 'B', pos_: 4 }, { letter: 'G', pos_: 5 }  ]

    if ( !['I', 'E'].includes(letter_1 ) ) {
        for (let j = 0; j < btn_select_damage.length; j++) {
            if (letter_1!='L' && letter_1!='F' ) {
                for (let i = 1; i < secciones_contenedor; i++) 
                    secciones (i, btn_select_damage[j].letter, j, letter_1, secciones_contenedor, btn_select_damage[j].pos_)
            }else{
                for (let i = (secciones_contenedor-1); i > 0; i--) 
                    secciones (i, btn_select_damage[j].letter, j, letter_1, secciones_contenedor, btn_select_damage[j].pos_)
            }
        }  
        //La franja de numeros de las secciones del contenedor
        box_seccion += '<div class="box-section-damage"></div>' + content_seccion + '</div>'
    }else
        content_seccion_btn = '<div class="box-section-damage btn btn-full" data-seccion="X" data-letter="X" data-horizontal=""</div>'

    function secciones (i, letter, j, letter_select, sizes, pos_) {
        var seccion = i == 10 ? 0 : i
        content_seccion += j==0 ? '<div class="box-section-damage text-center"><span>' + seccion + '</span></div>' : '', 
        content_seccion_btn += (letter_select=='L' && seccion== 0) || (!['L','F'].includes(letter_select) && seccion==1 ) || (letter_select=='F' && seccion==4 ) ? '<div class="col-12 d-flex justify-content-center align-items-center"><div class="box-section-damage letter-section m-0"><span>' + letter + '</span></div>' : ''
        content_seccion_btn += '<div class="box-section-damage btn" data-seccion="' + seccion + '" data-letter="' + letter + '" data-horizontal="' + pos_ + '"></div>';
        content_seccion_btn += (['L','F'].includes(letter_select) && seccion== 1) || (letter_select!='L' && seccion==0 && sizes==11) || (letter_select!='L' && seccion==5 && sizes==6) || (seccion==4 && letter_select=='D') || (seccion==1 && letter_select=='F') ? '</div>' : ''
    }
    //Codigo de ubicacion del daño
    $('#codigo_generado_damage').text( letter_1 )
    $('[name=letter_1_damage]').val( letter_1 )
    $('#box-damage').empty().append( box_seccion + content_seccion_btn )
})
//END
</script>