<script>
//CHANGE conductor: resetea placa
$('#principal #id_conductor').on('change', function () {
    var id_conductor = $(this).val()
    changeConductor( id_conductor )
})
function changeConductor( id_conductor ){
    var items = list_vehiculos.filter( vehiculo => vehiculo.id_conductor === id_conductor ),
        content = '<option value="" style="display:none">Seleccionar</option>';
    if ( items.length > 0 ) {
        for ( let i = 0; i < items.length; i++ ) 
            content += '<option value="' + items[i].placa + '">' + items[i].placa + '</option>';
    }
    $('#principal #placa_vehiculo').removeAttr('disabled').empty().append( content )
}
//END
</script>