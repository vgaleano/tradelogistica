<script src="<?=base_url()?>public/js/campos_modal_dinamico.js"></script>
<script>
    var forms_add = <?=json_encode($forms_add);?>;
    var list_conductores = <?=json_encode($list_conductores);?>;
    var list_vehiculos = <?=json_encode($list_vehiculos);?>;
    var list_naviera_codigo = <?=json_encode($list_naviera_codigo);?>;
    var control_digitos_c = <?=json_encode($control_digitos_contenedor);?>;
    var list_codigo_reparacion = <?=json_encode($list_codigo_reparacion);?>;
    var hijo_tr = 0,
        hijo_actual_tr = '',
        add_input = false,
        data_normal = ['transportadores', 'clientes', 'navieras'],
        codigosT = [],
        modal_lg = ['codigoiso', 'contenedores', 'vehiculos', 'clasificaciones', 'conductores'],
        btn_select_damage=[];

    $('#adicional-info>div').last().addClass('d-none')
    
    

    function addInputContent( data_input ) {
        var content = '<div class="input-group mb-3">'
            for (let i = 0; i < data_input.length; i++) 
                content += '<input type="' + data_input[i].type + '" name="' + data_input[i].name + '" placeholder="' + data_input[i].placeholder + '" class="' + data_input[i].class + '">'
            content += '</div>'
        return content;
    }

    //Elimina el 2 form del modal, cuando se da crear campo
    $('body').on('click', '.remove-add-item', function (){
        removeAddItemModal()
    })
    function removeAddItemModal() {
        $('.modal form .row').eq(1).remove()
        $('.modal form .row').eq(0).attr('class', 'row mx-0 w-100')
        $('.modal form .row .col-lg-12').removeClass('col-lg-12').addClass('col-lg-6')
    }
    //END

    //Guardar el codigo de ubicacion de daño
    $('#save-codigo-localizacion').on('click', function () {
        var codigo_localizacion = $('.modal #codigo_generado_damage').text(),
            content = '<option value="" style="display:none">Seleccionar</option>';
        $('#id_medida').val('').empty().append(content).attr('disabled', true)
        if ( codigo_localizacion!='' && codigo_localizacion!='ERROR' ) {
            $('[name=localizacion_damage]').val( codigo_localizacion )
            getComponente_(codigo_localizacion)
        }else
            $('#id_componente').attr('disabled', true).val('').empty().append(content)
        $('#modalUbicacionDamage').modal('hide')
    })

    function getComponente_(codigo_localizacion){
        var content = '<option value="" style="display:none">Seleccionar</option>'
        ajax('inspecciones/getComponentsByLocation',{
            codigo_localizacion: codigo_localizacion
        }, function (data) {
            if (data.res=='ok' && data.datos.length>0) {
                for (let i = 0; i < data.datos.length; i++) 
                    content += '<option value="' + data.datos[i].codigo + '">' + data.datos[i].codigo + '</option>'
                $('#id_componente').removeAttr('disabled').val('').empty().append(content)
            }
        })
    }

    //change componente y codigo de reparacion
    $('#id_componente, #codigo_reparacion').on('change', function () {
        var codigo_componente = $('#id_componente').val(),
            codigo_reparacion = $('#codigo_reparacion').val(),
            content = '<option value="" style="display:none">Seleccionar</option>';

        $('#id_medida').val('').empty().append(content).attr('disabled', true)
        if ( codigo_componente!='' && codigo_reparacion!='' ) 
            getMedidas (content, codigo_reparacion, codigo_componente)
    })

    function getMedidas (content, codigo_reparacion, codigo_componente)
    {
        ajax('inspecciones/getMedidasByComponentReparacion',{
            codigo_reparacion: codigo_reparacion,
            codigo_componente: codigo_componente
        }, function (data) {
            if (data.res=='ok' && data.datos.length>0) {
                for (let i = 0; i < data.datos.length; i++) 
                    content += '<option value="' + data.datos[i].id + '">' + data.datos[i].medida + '</option>'
                $('#id_medida').removeAttr('disabled').val('').empty().append(content)
            }
        })
    }

    //guardar codigo de daño
    $('#save-box-damage-campo').on('click', function () {
        var bandera = true;
        if ( $('.box-damage-campo #codigo_damage').val()=='' ) {
            bandera = false
            return Swal.fire({ icon: 'error', title: 'Oops...', text: 'Debe seleccionar una descripción del daño.' })
        }

        if ( $('.box-damage-campo #codigo_reparacion').val()=='' ) {
            bandera = false
            return Swal.fire({ icon: 'error', title: 'Oops...', text: 'Debe seleccionar el código de reparación.' })
        }

        if ( $('.box-damage-campo #id_cargo').val()=='' ) {
            bandera = false
            return Swal.fire({ icon: 'error', title: 'Oops...', text: 'Debe seleccionar cargo a.' })
        }

        if ( $('.box-damage-campo #localizacion_damage').val()=='' ) {
            bandera = false
            return Swal.fire({ icon: 'error', title: 'Oops...', text: 'Debe seleccionar la localización del daño.' })
        }

        if (bandera)
        {
            if ($('#table-damage tr.datos-none').length==1) 
                $('#table-damage tr.datos-none').remove()

            if ( hijo_actual_tr!='' && $('#table-damage tr#hijo-'+hijo_actual_tr)!==undefined) 
                $('#table-damage tr#hijo-'+hijo_actual_tr).remove()
                
            var content = '<tr id="hijo-' + hijo_tr +'">'
                    content += '<td>' + $('.box-damage-campo #codigo_damage').val() + '</td>'
                    content += '<td>' + $('.box-damage-campo #localizacion_damage').val() + '</td>'
                    content += '<td>' + ($('.box-damage-campo #id_componente').val()!='' && $('.box-damage-campo #id_componente').val()!=null ? $('.box-damage-campo #id_componente').val() : '') + '</td>'
                    content += '<td>' + ($('.box-damage-campo #id_medida').val()!='' && $('.box-damage-campo #id_medida').val()!=null ? $('.box-damage-campo #id_medida option:selected').text() : '') + '<input type="hidden" value="' + $('.box-damage-campo #id_medida').val() + '">' + '</td>'
                    content += '<td>' + $('.box-damage-campo #codigo_reparacion').val() + '</td>'
                    content += '<td>' + $('.box-damage-campo #id_cargo').val() + '</td>'
                    content += '<td>'
                        content += '<button type="button" class="btn btn-warning edit-row-table"><i class="mdi mdi-pencil"></i></button>'
                        content += '<button type="button" class="btn btn-danger delete-row-table"><i class="mdi mdi-delete"></i></button>'
                    content += '</td>'
                content += '</tr>'
            $('#table-damage').append(content);
            $('.box-damage-campo input, .box-damage-campo select').val('')
            $('.box-damage-campo #codigo_reparacion, .box-damage-campo #id_componente, .box-damage-campo #id_medida').empty().attr('disabled', true).append('<option value="" style="display:none">Seleccionar</option>')
            $('.box-damage-campo').addClass('d-none')
            hijo_tr++;
        }
    })
            
    //Guardar
    $('body').on('click', '.save', function () {
        event.preventDefault();
        var form_ = $(this).attr('id')===undefined ? $(this).closest('form') : $(this).closest('.modal').find('form')
            url = $(this).attr('id')===undefined ? form_.eq(0).attr('action') : $(this).closest('form').attr('action') ,
            bandera = true,
            array_conductor = [],
            list_ = [],
            name_from_modal = $('.modal form #modo').val(),
            eso = $(this),
            name_form_save = eso.attr('id')!==undefined ? '' : url.replace('/new', ''),
            list_all = [];
        
        if ( data_normal.includes(name_form_save) ) 
            var data = form_.eq(0).serialize()                

        //data para conductores
        if ( name_form_save == 'conductores') {
            $(form_).find('.id_transportador').each(function(){
                if ( $(this).val()=='' ) {
                    bandera = false
                    return Swal.fire({ icon: 'error', title: 'Oops...', text: 'Debe seleccionar un transportador.' })
                }else {
                    list_all.push({ conductor: $('form [name=conductor]').val().trim(), numero_documento:  $('form [name=numero_documento]').val().trim(), id_transportador: $(this).val() })
                    list_.push({ id_transportador: $(this).val() })
                }
            })
            if (list_.length==0) {
                bandera = false
                return Swal.fire({ icon: 'error', title: 'Oops...', text: 'Debe agregar un transportador para guardar el conductor.' })
            }
            var data = {
                conductor: $('form [name=conductor]').val().trim(),
                list_transportador: list_,
                numero_documento: $('form [name=numero_documento]').val().trim(),
                list_: list_all
            }
        }

        //data para vehiculos
        if ( name_form_save == 'vehiculos') {
            $(form_).find('.id_conductor').each(function(){
                if ( $(this).val()=='' ) {
                    bandera = false
                    return Swal.fire({ icon: 'error', title: 'Oops...', text: 'Debe seleccionar un conductor.' })
                }else {
                    list_all.push({ placa: $('form [name=placa]').val().trim(), id_conductor:  $(this).val() })
                    list_.push({ id_conductor: $(this).val() })
                }
            })
            if (list_.length==0) {
                bandera = false
                return Swal.fire({ icon: 'error', title: 'Oops...', text: 'Debe agregar un conductor para guardar el vehículo.' })
            }
            var data = {
                placa: $('form [name=placa]').val().trim(),
                list_conductor: list_,
                list_: list_all
            }
        }

        //data para clasificacion - codigoiso
        if ( name_form_save == 'clasificaciones' || name_form_save == 'codigoiso') {
            var text = name_form_save == 'clasificaciones' ? 'vehículo' : 'código ISO'
            $(form_).find('.id_naviera').each(function(){
                if ( $(this).val()=='' ) {
                    bandera = false
                    return Swal.fire({ icon: 'error', title: 'Oops...', text: 'Debe seleccionar la naviera.' })
                }else {
                    if ( name_form_save == 'clasificaciones' ) {
                        list_all.push({ id_naviera:  $(this).val() })
                        list_.push({ id_naviera: $(this).val() })
                    }else {
                        if ( $(this).next('[name=codigo_naviera]').val().trim()=='' ) {
                            bandera = false
                            return Swal.fire({ icon: 'error', title: 'Oops...', text: 'Debe agregar un código ISo para la naviera.' })
                        }
                        if ( $('[name=dimension_largo]').val().trim()=='' || $('form [name=dimension_alto]').val().trim()=='' || $('form [name=dimension_ancho]').val().trim()=='' ) {
                            bandera = false
                            return Swal.fire({ icon: 'error', title: 'Oops...', text: 'Debe agregar la dimensión para el código ISo.' })
                        }
                        list_all.push({ 
                            codigo_iso: $('form [name=codigo]').val().trim(), 
                            id_naviera: $(this).val(), 
                            codigo_naviera: $(this).next('[name=codigo_naviera]').val().trim(), 
                            naviera: $('option:selected', this).text(),
                            largo: $('form [name=dimension_largo').val().trim(),
                            alto: $('form [name=dimension_alto]').val().trim(),
                            ancho: $('form [name=dimension_ancho]').val().trim()
                            })
                        list_.push({ 
                            id_naviera: $(this).val(), 
                            codigo_iso: $('form [name=codigo]').val().trim(), 
                            dimensiones: $('form [name=dimension_largo]').val().trim() + 'X' + $('form [name=dimension_alto]').val().trim() + 'X' + $('form [name=dimension_ancho]').val().trim() , 
                            codigo_naviera: $(this).next('[name=codigo_naviera]').val().trim(),
                            largo: $('form [name=dimension_largo]').val().trim()
                            })  
                    }                        
                }
            })
            if (list_.length==0) {
                bandera = false
                return Swal.fire({ icon: 'error', title: 'Oops...', text: 'Debe agregar la naviera para guardar el ' + text +'.' })
            }
            if ( name_form_save == 'clasificaciones' ) 
                var data = { clasificacion: $('form [name=clasificacion]').val().trim(), list_naviera: list_, list_: list_all }
            if ( name_form_save == 'codigoiso' )
                var data = { codigo: $('form [name=codigo]').val().trim(), dimensiones: $('form [name=dimension_largo]').val().trim() + 'X' + $('form [name=dimension_alto]').val().trim() + 'X' + $('form [name=dimension_ancho]').val().trim(), list_relacion: list_, list_: list_all, largo: $('form [name=dimension_largo]').val().trim(), alto: $('form [name=dimension_alto]').val().trim(), ancho: $('form [name=dimension_ancho]').val().trim() }
        }

        //data para contenedores
        if ( name_form_save == 'contenedores' ) {
            var data = {
                codigo: $('form [name=1_contenedor]').val().toUpperCase() + $('form [name=2_contenedor]').val() + $('form [name=3_contenedor]').val(),
                codigo_iso: $('form [name=codigo_iso]').val(),
                id_naviera: $('form [name=id_naviera').val()
            }
        }

        //Guardar la inspeccion
        if ( eso.attr('id')!==undefined )
        {
            var array_damage = [];
            $('#table-damage tr').each( function () {
                var tmp = {
                    codigo_damage: $(this).children('td').eq(0).text(),
                    codigo_localizacion_damage: $(this).children('td').eq(1).text(),
                    codigo_componente: $(this).children('td').eq(2).text(),
                    id_medida: $(this).children('input').val(),
                    codigo_reparacion: $(this).children('td').eq(4).text(),
                    codigo_responsabilidad: $(this).children('td').eq(5).text()
                }
                if ( $(this).attr('data-id')!==undefined ) {
                    tmp.id = $(this).attr('data-id')
                    tmp.id_inspeccion = '<?=isset($id)?$id:""?>'
                }
                array_damage.push( tmp )
            })
            
            if ( array_damage.length>0 ) {
                var data = {
                    codigo_contenedor: $('#codigo_contenedor').val(),
                    id_transportador: $('#id_transportador').val(),
                    codigo_iso: $('#id_tipo').val(),
                    id_clasificacion: $('#id_clasificacion').val(),
                    id_conductor: $('#id_conductor').val(),
                    placa_vehiculo: $('#placa_vehiculo').val(),
                    estado_in_out: $('input#estado_ingresoingreso').is(':checked') ? 1 : -1,
                    estado_llenado: $('input#estado_llenadolleno').is(':checked') ? 1 : -1,
                    id_cliente: $('#id_cliente').val(),
                    id_naviera: $('#codigo_naviera').val(),
                    observaciones: $('#observacion').val(),
                    array_damage: array_damage
                }
            }
        }else{
            if ($('#principal [name=codigo_iso]').attr('class').indexOf('d-none')===-1 && $('#principal [name=codigo_iso]').val()=='') {
                bandera = false
                return Swal.fire({ icon: 'error', title: 'Oops...', text: 'Debe seleccionar el código ISO.' })
            }
        }

        //bandera = false
        if (bandera) {
            ajax(url, data, function (data) {
                if (data.res=='ok') 
                {
                    if ( eso.attr('id')!==undefined )
                        $('#staticBackdrop').modal('hide')
                    Swal.fire({ icon: 'success',  title: data.msj, showConfirmButton: false, timer: 1500 })
                    if ( name_form_save != 'inspecciones' ) 
                    {
                        if ( eso.attr('id')===undefined )
                            removeAddItemModal()
                        else
                            setTimeout(function() { window.location.href = base_url+'inspecciones/list' }, 1500);                        
                        
                        if ( Object.keys( select_item_add ).includes( name_from_modal ) ) 
                        {
                            //agrega al array de navieras
                            if ( name_form_save=='navieras' || name_form_save=='codigoiso' ) 
                                list_naviera_codigo = list_naviera_codigo.concat(data.list_)
                            //agrega al array de conductores
                            if ( name_form_save=='conductores' ) 
                                list_conductores = list_conductores.concat(data.list_)
                            //agrega al array de vehiculos
                            if ( name_form_save=='vehiculos' ) 
                                list_vehiculos = list_vehiculos.concat(data.list_)
                            //agregar item en el select de la vista principal cuando es conductores y esté seleccionado el transportador
                            if ( select_item_add[name_from_modal][0].principal && name_form_save=='conductores' && $('#principal #id_transportador').val()!='' ) 
                            {
                                var id_transportador_select = $('#principal #id_transportador').val(),
                                    active_ = data.list_.filter(conductor_data => conductor_data['id_transportador'] === id_transportador_select);
                                if ( active_.length >0 ) {
                                    for (let i = 0; i < active_.length; i++) 
                                        $( select_item_add[name_from_modal][0].principal_select ).append('<option value="' + active_[i].id + '">' + active_[i].conductor + ' - ' + active_[i].numero_documento + '</option>')
                                }
                            }
                            //agregar item en el select de la vista principal cuando no es conductores
                            if ( select_item_add[name_from_modal][0].principal && name_form_save!='conductores' )
                                $( select_item_add[name_from_modal][0].principal_select ).append('<option value="' + data.id + '">' + data.nombre + '</option>')
                            //agregar item al forms_add para que sea super dinamico para el modal cuando no es codigoiso
                            if ( select_item_add[name_from_modal][0].save_principal_modal && name_form_save!='codigoiso' ) {
                                for (let i = 0; i < select_item_add[name_from_modal][0].save_principal_modal.length; i++) 
                                    forms_add[ select_item_add[name_from_modal][0].save_principal_modal[i].form_add ].form_add[ select_item_add[name_from_modal][0].save_principal_modal[i].position ].options.push( { value: data.id, name: data.nombre } )
                            }
                            //agregar item al forms_add para que sea super dinamico para el modal cuando es codigoiso
                            if ( select_item_add[name_from_modal][0].save_principal_modal && name_form_save=='codigoiso' ) {
                                for (let i = 0; i < select_item_add[name_from_modal][0].save_principal_modal.length; i++) 
                                    forms_add[ select_item_add[name_from_modal][0].save_principal_modal[i].form_add ].form_add[ select_item_add[name_from_modal][0].save_principal_modal[i].position ].options.push( { value: data.id, name: data.nombre, extra_data: data.extra_data, largo: data.largo } )                                
                            }
                            //agrega item en el select del modal, cuando fue creado desde crear campo en el modal
                            if ( select_item_add[name_from_modal][0].modal_add_item_selects ) 
                            {
                                //agrega el item al select de la vista principal
                                if ( select_item_add[name_from_modal][0].modal_add_item_selects[name_form_save] && select_item_add[name_from_modal][0].modal_add_item_selects[name_form_save][0].principal_select ) 
                                {
                                    if ( name_form_save=='conductores' && $('#principal #id_transportador').val()!='' ) 
                                    {
                                        var id_transportador_select = $('#principal #id_transportador').val(),
                                            active_ = data.list_.filter(conductor_data => conductor_data['id_transportador'] === id_transportador_select);
                                        if ( active_.length >0 ) {
                                            for (let i = 0; i < active_.length; i++) 
                                                $( select_item_add[name_from_modal][0].modal_add_item_selects[name_form_save][0].principal_select ).append('<option value="' + active_[i].id + '">' + active_[i].conductor + ' - ' + active_[i].numero_documento + '</option>')
                                        }
                                    }
                                    if ( name_form_save!='conductores' ) 
                                        $( select_item_add[name_from_modal][0].modal_add_item_selects[name_form_save][0].principal_select ).append('<option value="' + data.id + '">' + data.nombre + '</option>')
                                }
                                    
                                //agrega el item al select del modal
                                if ( select_item_add[name_from_modal][0].modal_add_item_selects[name_form_save] && select_item_add[name_from_modal][0].modal_add_item_selects[name_form_save][0].modal_select )
                                    $( select_item_add[name_from_modal][0].modal_add_item_selects[name_form_save][0].modal_select ).append('<option value="' + data.id + '">' + data.nombre + '</option>')
                                //agregar item al forms_add para que sea super dinamico
                                if ( select_item_add[name_from_modal][0].modal_add_item_selects[name_form_save] && select_item_add[name_from_modal][0].modal_add_item_selects[name_form_save][0].forms_add ) {
                                    for (let i = 0; i < select_item_add[name_from_modal][0].modal_add_item_selects[name_form_save][0].forms_add.length; i++) {
                                        if ( name_form_save!='codigoiso' )
                                            forms_add[ select_item_add[name_from_modal][0].modal_add_item_selects[name_form_save][0].forms_add[i].form_add ].form_add[ select_item_add[name_from_modal][0].modal_add_item_selects[name_form_save][0].forms_add[i].position ].options.push( { value: data.id, name: data.nombre } )
                                        else
                                            forms_add[ select_item_add[name_from_modal][0].modal_add_item_selects[name_form_save][0].forms_add[i].form_add ].form_add[ select_item_add[name_from_modal][0].modal_add_item_selects[name_form_save][0].forms_add[i].position ].options.push( { value: data.id, name: data.nombre, extra_data: data.extra_data, largo: data.largo } )
                                    }
                                }
                            }
                        }
                    } else
                        setTimeout(function() { location.reload(); }, 1500);
                } else {
                    var content = '';
                    for(let i in data.errors) 
                        content+=data.errors[i]+'\n'
                    Swal.fire({ icon: 'error', title: 'Oops...', text: content })
                }
            })
        }
    })

    $('body').on('click', '.delete-row-table', function () {
        $(this).closest('tr').remove()
    })

    $('body').on('click', '.edit-row-table', function () {
        $('.addItemTable').click()
        var codigo_damage = $(this).closest('tr').children('td').eq(0).text(),
            codigo_localizacion = $(this).closest('tr').children('td').eq(1).text(),
            codigo_reparacion = $(this).closest('tr').children('td').eq(4).text()
            codigo_componente = $(this).closest('tr').children('td').eq(2).text();
        $('#codigo_damage').val( codigo_damage )
        changeCodigoDamage (codigo_damage)
        $('#codigo_reparacion').val( codigo_reparacion )
        $('#id_cargo').val( $(this).closest('tr').children('td').eq(5).text() )
        $('#localizacion_damage').val( codigo_localizacion )
        getComponente_(codigo_localizacion)
        $('#id_componente').val( codigo_componente )
        var content = '<option value="" style="display:none">Seleccionar</option>'
        getMedidas (content, codigo_reparacion, codigo_componente)
        $('#id_medida').val( $(this).closest('tr').children('td').eq(3).find('input').val() )
        hijo_actual_tr = $(this).closest('tr').attr('id').replace('hijo-', '')
    })

</script>
<?php $this->load->view('admin/inspecciones/buscador_contenedor');?>
<?php $this->load->view('admin/contenedores/change_codigo_iso');?>
<?php $this->load->view('admin/contenedores/rules_container_number');?>
<?php $this->load->view('admin/inspecciones/change_transportador');?>
<?php $this->load->view('admin/inspecciones/change_conductor');?>
<?php $this->load->view('admin/inspecciones/change_clasificacion_damage');?>
<?php $this->load->view('admin/inspecciones/add_remove_item_table_damage');?>
<?php $this->load->view('admin/inspecciones/change_codigo_damage');?>
<?php $this->load->view('admin/inspecciones/modal_ubicacion_damages');?>
<?php $this->load->view('admin/inspecciones/generar_codigo_ubicacion_damage');?>
<?php $this->load->view('admin/inspecciones/add-item_modal');?>
<?php $this->load->view('common/addItemContent');?>
<?php $this->load->view('common/add_remove_campo');?>