<?php $this->load->view('common/head_admin'); ?>

</head>

<body class="fix-header fix-sidebar card-no-border">
    <?php $this->load->view('common/preloader'); ?>
    <div id="main-wrapper">
        
        <?php $this->load->view('common/header_admin');
        $this->load->view('common/menu_admin'); ?>

        <div class="page-wrapper">
            <div class="container-fluid">

                <?php $this->load->view('common/titulo_page_admin');?>

                <div class="row py-3 px-5">
                    <div class="col-12">
                        <div class="card" id="no-more-tables">
                            <div class="card-block">
                                <table class="table">
                                    <thead>
                                        <tr>
                                        <?php foreach ( $columns as $column ): 
                                            if (empty($column['key'])) { continue; } ?>
                                            <th><?=$column['name']?></th>
                                        <?php endforeach; ?>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php if ( !empty($lists) && is_array($lists) ):
                                        foreach ( $lists as $key_list => $list ):  ?>
                                            <tr>
                                                <td data-title="<?=$columns[0]['name']?>"><?=$list->placa?></td>
                                                <td data-title="<?=$columns[1]['name']?>">
                                                    <?php if (count($list->conductores)>0) { ?>
                                                        <ul>
                                                        <?php foreach ($list->conductores as$v) { ?>
                                                            <?="<li>".$v->conductor."</li>"?>
                                                        <?php } ?>
                                                        </ul>
                                                    <?php }else{
                                                        echo '---';
                                                    } ?>
                                                </td>
                                                <td data-title="<?=$columns[2]['name']?>">
                                                    <button type="button" class="btn btn-warning editar mb-1" data-pos="<?=$key_list?>" data-toggle="tooltip" data-placement="top" title="Editar" <?php in_array($editar_form, $this->session->permisos)?'':'disabled'?>><i class="mdi mdi-pencil"></i></button>
                                                    <button type="button" class="btn btn-danger eliminar mb-1" data-id="<?=$list->id?>" data-toggle="tooltip" data-placement="top" title="Eliminar" <?php in_array($eliminar_form, $this->session->permisos)?'':'disabled'?>><i class="mdi mdi-delete"></i></button>
                                                </td>
                                            </tr>
                                        <?php endforeach;
                                    else : ?>
                                        <tr>
                                            <td colspan="<?=count($columns);?>" class="text-center font-weight-bold text-break">No hay registros</td>
                                        </tr>
                                    <?php endif ?>
                                    </tbody>
                                </table>
                            </div>
                            <?php if ( $this->pagination->create_links() !== null && $this->pagination->create_links() != '' ) { ?>
                            <div class="card-footer">
                                <?php echo $this->pagination->create_links(); ?>
                            </div>
                            <?php } ?>
                        </div>                            
                    </div>
                </div>

            </div>
        </div>

        <div class="modal" tabindex="-1" id="modalNew" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title"><i class="mdi mdi-plus"></i> Crear <?=$titulo_singular?></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="<?=$url;?>/new" method="POST" onsubmit="return false">
                            <div class="row">
                            <?php foreach ( $forms as $form ): ?>
                                <div class="col-12 col-lg-<?=$form['column']?> form-group">

                                    <?php if ( isset($form['label']) ): ?>
                                        <label for="<?=$form['for']?>"><?=$form['label']?></label>
                                    <?php endif;
                                    if ( isset($form['btn-info']) && $form['btn-info'] ) { ?>
                                        <button type="button" class="btn btn-outline-warning view-info-btn float-right d-sm-none d-block"><i class="mdi mdi-alert-circle-outline"></i></button>
                                    <?php } 
                                    $data['form'] = $form;
                                    if ($form['form_control']!='select-add') {                                        
                                        $this->load->view('common/form_control/'.$form['form_control'], $data);
                                    }else{ ?>
                                        <div class="input-group mb-3">
                                            <?php $this->load->view('common/form_control/select', $data); ?>
                                            <div class="input-group-append">
                                                <span class="input-group-text h-100">
                                                    <button type="button" class="btn btn-danger h-100 removeItem"><i class="mdi mdi-close"></i></button>
                                                </span>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            <?php endforeach; ?>
                                <div class="col-12 text-right">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                    <button type="submit" class="btn btn-info save" id="save">Guardar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <?php $this->load->view('common/js_admin'); ?>
        <script>
            var lists = <?=json_encode($lists);?>;
            var forms = <?=json_encode($forms);?>;
            var array_conductor = forms[1].options;
            var add_input = false;

            $('document').ready(function () {
                var service = '<?=$service?>'
                if (service!='') 
                    $('#modalNew').modal('show')
            })

            //Nuevo Modal
            $('body').on('click', '#btn-newModal', function () {
                resetModal()
                $('#modalNew h5').html('<i class="mdi mdi-plus"></i> Nuevo <?=$titulo_singular?>')
            })

            //Editar Modal
            $('body').on('click', 'table .editar', function () {
                $('#modalNew').modal('show')
                $('form')[0].reset()
                $('form .form-group .input-group').remove()
                var pos_item = $(this).attr('data-pos'),
                item = lists[pos_item];
                $('#modalNew form').attr('action', '<?=$url?>/update/'+item.id)
                $('#modalNew h5').html('<i class="mdi mdi-pencil"></i> Editar <?=$titulo_singular?>').append('<input type="hidden" name="id" value="'+item.id+'">')
                
                $('form [name=placa]').val(item.placa)
                if ( item.conductores.length>0 ) {
                    var content = ''
                    for (let i = 0; i < item.conductores.length; i++) 
                        content += addItemContent(item.conductores[i].id, item.conductores[i].id_conductor)
                    $('form .form-group').eq(1).append(content)
                }
            })

            //Formulario - guardar
            $( "body" ).on( "click", ".save", function( event ) {
                event.preventDefault();
                var url = $(this).closest('form').eq(0).attr('action'),
                    bandera = true,
                    list_conductor = [],
                    eso = $(this);
                
                if (eso.attr('id')!==undefined) {
                    $('form .id_conductor').each(function(){
                        var tmp = {id_conductor: $(this).val()}
                        if ( $('.modal [name=id]').val()!==undefined ) 
                            tmp['id_vehiculo'] = $('.modal [name=id]').val()
                        if ( $(this).attr('data-id')!==undefined ) 
                            tmp.id = $(this).attr('data-id')
                        list_conductor.push(tmp)
                    })
                    if (list_conductor.length==0) {
                        bandera = false
                        return Swal.fire({ icon: 'error', title: 'Oops...', text: 'Debe agregar un conductor para guardar el vehículo.' })
                    }
                    var data = {
                        placa: $('form [name=placa]').val(),
                        list_conductor: list_conductor
                    }
                }else
                    var data = $(this).closest('form').eq(0).serialize()
                
                if (bandera) {
                    ajax(url, data, function (data) {
                        if (data.res=='ok') {
                            Swal.fire({ icon: 'success',  title: data.msj, showConfirmButton: false, timer: 1500 })
                            if (add_input && eso.attr('id')===undefined) {
                                removeAddItemModal()
                                $('.modal form .id_conductor').append('<option value="'+data.id+'">'+data.nombre+'</option>')
                                array_conductor.push({value: data.id, name: data.nombre})
                            }else
                                setTimeout(function() { location.reload(); }, 1500);
                        } else {
                            var content = '';
                            for(let i in data.errors) 
                                content+=data.errors[i]+'\n'
                            Swal.fire({ icon: 'error', title: 'Oops...', text: content })
                        }
                    })
                }
            });

            //ADD item
            $('body').on('click', '.addItem', function () {
                if ( $('form .id_conductor').length < array_conductor.length ) {
                    var content = addItemContent()
                    $(this).closest('.form-group').append(content)
                }                
            })

            function addItemContent(id=false, id_conductor=false) {
                var content = '<div class="input-group mb-3">'
                        content += '<select class="form-control id_conductor" name="id_conductor" required="true" '
                        content += id?'data-id="'+id+'"':''
                        content +='>'
                            content += '<option value="" style="display:none">Seleccionar</option>'
                            for (let j = 0; j < array_conductor.length; j++) {
                            content += '<option value="'+array_conductor[j].value+'"'
                            content += id_conductor && id_conductor==array_conductor[j].value?'selected':'' 
                            content += '>'+array_conductor[j].name+'</option>'
                            }
                        content += '</select>'
                        content += '<div class="input-group-append">'
                            content += '<span class="input-group-text h-100">'
                                content += '<button type="button" class="btn btn-danger h-100 removeItem"><i class="mdi mdi-close"></i></button>'
                            content += '</span>'
                        content += '</div>'
                    content += '</div>'
                return content;
            }

            //REMOVE item
            $('body').on('click', '.removeItem', function () {
                $(this).closest('.input-group').remove()
            })

            //reset form when open modal
            $(document).on('show.bs.modal', '.modal', function () {
                resetModal()
                removeAddItemModal()
            });
            
            function resetModal() {
                $('form')[0].reset()
                $('form .form-group .input-group').remove()
                var content = addItemContent()
                $('form .form-group').eq(1).append(content)
            }

            //Crear contenido dentro del modal
            <?php if (isset($form_add)) { ?>
            $('body').on('click', '.add-item', function(){
                $('.modal form .row').eq(1).remove()
                add_input = true
                $('.modal form').addClass('row')
                $('.modal form .row').eq(0).addClass('col-12 col-lg-6 mx-0 w-100')
                $('.modal form .row .col-lg-4').removeClass('col-lg-4').addClass('col-lg-12')
                $('.modal form .row .col-lg-6').removeClass('col-lg-6').addClass('col-lg-12')
                var content = '<div class="row col-12 col-lg-6  mx-0 w-100 border-left">'
                        content += '<form action="<?=$form_add_url;?>/new" method="POST" onsubmit="return false">'
                            content += '<h3 class="mb-0">Nuevo <?=$form_add_titulo?></h3>'
                            <?php foreach ( $form_add as $form ): ?>
                            content += '<div class="col-12 col-lg-<?=$form['column']?> form-group">'
                                <?php if ( isset($form['label']) ): ?>
                                content += '<label for="<?=$form['for']?>"><?=$form['label']?></label>'
                                <?php endif;
                                $data['form'] = $form; ?>
                                content += '<?php $this->load->view('common/form_control/'.$form['form_control'], $data); ?>'
                            content += '</div>'
                            <?php endforeach; ?>
                            content += '<div class="col-12 text-right">'
                                content += '<button type="button" class="btn btn-secondary remove-add-item">Cancelar</button>'
                                content += '<button type="submit" class="btn btn-info ml-2 save">Guardar</button>'
                            content += '</div>'
                        content += '</form>'
                    content += '</div>'
                $('.modal form').append(content)
            })
            <?php } ?>

            $('body').on('click', '.remove-add-item', function (){
                removeAddItemModal()
            })

            function removeAddItemModal() {
                $('.modal form .row').eq(1).remove()
                $('.modal form .row').eq(0).attr('class', 'row mx-0 w-100')
                $('.modal form .row .col-12').eq(0).removeClass('col-lg-12').addClass('col-lg-4')
                $('.modal form .row .col-12').eq(1).removeClass('col-lg-12').addClass('col-lg-6')
            }
        </script>
        
    </body>

</html>