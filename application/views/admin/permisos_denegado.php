<?php $this->load->view('common/head_admin'); ?>

</head>

<body class="fix-header fix-sidebar">
    <?php $this->load->view('common/preloader'); ?>
    <div id="main-wrapper">
        <header class="topbar bg-dark">
            <nav class="navbar top-navbar navbar-toggleable-sm navbar-light">
                <div class="navbar-collapse">
                
                    <?php $this->load->view('common/navbar_right_admin'); ?>

                </div>
            </nav>
        </header>
        <div class="page-wrapper ml-0">
            <div class="container-fluid">

                <div class="row pt-5 dashboard-resource px-5 d-flex justify-content-center align-items-center">
                <div class="col-12 col-md-6">
                        <div class="card rounded-0 border border-secondary">
                            <div class="card-block">
                                <h3 class="card-title text-center text-break">PERMISOS DENEGADO.</h3>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        
        <?php $this->load->view('common/js_admin');?>
    </body>

</html>