<?php $this->load->view('common/head_admin'); ?>
</head>

<body class="fix-header fix-sidebar card-no-border">
    <?php $this->load->view('common/preloader'); ?>
    <div id="main-wrapper">
        
        <?php $this->load->view('common/header_admin');
        $this->load->view('common/menu_admin'); ?>

        <div class="page-wrapper">
            <div class="container-fluid">

                <?php $this->load->view('common/titulo_page_admin');?>

                <div class="row py-3 px-5">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-block" id="no-more-tables">
                                <?php $this->load->view('common/table_list_admin'); ?>
                            </div>
                            <?php if ( $this->pagination->create_links() !== null && $this->pagination->create_links() != '' ) { ?>
                            <div class="card-footer">
                                <?php echo $this->pagination->create_links(); ?>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

        <?php $this->load->view('common/modal_forms_admin');
        $this->load->view('common/js_admin');
        $this->load->view('common/form_crud_admin');?>
        <script>            
            //Formulario - guardar
            $( "body" ).on( "click", ".save", function( event ) {
                event.preventDefault();
                var url = $(this).closest('form').eq(0).attr('action'),
                    data = $(this).closest('form').eq(0).serialize(),
                    eso = $(this);
                ajax(url, data, function (data) {
                    if (data.res=='ok') {
                        Swal.fire({ icon: 'success',  title: data.msj, showConfirmButton: false, timer: 1500 })
                        if (add_input && eso.attr('id')===undefined) {
                            <?php if (isset($form_add)) { ?>
                            removeAddItemModal()
                            <?php } ?>
                            appendAddItemModal(data.id, data.nombre)
                        }else
                            setTimeout(function() { location.reload(); }, 1500);        
                    } else {
                        var content = '';
                        for(let i in data.errors) 
                            content+=data.errors[i]+'\n'
                        Swal.fire({ icon: 'error', title: 'Oops...', text: content })
                    }
                })
            });

            function resetModal() {
                $('form')[0].reset()
            }
        </script>
        
    </body>

</html>