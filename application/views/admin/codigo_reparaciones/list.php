<?php $this->load->view('common/head_admin'); ?>

</head>

<body class="fix-header fix-sidebar card-no-border">
    <?php $this->load->view('common/preloader'); ?>
    <div id="main-wrapper">
        
        <?php $this->load->view('common/header_admin');
        $this->load->view('common/menu_admin'); ?>

        <div class="page-wrapper">
            <div class="container-fluid">

                <?php $this->load->view('common/titulo_page_admin');?>

                <div class="row py-3 px-5">
                    <div class="col-12">
                        <div class="card" id="no-more-tables">
                            <div class="card-block">
                                <table class="table">
                                    <thead>
                                        <tr>
                                        <?php foreach ( $columns as $column ): 
                                            if (empty($column['key'])) { continue; } ?>
                                            <th><?=$column['name']?></th>
                                        <?php endforeach; ?>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php if ( !empty($lists) && is_array($lists) ):
                                        foreach ( $lists as $key_list => $list ):  ?>
                                            <tr>
                                                <td data-title="<?=$columns[0]['name']?>"><?=$list->codigo?></td>
                                                <td data-title="<?=$columns[1]['name']?>"><?=$list->descripcion?></td>
                                                <td data-title="<?=$columns[2]['name']?>"><?=$list->traduccion?></td>
                                                <td data-title="<?=$columns[3]['name']?>">
                                                    <?php if (count($list->damages)>0) { ?>
                                                        <ul>
                                                        <?php foreach ($list->damages as$v) { ?>
                                                            <?="<li>".$v->codigo_damage."</li>"?>
                                                        <?php } ?>
                                                        </ul>
                                                    <?php }else{
                                                        echo '---';
                                                    } ?>
                                                </td>
                                                <td data-title="<?=$columns[4]['name']?>">
                                                    <button type="button" class="btn btn-warning editar mb-1" data-pos="<?=$key_list?>" data-toggle="tooltip" data-placement="top" title="Editar" <?php in_array($editar_form, $this->session->permisos)?'':'disabled'?>><i class="mdi mdi-pencil"></i></button>
                                                    <button type="button" class="btn btn-danger eliminar mb-1" data-id="<?=$list->id?>" data-toggle="tooltip" data-placement="top" title="Eliminar" <?php in_array($eliminar_form, $this->session->permisos)?'':'disabled'?>><i class="mdi mdi-delete"></i></button>
                                                </td>
                                            </tr>
                                        <?php endforeach;
                                    else : ?>
                                        <tr>
                                            <td colspan="<?=count($columns);?>" class="text-center font-weight-bold text-break">No hay registros</td>
                                        </tr>
                                    <?php endif ?>
                                    </tbody>
                                </table>
                            </div>
                            <?php if ( $this->pagination->create_links() !== null && $this->pagination->create_links() != '' ) { ?>
                            <div class="card-footer">
                                <?php echo $this->pagination->create_links(); ?>
                            </div>
                            <?php } ?>
                        </div>                        
                    </div>
                </div>

            </div>
        </div>

        <?php $this->load->view('common/modal_forms_admin');
        $this->load->view('common/js_admin');
        $this->load->view('common/form_crud_admin'); ?>
        <script>
            var array_damage = forms[3].options;
            var add_input = false;

            function addItemEditarModal(pos_item) {
                var item = lists[pos_item]
                if ( item.damages.length>0 ) {
                    var content = ''
                    for (let i = 0; i < item.damages.length; i++) {
                        if ( i==0 ) {
                            $('.modal form>.row>.form-group .codigo_damage').val( item.damages[i].codigo_damage )
                            continue;
                        }
                        content += addItemContent(item.damages[i].id, item.damages[i].codigo_damage)
                    }
                    $('form .form-group').eq(3).append(content)
                }
            }

            function addItemContent(id=false, codigo_damage=false) {
                var  content = '<div class="input-group mb-3">'
                        content += '<select class="form-control codigo_damage" name="codigo_damage" required="true"'
                        content += id?'data-id="'+id+'"':''
                        content += '>'
                            content += '<option value="" style="display:none">Seleccionar</option>'
                            for (let j = 0; j < array_damage.length; j++) {
                            content += '<option value="'+array_damage[j].value+'"'
                            content += codigo_damage && codigo_damage==array_damage[j].value?'selected':'' 
                            content += '>'+array_damage[j].name+'</option>'
                            }
                        content += '</select>'
                        content += '<div class="input-group-append">'
                            content += '<span class="input-group-text h-100">'
                                content += '<button type="button" class="btn btn-danger h-100 removeItem"><i class="mdi mdi-close"></i></button>'
                            content += '</span>'
                        content += '</div>'
                    content += '</div>'
                return content;
            }

            //Formulario - guardar
            $( "body" ).on( "click", ".save", function( event ) {
                event.preventDefault();
                var url = $(this).closest('form').eq(0).attr('action'),
                    bandera = true,
                    list_damage = [],
                    eso = $(this);

                if (eso.attr('id')!==undefined) {
                    $('form .codigo_damage').each(function(){
                        var tmp = {codigo_damage: $(this).val(), codigo_reparacion: $('form [name=codigo]').val()}
                        if ( $(this).attr('data-id')!==undefined )
                            tmp.id = $(this).attr('data-id')
                        list_damage.push(tmp)
                    })
                    if (list_damage.length==0) {
                        bandera = false
                        Swal.fire({ icon: 'error', title: 'Oops...', text: 'Debe seleccionar un código de daño para guardar el código de rapración.' })
                    }
                    var data = {
                        codigo: $('form [name=codigo]').val(),
                        descripcion: $('form #descripcion').val(),
                        traduccion: $('form #traduccion').val(),
                        list_damage: list_damage
                    }
                }else
                    var data = $(this).closest('form').eq(0).serialize()
                
                if (bandera) {
                    ajax(url, data, function (data) {
                        if (data.res=='ok') {
                            Swal.fire({ icon: 'success',  title: data.msj, showConfirmButton: false, timer: 1500 })
                            if (add_input && eso.attr('id')===undefined) {
                                removeAddItemModal()
                                $('.modal form .codigo_damage').append('<option value="'+data.id+'">'+data.nombre+'</option>')
                                array_damage.push({value: data.id, name: data.nombre})
                            }else
                                setTimeout(function() { location.reload(); }, 1500);
                        } else {
                            var content = '';
                            for(let i in data.errors) 
                                content+=data.errors[i]+'\n'
                            Swal.fire({ icon: 'error', title: 'Oops...', text: content })
                        }
                    })
                }
            });


            //ADD item dinamico
            $('body').on('click', '.addItem', function () {
                if ( $('.modal form .id_naviera').length < array_damage.length ) {
                    var content = addItemContent()
                    $(this).closest('.form-group').append(content)
                }                
            })

            //REMOVE item
            $('body').on('click', '.removeItem', function () {
                $(this).closest('.input-group').remove()
            })

            function resetModal() {
                $('form')[0].reset()
                $('form .form-group .input-group').remove()
                var content = addItemContent()
                $('form .form-group').eq(3).append(content)
            }

            document.addEventListener('DOMContentLoaded', () => {
                document.querySelectorAll('input[type=text]').forEach( node => node.addEventListener('keypress', e => {
                    if(e.keyCode == 13) 
                        e.preventDefault();
                }))
            });
        </script>
        <?php if (isset($form_add)) {
            $this->load->view('common/js_modal_forms_add');
        } ?>
        
    </body>

</html>