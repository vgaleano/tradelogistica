<?php $this->load->view('common/head_admin'); ?>

</head>

<body class="fix-header fix-sidebar card-no-border">
    <?php $this->load->view('common/preloader'); ?>
    <div id="main-wrapper">
        
        <?php $this->load->view('common/header_admin');
        $this->load->view('common/menu_admin'); ?>

        <div class="page-wrapper">
            <div class="container-fluid">

                <?php $this->load->view('common/titulo_page_admin');?>

                <div class="row py-3 px-5">
                    <div class="col-12">
                        <div class="card" id="no-more-tables">
                            <div class="card-block">
                                <table class="table text-center">
                                    <thead>
                                        <tr>
                                            <th class="text-center">Nombre</th>
                                            <th class="text-center">Traducción</th>
                                            <th class="text-center">Código componente</th>
                                            <th class="text-center">Código de localización</th>
                                            <th class="text-center">Assembly</th>
                                            <th class="text-center">Type designation</th>
                                            <th class="text-center">Códigos de daños</th>
                                            <th class="text-center">Acción</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php if ( !empty($lists) && is_array($lists) ):
                                        foreach ( $lists as $key_list => $list ):  ?>
                                            <tr>
                                                <td data-title="Nombre"><?=$list->nombre?></td>
                                                <td data-title="Traducción"><?=$list->traduccion?></td>
                                                <td data-title="Código componente"><?=$list->codigo?></td>
                                                <td data-title="Código de localización"><?=$list->codigo_localizacion?></td>
                                                <td data-title="Assembly"><?=$list->assembly?></td>
                                                <td data-title="Type designation"><?=$list->type_designation?></td>
                                                <td data-title="Códigos de daños">
                                                    <?php if (count($list->damages)>0) { ?>
                                                    <ul>
                                                    <?php foreach ( $list->damages as $damage ): ?>
                                                        <li><?=$damage->codigo_damage?></li>
                                                    <?php endforeach; ?>
                                                    </ul>
                                                    <?php }else{
                                                        echo '---';
                                                    } ?>
                                                </td>
                                                <td data-title="Acción">
                                                    <button type="button" class="btn btn-warning editar mb-1" data-pos="<?=$key_list?>" data-toggle="tooltip" data-placement="top" title="Editar" <?php in_array($editar_form, $this->session->permisos)?'':'disabled'?>><i class="mdi mdi-pencil"></i></button>
                                                    <button type="button" class="btn btn-danger eliminar mb-1" data-id="<?=$list->id?>" data-toggle="tooltip" data-placement="top" title="Eliminar" <?php in_array($eliminar_form, $this->session->permisos)?'':'disabled'?>><i class="mdi mdi-delete"></i></button>
                                                </td>
                                            </tr>
                                        <?php endforeach;
                                    else : ?>
                                        <tr>
                                            <td colspan="8" class="text-center font-weight-bold text-break">No hay registros</td>
                                        </tr>
                                    <?php endif ?>
                                    </tbody>
                                </table>
                            </div>
                            <?php if ( $this->pagination->create_links() !== null && $this->pagination->create_links() != '' ) { ?>
                            <div class="card-footer">
                                <?php echo $this->pagination->create_links(); ?>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <?php $this->load->view('common/modal_forms_admin');
        $this->load->view('common/js_admin'); 
        $this->load->view('common/form_crud_admin'); ?>
        <script>
            var add_item = $('.modal form div.input-group').html();
            var list_damage = <?=json_encode($list_damage);?>;
            var add_input = false;
            
            function addItemContent(id=false, codigo_damage=false) {
                var content = '<div class="input-group mb-3">'
                        content += '<select class="form-control codigo_damage" name="codigo_damage" required="true" '
                        content += id?'data-id="'+id+'"':''
                        content +='>'
                            content += '<option value="" style="display:none">Seleccionar</option>'
                            for (let j = 0; j < list_damage.length; j++) {
                            content += '<option value="'+list_damage[j].value+'"'
                            content += codigo_damage && codigo_damage==list_damage[j].value?'selected':'' 
                            content += '>'+list_damage[j].name+'</option>'
                            }
                        content += '</select>'
                        content += '<div class="input-group-append">'
                            content += '<span class="input-group-text h-100">'
                                content += '<button type="button" class="btn btn-danger h-100 removeItem"><i class="mdi mdi-close"></i></button>'
                            content += '</span>'
                        content += '</div>'
                    content += '</div>'
                return content;
            }

            function addItemEditarModal(pos_item) {
                var item = lists[pos_item];
                if ( item.damages.length>0 ) {
                    var content = ''
                    for (let i = 0; i < item.damages.length; i++) {
                        if ( i==0 ) {
                            $('.modal form>.row>.form-group .codigo_damage').val( item.damages[i].codigo_damage )
                            continue;
                        }
                        content += addItemContent(item.damages[i].id, item.damages[i].codigo_damage)
                    }
                    $('.modal form>.row>.form-group').last().append(content)
                }
            }

            //Formulario - guardar
            $( "body" ).on( "click", ".save", function( event ) {
                event.preventDefault();
                var url = $(this).closest('form').eq(0).attr('action'),
                    bandera = true,
                    list_relacion = [],
                    eso = $(this);
                
                if (eso.attr('id')!==undefined) {
                    $('form .codigo_damage').each(function(){
                        if ($(this).val()=='') {
                            /*bandera = false
                            return Swal.fire({ icon: 'error', title: 'Oops...', text: 'Debe seleccionar el código de daño.' })*/
                        }else{
                            var tmp = { codigo_damage: $(this).val(), codigo_componente: $('form [name=codigo]').val() }
                            if ( $(this).attr('data-id')!==undefined ) 
                                tmp.id = $(this).attr('data-id')
                            list_relacion.push(tmp)
                        }
                    })
                    /*if (list_relacion.length==0) {
                        bandera = false
                        Swal.fire({ icon: 'error', title: 'Oops...', text: 'Debe agregar un código de daño para guardar el componente.' })
                    }*/
                    var data = {
                        nombre: $('form [name=nombre]').val(),
                        traduccion: $('form [name=traduccion]').val(),
                        codigo: $('form [name=codigo]').val(),
                        codigo_localizacion: $('form [name=codigo_localizacion]').val(),
                        id_assembly: $('form [name=id_assembly]').val(),
                        id_type_designation: $('form [name=id_type_designation]').val(),
                        list_relacion: list_relacion
                    }
                }else
                    var data = $(this).closest('form').eq(0).serialize()
                
                if (bandera) {
                    ajax(url, data, function (data) {
                        if (data.res=='ok') {
                            Swal.fire({ icon: 'success',  title: data.msj, showConfirmButton: false, timer: 1500 })
                            if (add_input && eso.attr('id')===undefined) {
                                removeAddItemModal()
                                $('.modal form .codigo_damage').append('<option value="'+data.id+'">'+data.nombre+'</option>')
                                list_damage.push({value: data.id, name: data.nombre})
                            }else
                                setTimeout(function() { location.reload(); }, 1500);
                        } else {
                            var content = '';
                            for(let i in data.errors) 
                                content+=data.errors[i]+'\n'
                            Swal.fire({ icon: 'error', title: 'Oops...', text: content })
                        }
                    })
                }
            });


            //ADD item
            $('body').on('click', '.addItem', function () {
                if ( $('.modal form .codigo_damage').length < list_damage.length ) {
                    var content = addItemContent()
                    $(this).closest('.form-group').append(content)
                }  
            })

            //REMOVE item
            $('body').on('click', '.removeItem', function () {
                $(this).closest('.input-group').remove()
            })

            function resetModal() {
                $('form')[0].reset()
                $('form .form-group .input-group').remove()
                var content = addItemContent()
                $('form .form-group').eq(6).append(content)
            }
            
            document.addEventListener('DOMContentLoaded', () => {
                document.querySelectorAll('input[type=text]').forEach( node => node.addEventListener('keypress', e => {
                    if(e.keyCode == 13) 
                        e.preventDefault();
                }))
            });
        </script>
        <?php if (isset($form_add)) {
            $this->load->view('common/js_modal_forms_add');
        } ?>
    </body>

</html>