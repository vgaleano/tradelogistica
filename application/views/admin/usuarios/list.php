<?php $this->load->view('common/head_admin'); ?>
<link href="<?=base_url()?>public/css/signature-pad.css" rel="stylesheet">
</head>

<body class="fix-header fix-sidebar card-no-border" onselectstart="return false">
    <?php $this->load->view('common/preloader'); ?>
    <div id="main-wrapper">

        <?php $this->load->view('common/header_admin');
        $this->load->view('common/menu_admin'); ?>

        <div class="page-wrapper">
            <div class="container-fluid">

                <?php $this->load->view('common/titulo_page_admin');?>

                <div class="row py-3 px-5">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-block" id="no-more-tables">
                                <?php $this->load->view('common/table_list_admin'); ?>
                            </div>
                            <?php if ( $this->pagination->create_links() !== null && $this->pagination->create_links() != '' ) { ?>
                            <div class="card-footer">
                                <?php echo $this->pagination->create_links(); ?>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
        <div class="modal" tabindex="-1" id="modalNew" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title"><i class="mdi mdi-plus"></i> Crear <?=$titulo_singular?></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="<?=base_url().$url;?>/new" onsubmit="return false" method="POST" target="my_iframe" enctype="multipart/form-data">
                            <div class="row">
                            <?php foreach ( $forms as $form ): ?>
                                <div class="col-12 col-lg-<?=$form['column']?> form-group">
                                    <?php if ( isset($form['label']) ): ?>
                                        <label for="<?=$form['for']?>"><?=$form['label']?></label>
                                    <?php endif;
                                    $data['form'] = $form;
                                    $this->load->view('common/form_control/'.$form['form_control'], $data); ?>
                                </div>
                            <?php endforeach; ?>
                                <div class="col-12 col-lg-6 d-none pb-3">
                                    <label>Firma<span class='text-danger'>*</span></label>
                                    <img src="" class="img-fluid" id="firma_user">
                                </div>
                                <div class="col-12 mb-4">
                                    <div id="signature-pad" class="m-signature-pad">
                                        <div class="m-signature-pad--body">
                                            <canvas></canvas>
                                        </div>
                                        <div class="m-signature-pad--footer">
                                            <div class="description">Agregar Firma Digital Aquí</div>
                                            <button type="button" class="button clear" data-action="clear">Limpiar</button>
                                        </div>
                                    </div>
                                </div>
                                <input  type="text" class="hide" id="firma" name="firma" value="">
                                <div class="col-12 text-right">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                    <button type="submit" class="btn btn-info" id="save">Guardar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
        <iframe id="my_iframe" name="my_iframe" src="" class="d-none" style="width: 100%; height: 500px;"></iframe>
        <?php $this->load->view('common/js_admin'); ?>
        <script src="<?=base_url()?>public/js/signature_pad.js"></script>
        <script>
            var lists = <?=json_encode($lists);?>;
            var forms = <?=json_encode($forms);?>;
            var input = ['input', 'textarea', 'select'];
            var wrapper,
                canvas,
                signaturePad;

            $('document').ready(function () {
                var service = '<?=$service?>'
                if (service!='' ) { //|| (sessionStorage.ActualView-1) == sessionStorage.AnteriorView 
                    $('#modalNew').modal('show')
                    resizeCanvas()
                }
            })
            
            //Nuevo Modal
            $('body').on('click', '#btn-newModal', function () {
                $('#modalNew form').attr( 'action', '<?=base_url().$url?>/new' )
                $('form input[type=radio], form input[type=checkbox]').attr('checked', false)
                $('#modalNew h5').html( '<i class="mdi mdi-plus"></i> Nuevo <?=$titulo_singular?>' )
                $('#firma_user').attr('src', '').parent().removeClass('d-none')
                $('#signature-pad').parent().removeClass('d-none')
                $('.modal form .row>div').last().removeClass('d-none')
            })

            //Ver Modal
            $('body').on('click', 'table .ver', function () {
                $('#modalNew form').attr( 'action', '' )
                loadDataUserModal($(this))
                $('#signature-pad').parent().addClass('d-none')
                $('.modal form .row>div').last().addClass('d-none')
                $('#modalNew input, #modalNew textarea, #modalNew select, #modalNew input[type=radio], #modalNew input[type=checkbox]').attr('disabled', true)
            })

            //Editar Modal
            $('body').on('click', 'table .editar', function () {
                loadDataUserModal($(this), 'editar')
                $('#signature-pad').parent().removeClass('d-none')
                $('.modal form .row>div').last().removeClass('d-none')
                resetFirma()
            })

            //Cargar los datos del usuario en el modal
            function loadDataUserModal(eso, modo=false) {
                $('#modalNew').modal('show')
                $('form input[type=radio], form input[type=checkbox]').attr('checked', false)
                var pos_item = eso.attr('data-pos'),
                    item = lists[pos_item];
                    
                $('#modalNew form').attr( 'action', modo=='editar' ? '<?=base_url().$url?>/update/' + item.id : '' )
                $('#modalNew h5').html( modo=='editar' ? '<i class="mdi mdi-pencil"></i> Editar <?=$titulo_singular?>' : '<i class="mdi mdi-eye"></i> Ver <?=$titulo_singular?>' ).append( '<input type="hidden" name="id" value="' + item.id + '">' )
                for ( let i = 0; i < forms.length; i++ ) {
                    if ( input.includes(forms[i].form_control) && forms[i].for!='password' ) 
                        $('form [name='+forms[i].attr.name+']').val( item[forms[i].attr.name] )
                    
                    if ( forms[i].form_control=='radio' ) 
                        $( 'form #' + forms[i].attr.id+item[forms[i].attr.name] ).attr('checked', true)
                    
                    if ( forms[i].form_control=='checkbox' && item.status_terminos=='1' ) {
                        $( 'form #' + forms[i].attr.id ).attr('checked', true)
                        $( 'form [name=' + forms[i].attr.id+']' ).val('1')
                    }
                }
                $('#firma_user').attr( 'src', item['url_firma'] ? base_url+item['url_firma'] : '' ).parent().removeClass('d-none')
            }

            //Formulario - guardar (nuevo o editar)
            $( "body" ).on( "click", "#save", function( event ) {
                event.preventDefault();
                var bandera = true
                if ( $('.modal [name=nombre]').val().trim()=='') {
                    bandera = false
                    return Swal.fire({ icon: 'error', title: 'Oops...', text: 'El campo nombre es requerido.' })
                }
                if ( $('.modal [name=email]').val().trim()=='') {
                    bandera = false
                    return Swal.fire({ icon: 'error', title: 'Oops...', text: 'El campo correo electrónico es requerido.' })
                }
                if ($('.modal form').attr('action').indexOf('new')!==-1) {
                    if ( $('.modal [name=password]').val().trim()=='') {
                        bandera = false
                        return Swal.fire({ icon: 'error', title: 'Oops...', text: 'El campo contraseña es requerido.' })
                    }
                    if (signaturePad.isEmpty()) {
                        bandera = false
                        return Swal.fire({ icon: 'error', title: 'Oops...', text: 'Olvidó agregar su firma digital.' })
                    }
                }
                //Firma
                if (!signaturePad.isEmpty()) {
                    var png= signaturePad.toDataURL("image/png", 100); 
                    document.getElementById("firma").value =png;
                }
                if ( $('.modal [name=id_perfil]').val().trim()=='') {
                    bandera = false
                    return Swal.fire({ icon: 'error', title: 'Oops...', text: 'El campo perfil es requerido.' })
                }
                if ( !$('.modal #status_terminos').is(":checked") ) {
                    bandera = false
                    return Swal.fire({ icon: 'error', title: 'Oops...', text: 'Debe aceptar los términos y condiciones.' })
                }
                if (bandera) 
                    $('form')[0].submit()
            });

            //reset form when open modal
            $(document).on('show.bs.modal', '.modal', function () {
                $('form')[0].reset()
                $('#modalNew input, #modalNew textarea, #modalNew select, #modalNew input[type=radio], #modalNew input[type=checkbox]').removeAttr('disabled')
                
            });
            
            $(document).on('shown.bs.modal', '.modal', function () {
                $('form')[0].reset()
                //Inicializa la firma para editar y nuevo usuario
                if ( $('#modalNew form').attr( 'action').indexOf('new')!==-1 || $('#modalNew form').attr( 'action').indexOf('update')!==-1 ) 
                    resetFirma()
            });

            function resetFirma() {
                //Inicializa la firma
                wrapper = document.getElementById("signature-pad")
                canvas = wrapper.querySelector("canvas")
                signaturePad = new SignaturePad(canvas);
                resizeCanvas()
                signaturePad = new SignaturePad(canvas);
            }
            
            // Adjust canvas coordinate space taking into account pixel ratio,
            // to make it look crisp on mobile devices.
            // This also causes canvas to be cleared.
            function resizeCanvas() {
                // When zoomed out to less than 100%, for some very strange reason,
                // some browsers report devicePixelRatio as less than 1
                // and only part of the canvas is cleared then.
                var ratio =  Math.max(window.devicePixelRatio || 1, 1);
                canvas.width = canvas.offsetWidth * ratio;
                canvas.height = canvas.offsetHeight * ratio;
                canvas.getContext("2d").scale(ratio, ratio);
                signaturePad.clear();
            }

            window.onresize = function() { resizeCanvas };

            $('body').on('click', '#signature-pad .clear', function () {
                signaturePad.clear();
            })
            //Luego del guardado del form entra por acá para la respuesta
            $(window).on("message onmessage", function(e) {
                var data1 = e.originalEvent.data;
                datos = JSON.stringify(data1);    
                if (datos!=='""') {
                    datos = JSON.parse(datos);
                    datos = JSON.parse(datos);
                    if(datos.res == "ok"){
                        $('form')[0].reset();
                        signaturePad.clear();
                        Swal.fire( { icon: 'success', title: datos.msj, showConfirmButton: false, timer: 3000 } )
                        setTimeout(function() { location.reload(); }, 3000);   
                    }else
                        return Swal.fire( { title: 'Error!', text: datos.msj, icon: 'error', confirmButtonText: 'Ok' } )
                }
                $('#my_iframe').attr('src', '');
            }); 
            
        </script>
        
    </body>

</html>