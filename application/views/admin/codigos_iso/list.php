<?php $this->load->view('common/head_admin'); ?>

</head>

<body class="fix-header fix-sidebar card-no-border">
    <?php $this->load->view('common/preloader'); ?>
    <div id="main-wrapper">
        
        <?php $this->load->view('common/header_admin');
        $this->load->view('common/menu_admin'); ?>

        <div class="page-wrapper">
            <div class="container-fluid">

                <?php $this->load->view('common/titulo_page_admin');?>

                <div class="row py-3 px-5">
                    <div class="col-12">
                        <div class="card" id="no-more-tables">
                            <div class="card-block">
                                <table class="table">
                                    <thead>
                                        <tr>
                                        <th>Código ISO</th>
                                        <th>Dimensión</th>
                                        <?php if (count($list_naviera)>0) {
                                            foreach ( $list_naviera as $naviera ): ?>
                                                <th><?=$naviera->name?></th>
                                            <?php endforeach;
                                        } ?>
                                        <th>Acción</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php if ( !empty($lists) && is_array($lists) ):
                                        foreach ( $lists as $key_list => $list ):  ?>
                                            <tr>
                                                <td data-title="Código ISO"><?=$list->codigo?></td>
                                                <td data-title="Dimensión"><?=$list->dimensiones?></td>
                                                <?php if (count($list_naviera)>0) {
                                                    foreach ( $list_naviera as $naviera ):
                                                        $id_camp = $naviera->value;
                                                        $activas=array_filter($list->navieras, function ($tT) use ($id_camp) 
                                                        {
                                                            if ($id_camp==$tT['id_naviera']) {
                                                                return $tT;
                                                            }
                                                        }); ?>
                                                        <td data-title="<?=$naviera->name?>">
                                                        <?php if (count($activas)>0) { ?>
                                                            <?php foreach ($activas as $v) { ?>
                                                                <?=$v['codigo_naviera'];?>
                                                            <?php } ?>
                                                        <?php }else{
                                                            echo '---';
                                                        } ?>
                                                        </td>
                                                    <?php endforeach;
                                                } ?>
                                                <td data-title="Acción">
                                                    <button type="button" class="btn btn-warning editar mb-1" data-pos="<?=$key_list?>" data-toggle="tooltip" data-placement="top" title="Editar" <?php in_array($editar_form, $this->session->permisos)?'':'disabled'?>><i class="mdi mdi-pencil"></i></button>
                                                    <button type="button" class="btn btn-danger eliminar mb-1" data-id="<?=$list->id?>" data-toggle="tooltip" data-placement="top" title="Eliminar" <?php in_array($eliminar_form, $this->session->permisos)?'':'disabled'?>><i class="mdi mdi-delete"></i></button>
                                                </td>
                                            </tr>
                                        <?php endforeach;
                                    else : ?>
                                        <tr>
                                            <td colspan="<?=count($columns);?>" class="text-center font-weight-bold text-break">No hay registros</td>
                                        </tr>
                                    <?php endif ?>
                                    </tbody>
                                </table>
                            </div>
                            <?php if ( $this->pagination->create_links() !== null && $this->pagination->create_links() != '' ) { ?>
                            <div class="card-footer">
                                <?php echo $this->pagination->create_links(); ?>
                            </div>
                            <?php } ?>
                        </div>                        
                    </div>
                </div>

            </div>
        </div>

        <div class="modal" tabindex="-1" id="modalNew" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title"><i class="mdi mdi-plus"></i> Crear <?=$titulo_singular?></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="<?=$url;?>/new" method="POST" onsubmit="return false">
                            <div class="row">
                            <?php foreach ( $forms as $form ): ?>
                                <div class="col-12 col-lg-<?=$form['column']?> form-group">

                                    <?php if ( isset($form['label']) ): ?>
                                        <label for="<?=$form['for']?>"><?=$form['label']?></label>
                                    <?php endif;
                                    if ( isset($form['btn-info']) && $form['btn-info'] ) { ?>
                                        <button type="button" class="btn btn-outline-warning view-info-btn float-right d-block d-sm-none"><i class="mdi mdi-alert-circle-outline"></i></button>
                                    <?php } 
                                    $data['form'] = $form;
                                    if ($form['form_control']!='select-add') {                                        
                                        $this->load->view('common/form_control/'.$form['form_control'], $data);
                                    }else{ ?>
                                        <div class="input-group mb-3">
                                            <?php $this->load->view('common/form_control/select', $data); ?>
                                            <input type="text" name="codigo_naviera" placeholder="Código" class="form-control">
                                            <div class="input-group-append">
                                                <span class="input-group-text h-100">
                                                    <button type="button" class="btn btn-danger h-100 removeItem"><i class="mdi mdi-close"></i></button>
                                                </span>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            <?php endforeach; ?>
                                <div class="col-12 text-right">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                    <button type="submit" class="btn btn-info save" id="save">Guardar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <?php $this->load->view('common/js_admin');
        $this->load->view('common/form_crud_admin'); ?>
        <script>
            var array_naviera = <?=json_encode($list_naviera);?>;
            var add_input = false;

            function addItemEditarModal(pos_item) {
                var item = lists[pos_item];
                var item_navieras = item.navieras.length>0 ? item.navieras.length : 1
                var content = ''
                for (let i = 0; i < item_navieras; i++) {
                    if ( i==0 ) {
                        $('.modal form>.row>.form-group .id_naviera').val( item.navieras[i].id_naviera )
                        $('.modal form>.row>.form-group .id_naviera').next().val( item.navieras[i].codigo_naviera )
                        continue;
                    }
                    content += addItemContent(( item.navieras.length>0 ? item.navieras[i].id:false), ( item.navieras.length>0 ? item.navieras[i].id_naviera:false), ( item.navieras.length>0 ? item.navieras[i].codigo_naviera:false))
                }
                $('form .form-group').eq(2).append(content)
            }

            function resetModal() {
                $('form')[0].reset()
                $('form .form-group .input-group').remove()
                var content = addItemContent()
                $('form .form-group').eq(2).append(content)
            }

            //Formulario - guardar
            $( "body" ).on( "click", ".save", function( event ) {
                event.preventDefault();
                var url = $(this).closest('form').eq(0).attr('action'),
                    bandera = true,
                    list_relacion = [],
                    eso = $(this);

                if (eso.attr('id')!==undefined) {
                    $('form .id_naviera').each(function(){
                        if ($(this).val()=='' && $(this).next('[name=codigo_naviera]').val()=='') {
                        }else{
                            if ($(this).val().trim()=='') {
                                bandera = false
                                return Swal.fire({ icon: 'error', title: 'Oops...', text: 'Debe seleccionar la naviera.' })
                            }
                            if ($(this).next('[name=codigo_naviera]').val().trim()=='') {
                                bandera = false
                                return Swal.fire({ icon: 'error', title: 'Oops...', text: 'Debe agregar un código ISo para la naviera.' })
                            }
                            var tmp = { id_naviera: $(this).val(), codigo_naviera: $(this).next('[name=codigo_naviera]').val().trim(), codigo_iso: $('form [name=codigo]').val().trim(), dimensiones: $('form [name=dimensiones]').val().trim() }
                            if ( $(this).attr('data-id')!==undefined ) 
                                tmp.id = $(this).attr('data-id')
                            list_relacion.push(tmp)
                        }
                    })
                    /*if (list_relacion.length==0) {
                        bandera = false
                        Swal.fire({ icon: 'error', title: 'Oops...', text: 'Debe agregar una naviera para guardar el código ISO.' })
                    }*/
                    var data = {
                        codigo: $('form [name=codigo]').val().trim(),
                        dimensiones: $('form [name=dimensiones]').val().trim(),
                        list_relacion: list_relacion
                    }
                }else
                    var data = $(this).closest('form').eq(0).serialize()
                
                if (bandera) {
                    ajax(url, data, function (data) {
                        if (data.res=='ok') {
                            Swal.fire({ icon: 'success',  title: data.msj, showConfirmButton: false, timer: 1500 })
                            if (add_input && eso.attr('id')===undefined) {
                                removeAddItemModal()
                                $('.modal form [name=id_naviera]').append('<option value="'+data.id+'">'+data.nombre+'</option>')
                                array_naviera.push({value: data.id, name: data.nombre})
                            }else
                                setTimeout(function() { location.reload(); }, 1500);
                        } else {
                            var content = '';
                            for(let i in data.errors) 
                                content+=data.errors[i]+'\n'
                            Swal.fire({ icon: 'error', title: 'Oops...', text: content })
                        }
                    })
                }
            });

            //ADD item
            $('body').on('click', '.addItem', function () {
                if ( $('.modal form .id_naviera').length < array_naviera.length ) {
                    var content = addItemContent()
                    $(this).closest('.form-group').append(content)
                }  
            })
            //REMOVE item
            $('body').on('click', '.removeItem', function () {
                $(this).closest('.input-group').remove()
            })

            function resetModal() {
                $('form')[0].reset()
                $('form .form-group .input-group').remove()
                var content = addItemContent()
                $('form .form-group').eq(2).append(content)
            }

            function addItemContent(id=false, id_naviera=false, codigo_naviera=false) {
                var content = '<div class="input-group mb-3">'
                    content += '<select class="form-control id_naviera" name="id_naviera" required="true" '+ ( id?'data-id="'+id+'"':'' )+'>'
                        content += '<option value="" style="display:none">Seleccionar</option>'
                        for (let j = 0; j < array_naviera.length; j++) 
                            content += '<option value="'+array_naviera[j].value+'" '+ ( id_naviera && id_naviera==array_naviera[j].value?'selected':'' ) +'>'+array_naviera[j].name+'</option>'
                    content += '</select><input type="text" name="codigo_naviera" placeholder="Código" class="form-control" value="'+( codigo_naviera ? codigo_naviera:'')+'">'
                    content += '<div class="input-group-append"><span class="input-group-text h-100"><button type="button" class="btn btn-danger h-100 removeItem"><i class="mdi mdi-close"></i></button></span></div></div>'
                return content;
            }

            //Crear contenido dentro del modal
            <?php if (isset($form_add)) { ?>
            $('body').on('click', '.add-item', function(){
                $('.modal form .row').eq(1).remove()
                add_input = true
                $('.modal form').addClass('row')
                $('.modal form .row').eq(0).addClass('col-12 col-lg-6 mx-0 w-100')
                $('.modal form .row .col-lg-6').removeClass('col-lg-6').addClass('col-lg-12')
                var content = '<div class="row col-12 col-lg-6  mx-0 w-100 border-left">'
                        content += '<form action="<?=$form_add_url;?>/new" method="POST" onsubmit="return false">'
                            content += '<h3 class="mb-0">Nuevo <?=$form_add_titulo?></h3>'
                            <?php foreach ( $form_add as $form ): ?>
                            content += '<div class="col-12 col-lg-<?=$form['column']?> form-group">'
                                <?php if ( isset($form['label']) ): ?>
                                content += '<label for="<?=$form['for']?>"><?=$form['label']?></label>'
                                <?php endif;
                                $data['form'] = $form; ?>
                                content += '<?php $this->load->view('common/form_control/'.$form['form_control'], $data); ?>'
                            content += '</div>'
                            <?php endforeach; ?>
                            content += '<div class="col-12 text-right">'
                                content += '<button type="button" class="btn btn-secondary remove-add-item">Cancelar</button>'
                                content += '<button type="submit" class="btn btn-info ml-2 save">Guardar</button>'
                            content += '</div>'
                        content += '</form>'
                    content += '</div>'
                $('.modal form').append(content)
            })
            <?php } ?>

            $('body').on('click', '.remove-add-item', function (){
                removeAddItemModal()
            })

            function removeAddItemModal() {
                $('.modal form .row').eq(1).remove()
                $('.modal form .row').eq(0).attr('class', 'row mx-0 w-100')
                $('.modal form .row .col-lg-12').removeClass('col-lg-12').addClass('col-lg-6')
            }

            document.addEventListener('DOMContentLoaded', () => {
                document.querySelectorAll('input[type=text]').forEach( node => node.addEventListener('keypress', e => {
                    if(e.keyCode == 13) {
                        e.preventDefault()
                }))
            });
        </script>        
    </body>

</html>